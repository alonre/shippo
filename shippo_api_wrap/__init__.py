import urllib2
import elements

API_URL = "https://api.goshippo.com/v1/"
API_USER = "alonx.regev@gmail.com"
API_PASS = "testing"
#API_URL_OPENER = None

def InitConnection(api_user,api_pass):
    # create a password manager
    password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
    
    # Add the username and password.
    # If we knew the realm, we could use it instead of None.
    password_mgr.add_password(None, API_URL, api_user, api_pass)
    
    handler = urllib2.HTTPBasicAuthHandler(password_mgr)
    
    # create "opener" (OpenerDirector instance)
    API_URL_OPENER = urllib2.build_opener(handler)
    
    # use the opener to fetch a URL
    api = API_URL_OPENER.open(API_URL+"addresses")
    print api.readlines()
    
    # Install the opener.
    # Now all calls to urllib2.urlopen use our opener.
    urllib2.install_opener(API_URL_OPENER)

