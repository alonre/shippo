import urllib2
import urllib
import json

print "address imported"
ELEMENT_API_URL = "https://api.goshippo.com/v1/addresses/"

class Address():
    def __init__(self,  request_data):
        self.request_data = request_data
        
        # craft the Request object
        req = urllib2.Request(ELEMENT_API_URL, urllib.urlencode(  request_data ) )
        
        # perform the request
        res = urllib2.urlopen(req)
        
        # fill in the blanks
        res_string = res.read()        
        print res_string
        self.properties = json.loads(res_string)
        print "Decoded:"
        print self.properties
        
    @staticmethod
    def getRequestDataTemplate():
        """ @return Dictionary the data structure required to create this object in Shippo's DB """
        return {"object_purpose": "QUOTE",
            "name": "",
            "company": "",
            "street1": "",
            "street_no": "",
            "street2": "",
            "city": "",
            "state": "",
            "zip": "",
            "country": "",
            "phone": "",
            "email": "",
            "ip": "",
            "metadata": ""}

    def getId(self):
        """ @return String the objects ID in Shippo's DB """
        return self.properties["object_id"]