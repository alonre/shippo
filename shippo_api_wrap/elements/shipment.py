import urllib2
import urllib
import json

print "shipment imported"
ELEMENT_API_URL = "https://api.goshippo.com/v1/shipments/"

class Shipment():
    def __init__(self,  request_data):
        self.request_data = request_data
        
        # craft the Request object
        req = urllib2.Request(ELEMENT_API_URL, urllib.urlencode(  request_data ) )
        
        # perform the request
        res = urllib2.urlopen(req)
        
        # fill in the blanks
        res_string = res.read()        
        print res_string
        self.properties = json.loads(res_string)
        print "Decoded:"
        print self.properties

#    def __init__(self, object_purpose_str, address_from_obj, address_to_obj, parcel_obj, metadata_str):
#        self.request_data = {"object_purpose": object_purpose_str,
#            "address_from": str(address_from_obj.properties["object_id"]),
#            "address_to": str(address_to_obj.properties["object_id"]),
#            "parcel": parcel_obj.properties["object_id"],
#            "metadata": metadata_str}
#        self.__init__(self.request_data)

    @staticmethod
    def getRequestDataTemplate():
        """ @return Dictionary the data structure required to create this object in Shippo's DB """
        return {"object_purpose": "QUOTE",
            "address_from": "",
            "address_to": "",
            "parcel": "",
            "metadata": ""}

    def getId(self):
        """ @return String the objects ID in Shippo's DB """
        return self.properties["object_id"]