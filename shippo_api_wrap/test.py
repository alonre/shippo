import shippo_api_wrap

print "Shippo API wrap - test"

add_data = { "object_purpose": "QUOTE",
        "name": "Laura Behrens Wu",
        "company": "Shippo",
        "street1": "Clayton St.",
        "street_no": "215",
        "street2": "",
        "city": "San Francisco",
        "state": "CA",
        "zip": "94117",
        "country": "US",
        "phone": "+1 555 341 9393",
        "email": "laura@goshippo.com",
        "ip": "",
        "metadata": "Customer ID 123456" }
        
par_data = {"length": "5",
    "width": "5",
    "height": "5",
    "distance_unit": "cm",
    "weight": "2",
    "mass_unit": "lb",
    "metadata": "Customer ID 123456"}

shippo_api_wrap.InitConnection("alonx.regev@gmail.com","testing")        

add1 = shippo_api_wrap.elements.Address(data)
add2 = shippo_api_wrap.elements.Address(data)
par = shippo_api_wrap.elements.Parcel(par_data)

ship_data = shippo_api_wrap.elements.Shipment.getRequestDataTemplate()
ship_data["address_from"] = add1.getId()
ship_data["address_to"] = add2.getId()
ship_data["parcel"] = par.getId()
ship = shippo_api_wrap.elements.Shipment(ship_data)