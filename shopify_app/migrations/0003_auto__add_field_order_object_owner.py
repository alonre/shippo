# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Order.object_owner'
        db.add_column(u'shopify_app_order', 'object_owner',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='shopifyorder_apiuser', null=True, to=orm['api.APIUser']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Order.object_owner'
        db.delete_column(u'shopify_app_order', 'object_owner_id')


    models = {
        u'api.apiaddress': {
            'Meta': {'object_name': 'APIAddress'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'company': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.Country']", 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'street1': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'street2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'street_no': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        u'api.apibilling': {
            'Meta': {'object_name': 'APIBilling'},
            'account_authorized': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ach_account_holder': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'apibilling_ach_account_holder'", 'null': 'True', 'to': u"orm['api.APIAddress']"}),
            'ach_account_number': ('django.db.models.fields.CharField', [], {'max_length': '25', 'blank': 'True'}),
            'ach_authorized': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ach_bank': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'apibilling_ach_bank'", 'null': 'True', 'to': u"orm['api.APIAddress']"}),
            'ach_entry_class': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'ach_entry_sent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ach_routing_number': ('django.db.models.fields.CharField', [], {'max_length': '9', 'blank': 'True'}),
            'billing_address': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'apibilling_billing_address'", 'null': 'True', 'to': u"orm['api.APIAddress']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'payment_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'paypal_authorized': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'paypal_email': ('django.db.models.fields.EmailField', [], {'max_length': '200', 'blank': 'True'}),
            'sepa_account_holder': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'apibilling_sepa_account_holder'", 'null': 'True', 'to': u"orm['api.APIAddress']"}),
            'sepa_account_number': ('django.db.models.fields.CharField', [], {'max_length': '25', 'blank': 'True'}),
            'sepa_authorized': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sepa_bank': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'apibilling_sepa_bank'", 'null': 'True', 'to': u"orm['api.APIAddress']"}),
            'sepa_bic': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'sepa_iban': ('django.db.models.fields.CharField', [], {'max_length': '34', 'blank': 'True'}),
            'sepa_mandate': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'sepa_mandate_reference': ('django.db.models.fields.CharField', [], {'max_length': '35', 'blank': 'True'}),
            'stripe_authorized': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'stripe_token': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        },
        u'api.apiuser': {
            'Meta': {'object_name': 'APIUser'},
            'address': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.APIAddress']", 'null': 'True', 'blank': 'True'}),
            'billing': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.APIBilling']", 'null': 'True', 'blank': 'True'}),
            'endicia_account_id': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'endicia_challenge_answer': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'endicia_challenge_question': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'endicia_passphrase': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'endicia_production': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'endicia_requester_id': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'endicia_temporary_account': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'endicia_webpassword': ('django.db.models.fields.CharField', [], {'max_length': '17', 'blank': 'True'}),
            'fedex_account': ('django.db.models.fields.CharField', [], {'max_length': '9', 'blank': 'True'}),
            'fedex_key': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'fedex_meter': ('django.db.models.fields.CharField', [], {'max_length': '9', 'blank': 'True'}),
            'fedex_password': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'fedex_production': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fee_payment_fixed': ('django.db.models.fields.DecimalField', [], {'default': '0.3', 'max_digits': '8', 'decimal_places': '4'}),
            'fee_payment_fixed_currency': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'apiuser_fee_payment_fixed_currency'", 'null': 'True', 'to': u"orm['api.Currency']"}),
            'fee_payment_relative': ('django.db.models.fields.DecimalField', [], {'default': '0.029', 'max_digits': '5', 'decimal_places': '4'}),
            'fee_shippo_fixed': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '8', 'decimal_places': '4'}),
            'fee_shippo_fixed_currency': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'apiuser_fee_shippo_fixed_currency'", 'null': 'True', 'to': u"orm['api.Currency']"}),
            'fee_shippo_relative': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '5', 'decimal_places': '4'}),
            'gadb_production': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mixpanel_user_id': ('django.db.models.fields.CharField', [], {'max_length': '254', 'blank': 'True'}),
            'parcel2go_api_key': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'parcel2go_card_reference': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'parcel2go_production': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'shopify_access_token': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'shopify_connected': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'shopify_shop_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'ups_access_license_number': ('django.db.models.fields.CharField', [], {'max_length': '24', 'blank': 'True'}),
            'ups_account': ('django.db.models.fields.CharField', [], {'max_length': '24', 'blank': 'True'}),
            'ups_password': ('django.db.models.fields.CharField', [], {'max_length': '24', 'blank': 'True'}),
            'ups_production': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ups_user_id': ('django.db.models.fields.CharField', [], {'max_length': '24', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'api.country': {
            'Meta': {'ordering': "['name', 'iso2']", 'object_name': 'Country'},
            'capital': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.Currency']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'iso3': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'language': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.Language']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'requires_state': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'requires_zip': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'tld': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '3', 'null': 'True', 'blank': 'True'})
        },
        u'api.currency': {
            'Meta': {'object_name': 'Currency'},
            'base': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['api.Currency']"}),
            'fedex_iso': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rate': ('django.db.models.fields.DecimalField', [], {'default': '1.0', 'max_digits': '15', 'decimal_places': '6'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'api.language': {
            'Meta': {'object_name': 'Language'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'shopify_app.customer': {
            'Meta': {'object_name': 'Customer'},
            'accepts_marketing': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifycustomer_order'", 'null': 'True', 'to': u"orm['shopify_app.Order']"})
        },
        u'shopify_app.fulfillment': {
            'Meta': {'object_name': 'Fulfillment'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyfulfillment_order'", 'null': 'True', 'to': u"orm['shopify_app.Order']"}),
            'receipt_authorization': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'receipt_testcase': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'service': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'tracking_company': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'tracking_number': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'tracking_url': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'shopify_app.lineitem': {
            'Meta': {'object_name': 'LineItem'},
            'fulfillment': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifylineitem_fulfillment'", 'null': 'True', 'to': u"orm['shopify_app.Fulfillment']"}),
            'fulfillment_service': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'fulfillment_status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'grams': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifylineitem_order'", 'null': 'True', 'to': u"orm['shopify_app.Order']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'product_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'requires_shipping': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sku': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'variant_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'variant_title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'vendor': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'shopify_app.order': {
            'Meta': {'object_name': 'Order'},
            'buyer_accepts_marketing': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'closed_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyorder_currency'", 'null': 'True', 'to': u"orm['api.Currency']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'blank': 'True'}),
            'financial_status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'fulfillment_status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'object_owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyorder_apiuser'", 'null': 'True', 'to': u"orm['api.APIUser']"}),
            'order_number': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'subtotal_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'taxes_included': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'test': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'total_line_items_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'total_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'total_price_usd': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'total_tax': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'total_weight': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'shopify_app.shippingaddress': {
            'Meta': {'object_name': 'ShippingAddress'},
            'address1': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'address2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'company': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyshippingaddress_country'", 'null': 'True', 'to': u"orm['api.Country']"}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyshippingaddress_order'", 'null': 'True', 'to': u"orm['shopify_app.Order']"}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'province': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'province_code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'shopify_app.shippingline': {
            'Meta': {'object_name': 'ShippingLine'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyshippingline_order'", 'null': 'True', 'to': u"orm['shopify_app.Order']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'source': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'shopify_app.tracking': {
            'Meta': {'object_name': 'Tracking'},
            'fulfillment': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifytracking_fulfillment'", 'null': 'True', 'to': u"orm['shopify_app.Fulfillment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tracking_number': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'tracking_url': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'})
        }
    }

    complete_apps = ['shopify_app']