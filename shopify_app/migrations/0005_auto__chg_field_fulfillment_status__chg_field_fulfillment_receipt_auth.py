# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Fulfillment.status'
        db.alter_column(u'shopify_app_fulfillment', 'status', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Fulfillment.receipt_authorization'
        db.alter_column(u'shopify_app_fulfillment', 'receipt_authorization', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Fulfillment.service'
        db.alter_column(u'shopify_app_fulfillment', 'service', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Fulfillment.tracking_number'
        db.alter_column(u'shopify_app_fulfillment', 'tracking_number', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Fulfillment.tracking_company'
        db.alter_column(u'shopify_app_fulfillment', 'tracking_company', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Fulfillment.tracking_url'
        db.alter_column(u'shopify_app_fulfillment', 'tracking_url', self.gf('django.db.models.fields.CharField')(max_length=500, null=True))

        # Changing field 'ShippingAddress.province'
        db.alter_column(u'shopify_app_shippingaddress', 'province', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'ShippingAddress.city'
        db.alter_column(u'shopify_app_shippingaddress', 'city', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'ShippingAddress.first_name'
        db.alter_column(u'shopify_app_shippingaddress', 'first_name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'ShippingAddress.last_name'
        db.alter_column(u'shopify_app_shippingaddress', 'last_name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'ShippingAddress.name'
        db.alter_column(u'shopify_app_shippingaddress', 'name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'ShippingAddress.zip'
        db.alter_column(u'shopify_app_shippingaddress', 'zip', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'ShippingAddress.province_code'
        db.alter_column(u'shopify_app_shippingaddress', 'province_code', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'ShippingAddress.address1'
        db.alter_column(u'shopify_app_shippingaddress', 'address1', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'ShippingAddress.address2'
        db.alter_column(u'shopify_app_shippingaddress', 'address2', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'ShippingAddress.phone'
        db.alter_column(u'shopify_app_shippingaddress', 'phone', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'ShippingAddress.company'
        db.alter_column(u'shopify_app_shippingaddress', 'company', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Order.user_id'
        db.alter_column(u'shopify_app_order', 'user_id', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Order.financial_status'
        db.alter_column(u'shopify_app_order', 'financial_status', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Order.name'
        db.alter_column(u'shopify_app_order', 'name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Order.fulfillment_status'
        db.alter_column(u'shopify_app_order', 'fulfillment_status', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Order.token'
        db.alter_column(u'shopify_app_order', 'token', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'ShippingLine.code'
        db.alter_column(u'shopify_app_shippingline', 'code', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'ShippingLine.title'
        db.alter_column(u'shopify_app_shippingline', 'title', self.gf('django.db.models.fields.CharField')(max_length=200, null=True))

        # Changing field 'ShippingLine.source'
        db.alter_column(u'shopify_app_shippingline', 'source', self.gf('django.db.models.fields.CharField')(max_length=200, null=True))

        # Changing field 'Tracking.tracking_number'
        db.alter_column(u'shopify_app_tracking', 'tracking_number', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Tracking.tracking_url'
        db.alter_column(u'shopify_app_tracking', 'tracking_url', self.gf('django.db.models.fields.CharField')(max_length=500, null=True))

        # Changing field 'Customer.first_name'
        db.alter_column(u'shopify_app_customer', 'first_name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Customer.last_name'
        db.alter_column(u'shopify_app_customer', 'last_name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Customer.email'
        db.alter_column(u'shopify_app_customer', 'email', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'LineItem.sku'
        db.alter_column(u'shopify_app_lineitem', 'sku', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'LineItem.fulfillment_service'
        db.alter_column(u'shopify_app_lineitem', 'fulfillment_service', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'LineItem.vendor'
        db.alter_column(u'shopify_app_lineitem', 'vendor', self.gf('django.db.models.fields.CharField')(max_length=200, null=True))

        # Changing field 'LineItem.title'
        db.alter_column(u'shopify_app_lineitem', 'title', self.gf('django.db.models.fields.CharField')(max_length=200, null=True))

        # Changing field 'LineItem.fulfillment_status'
        db.alter_column(u'shopify_app_lineitem', 'fulfillment_status', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'LineItem.variant_title'
        db.alter_column(u'shopify_app_lineitem', 'variant_title', self.gf('django.db.models.fields.CharField')(max_length=200, null=True))

        # Changing field 'LineItem.name'
        db.alter_column(u'shopify_app_lineitem', 'name', self.gf('django.db.models.fields.CharField')(max_length=200, null=True))

    def backwards(self, orm):

        # Changing field 'Fulfillment.status'
        db.alter_column(u'shopify_app_fulfillment', 'status', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Fulfillment.receipt_authorization'
        db.alter_column(u'shopify_app_fulfillment', 'receipt_authorization', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Fulfillment.service'
        db.alter_column(u'shopify_app_fulfillment', 'service', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Fulfillment.tracking_number'
        db.alter_column(u'shopify_app_fulfillment', 'tracking_number', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Fulfillment.tracking_company'
        db.alter_column(u'shopify_app_fulfillment', 'tracking_company', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Fulfillment.tracking_url'
        db.alter_column(u'shopify_app_fulfillment', 'tracking_url', self.gf('django.db.models.fields.CharField')(default='', max_length=500))

        # Changing field 'ShippingAddress.province'
        db.alter_column(u'shopify_app_shippingaddress', 'province', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'ShippingAddress.city'
        db.alter_column(u'shopify_app_shippingaddress', 'city', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'ShippingAddress.first_name'
        db.alter_column(u'shopify_app_shippingaddress', 'first_name', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'ShippingAddress.last_name'
        db.alter_column(u'shopify_app_shippingaddress', 'last_name', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'ShippingAddress.name'
        db.alter_column(u'shopify_app_shippingaddress', 'name', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'ShippingAddress.zip'
        db.alter_column(u'shopify_app_shippingaddress', 'zip', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'ShippingAddress.province_code'
        db.alter_column(u'shopify_app_shippingaddress', 'province_code', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'ShippingAddress.address1'
        db.alter_column(u'shopify_app_shippingaddress', 'address1', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'ShippingAddress.address2'
        db.alter_column(u'shopify_app_shippingaddress', 'address2', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'ShippingAddress.phone'
        db.alter_column(u'shopify_app_shippingaddress', 'phone', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'ShippingAddress.company'
        db.alter_column(u'shopify_app_shippingaddress', 'company', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Order.user_id'
        db.alter_column(u'shopify_app_order', 'user_id', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Order.financial_status'
        db.alter_column(u'shopify_app_order', 'financial_status', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Order.name'
        db.alter_column(u'shopify_app_order', 'name', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Order.fulfillment_status'
        db.alter_column(u'shopify_app_order', 'fulfillment_status', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Order.token'
        db.alter_column(u'shopify_app_order', 'token', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'ShippingLine.code'
        db.alter_column(u'shopify_app_shippingline', 'code', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'ShippingLine.title'
        db.alter_column(u'shopify_app_shippingline', 'title', self.gf('django.db.models.fields.CharField')(default='', max_length=200))

        # Changing field 'ShippingLine.source'
        db.alter_column(u'shopify_app_shippingline', 'source', self.gf('django.db.models.fields.CharField')(default='', max_length=200))

        # Changing field 'Tracking.tracking_number'
        db.alter_column(u'shopify_app_tracking', 'tracking_number', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Tracking.tracking_url'
        db.alter_column(u'shopify_app_tracking', 'tracking_url', self.gf('django.db.models.fields.CharField')(default='', max_length=500))

        # Changing field 'Customer.first_name'
        db.alter_column(u'shopify_app_customer', 'first_name', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Customer.last_name'
        db.alter_column(u'shopify_app_customer', 'last_name', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Customer.email'
        db.alter_column(u'shopify_app_customer', 'email', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'LineItem.sku'
        db.alter_column(u'shopify_app_lineitem', 'sku', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'LineItem.fulfillment_service'
        db.alter_column(u'shopify_app_lineitem', 'fulfillment_service', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'LineItem.vendor'
        db.alter_column(u'shopify_app_lineitem', 'vendor', self.gf('django.db.models.fields.CharField')(default='', max_length=200))

        # Changing field 'LineItem.title'
        db.alter_column(u'shopify_app_lineitem', 'title', self.gf('django.db.models.fields.CharField')(default='', max_length=200))

        # Changing field 'LineItem.fulfillment_status'
        db.alter_column(u'shopify_app_lineitem', 'fulfillment_status', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'LineItem.variant_title'
        db.alter_column(u'shopify_app_lineitem', 'variant_title', self.gf('django.db.models.fields.CharField')(default='', max_length=200))

        # Changing field 'LineItem.name'
        db.alter_column(u'shopify_app_lineitem', 'name', self.gf('django.db.models.fields.CharField')(default='', max_length=200))

    models = {
        u'api.country': {
            'Meta': {'ordering': "['name', 'iso2']", 'object_name': 'Country'},
            'capital': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.Currency']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'iso3': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'language': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.Language']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'requires_state': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'requires_zip': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'tld': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '3', 'null': 'True', 'blank': 'True'})
        },
        u'api.currency': {
            'Meta': {'object_name': 'Currency'},
            'base': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['api.Currency']"}),
            'fedex_iso': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rate': ('django.db.models.fields.DecimalField', [], {'default': '1.0', 'max_digits': '15', 'decimal_places': '6'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'api.language': {
            'Meta': {'object_name': 'Language'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'shopify_app.customer': {
            'Meta': {'object_name': 'Customer'},
            'accepts_marketing': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifycustomer_order'", 'null': 'True', 'to': u"orm['shopify_app.Order']"})
        },
        u'shopify_app.fulfillment': {
            'Meta': {'object_name': 'Fulfillment'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyfulfillment_order'", 'null': 'True', 'to': u"orm['shopify_app.Order']"}),
            'receipt_authorization': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'receipt_testcase': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'service': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'tracking_company': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'tracking_number': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'tracking_url': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'shopify_app.lineitem': {
            'Meta': {'object_name': 'LineItem'},
            'fulfillment': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifylineitem_fulfillment'", 'null': 'True', 'to': u"orm['shopify_app.Fulfillment']"}),
            'fulfillment_service': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'fulfillment_status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'grams': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifylineitem_order'", 'null': 'True', 'to': u"orm['shopify_app.Order']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'product_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'requires_shipping': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sku': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'variant_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'variant_title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'vendor': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'shopify_app.order': {
            'Meta': {'object_name': 'Order'},
            'buyer_accepts_marketing': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'closed_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyorder_currency'", 'null': 'True', 'to': u"orm['api.Currency']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'blank': 'True'}),
            'financial_status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'fulfillment_status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'object_owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyorder_user'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'order_number': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'subtotal_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'taxes_included': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'test': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'total_line_items_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'total_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'total_price_usd': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'total_tax': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'total_weight': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'shopify_app.shippingaddress': {
            'Meta': {'object_name': 'ShippingAddress'},
            'address1': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'address2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'company': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyshippingaddress_country'", 'null': 'True', 'to': u"orm['api.Country']"}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyshippingaddress_order'", 'null': 'True', 'to': u"orm['shopify_app.Order']"}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'province': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'province_code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'shopify_app.shippingline': {
            'Meta': {'object_name': 'ShippingLine'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyshippingline_order'", 'null': 'True', 'to': u"orm['shopify_app.Order']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'source': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'shopify_app.tracking': {
            'Meta': {'object_name': 'Tracking'},
            'fulfillment': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifytracking_fulfillment'", 'null': 'True', 'to': u"orm['shopify_app.Fulfillment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tracking_number': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'tracking_url': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['shopify_app']