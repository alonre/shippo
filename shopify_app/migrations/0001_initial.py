# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Order'
        db.create_table(u'shopify_app_order', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('buyer_accepts_marketing', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('closed_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('currency', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='shopifyorder_currency', null=True, to=orm['api.Currency'])),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=254, blank=True)),
            ('financial_status', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('fulfillment_status', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('object_id', self.gf('django.db.models.fields.IntegerField')(max_length=100, null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('number', self.gf('django.db.models.fields.IntegerField')(max_length=100, null=True, blank=True)),
            ('subtotal_price', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=2, blank=True)),
            ('taxes_included', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=2, blank=True)),
            ('test', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('token', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('total_line_items_price', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=2, blank=True)),
            ('total_price', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=2, blank=True)),
            ('total_price_usd', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=2, blank=True)),
            ('total_tax', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=2, blank=True)),
            ('total_weight', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=2, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('user_id', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('order_number', self.gf('django.db.models.fields.IntegerField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'shopify_app', ['Order'])

        # Adding model 'Fulfillment'
        db.create_table(u'shopify_app_fulfillment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='shopifyfulfillment_order', null=True, to=orm['shopify_app.Order'])),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('object_id', self.gf('django.db.models.fields.IntegerField')(max_length=100, null=True, blank=True)),
            ('service', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('tracking_company', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('tracking_number', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('tracking_url', self.gf('django.db.models.fields.CharField')(max_length=500, blank=True)),
            ('receipt_testcase', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('receipt_authorization', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
        ))
        db.send_create_signal(u'shopify_app', ['Fulfillment'])

        # Adding model 'LineItem'
        db.create_table(u'shopify_app_lineitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='shopifylineitem_order', null=True, to=orm['shopify_app.Order'])),
            ('fulfillment', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='shopifylineitem_fulfillment', null=True, to=orm['shopify_app.Fulfillment'])),
            ('fulfillment_service', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('fulfillment_status', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('grams', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=2, blank=True)),
            ('object_id', self.gf('django.db.models.fields.IntegerField')(max_length=100, null=True, blank=True)),
            ('price', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=2, blank=True)),
            ('product_id', self.gf('django.db.models.fields.IntegerField')(max_length=100, null=True, blank=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')(max_length=100, null=True, blank=True)),
            ('requires_shipping', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('sku', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('variant_id', self.gf('django.db.models.fields.IntegerField')(max_length=100, null=True, blank=True)),
            ('variant_title', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('vendor', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
        ))
        db.send_create_signal(u'shopify_app', ['LineItem'])

        # Adding model 'ShippingLine'
        db.create_table(u'shopify_app_shippingline', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='shopifyshippingline_order', null=True, to=orm['shopify_app.Order'])),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('price', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=2, blank=True)),
            ('source', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
        ))
        db.send_create_signal(u'shopify_app', ['ShippingLine'])

        # Adding model 'ShippingAddress'
        db.create_table(u'shopify_app_shippingaddress', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='shopifyshippingaddress_order', null=True, to=orm['shopify_app.Order'])),
            ('address1', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('address2', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('company', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='shopifyshippingaddress_country', null=True, to=orm['api.Country'])),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('province', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('zip', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('province_code', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
        ))
        db.send_create_signal(u'shopify_app', ['ShippingAddress'])

        # Adding model 'Customer'
        db.create_table(u'shopify_app_customer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='shopifycustomer_order', null=True, to=orm['shopify_app.Order'])),
            ('accepts_marketing', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('object_id', self.gf('django.db.models.fields.IntegerField')(max_length=100, null=True, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
        ))
        db.send_create_signal(u'shopify_app', ['Customer'])


    def backwards(self, orm):
        # Deleting model 'Order'
        db.delete_table(u'shopify_app_order')

        # Deleting model 'Fulfillment'
        db.delete_table(u'shopify_app_fulfillment')

        # Deleting model 'LineItem'
        db.delete_table(u'shopify_app_lineitem')

        # Deleting model 'ShippingLine'
        db.delete_table(u'shopify_app_shippingline')

        # Deleting model 'ShippingAddress'
        db.delete_table(u'shopify_app_shippingaddress')

        # Deleting model 'Customer'
        db.delete_table(u'shopify_app_customer')


    models = {
        u'api.country': {
            'Meta': {'ordering': "['name', 'iso2']", 'object_name': 'Country'},
            'capital': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.Currency']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'iso3': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'language': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.Language']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'requires_state': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'requires_zip': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'tld': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '3', 'null': 'True', 'blank': 'True'})
        },
        u'api.currency': {
            'Meta': {'object_name': 'Currency'},
            'base': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['api.Currency']"}),
            'fedex_iso': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rate': ('django.db.models.fields.DecimalField', [], {'default': '1.0', 'max_digits': '15', 'decimal_places': '6'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'api.language': {
            'Meta': {'object_name': 'Language'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'shopify_app.customer': {
            'Meta': {'object_name': 'Customer'},
            'accepts_marketing': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifycustomer_order'", 'null': 'True', 'to': u"orm['shopify_app.Order']"})
        },
        u'shopify_app.fulfillment': {
            'Meta': {'object_name': 'Fulfillment'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyfulfillment_order'", 'null': 'True', 'to': u"orm['shopify_app.Order']"}),
            'receipt_authorization': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'receipt_testcase': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'service': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'tracking_company': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'tracking_number': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'tracking_url': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'shopify_app.lineitem': {
            'Meta': {'object_name': 'LineItem'},
            'fulfillment': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifylineitem_fulfillment'", 'null': 'True', 'to': u"orm['shopify_app.Fulfillment']"}),
            'fulfillment_service': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'fulfillment_status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'grams': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifylineitem_order'", 'null': 'True', 'to': u"orm['shopify_app.Order']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'product_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'requires_shipping': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sku': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'variant_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'variant_title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'vendor': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'shopify_app.order': {
            'Meta': {'object_name': 'Order'},
            'buyer_accepts_marketing': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'closed_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyorder_currency'", 'null': 'True', 'to': u"orm['api.Currency']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'blank': 'True'}),
            'financial_status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'fulfillment_status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'order_number': ('django.db.models.fields.IntegerField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'subtotal_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'taxes_included': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'test': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'total_line_items_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'total_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'total_price_usd': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'total_tax': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'total_weight': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'shopify_app.shippingaddress': {
            'Meta': {'object_name': 'ShippingAddress'},
            'address1': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'address2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'company': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyshippingaddress_country'", 'null': 'True', 'to': u"orm['api.Country']"}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyshippingaddress_order'", 'null': 'True', 'to': u"orm['shopify_app.Order']"}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'province': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'province_code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'shopify_app.shippingline': {
            'Meta': {'object_name': 'ShippingLine'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shopifyshippingline_order'", 'null': 'True', 'to': u"orm['shopify_app.Order']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'source': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        }
    }

    complete_apps = ['shopify_app']