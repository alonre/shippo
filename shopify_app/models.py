from django.db import models
from api.models import Address, Currency, Country, APIUser, Transaction

class Order(models.Model):
	'''An order is a customer's completed request to purchase one or more products from a shop. '''
	object_owner = models.ForeignKey('auth.User', related_name='shopifyorder_user', blank=True, null=True)
	object_id = models.IntegerField(max_length=100, blank=True, null=True)
	buyer_accepts_marketing = models.BooleanField(default=True)
	closed_at = models.DateTimeField(blank=True, null=True)
	created_at = models.DateTimeField(blank=True, null=True)
	currency = models.ForeignKey(Currency, related_name='shopifyorder_currency', blank=True, null=True)
	email = models.EmailField(max_length=254, blank=True)
	financial_status = models.CharField(max_length=100, blank=True, null=True)
	fulfillment_status = models.CharField(max_length=100, blank=True, null=True)
	name = models.CharField(max_length=100, blank=True, null=True)
	number = models.IntegerField(max_length=100, blank=True, null=True)
	subtotal_price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	taxes_included = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	test = models.BooleanField(default=True)
	token = models.CharField(max_length=100, blank=True, null=True)
	total_line_items_price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	total_price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	total_price_usd = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	total_tax = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	total_weight = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	updated_at = models.DateTimeField(auto_now=True, editable=True)
	user_id = models.CharField(max_length=100, blank=True, null=True)
	order_number = models.IntegerField(max_length=100, blank=True, null=True)

	def __unicode__(self):
		return '%s (%s)' % (self.object_id, self.name)

class Fulfillment(models.Model):
	'''A fulfillment represents a shipment of one or more items in an order. Ideally, a shop owner would want to make just one fulfillment per order.'''
	order = models.ForeignKey(Order, related_name='shopifyfulfillment_order', blank=True, null=True)
	created_at = models.DateTimeField(auto_now=True, editable=True)
	object_id = models.IntegerField(max_length=100, blank=True, null=True)
	service = models.CharField(max_length=100, blank=True, null=True)
	status = models.CharField(max_length=100, blank=True, null=True)
	tracking_company = models.CharField(max_length=100, blank=True, null=True)
	updated_at = models.DateTimeField(auto_now=True, editable=True)
	tracking_number = models.CharField(max_length=100, blank=True, null=True)
	tracking_url = models.CharField(max_length=500, blank=True, null=True)
	receipt_testcase = models.BooleanField(default=True)
	receipt_authorization = models.CharField(max_length=100, blank=True, null=True)

	def __unicode__(self):
		return '%s (%s)' % (self.object_id, self.service)

class Tracking(models.Model):
	'''Tracking information for a given Fulfillment object.'''
	fulfillment = models.ForeignKey(Fulfillment, related_name='shopifytracking_fulfillment', blank=True, null=True)
	tracking_number = models.CharField(max_length=100, blank=True, null=True)
	tracking_url = models.CharField(max_length=500, blank=True, null=True)

	def __unicode__(self):
		return '%s (%s)' % (self.tracking_number, self.fulfillment)

class LineItem(models.Model):
	'''A single product as part of a Shopify Order.'''
	order = models.ForeignKey(Order, related_name='shopifylineitem_order', blank=True, null=True)
	fulfillment = models.ForeignKey(Fulfillment, related_name='shopifylineitem_fulfillment', blank=True, null=True)
	fulfillment_service = models.CharField(max_length=100, blank=True, null=True)
	fulfillment_status = models.CharField(max_length=100, blank=True, null=True)
	grams = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	object_id = models.IntegerField(max_length=100, blank=True, null=True)
	price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	product_id = models.IntegerField(max_length=100, blank=True, null=True)
	quantity = models.IntegerField(max_length=100, blank=True, null=True)
	requires_shipping = models.BooleanField(default=True)
	sku = models.CharField(max_length=100, blank=True, null=True)
	title = models.CharField(max_length=200, blank=True, null=True)
	variant_id = models.IntegerField(max_length=100, blank=True, null=True)
	variant_title = models.CharField(max_length=200, blank=True, null=True)
	vendor = models.CharField(max_length=200, blank=True, null=True)
	name = models.CharField(max_length=200, blank=True, null=True)

	def __unicode__(self):
		return '%s (%s)' % (self.object_id, self.title)

class ShippingLine(models.Model):
	'''The Shipping Method of a Shopify Order.'''
	order = models.ForeignKey(Order, related_name='shopifyshippingline_order', blank=True, null=True)
	code = models.CharField(max_length=100, blank=True, null=True)
	price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	source = models.CharField(max_length=200, blank=True, null=True)
	title = models.CharField(max_length=200, blank=True, null=True)

	def __unicode__(self):
		return '%s (%s)' % (self.title, self.price)

class ShippingAddress(models.Model):
	'''The Shipping Address of a Shopify Order.'''
	order = models.ForeignKey(Order, related_name='shopifyshippingaddress_order', blank=True, null=True)
	address1 = models.CharField(max_length=100, blank=True, null=True)
	address2 = models.CharField(max_length=100, blank=True, null=True)
	city = models.CharField(max_length=100, blank=True, null=True)
	company = models.CharField(max_length=100, blank=True, null=True)
	country = models.ForeignKey(Country, related_name='shopifyshippingaddress_country', blank=True, null=True)
	first_name = models.CharField(max_length=100, blank=True, null=True)
	last_name = models.CharField(max_length=100, blank=True, null=True)
	phone = models.CharField(max_length=100, blank=True, null=True)
	province = models.CharField(max_length=100, blank=True, null=True)
	zip = models.CharField(max_length=100, blank=True, null=True)
	name = models.CharField(max_length=100, blank=True, null=True)
	province_code = models.CharField(max_length=100, blank=True, null=True)

	def __unicode__(self):
		return '%s %s (%s)' % (self.first_name, self.last_name, self.city)

class Customer(models.Model):
	'''The customer of a Shopify Order.'''
	order = models.ForeignKey(Order, related_name='shopifycustomer_order', blank=True, null=True)
	accepts_marketing = models.BooleanField(default=True)
	email = models.CharField(max_length=100, blank=True, null=True)
	first_name = models.CharField(max_length=100, blank=True, null=True)
	object_id = models.IntegerField(max_length=100, blank=True, null=True)
	last_name = models.CharField(max_length=100, blank=True, null=True)

	def __unicode__(self):
		return '%s %s (%s)' % (self.first_name, self.last_name, self.object_id)

class ShipmentTransaction(models.Model):
	'''A Shippo Transaction of a given Shopify Order.'''
	order = models.ForeignKey(Order, related_name='shopifyshipmenttransaction_order', blank=True, null=True)
	created_at = models.DateTimeField(editable=True, blank=True, null=True)
	pickup_date = models.DateTimeField(auto_now=True, editable=True)
	transaction = models.ForeignKey(Transaction, related_name='shopifyshipmenttransaction_transaction', blank=True, null=True)