from django.conf.urls import patterns, url
from shopify_app import views

urlpatterns = patterns('',
    url(r'^$', views.login, name='login'),
    url(r'^account/$', views.account_landingpage, name='shopifyaccountlandingpage'),
    url(r'^authenticate/$', views.authenticate, name='authenticate'),
    url(r'^finalize/$', views.finalize, name='finalize'),
    url(r'^logout/$', views.logout, name='logout'),
)