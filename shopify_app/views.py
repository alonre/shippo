from django.shortcuts import render_to_response, redirect, render
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail
from django.template import RequestContext
from django.conf import settings
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from decimal import Decimal
from api.models import APIUser, Rate, Transaction
from rest_framework.authtoken.models import Token
from models import *
import shopify, datetime, sys
from user.views import loginFunction as cms_login
from websearch.shippo_api.shippo_api import ShippoAPI
from helpers.import_orders import import_orders
from helpers import shippo_api_objects
from helpers.shopify_session import start_session

def _return_address(request):
    return request.session.get('return_to') or reverse('user:userorders') #tbd maybe reverse to dashboard

def login(request):
    # Ask user for their ${shop}.myshopify.com address
    # If the ${shop}.myshopify.com address is already provided in the URL,
    # just skip to authenticate
    if not request.user.is_authenticated():
        #save shop name in session to instantly authenticate
        request.session['shopify_shop'] = request.REQUEST.get('shop')
        return redirect(reverse('shopify_app:shopifyaccountlandingpage'))
    if request.REQUEST.get('shop') or request.session['shopify_shop']:
        return authenticate(request)
    return render_to_response('shopify_app/connect_store_manually.html', {},
                              context_instance=RequestContext(request))

def account_landingpage(request):
    error = False
    if request.method == 'POST':
        success, error = cms_login(request)
        if success:
            return HttpResponseRedirect(reverse('shopify_app:login'))
    context = {'error': error}
    return render(request, 'shopify_app/account_landingpage.html', context)

def authenticate(request):
    shop = request.REQUEST.get('shop')
    if shop:
        apiuser = APIUser.objects.get(user=request.user)
        apiuser.shopify_shop_name = shop
        apiuser.save()
        session = shopify.Session(shop)
        scope = settings.SHOPIFY_API_SCOPE
        redirect_uri = request.build_absolute_uri(reverse('shopify_app:finalize'))
        permission_url = session.create_permission_url(scope, redirect_uri)
        return redirect(permission_url)

    return redirect(_return_address(request))

def finalize(request):
    current_user = request.user
    apiuser = APIUser.objects.get(user=current_user)
    shop_url = request.REQUEST.get('shop')
    params = {
        'shop': request.REQUEST.get('shop'),
        'timestamp': request.REQUEST.get('timestamp'),
        'signature': request.REQUEST.get('signature'),
        'code': request.REQUEST.get('code')
    }
    try:
        session = shopify.Session(shop_url)
        token = session.request_token(params)
    except:
        import sys
        print >>sys.stderr, "Shopify views.finalize: ValidationException when trying to get session token after temp auth"
        subject = 'Shopify views.finalize: ValidationException'
        message = 'Shopify views.finalize: ValidationException when trying to get session token after temp auth; Shop URL: ' + shop_url + ', User: ' + current_user.username
        to_mail = settings.SHOPIFY_ERROR_CONTACT_EMAIL
        send_mail(subject, message, 'error@goshippo.com', [to_mail], fail_silently=True)
        return redirect(reverse('shopify_app:login'))

    apiuser.shopify_access_token = token
    apiuser.shopify_connected = True
    apiuser.save()

    response = redirect(_return_address(request))
    request.session.pop('return_to', None)
    return response

def logout(request):
    request.session.pop('shopify', None)
    return redirect(reverse('shopify_app:login'))