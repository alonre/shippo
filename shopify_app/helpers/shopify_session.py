from django.core.mail import send_mail
import shopify
from django.conf import settings

def start_session(apiuser):
	try:
		session = shopify.Session(apiuser.shopify_shop_name, apiuser.shopify_access_token)
		shopify.ShopifyResource.activate_session(session)
		return True
	except:
		import sys
		print >>sys.stderr, "Could not establish a Shopify Session for APIUser " + str(apiuser.user.email)
		subject = 'Shippo: Shopify Session Error'
        message = "Could not establish a Shopify Session for APIUser " + str(apiuser.user.email)
        to_mail = settings.BACKEND_ERROR_CONTACT_EMAIL
        send_mail(subject, message, 'error@goshippo.com', [to_mail], fail_silently=True)