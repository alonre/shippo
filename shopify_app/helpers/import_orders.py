from shopify_app.models import *
from user.models import Order as User_Order
from user.models import Item as User_Item
from shopify_app.helpers.shopify_session import start_session
from api.models import Address, APIUser, Currency, Country
from django.conf import settings
from django.contrib.auth.models import User
import shopify
from django.core.mail import send_mail

def import_orders(current_user, apiuser):
    shopify_session = start_session(apiuser)

    if shopify_session:
        return get_unimported_orders(current_user)

def get_unimported_orders(current_user):
    #get last imported order
    existing_orders = User_Order.objects.filter(object_owner=current_user, shop_app='Shopify')
    existing_order_ids = []
    last_imported_order = None
    for order in existing_orders:
        existing_order_ids.append(order.object_id)
    new_orders = shopify.Order.find(closed_at='null', cancelled_at='null')
    for order in new_orders:
        if order.id not in existing_order_ids:
            try:
                save_data(order, current_user)
            except Exception as e:
                import sys
                print >>sys.stderr, 'Shopify Import: Save Data Exception: ' + str(e)
    return True

def save_data(order, current_user):
    user_order = save_user_order(order, current_user)
    for line_item in order.line_items:
        user_item = save_user_item(user_order, line_item)
    try:
        to_address = save_customer_address(user_order, order.shipping_address, order.customer)
    except:
        #order didnt have a shipping address which is fine (digital goods / services )
        to_address = save_customer_address(user_order, None, order.customer)
    try:
        user_order.to_address = to_address
        user_order.save()
    except Exception as e:
        import sys
        print >>sys.stderr, 'Shopify Import: To Address Exception: ' + str(e)
    return user_order

def save_user_order(order, current_user):
    user_order = User_Order()
    user_order.object_owner = current_user
    user_order.shop_app = 'Shopify'
    user_order.object_id = order.id
    user_order.created_at = order.created_at[:19] #we need the datetime substring, Shopify gives us a wrong string
    user_order.currency = Currency.objects.get(iso='USD')
    user_order.order_number = order.name
    user_order.subtotal_price = order.subtotal_price
    user_order.total_price = order.total_price_usd
    user_order.total_tax = order.total_tax
    user_order.total_weight = order.total_weight
    if order.shipping_lines:
        user_order.shipping_method = order.shipping_lines[0].title
        user_order.shipping_cost = order.shipping_lines[0].price
    user_order.shipping_cost_currency = Currency.objects.get(iso='USD')
    user_order.save()
    return user_order

def save_user_item(user_order, line_item):
    user_item = User_Item()
    user_item.order = user_order
    user_item.object_id = line_item.id
    user_item.grams = line_item.grams
    user_item.price = line_item.price
    user_item.currency = Currency.objects.get(iso='USD')
    user_item.product_id = line_item.product_id
    user_item.quantity = line_item.quantity
    user_item.title = line_item.title
    user_item.save()
    return user_item

def save_customer_address(user_order, shipping_address, customer):
    address = Address()
    address.object_owner = user_order.object_owner
    if shipping_address:
        address.object_purpose = 'PURCHASE'
        if shipping_address.first_name or shipping_address.last_name:
            address.name = shipping_address.first_name + ' ' + shipping_address.last_name
        elif customer.first_name or customer.last_name:
            address.name = customer.first_name + ' ' + customer.last_name
        else:
            address.name = 'Unknown'
        address.company = shipping_address.company
        address.street1 = shipping_address.address1
        address.street2 = shipping_address.address2
        address.state = shipping_address.province_code
        address.city = shipping_address.city
        address.zip = shipping_address.zip
        address.country = Country.objects.get(iso2=shipping_address.country_code)
        address.phone = shipping_address.phone
        if not address.phone:
            address.phone = '4151234567'
    else:
        address.object_purpose = 'QUOTE'
        if customer.first_name or customer.last_name:
            address.name = customer.first_name + ' ' + customer.last_name
        #we have to provide a country to save an address so let's use US
        address.country = Country.objects.get(iso2='US')
    address.email = customer.email
    if not address.email:
        address.email = 'info@goshippo.com'
    address.save()
    return address
