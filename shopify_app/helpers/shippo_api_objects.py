from api.models import Address, Parcel
from shopify_app.models import *
from websearch.shippo_api.shippo_api import ShippoAPI

def create_to_address(user, order):
	shipping_address = ShippingAddress.objects.get(order=order)
	customer = Customer.objects.get(order=order)
	to_address_query = {}
	if shipping_address.first_name or shipping_address.last_name:
		to_address_query['name'] = shipping_address.first_name + ' ' + shipping_address.last_name
	elif customer.first_name or customer.last_name:
		to_address_query['name'] = customer.first_name + ' ' + customer.last_name
	else:
		raise Exception('Recipient address name is missing or invalid.')
	to_address_query['company'] = shipping_address.company
	to_address_query['street1'] = shipping_address.address1
	to_address_query['street2'] = shipping_address.address2
	to_address_query['state'] = shipping_address.province_code
	to_address_query['city'] = shipping_address.city
	to_address_query['zip'] = shipping_address.zip
	to_address_query['country'] = shipping_address.country.iso2
	to_address_query['phone'] = shipping_address.phone
	if not to_address_query['phone']:
		to_address_query['phone'] = '4151234567'
	to_address_query['email'] = customer.email
	if not to_address_query['email']:
		to_address_query['email'] = 'info@goshippo.com'
	return to_address_query

