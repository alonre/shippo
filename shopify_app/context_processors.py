import shopify

def shopify_connected(request):
	from api.models import APIUser
	from django.contrib.auth.models import User
	if request.user.is_authenticated():
		current_user = request.user
		if current_user:
			apiuser = APIUser.objects.get(user=current_user)
			if apiuser.shopify_shop_name and apiuser.shopify_access_token and apiuser.shopify_connected:
				return {'shopify': apiuser.shopify_shop_name }
	return {'shopify': False }
