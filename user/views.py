from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from models import *
from api.models import *
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from rest_framework.authtoken.models import Token
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import uuid, sys, stripe, requests, datetime
from shopify_app.models import ShipmentTransaction
from shopify_app.helpers.import_orders import import_orders as import_shopify_orders
from etsy_app.helpers.import_orders import import_orders as import_etsy_orders
from cms.helpers.mandrill.base import MandrillMailService
from rq import Queue
from worker import conn
from websearch.shippo_api.shippo_api import ShippoAPI
from django.core.mail import send_mail

def loginFunction(request):
    error = True
    success = False
    if request.method == 'POST':
        try:
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    success = True
                else:
                    error = 'Your account is currently disabled. Have you already confirmed your e-mail?'
            else:
                error = 'Your e-mail or password is incorrect. Please try again!'
        except KeyError:
            error = 'There was a problem processing your data. Please try again or contact info@goshippo.com for more information. Sorry for the trouble!'
    return success, error

def registerUser(request):
    error = False
    success = False
    new_user = False
    if request.method == 'POST':
        email = request.POST['email']
        #check if username (=email) already exists
        existing_user = User.objects.filter(username=email)
        if existing_user:
            error = 'This e-mail address is already registered. Please use a different one or contact info@goshippo.com for help.'
        else:
            password = request.POST['password']
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']
            password = request.POST['password']
            #create User object
            new_user = User.objects.create_user(email, email, password)
            new_user.first_name = first_name
            new_user.last_name = last_name
            new_user.save()
            #we need to also create an APIUser (APIAddress and APIBilling will be handled by signals.py)
            new_apiuser = APIUser(user=new_user)
            #save a random activation code for email confirmation
            new_apiuser.shippo_activation_code = str(uuid.uuid4())
            new_apiuser.mixpanel_user_id = email
            new_apiuser.save()
            new_user.apiuser = new_apiuser
            #set user to false due to email confirmation
            new_user.is_active = False
            new_user.save()
            #queue activation mail
            q = Queue('low', connection=conn)
            q.enqueue_call(func=sendActivationMail, args=(new_apiuser,), result_ttl=0)
            #send Hipchat notif
            q = Queue('low', connection=conn)
            q.enqueue_call(func=sendHipchatNotif, args=(new_user,), result_ttl=0)
            #return success message
            success = 'Successfully registered! Please check your e-mail inbox to activate your account.'
    # we also return the (registered) user object to track regsitration on Mixpanel via JS
    context = {'error': error, 'success': success, 'new_user': new_user}
    return render(request, 'registration/registration.html', context)

def sendActivationMail(apiuser):
    #we need a separate function for that since RQ apparently can't handle methods
    mandrill_mail = MandrillMailService()
    return mandrill_mail.ActivationMail(apiuser)

def sendHipchatNotif(user):
    #we need to write a helper for that
    base_url = 'https://api.hipchat.com/v1/rooms/message'
    params = {
        'auth_token': '04ad28d4e7518895b480aad3cc15e8',
        'room_id': '249701',
        'from': 'Shippo Admin',
        'message': 'New API registration: ' + user.first_name + ' ' + user.last_name + ' (' + user.email + ')',
        'color': 'green',
        'notify': '1'
    }
    r = requests.get(base_url, params=params)
    return True

def activateUser(request, email, activation_code):
    error = False
    try:
        #get user for email who is not active
        email = email.replace('%40', '@')
        user = User.objects.get(email=email, is_active=False)
        #get apiuser for activation code that matches the user
        apiuser = APIUser.objects.get(shippo_activation_code=activation_code, user=user)
        if user and apiuser:
            #activate user
            user.is_active = True
            user.save()
            #queue welcome mail
            q = Queue('low', connection=conn)
            q.enqueue_call(func=sendWelcomeMail, args=(apiuser,), result_ttl=0)
            #forward user to login page
            context = {'success': 'You have successfully activated your account. Log in now to start shipping in minutes!'}
            return render(request, 'registration/login.html', context)
    except:
        error = 'We did not find a user for this URL that needs to be activated. Please try again or contact info@goshippo.com for help.'
    context = {'error': error}
    return render(request, 'registration/activation.html', context)

def sendWelcomeMail(apiuser):
    #we need a separate function for that since RQ apparently can't handle methods
    mandrill_mail = MandrillMailService()
    return mandrill_mail.WelcomeMail(apiuser)

def loginUser(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('user:userdashboard'))
    error = False
    if request.method == 'POST':
        success, error = loginFunction(request)
        if success:
            return HttpResponseRedirect(reverse('user:userdashboard'))
    context = {'error': error}
    return render(request, 'registration/login.html', context)

def logoutUser(request):
    logout(request)
    return HttpResponseRedirect(reverse('cms:homepage'))

@login_required
def CarrierOverview(request, success=False, endicia=False):
    apiuser = APIUser.objects.get(user=request.user)
    context = {'modal': False, 'success': success, 'endicia_success': endicia}
    return render(request, 'user/carriers.html', context)

@login_required
def CarrierOverviewFedEx(request):
    current_user = request.user
    apiuser = APIUser.objects.get(user=current_user)
    error = False
    if request.method == 'POST':
        try:
            apiuser.fedex_account = request.POST['fedex_account']
            apiuser.fedex_meter = request.POST['fedex_meter']
            if request.POST['fedex_production'] == 'True': apiuser.fedex_production = True
            elif request.POST['fedex_production'] == 'False': apiuser.fedex_production = False
            apiuser.save()
            return HttpResponseRedirect(reverse('user:usercarrierssuccess'))
        except:
            error = 'There was a problem saving your data.'
    context = {'modal': True, 'error': error}
    return render(request, 'user/carriers/fedex.html', context)

@login_required
def CarrierOverviewUPS(request):
    current_user = request.user
    apiuser = APIUser.objects.get(user=current_user)
    error = False
    if request.method == 'POST':
        try:
            apiuser.ups_account = request.POST['ups_account']
            apiuser.ups_user_id = request.POST['ups_user_id']
            apiuser.ups_password = request.POST['ups_password']
            if request.POST['ups_production'] == 'True': apiuser.ups_production = True
            elif request.POST['ups_production'] == 'False': apiuser.ups_production = False
            apiuser.save()
            return HttpResponseRedirect(reverse('user:usercarrierssuccess'))
        except:
            error = 'There was a problem saving your data.'
    context = {'modal': True, 'error': error}
    return render(request, 'user/carriers/ups.html', context)

@login_required
def CarrierOverviewUSPS(request):
    current_user = request.user
    apiuser = APIUser.objects.get(user=current_user)
    error = False
    if request.method == 'POST':
        try:
            if not apiuser.endicia_temporary_account:
                apiuser.endicia_account_id = request.POST['endicia_account_id']
                #only save passphrase if no temporary account (i.e. just created via Endicia API)
                apiuser.endicia_passphrase = request.POST['endicia_passphrase']
                apiuser.endicia_refill_amount = request.POST['endicia_refill_amount']
                if request.POST['endicia_production'] == 'True': apiuser.endicia_production = True
                elif request.POST['endicia_production'] == 'False': apiuser.endicia_production = False
                apiuser.save()
            if apiuser.endicia_temporary_account:
                #we need to reset the passphrase after user has been created through the API
                from adapter.endicia.config import EndiciaConfig
                from adapter.endicia.user_service import EndiciaUserService
                config = EndiciaConfig(current_user)
                adapter = EndiciaUserService(config)
                new_passphrase = str(uuid.uuid4())
                response = adapter.change_passphrase(new_passphrase)
                if response == True:
                    #passphrase successfully changed
                    apiuser.endicia_account_id = request.POST['endicia_account_id']
                    apiuser.endicia_passphrase = new_passphrase
                    apiuser.endicia_refill_amount = request.POST['endicia_refill_amount']
                    apiuser.endicia_temporary_account = False
                    if request.POST['endicia_production'] == 'True': apiuser.endicia_production = True
                    elif request.POST['endicia_production'] == 'False': apiuser.endicia_production = False
                    apiuser.save()
                    return HttpResponseRedirect(reverse('user:usercarrierssuccess'))
                else:
                    #there was an error with the account -> get the account status and display it to user; TODO: reset passphrase etc.
                    response = adapter.get_account_status()
                    error_message = 'There was an error with your Endicia account: ' + reponse + ' Please contact info@goshippo.com for further instructions. Sorry for the trouble!'
            return HttpResponseRedirect(reverse('user:usercarrierssuccess'))
        except:
            error = 'There was a problem saving your data.'
    context = {'modal': True, 'error': error}
    return render(request, 'user/carriers/usps.html', context)

@login_required
def CarrierOverviewUSPSEndiciaRegistration(request):
    current_user = request.user
    apiuser = APIUser.objects.get(user=current_user)
    error_message = False
    if request.method == 'POST':
        try:
            endicia_user = {}
            endicia_user['first_name'] = request.POST['customer_firstname']
            endicia_user['last_name'] = request.POST['customer_lastname']
            endicia_user['phone'] = request.POST['customer_phone']
            endicia_user['challenge_question'] = request.POST['security_question']
            endicia_user['challenge_answer'] = request.POST['security_answer']
            endicia_user['physical_street'] = request.POST['physical_street']
            endicia_user['physical_city'] = request.POST['physical_city']
            endicia_user['physical_state'] = request.POST['physical_state']
            endicia_user['physical_zip'] = request.POST['physical_zip']
            endicia_user['cc_number'] = request.POST['credit_number']
            endicia_user['cc_street'] = request.POST['billing_street']
            endicia_user['cc_city'] = request.POST['billing_city']
            endicia_user['cc_state'] = request.POST['billing_state']
            endicia_user['cc_zip'] = request.POST['billing_zip']
            endicia_user['cc_exp_mm'] = request.POST['credit_exp_mm']
            endicia_user['cc_exp_yy'] = request.POST['credit_exp_yy']
            endicia_user['passphrase'] = str(uuid.uuid4())
            endicia_user['webpassword'] = str(uuid.uuid4())[:15]
            if not apiuser.address:
                apiuser.address = APIAddress.objects.create()
                apiuser.address.save()
            if not apiuser.address.name:
                apiuser.address.name = endicia_user['first_name'] + ' ' + endicia_user['last_name']
            if not apiuser.address.phone:
                apiuser.address.phone = endicia_user['phone']
            if not apiuser.address.city and not apiuser.address.country:
                apiuser.address.street1 = endicia_user['physical_street']
                apiuser.address.city = endicia_user['physical_city']
                apiuser.address.state = endicia_user['physical_state']
                apiuser.address.zip = endicia_user['physical_zip']
                apiuser.address.country = Country.objects.get(iso2='US')
            apiuser.address.save()
            apiuser.endicia_challenge_question = endicia_user['challenge_question']
            apiuser.endicia_challenge_answer = endicia_user['challenge_answer']
            apiuser.endicia_passphrase = endicia_user['passphrase']
            apiuser.endicia_webpassword = endicia_user['webpassword']
            apiuser.endicia_temporary_account = True
            #save user and apiuser
            apiuser.save()
            #request Endicia API user account
            from adapter.endicia.config import EndiciaConfig
            from adapter.endicia.user_service import EndiciaUserService
            config = EndiciaConfig(current_user)
            adapter = EndiciaUserService(config)
            response = adapter.generate_account(endicia_user)
            if response == True:
                return HttpResponseRedirect(reverse('user:usercarrierssuccessendicia'))
            else:
                error_message = response
        except:
            error_message = 'There was a problem saving your data.'
    context = {'modal': True, 'error_message': error_message}
    return render(request, 'user/carriers/usps-endicia-new.html', context)

@login_required
def CarrierOverviewParcel2Go(request):
    current_user = request.user
    apiuser = APIUser.objects.get(user=current_user)
    error = False
    if request.method == 'POST':
        try:
            apiuser.parcel2go_api_key = request.POST['parcel2go_api_key']
            apiuser.parcel2go_card_reference = request.POST['parcel2go_card_reference']
            if request.POST['parcel2go_production'] == 'True': apiuser.parcel2go_production = True
            elif request.POST['parcel2go_production'] == 'False': apiuser.parcel2go_production = False
            apiuser.save()
            return HttpResponseRedirect(reverse('user:usercarrierssuccess'))
        except:
            error = 'There was a problem saving your data.'
    context = {'modal': True, 'error': error}
    return render(request, 'user/carriers/parcel2go.html', context)

@login_required
def CarrierOverviewRoyalMail(request):
    current_user = request.user
    apiuser = APIUser.objects.get(user=current_user)
    error = False
    if request.method == 'POST':
        try:
            apiuser.royalmail_username = request.POST['royalmail_username']
            apiuser.royalmail_password = request.POST['royalmail_password']
            if request.POST['royalmail_production'] == 'True': apiuser.royalmail_production = True
            elif request.POST['royalmail_production'] == 'False': apiuser.royalmail_production = False
            apiuser.save()
            return HttpResponseRedirect(reverse('user:usercarrierssuccess'))
        except:
            error = 'There was a problem saving your data.'
    context = {'modal': True, 'error': error}
    return render(request, 'user/carriers/royalmail.html', context)

@login_required
def CarrierOverviewDeutschePost(request):
    current_user = request.user
    apiuser = APIUser.objects.get(user=current_user)
    error = False
    if request.method == 'POST':
        try:
            apiuser.portokasse_mail = request.POST['portokasse_mail']
            apiuser.portokasse_password = request.POST['portokasse_password']
            if request.POST['deutsche_post_production'] == 'True': apiuser.deutsche_post_production = True
            elif request.POST['deutsche_post_production'] == 'False': apiuser.deutsche_post_production = False
            apiuser.save()
            return HttpResponseRedirect(reverse('user:usercarrierssuccess'))
        except:
            error = 'There was a problem saving your data.'
    context = {'modal': True, 'error': error}
    return render(request, 'user/carriers/deutschepost.html', context)

@login_required
def CarrierOverviewDeutschePostDHL(request):
    current_user = request.user
    apiuser = APIUser.objects.get(user=current_user)
    error = False
    if request.method == 'POST':
        try:
            apiuser.dhl_account_number = request.POST['dhl_account_number']
            apiuser.dhl_intraship_username = request.POST['dhl_intraship_username']
            apiuser.dhl_intraship_password = request.POST['dhl_intraship_password']
            apiuser.postpay_mail = request.POST['postpay_mail']
            apiuser.postpay_password = request.POST['postpay_password']
            if request.POST['dp_dhl_production'] == 'True': apiuser.dp_dhl_production = True
            elif request.POST['dp_dhl_production'] == 'False': apiuser.dp_dhl_production = False
            if request.POST['dhl_production'] == 'True': apiuser.dhl_production = True
            elif request.POST['dhl_production'] == 'False': apiuser.dhl_production = False
            apiuser.save()
            return HttpResponseRedirect(reverse('user:usercarrierssuccess'))
        except:
            error = 'There was a problem saving your data.'
    context = {'modal': True, 'error': error}
    return render(request, 'user/carriers/deutschepostdhl.html', context)

@login_required
def UserProfile(request):
    current_user = request.user
    apiuser = APIUser.objects.get(user=current_user)
    country_list = Country.objects.all()
    success = False
    error = False
    if request.method == 'POST':
        try:
            current_user.first_name = request.POST['user_firstname']
            current_user.last_name = request.POST['user_lastname']
            if request.POST['user_password']:
                current_user.set_password(request.POST['user_password'])
            if not apiuser.address:
                apiuser.address = APIAddress.objects.create()
                apiuser.address.save()
            apiuser.address.phone = request.POST['user_phone']
            apiuser.address.company = request.POST['user_company']
            apiuser.address.street1 = request.POST['user_street1']
            apiuser.address.street_no = request.POST['user_street_no']
            apiuser.address.street2 = request.POST['user_street2']
            apiuser.address.city = request.POST['user_city']
            apiuser.address.zip = request.POST['user_zip']
            apiuser.address.state = request.POST['user_state']
            if request.POST['user_country'] is not None:
                try:
                    user_country = Country.objects.get(iso2=(request.POST['user_country']))
                    apiuser.address.country = user_country
                except:
                    pass
            current_user.save()
            apiuser.address.save()
            apiuser.save()
            success = True
        except:
            error = 'There was a problem saving your data.'
    context = {'success': success,'error': error, 'country_list': country_list}
    return render(request, 'user/profile.html', context)

@login_required
def UserBilling(request):
    current_user = request.user
    apiuser = APIUser.objects.get(user=current_user)
    country_list = Country.objects.all()
    success = False
    error = False
    stripe_publishable_key = settings.STRIPE_PUBLISHABLE_KEY_PRODUCTION
    if request.method == 'POST':
        try:
            #set apiuser billing object
            if not apiuser.billing:
                apiuser.billing = APIBilling.objects.create()
                apiuser.billing.save()
            #set apiuser billing address
            if not apiuser.billing.billing_address:
                apiuser.billing.billing_address = APIAddress.objects.create()
                apiuser.billing.billing_address.save()
            apiuser.billing.billing_address.name = request.POST['billing_name']
            apiuser.billing.billing_address.company = request.POST['billing_company']
            apiuser.billing.billing_address.street1 = request.POST['billing_street1']
            apiuser.billing.billing_address.street_no = request.POST['billing_street_no']
            apiuser.billing.billing_address.street2 = request.POST['billing_street2']
            apiuser.billing.billing_address.city = request.POST['billing_city']
            apiuser.billing.billing_address.zip = request.POST['billing_zip']
            apiuser.billing.billing_address.state = request.POST['billing_state']
            if request.POST['billing_country'] is not None:
                try:
                    billing_country = Country.objects.get(iso2=(request.POST['billing_country']))
                    apiuser.billing.billing_address.country = billing_country
                except:
                    pass
            apiuser.billing.billing_address.save()
            #set apiuser billing stripe settings
            if 'stripe_token' in request.POST:
                try:
                    stripe.api_key = settings.STRIPE_SECRET_KEY_PRODUCTION
                    token = request.POST['stripe_token']
                    if apiuser.billing.stripe_token:
                        stripe_customer = stripe.Customer.retrieve(apiuser.billing.stripe_token)
                        stripe_customer.card = token
                        stripe_customer.email = apiuser.user.username
                        stripe_customer.metadata = {'First name': apiuser.user.first_name, 'Last name': apiuser.user.last_name}
                        stripe_customer.save()
                    else:
                        stripe_customer = stripe.Customer.create(
                            card=token,
                            description='',
                            email=apiuser.user.username,
                            metadata={'First name': apiuser.user.first_name, 'Last name': apiuser.user.last_name}
                        )
                        apiuser.billing.stripe_token = stripe_customer.id
                    apiuser.billing.stripe_authorized = True
                    apiuser.billing.payment_type = 'STRIPE'
                except stripe.CardError, e:
                    body = e.json_body
                    err  = body['error']
                    print >>sys.stderr, "Card Error when creating or saving stripe customer id data for user " + apiuser.user.username
                    print >>sys.stderr, "Status is: %s" % e.http_status
                    print >>sys.stderr, "Type is: %s" % err['type']
                    print >>sys.stderr, "Code is: %s" % err['code']
                    print >>sys.stderr, "Message is: %s" % err['message']
                    error = err['message']
                except stripe.InvalidRequestError, e:
                    print >>sys.stderr, "Invalid request error when creating or saving stripe customer id data for user " + apiuser.user.username
                    error = 'There was a problem saving your credit card data. Please try again or contact info@goshippo.com for help. Sorry for the trouble!'
                except stripe.AuthenticationError, e:
                    print >>sys.stderr, "Authentication error when creating or saving stripe customer id data for user " + apiuser.user.username
                    error = 'There was a problem saving your credit card data. Please try again or contact info@goshippo.com for help. Sorry for the trouble!'
                except stripe.APIConnectionError, e:
                    print >>sys.stderr, "API Connection error when creating or saving stripe customer id data for user " + apiuser.user.username
                    error = 'There was a problem saving your credit card data. Please try again or contact info@goshippo.com for help. Sorry for the trouble!'
                except stripe.StripeError, e:
                    print >>sys.stderr, "Stripe error when creating or saving stripe customer id data for user " + apiuser.user.username
                    error = 'There was a problem saving your credit card data. Please try again or contact info@goshippo.com for help. Sorry for the trouble!'
                except:
                    print >>sys.stderr, "Error when creating or saving stripe customer id data for user " + apiuser.user.username
                    error = 'There was a problem saving your credit card data. Please try again or contact info@goshippo.com for help. Sorry for the trouble!'
            #save everything
            apiuser.billing.save()
            apiuser.save()
            if error is False:
                success = True
        except:
            error = 'There was a problem saving your data.'
    context = {
        'success': success,
        'error': error,
        'country_list': country_list,
        'stripe_publishable_key': stripe_publishable_key }
    return render(request, 'user/billing.html', context)

@login_required
def UserAPIKeys(request):
    current_user = request.user
    token = Token.objects.get(user=current_user)
    token_auth = token.key
    context = {'token_auth': token_auth}
    return render(request, 'user/apikeys.html', context)

@login_required
def UserDashboard(request):
    current_user = request.user
    apiuser = APIUser.objects.get(user=current_user)
    error = False
    #get all transactions, paginated
    all_transactions = Transaction.objects.filter(object_owner=current_user).order_by('-object_updated')
    #filter by mode, default = production
    if request.GET.get('mode') and request.GET.get('mode') == 'test':
        was_test = True
        all_transactions = all_transactions.filter(was_test=True).order_by('-object_updated')
    else:
        was_test = False
        all_transactions = all_transactions.filter(was_test=False).order_by('-object_updated')
    #for integration users, filter by order (only reachable via link on order page)
    order = request.GET.get('order')
    if order:
        related_fulfillments = Fulfillment.objects.filter(order=order)
        fulfillment_transactions = []
        for obj in related_fulfillments:
            if obj.transaction:
                fulfillment_transactions.append(obj.transaction.object_id)
        all_transactions = all_transactions.filter(object_id__in=fulfillment_transactions)
    paginator = Paginator(all_transactions, 25)
    page = request.GET.get('page')
    try:
        transactions = paginator.page(page)
    except PageNotAnInteger:
        transactions = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        transactions = paginator.page(paginator.num_pages)
    if transactions:
        related_messages = Message.objects.filter(transaction__in=transactions)
        transaction_message_map = {}
        for message in related_messages:
            # start appending to a list keyed by the transaction ID for all related messages
            transaction_message_map.setdefault(message.transaction_id, []).append(message)

        for transaction in transactions:
            # set an attribute on the transaction that is the list created above
            transaction.messages = transaction_message_map.get(transaction.id)
    context = {'transactions': transactions, 'error': error, 'was_test': str(was_test)}
    return render(request, 'user/dashboard.html', context)

@login_required
def UserOrders(request, order_not_found=False):
    current_user = User.objects.get(id=request.user.id)
    apiuser = APIUser.objects.get(user=current_user)
    error = False
    orders = False
    if order_not_found:
        error = 'The order or transaction you have requested has not been found. Please try again or contact us at info@goshippo.com for help. Sorry for the trouble!'
    #import orders if requested
    if request.GET.get('import'):
        if apiuser.shopify_set:
            try:
                import_shopify_orders(current_user, apiuser)
            except Exception as e:
                import sys
                print >>sys.stderr, 'Shopify Import Failed with Exception ' + str(e)
        if apiuser.etsy_set:
            try:
                import_etsy_orders(apiuser)
            except Exception as e:
                import sys
                print >>sys.stderr, 'Etsy Import Failed with Exception ' + str(e)
    #get all orders
    all_orders = Order.objects.filter(object_owner=current_user).order_by('-created_at')
    paginator = Paginator(all_orders, 25)
    page = request.GET.get('page')
    try:
        orders = paginator.page(page)
    except PageNotAnInteger:
        orders = paginator.page(1)
    except EmptyPage:
        orders = paginator.page(paginator.num_pages)
    #get order customer
    if orders:
        #get order items
        related_item = Item.objects.filter(order__in=orders)
        order_item_map = {}
        for item in related_item:
            # start appending to a list keyed by the order ID for all related customer
            order_item_map.setdefault(item.order_id, []).append(item)
        for order in orders:
            # set an attribute on the order that is the list created above
            order.items = order_item_map.get(order.id)
            # get fulfillments
            related_fulfillments = Fulfillment.objects.filter(order=order, transaction__isnull=False)
            order.fulfillments = related_fulfillments
    context = {'orders': orders, 'error': error}
    return render(request, 'user/orders/orders.html', context)

@login_required
def UserOrderInfo(request, order_id):
    current_user = User.objects.get(id=request.user.id)
    apiuser = APIUser.objects.get(user=current_user)
    error=False
    try:
        order = Order.objects.get(object_owner=current_user, id=order_id)
    except:
        order = False
    if order:
        related_item = Item.objects.filter(order=order)
        item_list = []
        for item in related_item:
            if item.grams:
                item.kgs = Decimal(item.grams) / 1000
                item.lbs = Decimal(item.grams) / 453
            item.total = Decimal(item.price) * Decimal(item.quantity)
            item_list.append(item)
        order.items = item_list
        context = {'order': order, 'error': error}
        return render(request, 'user/orders/order_info.html', context)
    else:
        return HttpResponseRedirect(reverse('user:userordersordernotfound'))

@login_required
def UserOrderRates(request, order_id):
    current_user = User.objects.get(id=request.user.id)
    apiuser = APIUser.objects.get(user=current_user)
    error = False
    order = None
    parcel = None
    try:
        order = Order.objects.get(object_owner=current_user, id=order_id)
    except:
        return HttpResponseRedirect(reverse('user:userordersordernotfound'))
    if request.method == 'POST':
        pickup_date = request.POST['shipment-date']
        if pickup_date:
            #create new Fulfillment, no matter if there are already existing ones
            pickup_date = datetime.datetime.strptime( str(pickup_date) + 'T12:00:00', "%Y-%m-%dT%H:%M:%S" )
            fulfillment = Fulfillment(pickup_date=pickup_date, order=order)
        else:
            #this should actually never happen since pickup_date is mandatory client-side
            fulfillment = Fulfillment(order=order)
        fulfillment.save()
    if not order or not pickup_date:
        return HttpResponseRedirect(reverse('user:userordersordernotfound'))
    else:
        related_item = Item.objects.filter(order=order)
        item_list = []
        for item in related_item:
            if item.grams:
                item.kgs = Decimal(item.grams) / 1000
                item.lbs = Decimal(item.grams) / 453
            item.total = Decimal(item.price) * Decimal(item.quantity)
            item_list.append(item)
        order.items = item_list
        try:
            from_address_id = ShippoAPI().saveAddress(
                    purpose='PURCHASE',
                    name=apiuser.address.name,
                    company=apiuser.address.company,
                    street1=apiuser.address.street1,
                    street_no=apiuser.address.street_no,
                    street2=apiuser.address.street2,
                    city=apiuser.address.city,
                    zip=apiuser.address.zip,
                    state=apiuser.address.state,
                    country=apiuser.address.country.iso2,
                    phone=apiuser.address.phone,
                    email=apiuser.address.email,
                    user=current_user
                )
            if from_address_id and order.to_address:
                parcel_id = ShippoAPI().saveParcel(
                    length=float(request.POST['dimensions-length']),
                    width=float(request.POST['dimensions-width']),
                    height=float(request.POST['dimensions-height']),
                    distance_unit=request.POST['dimensions-unit'],
                    weight=float(request.POST['weight-value']),
                    mass_unit=request.POST['weight-unit'],
                    user=current_user
                )
                if parcel_id:
                    shipment_id = ShippoAPI().saveShipment(
                        object_purpose='PURCHASE',
                        address_from=from_address_id,
                        address_to=order.to_address.object_id,
                        parcel=parcel_id,
                        user=current_user
                    )
                fulfillment.parcel = Parcel.objects.get(object_id=parcel_id)
                fulfillment.shipment = Shipment.objects.get(object_id=shipment_id)
                fulfillment.save()
        except Exception as e:
            #send error mail to shopify admin
            message = 'Order Rates: something went wrong when creating rates; Order ID: ' + str(order_id) + ', User: ' + current_user.username + ', Error: ' + str(e) + ' Shipment Transaction ID: ' + str(fulfillment.id)
            send_mail('Order: Rates Error', message, 'error@goshippo.com', [settings.FRONTEND_ERROR_CONTACT_EMAIL], fail_silently=True)
            #this redirect might be frustrating for users because message is too general and should be changed in the future
            return HttpResponseRedirect(reverse('user:userordersordernotfound'))
        #get javascript variables
        http_type = settings.SHIPPO_API_FRONTEND['http_type']
        base_url = settings.SHIPPO_API_FRONTEND['base_url']
        rates_url_ext = settings.SHIPPO_API_FRONTEND['rates_url_ext']
        shipments_url_ext = settings.SHIPPO_API_FRONTEND['shipments_url_ext']
        api_token = Token.objects.get(user=current_user).key
        context = {
            'shipment_id': shipment_id,
            'order': order,
            'error': error,
            'base_url': base_url,
            'rates_url_ext': rates_url_ext,
            'api_token': api_token,
            'shipments_url_ext': shipments_url_ext,
            'http_type': http_type,
            'fulfillment': fulfillment.id
            }
        return render(request, 'user/orders/order_rates.html', context)

@login_required
def UserOrderLabel(request, order_id, rate_id, fulfillment_id):
    current_user = User.objects.get(id=request.user.id)
    apiuser = APIUser.objects.get(user=current_user)
    error = False
    try:
        order = Order.objects.get(object_owner=current_user, id=order_id)
    except:
        return HttpResponseRedirect(reverse('user:userordersordernotfound'))
    if order:
        #get last fulfillment
        try:
            fulfillment = Fulfillment.objects.get(order=order, id=fulfillment_id)
            pickup_date = fulfillment.pickup_date
        except Exception as e:
            #send error mail to shopify admin
            message = 'Order Label Fulfillment: something went wrong when getting fulfillment while creating label (the label has NOT yet been created); Order ID: ' + str(order_id) + ', User: ' + current_user.username + ', Error: ' + str(e)
            send_mail('Order: Label Fulfillment Error', message, 'error@goshippo.com', [settings.FRONTEND_ERROR_CONTACT_EMAIL], fail_silently=True)
            #this redirect might be frustrating for users because message is too general and should be changed in the future
            return HttpResponseRedirect(reverse('user:userordersordernotfound'))
    if not rate_id or not fulfillment:
        #this redirect might be frustrating for users because message is too general and should be changed in the future
        return HttpResponseRedirect(reverse('user:userordersordernotfound'))
    else:
        #check if there already was a transaction attempt to prevent double charges
        if not fulfillment.transaction:
            try:
                transaction_id = ShippoAPI().saveTransaction(
                    rate=rate_id,
                    notification_email_from=False,
                    notification_email_to=False,
                    notification_email_other='',
                    pickup_date=pickup_date,
                    user=current_user
                )
                fulfillment.transaction = Transaction.objects.get(object_id=transaction_id)
                fulfillment.save()
                success = True
            except Exception as e:
                #send error mail to shopify admin
                message = "Order Transaction Error: something went wrong when creating label (the label MIGHT HAVE BEEN CREATED); Order ID: " + str(order_id) + '. Error: ' + str(e) + ' Fulfillment ID: ' + str(fulfillment)
                send_mail('Order: Label Transaction Error', message, 'error@goshippo.com', [settings.FRONTEND_ERROR_CONTACT_EMAIL], fail_silently=True)
                error = 'There was an error while requesting your label. Our technical team has been informed and is looking into this. We are very sorry for the trouble!'
                transaction_id = False
                success = False
            context = {'order': order, 'error': error, 'success': success, 'transaction_id': transaction_id, 'rate': rate_id, 'pickup_date': pickup_date}
            return render(request, 'user/orders/order_label.html', context)
        else:
            error = 'It seems like you have tried to open this page multiple times. In order to prevent multiple charges, your label has not been created. Please try again or contact info@goshippo.com. We are sorry for the trouble!'
            transaction_id = False
            success = False
            context = {'order': order, 'error': error, 'success': success, 'transaction_id': transaction_id, 'rate': rate_id, 'pickup_date': pickup_date}
            return render(request, 'user/orders/order_label.html', context)

