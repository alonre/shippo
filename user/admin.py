from django.contrib import admin
from models import *

class ItemInline(admin.StackedInline):
    model = Item
    max_num = 0 #this does not prevent existing objects from being displayed
    can_delete = False
    verbose_name_plural = 'Items'

class FulfillmentInline(admin.StackedInline):
    model = Fulfillment
    max_num = 0 #this does not prevent existing objects from being displayed
    can_delete = False
    verbose_name_plural = 'Items'

    readonly_fields = ['pickup_date', 'parcel', 'shipment', 'transaction_link',]

    def transaction_link(self, obj):
        if obj.transaction:
            url = reverse('admin:api_transaction_change', args=(obj.transaction.id,))
            return mark_safe("<a href='%s'>%s</a>" % (url, obj.transaction))
        else:
            return mark_safe("<a href='#'>no transaction</a>")

    # the following is necessary if 'link' method is also used in list_display
    transaction_link.allow_tags = True

class OrderAdmin(admin.ModelAdmin):
	list_display = ('order_number', 'object_owner', 'shop_app', 'created_at', 'total_price', 'currency',)
	list_filter = ('shop_app', 'object_owner',)
	list_per_page = 50
	save_on_top = True
	inlines = (ItemInline, FulfillmentInline)

admin.site.register(Order, OrderAdmin)
admin.site.register(Item)
admin.site.register(Fulfillment)