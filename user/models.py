from django.db import models
from model_utils import Choices
from api.models import Address, Parcel, Shipment, Currency, Country, APIUser, Transaction
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse

class Order(models.Model):
	'''An order is a customer's completed request to purchase one or more products from a shop. '''

	#Choices
	SHOP_APP_CHOICE = Choices('Shopify', 'Etsy')

	#Metafields
	object_owner = models.ForeignKey('auth.User', related_name='order_object_owner_user', blank=True, null=True)
	shop_app = models.CharField(choices=SHOP_APP_CHOICE, max_length=50, blank=True, null=True)
	imported_at = models.DateTimeField(auto_now_add=True)

	#Order-related
	#App's unique order ID
	object_id = models.IntegerField(max_length=100, blank=True, null=True)
	#App User's order ID
	order_number = models.CharField(max_length=200, blank=True, null=True)
	created_at = models.DateTimeField(blank=True, null=True)
	currency = models.ForeignKey(Currency, related_name='order_currency_currency', blank=True, null=True)
	subtotal_price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	total_price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	total_tax = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	total_weight = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	shipping_method = models.CharField(max_length=200, blank=True, null=True)
	shipping_cost = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	shipping_cost_currency = models.ForeignKey(Currency, related_name='order_shipppingcost_currency', blank=True, null=True)
	to_address = models.ForeignKey(Address, related_name='order_to_address_address', blank=True, null=True)

	def __unicode__(self):
		return '%s (%s)' % (self.object_id, self.object_id)

class Item(models.Model):
	'''A single product as part of an order.'''

	#Metafields
	order = models.ForeignKey(Order, related_name='item_order_order', blank=True, null=True)

	#Item-related
	object_id = models.IntegerField(max_length=100, blank=True, null=True)
	grams = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	currency = models.ForeignKey(Currency, related_name='item_currency_currency', blank=True, null=True)
	product_id = models.IntegerField(max_length=100, blank=True, null=True)
	quantity = models.IntegerField(max_length=100, blank=True, null=True)
	title = models.CharField(max_length=200, blank=True, null=True)

	def __unicode__(self):
		return '%s (%s)' % (self.object_id, self.product_id)

class Fulfillment(models.Model):
	'''A fulfillment of an order.'''

	#Metafields
	order = models.ForeignKey(Order, related_name='fulfillment_order_order', blank=True, null=True)

	pickup_date = models.DateTimeField(editable=True, blank=True, null=True)
	created_at = models.DateTimeField(auto_now_add=True, editable=True, blank=True, null=True)
	parcel = models.ForeignKey(Parcel, related_name='order_parcel_parcel', blank=True, null=True)
	shipment = models.ForeignKey(Shipment, related_name='order_shipment_shipment', blank=True, null=True)
	transaction = models.ForeignKey(Transaction, related_name='order_transaction_transaction', blank=True, null=True)