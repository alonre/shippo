def resolve_urlname(request):
    """Allows us to see what the matched urlname for this
    request is within the template"""
    from django.core.urlresolvers import resolve
    try:
        res = resolve(request.path)
        if res:
            return {'urlname' : res.url_name}
    except:
        return {}

def access_settings(request):
    """Allows us to see what the matched urlname for this
    request is within the template"""
    from django.conf import settings
    return {
    	'GA_CODE': settings.GOOGLE_ANALYTICS_CODE,
    	'MIXPANEL_CODE': settings.MIXPANEL_FRONTEND_PROD_CODE,
        'MIXPANEL_API_CODE': settings.MIXPANEL_API_CODE,
    }

def lookup_apiuser(request):
    """Check if there's an APIUser logged in and pass on the object if so"""
    from django.conf import settings
    from api.models import APIUser
    from django.contrib.auth.models import User
    if request.user.is_authenticated():
        current_user = request.user
        if current_user:
            apiuser = APIUser.objects.get(user=current_user)
            return {'apiuser': apiuser}
    return {'apiuser': False }
