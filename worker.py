import os

import redis
from rq import Worker, Queue, Connection

# prevent jobs from being moved to failed queue; instead immediate deletion!
def black_hole(job, *exc_info):
    return False

listen = ['high', 'default', 'low']

redis_url = os.getenv('REDISTOGO_URL', 'redis://localhost:6379')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shippo.settings")

conn = redis.from_url(redis_url)

if __name__ == '__main__':
    with Connection(conn):
        worker = Worker(map(Queue, listen))
        worker.push_exc_handler(black_hole)
        worker.work()

