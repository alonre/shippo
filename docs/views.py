from django.shortcuts import render
from django.contrib.auth.models import User

def APIDocsOverview(request):
    current_user = request.user
    context = {}
    return render(request, 'overview.html', context)

def APIDocsAddresses(request):
    current_user = request.user
    context = {}
    return render(request, 'addresses.html', context)

def APIDocsParcels(request):
    current_user = request.user
    context = {}
    return render(request, 'parcels.html', context)

def APIDocsShipments(request):
    current_user = request.user
    context = {}
    return render(request, 'shipments.html', context)

def APIDocsRates(request):
    current_user = request.user
    context = {}
    return render(request, 'rates.html', context)

def APIDocsLabels(request):
    current_user = request.user
    context = {}
    return render(request, 'labels.html', context)

def APIDocsTransactions(request):
    current_user = request.user
    context = {}
    return render(request, 'transactions.html', context)

def APIDocsBilling(request):
    current_user = request.user
    context = {}
    return render(request, 'billing.html', context)

def APIDocsTesting(request):
    current_user = request.user
    context = {}
    return render(request, 'apitesting.html', context)
