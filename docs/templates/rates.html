{% extends "docs.html" %}

{% block apicontent %}
<h2>Rates</h2>

<h4>/shipments/<em>&lt;SHIPMENT OBJECT ID&gt;</em>/rates/[<em>&lt;CURRENCY CODE&gt;</em>/]</h4>

<p>Each valid Shipment object will automatically trigger the calculation of all available Rates. Depending on your Addresses and Parcel, there may be none, one or multiple Rates.</p>
<p>By default, the calculated Rates will return the price in two currencies under the "amount" and "amount_local" keys, respectively.
The "amount" key will contain the price of a Rate expressed in the currency that is used in the country from which Parcel originates, and the "amount_local" key will contain the price expressed in the currency that is used in the country the Parcel is shipped to.
You can request Rates with prices expressed in a different currency by adding the desired currency code in the end of the resource URL. The full list of supported currencies along with their codes can be viewed on <a href="http://openexchangerates.org/api/currencies.json">open exchange rates</a>.</p>

<br><p>API Resource URL: <strong>https://api.goshippo.com/v1/shipments/<em>&lt;SHIPMENT OBJECT ID&gt;</em>/rates/[<em>&lt;CURRENCY CODE&gt;</em>/]</strong></p>

<h4>Example Response JSON schema</h4>
<div id="pre_response"></div>
<script type="text/javascript">
	var obj = {
		"object_state": "VALID",
        "object_purpose": "QUOTE",
        "object_created": "2013-12-09T01:56:52.780Z",
        "object_updated": "2013-12-09T01:56:52.780Z",
        "object_id": "cf6fea899f1848b494d9568e8266e076",
        "object_owner": "tech@goshippo.com",
        "available_shippo": true,
        "attributes": [],
        "amount_local": "46.60",
        "currency_local": "USD",
        "amount": "34.00",
        "currency": "EUR",
        "provider": "Deutsche Post DHL",
        "provider_image_75": "https://shippo-static.s3.amazonaws.com/providers/75/Deutsche_Post_DHL.png",
        "provider_image_200": "https://shippo-static.s3.amazonaws.com/providers/200/Deutsche_Post_DHL.png",
        "servicelevel_name": "Paket",
        "servicelevel_terms": "Trackable in over 60 countries world-wide. Insurance up to EUR 500 included for deliveries to the EU.",
        "days": 12,
        "arrives_by": null,
        "duration_terms": "Delivery time is dependent on destination country; the given transit time estimation is based on the average of countries in the zone you are sending into.",
        "trackable": true,
        "insurance": true,
        "insurance_amount_local": "685.28",
        "insurance_currency_local": "USD",
        "insurance_amount": "500.00",
        "insurance_currency": "EUR",
        "delivery_attempts": null,
        "tier_name": "Paket 5kg",
        "outbound_endpoint": "carrier office",
        "inbound_endpoint": "door",
        "messages": [
                {
                    "code": "",
                    "text": "Carrier Information: User Id and Shipper Number combination is qualified to receive the negotiated rates shown."
                }
            ]
	};
	var str = JSON.stringify(obj, undefined, 4);
	$('#pre_response').html('<pre>' + GlobalHelpers.PrettyPrintJSON(str) + '</pre>');
</script>

<h4>Response Schema</h4>
	<div class="table-responsive">
		<table class="table table-bordered table-hover table-col-1-str">
			<thead class="italic">
				<tr>
					<td width="20%">Key</td>
					<td width="20%">Format / Values</td>
					<td>Description</td>
				</tr>
			</thead><tbody>
				<tr>
					<td>object_state</td>
					<td>"VALID"</td>
					<td>Each Rate of a valid Shipment object is valid.</td>
				</tr>
				<tr>
					<td>object_purpose</td>
					<td>"QUOTE"<br>"PURCHASE"</td>
					<td>Indicates whether the corresponding Shipment object can be used to purchase Labels or only to obtain quote Rates.</td>
				</tr>
				<tr>
					<td>object_created</td>
					<td><em>datetime</em></td>
					<td>Date and time of Rate creation.</td>
				</tr>
				<tr>
					<td>object_updated</td>
					<td><em>datetime</em></td>
					<td>Date and time of last Rate update.</td>
				</tr>
				<tr>
					<td>object_id</td>
					<td><em>string</em></td>
					<td>Unique identifier of the given Rate object.</td>
				</tr>
				<tr>
					<td>object_owner</td>
					<td><em>string</em></td>
					<td>Username of the user who created the rate object.</td>
				</tr>
				<tr>
					<td>available_shippo</td>
					<td><em>bool</em></td>
					<td>Indicates whether the carrier allows Shippo to generate Labels for this Rate. If false, a label for this Rate can not be purchased online. You may, however, purchase a label at the corresponding carrier directly. When purchasing a Label for a non-available Rate through the Shippo API, you will be returned a PDF document containing detailed instructions on how and where to obtain the final label.</td>
				</tr>
				<tr>
					<td>attributes</td>
					<td><em>array</em></td>
					<td>Coming soon. This array will contain specific attributes of this Rate in context of the entire shipment. Attributes are assigned to the cheapest or fastest Rate(s) or to the Rate(s) with the overall best value.</td>
				</tr>
				<tr>
					<td>amount_local</td>
					<td><em>decimal</em></td>
					<td>Final Rate price, expressed in the currency used in the recipient's country.</td>
				</tr>
				<tr>
					<td>currency_local</td>
					<td>ISO 4217 Currency Code</td>
					<td>Currency used in the recipient's country, refers to "amount_local". The <a href="http://www.xe.com/iso4217.php">official ISO 4217</a> currency codes are used, e.g. "USD" or "EUR".</td>
				</tr>
				<tr>
					<td>amount</td>
					<td><em>decimal</em></td>
					<td>Final Rate price, expressed in the currency used in the sender's country.</td>
				</tr>
				<tr>
					<td>currency</td>
					<td>ISO 4217 Currency Code (<em>string</em>)</td>
					<td>Currency used in the sender's country, refers to "amount". The <a href="http://www.xe.com/iso4217.php">official ISO 4217</a> currency codes are used, e.g. "USD" or "EUR".</td>
				</tr>
				<tr>
					<td>provider</td>
					<td><em>string</em></td>
					<td>Carrier offering the rate, e.g., "FedEx" or "Deutsche Post DHL".</td>
				</tr>
				<tr>
					<td>provider_image_75</td>
					<td><em>string</em></td>
					<td>URL to the provider logo with max. dimensions of 75*75px. Please refer to the provider's Logo Usage Guidelines before using the logo.</td>
				</tr>
				<tr>
					<td>provider_image_200</td>
					<td><em>string</em></td>
					<td>URL to the provider logo with max. dimensions of 200*200px. Please refer to the provider's Logo Usage Guidelines before using the logo.</td>
				</tr>
				<tr>
					<td>servicelevel_name</td>
					<td><em>string</em></td>
					<td>Name of the Rate's servicelevel, e.g. "International Priority" or "Standard Post". A servicelevel commonly defines the transit time of a Shipment (e.g., Express vs. Standard), along with other properties. These names vary depending on the provider.</td>
				</tr>
				<tr>
					<td>servicelevel_terms</td>
					<td><em>string</em></td>
					<td>Further clarification of the service. This is sent for your convenience and allows you to obtain more details about the service.</td>
				</tr>
				<tr>
					<td>days</td>
					<td><em>integer</em></td>
					<td>Estimated transit time (duration) in days of the Parcel at the given servicelevel. Please note that this is not binding, but only an average value as given by the provider. Shippo is not able to guarantee any transit times.</td>
				</tr>
				<tr>
					<td>duration_terms</td>
					<td><em>string</em></td>
					<td>Further clarification of the transit times. Often, this includes notes that the transit time as given in "days" is only an average, not a guaranteed time. The duration terms may also include details, such as more specific transit times from/to certain regions or countries.</td>
				</tr>
				<tr>
					<td>trackable</td>
					<td><em>bool</em></td>
					<td>Whether the parcel is traceable or not. Please note that this also is an approximation: some providers generally offer tracking, but exclude certain destination country from that service. In that case, this value will still be true.</td>
				</tr>
				<tr>
					<td>insurance</td>
					<td><em>bool</em></td>
					<td>Indicates whether the Rate contains a parcel insurance.</td>
				</tr>
				<tr>
					<td>insurance_amount_local</td>
					<td><em>float</em></td>
					<td>The parcel insurance amount (i.e. the maximum value that is covered by the insurance in case of damage or loss) expressed in the recipient's country's currency.</td>
				</tr>
				<tr>
					<td>insurance_currency_local</td>
					<td>ISO 4217 Currency Code (<em>string</em>)</td>
					<td>The parcel insurance currency of the recipient's country (ISO 3 currency code).</td>
				</tr>
				<tr>
					<td>insurance_amount</td>
					<td><em>float</em></td>
					<td>The parcel insurance amount (i.e. the maximum value that is covered by the insurance in case of damage or loss) expressed in the sender's country's currency.</td>
				</tr>
				<tr>
					<td>insurance_currency</td>
					<td>ISO 4217 Currency Code (<em>string</em>)</td>
					<td>The parcel insurance currency of the sender's country (ISO 3 currency code).</td>
				</tr>
				<tr>
					<td>delivery_attempts</td>
					<td><em>integer</em></td>
					<td>The number of times the provider will attempt to deliver if the recipient is not at home. For most carriers, this is between 1-3 attempts. This information is not available for all providers and servicelevels.</td>
				</tr>
				<tr>
					<td>tier_name</td>
					<td><em>string</em></td>
					<td>Each provider classifies parcels in their own tier structure, based on dimensions and weight. Shippo returns this string for your reference.</td>
				</tr>
				<tr>
					<td>outbound_endpoint</td>
					<td>"door"<br>"door residential"<br>"door commercial"<br>"carrier office"</td>
					<td>The pickup service (from the sender) of the carrier. Door includes pickup at any sender's address, door commercial / residential refers to pickups only at commercial or residential addresses, as defined by the carrier. Carrier office service requires the sender to drop off the parcel at the closest carrier location (e.g. local offices, drop off stations, or similar).</td>
				</tr>
				<tr>
					<td>inbound_endpoint</td>
					<td>"door"<br>"door residential"<br>"door commercial"<br>"carrier office"</td>
					<td>The delivery service (inbound, to the recipient) of the carrier. Door includes delivery to any recipients's address, door commercial / residential refers to delivery only to commercial or residential addresses, as defined by the carrier. Carrier office service requires the recipient to pick up the parcel at the closest carrier location (e.g. local offices, drop off stations, or similar).</td>
				</tr>
				<tr>
					<td>messages</td>
					<td><em>array</em></td>
					<td>An array containing elements of the following schema:<br>"code" (<em>string</em>): an identifier for the corresponding message (not always available")<br>"message" (<em>string</em>): a publishable message containing further information regarding the Rate.</td>
				</tr>
			</tbody>
		</table>
	</div>
{% endblock %}