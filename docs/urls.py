from django.conf.urls import patterns, url
from docs import views

urlpatterns = patterns('',
    url(r'^$', views.APIDocsOverview, name='overview'),
    url(r'^addresses/$', views.APIDocsAddresses, name='addresses'),
    url(r'^parcels/$', views.APIDocsParcels, name='parcels'),
    url(r'^shipments/$', views.APIDocsShipments, name='shipments'),
    url(r'^rates/$', views.APIDocsRates, name='rates'),
    url(r'^transactions/$', views.APIDocsTransactions, name='transactions'),
    url(r'^billing/$', views.APIDocsBilling, name='billing'),
    url(r'^testing/$', views.APIDocsTesting, name='testing'),
)