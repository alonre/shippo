import xml.etree.ElementTree as ET

from django.conf import settings

from api.models import Currency, Country, Transaction

from base import UPSBaseService
from .. helpers.s3upload import S3Delivery
import elements

class UPSLabelService(UPSBaseService):
    def __init__(self, config, *args, **kwargs):
        super(UPSLabelService, self).__init__(config, *args, **kwargs)

    def get_label(self, transaction):
        if transaction.rate.is_purchasable(transaction, self.config.adapter) and self.config.credentials_valid(transaction=transaction):
            shipment_request = self.generate_confirm_request(transaction.rate)
            transaction.object_api_message = ET.tostring(shipment_request)
            transaction.set_was_test(self.config.production)
            
            transaction.save(trigger_generation=False)

            response_confirm = self.send_request(request=shipment_request, url_appendix='ShipConfirm', transaction=transaction)

            if response_confirm is None or not len(response_confirm):
                transaction.object_status = transaction.object_status = transaction.OBJECT_STATUS_CHOICE.ERROR
                return None

            accept_request = self.generate_accept_request(response_confirm)
            if accept_request:
                transaction.object_api_message += ET.tostring(accept_request)
            transaction.save(trigger_generation=False)

            if accept_request:
                response_accept = self.send_request(request=accept_request, url_appendix='ShipAccept', transaction=transaction)
            else:
                response_accept = None

            if response_accept is not None and len(response_accept):
                transaction.tracking_number = self.get_tracking_number(response_accept)
                transaction.label_url = self.get_and_upload_label(response_accept, transaction)
                transaction.object_status = transaction.OBJECT_STATUS_CHOICE.SUCCESS
            else:
                transaction.object_status = transaction.OBJECT_STATUS_CHOICE.ERROR

            transaction.save(trigger_generation=False)

    def generate_confirm_request(self, rate):
        rate_record = ET.fromstring(rate.object_api_record)
        english_system = self.use_english_system(rate.shipment.address_from)

        shipment_confirm_request_element = ET.Element('ShipmentConfirmRequest')
        elements.request_shipment_confirm(shipment_confirm_request_element, False)

        shipment_element = ET.SubElement(shipment_confirm_request_element, 'Shipment')
        elements.description(shipment_element, 'Goods or Documents')  # TODO more specific description

        elements.shipper(shipment_element, self.config, rate.shipment.address_from)
        elements.ship_to(shipment_element, self.config, rate.shipment.address_to)
        elements.ship_from(shipment_element, self.config, rate.shipment.address_from)
        package_element = elements.package(shipment_element, self.config, rate.shipment.parcel, english_system, '02')

        service_code = rate_record.find('./Service/Code').text
        elements.service(shipment_element, service_code)
        elements.payment_info(shipment_element, self.config)

        # ItemizedPaymentInformation could be used to shift billing of duties to s/o else but receiver. (p. 109 ff)
        elements.rate_information(shipment_element, True)
        elements.shipment_service_options(shipment_element, False)
        elements.package_service_options(package_element, rate.shipment.parcel, self.config)

        elements.label_specification(shipment_confirm_request_element)

        return shipment_confirm_request_element

    def generate_accept_request(self, response):
        try:
            shipment_accept_request_element = ET.Element('ShipmentAcceptRequest')
            elements.request_shipment_accept(shipment_accept_request_element, response.find('./ShipmentDigest').text)
        except:
            shipment_accept_request_element = None

        return shipment_accept_request_element

    def get_tracking_number(self, response):
        return response.find('./ShipmentResults/PackageResults/TrackingNumber').text

    def get_and_upload_label(self, reponse, transaction):
        base64_label = reponse.find('./ShipmentResults/PackageResults/LabelImage/GraphicImage').text
        extension = '.gif'
        url = S3Delivery().save_and_upload(base64content = base64_label, filename = str(transaction.object_id) + extension, contenttype = 'image/gif', expiration_sec = self.config.label_lifetime)
        return url

    def __unicode__(self):
        return 'UPSLabelService'