from decimal import *
from sys import stderr

import xml.etree.ElementTree as ET
from django.conf import settings

from adapter.models import Endpoint, Provider, Adapter, Zone, CountryZone, Servicelevel, Tier, Duration, Price
from api.models import Currency, Country, Rate, Location, Unit

from base import UPSBaseService
import elements

class UPSLocationService(UPSBaseService):
    def __init__(self, config, *args, **kwargs):
        super(UPSLocationService, self).__init__(config, *args, **kwargs)

    def get_locations(self, address):
        if self.config.credentials_valid():
            locator_request = self.generate_locator_request(address)
            response = self.send_request(request=locator_request, url_appendix='Locator')

            if response is not None and len(response):
                self.messages = self.get_messages(response)
                locations = self.generate_locations(response, address)
            else:
                return None
    
            if locations is not None and len(locations) > 0:
                return locations
            else:
                return None

    def generate_locator_request(self, address):
        english_system = self.use_english_system(address)
        
        locator_request_element = ET.Element('LocatorRequest')
        elements.request_locator(locator_request_element, '1')
        
        elements.origin_address(locator_request_element, address)
        elements.translate(locator_request_element, english_system)
        elements.location_search_criteria(locator_request_element)

        return locator_request_element

    def generate_locations(self, response, address):
        locations = []

        for location_raw in response.findall('./SearchResults/DropLocation'):
            location = Location(provider = self.config.provider, address=address)
            location.object_api_record = ET.tostring(location_raw)
            location.object_owner = self.config.user
            location.adapter = self.config.adapter

            location.company = location_raw.find('./AddressKeyFormat/ConsigneeName').text
            location.street1 = location_raw.find('./AddressKeyFormat/AddressLine').text
            if ET.iselement(location_raw.find('./AddressKeyFormat/AddressLine2')):
                location.street2 = location_raw.find('./AddressKeyFormat/AddressLine2').text
                
            location.city = location_raw.find('./AddressKeyFormat/PoliticalDivision2').text
            if ET.iselement(location_raw.find('./AddressKeyFormat/PoliticalDivision1')):
                location.state = location_raw.find('./AddressKeyFormat/PoliticalDivision1').text
            location.zip = location_raw.find('./AddressKeyFormat/PostcodePrimaryLow').text
            location.country = Country.objects.get(iso2=location_raw.find('./AddressKeyFormat/CountryCode').text)
            if ET.iselement(location_raw.find('./EMailAddress')):
                location.email = location.state = location_raw.find('./EMailAddress').text
            if ET.iselement(location_raw.find('./PhoneNumber')):
                location.phone = location_raw.find('./PhoneNumber').text

            location.distance_amount = Decimal(location_raw.find('./Distance/Value').text)
            location.distance_unit = Unit.objects.get(abbreviation=location_raw.find('./Distance/UnitOfMeasurement/Code').text.lower())
            
            location.lat = location_raw.find('./Geocode/Latitude').text
            location.lng = location_raw.find('./Geocode/Longitude').text
            location.further_information = 'Latest Dropoff Time: ' + location_raw.find('./LatestGroundDropOffTime').text + '<br/>'
            if ET.iselement(location_raw.find('./StandardHoursOfOperation')):
                location.further_information += 'Hours of operation: ' + location_raw.find('./StandardHoursOfOperation').text + '<br/>'
            if ET.iselement(location_raw.find('./Comments')):
                location.further_information += 'Comments: ' + location_raw.find('./Comments').text

            location.save()
            locations.append(location)

        return locations

    def __unicode__(self):
        return 'UPSLocationService'