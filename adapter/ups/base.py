import requests
import copy
import xml.etree.ElementTree as ET

from adapter.models import Zone, CountryZone
from api.models import Message
import elements, sys

class UPSBaseService(object):
    def __init__(self, config, *args, **kwargs):
        self.config = config

    def send_request(self, request, url_appendix, transaction=None):
        access_request = elements.access_request(config=self.config)
        send_data = self.config.xml_declaration + ET.tostring(access_request) + self.config.xml_declaration + ET.tostring(request)
        post = requests.post(self.config.base_url + url_appendix, data=(send_data))

        response = ET.fromstring(post.text.encode('utf8'))

        if transaction:
            transaction.object_api_response += post.text
            transaction.save(trigger_generation=False)
            self.messages = self.get_messages(response)
            self.save_messages_for_object(messages=self.messages, transaction=transaction)

        return self.check_for_error(response)

    def check_for_error(self, response):
        if response.find('.//ResponseStatusCode').text == '1':
            return response
        else:
            return None # QUESTIONABLE
            #return response

    def get_messages(self, response):
        messages = []

        # errors = response.iter('Error')
        for error in response.iter('Error'):
            message = Message(source=self.config.adapter)

            if error.find('ErrorSeverity') is not None:
                message.text = error.find('ErrorSeverity').text
                message.text += ': '

            if error.find('ErrorCode') is not None:
                message.code = error.find('ErrorCode').text

            if error.find('ErrorDescription') is not None:
                message.text += error.find('ErrorDescription').text

            already_exists = False
            for existing_message in messages:
                if existing_message.text == message.text:
                    already_exists = True
            if not already_exists:
                messages.append(message)
            messages.append(message)

        for warning in response.iter('RatedShipmentWarning'):
            import sys
            print >>sys.stderr, ET.tostring(warning)
            message = Message(source=self.config.adapter)
            message.text = 'RatedShipmentWarning: '
            message.text += warning.text

            already_exists = False
            for existing_message in messages:
                if existing_message.text == message.text:
                    already_exists = True
            if not already_exists:
                messages.append(message)

        return messages

    def use_english_system(self, address):
        english_system_zone = Zone.objects.get(name='UPS Imperial Units')
        english_system_country = CountryZone.objects.filter(zone=english_system_zone, outbound_country=address.country)

        return len(english_system_country) > 0

    def save_messages_for_object(self, messages, rate=None, transaction=None):
        for message in messages:
            message = copy.copy(message)

            if rate:
                message.rate = rate
            if transaction:
                message.transaction = transaction

            message.save()

    def __unicode__(self):
        return 'UPSBaseService'