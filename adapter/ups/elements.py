import xml.etree.ElementTree as ET
from api.models import Unit

def access_request(config):
    access_request = ET.Element('AccessRequest')
    access_request.set('xml:lang', 'en-US')

    access_license_no = ET.SubElement(access_request, 'AccessLicenseNumber')
    access_license_no.text = config.access_license_number
    user_id = ET.SubElement(access_request, 'UserId')
    user_id.text = config.user_id
    password = ET.SubElement(access_request, 'Password')
    password.text = config.password

    return access_request

def request_rating_and_service(rating_service_selection_request_element):
    # documentation page 36ff
    request = ET.SubElement(rating_service_selection_request_element, 'Request')
    transaction_reference = ET.SubElement(request, 'TransactionReference')
    customer_context = ET.SubElement(transaction_reference, 'CustomerContext')
    customer_context.text = 'Rating and Service'
    xpci_version = ET.SubElement(transaction_reference, 'XpciVersion')
    xpci_version.text = '1.0'
    request_action = ET.SubElement(request, 'RequestAction')
    request_action.text = 'Rate'
    request_option = ET.SubElement(request, 'RequestOption')
    request_option.text = 'Shop'

def request_shipment_confirm(shipment_confirm_request_element, validation=False):
    request = ET.SubElement(shipment_confirm_request_element, 'Request')
    request_action = ET.SubElement(request, 'RequestAction')
    request_action.text = 'ShipConfirm'
    request_option = ET.SubElement(request, 'RequestOption')
    # TODO: Therefore, it is the responsibility of the Shipping API User to ensure the address entered is correct to avoid an address correction fee.
    if validation:
        request_option.text = 'nonvalidate' # TODO
    else:
        request_option.text = 'nonvalidate'

def request_shipment_accept(shipment_accept_request_element, shipment_digest_text):
    request = ET.SubElement(shipment_accept_request_element, 'Request')
    request_action = ET.SubElement(request, 'RequestAction')
    request_action.text = 'ShipAccept'
    shipment_digest = ET.SubElement(shipment_accept_request_element, 'ShipmentDigest')
    shipment_digest.text = shipment_digest_text
    
def request_locator(locator_request_element, request_option):
    request = ET.SubElement(locator_request_element, 'Request')
    request_action = ET.SubElement(request, 'RequestAction')
    request_action.text = 'Locator'
    request_option_element = ET.SubElement(request, 'RequestOption')
    request_option_element.text = request_option

def pickup_type(rating_service_selection_request_element):
    pickup_type = ET.SubElement(rating_service_selection_request_element, 'PickupType')
    code_pickup_type = ET.SubElement(pickup_type, 'Code')
    code_pickup_type.text = '01'

def description(shipment_element, description):
    description_element = ET.SubElement(shipment_element, 'Description')
    description_element.text = description

def address(et_element, config, address, apiuser, attention_name=False):
    name = ET.SubElement(et_element, 'Name')
    if address.name:
        name.text = address.name
    else:
        name.text = apiuser.user.first_name + ' ' + apiuser.user.last_name

    if attention_name:
        attention_name = ET.SubElement(et_element, 'AttentionName')
        attention_name.text = address.name

    company_name = ET.SubElement(et_element, 'CompanyName')
    if address.company:
        company_name.text = address.company
    else:
        company_name.text = address.name

    email = ET.SubElement(et_element, 'EMailAddress')
    email.text = address.email

    if address.phone:
        phone = ET.SubElement(et_element, 'PhoneNumber')
        if address.country == config.country_us:
            phone.text = address.get_cleaned_phone(max_digits=10, us_national=True)
        else:
            phone.text = address.get_cleaned_phone(max_digits=15)

    address_element = ET.SubElement(et_element, 'Address')

    address_line_1 = ET.SubElement(address_element, 'AddressLine1')
    address_line_1.text = address.street_no + ' ' + address.street1
    address_line_2 = ET.SubElement(address_element, 'AddressLine2')
    address_line_2.text = address.street2

    city = ET.SubElement(address_element, 'City')
    city.text = address.city
    state_prov = ET.SubElement(address_element, 'StateProvinceCode')
    state_prov.text = address.state
    postal_code = ET.SubElement(address_element, 'PostalCode')
    postal_code.text = address.zip
    country_code = ET.SubElement(address_element, 'CountryCode')
    if address.country:
        country_code.text = address.country.iso2

    # TODO: residential?

def shipper(shipment_element, config, address_from):
    shipper = ET.SubElement(shipment_element, 'Shipper')
    shipper_no = ET.SubElement(shipper, 'ShipperNumber')
    shipper_no.text = config.account.lstrip('0')
    # the shipper address is NOT the account owner's address, but the "responsible" party of the shipment; in 99% of cases = ShipFrom
    # return shipments usually go to this address
    address(shipper, config, config.apiuser.address, config.apiuser, True)

def ship_to(shipment_element, config, address_to):
    ship_to = ET.SubElement(shipment_element, 'ShipTo')
    address(ship_to, config, address_to, config.apiuser, True)

def ship_from(shipment_element, config, address_from):
    ship_from = ET.SubElement(shipment_element, 'ShipFrom')
    address(ship_from, config, address_from, config.apiuser, True)

def package(shipment_element, config, parcel, english_system=False, packaging_type='02'):
    package = ET.SubElement(shipment_element, 'Package')
    packaging_type_element = ET.SubElement(package, 'PackagingType')
    code_packaging_type = ET.SubElement(packaging_type_element, 'Code')
    code_packaging_type.text = packaging_type

    dimensions = ET.SubElement(package, 'Dimensions')
    unit_of_measurement_dim = ET.SubElement(dimensions, 'UnitOfMeasurement')
    unit_code_dim = ET.SubElement(unit_of_measurement_dim, 'Code')

    length = ET.SubElement(dimensions, 'Length')
    width = ET.SubElement(dimensions, 'Width')
    height = ET.SubElement(dimensions, 'Height')

    package_weight = ET.SubElement(package, 'PackageWeight')
    unit_of_measurement_wgh = ET.SubElement(package_weight, 'UnitOfMeasurement')
    unit_code_wgh = ET.SubElement(unit_of_measurement_wgh, 'Code')
    weight = ET.SubElement(package_weight, 'Weight')

    # TODO: if girth larger than 130 inch smaller than 165 inch. question: then we seem to need the dimensions above not anymore?!
    #large_package = ET.SubElement(package, 'LargePackageIndicator')

    if english_system:
        unit_code_dim.text = config.unit_in.abbreviation.upper()
        length.text = str(parcel.get_length(unit=config.unit_in, decimal_places=0))
        width.text = str(parcel.get_width(unit=config.unit_in, decimal_places=0))
        height.text = str(parcel.get_height(unit=config.unit_in, decimal_places=0))
        unit_code_wgh.text = (config.unit_lb.abbreviation + 's').upper() # UPS uses a non-standard plural form
        weight.text = str(parcel.get_weight(unit=config.unit_lb, decimal_places=1))
    else:
        unit_code_dim.text = config.unit_cm.abbreviation.upper()
        length.text = str(parcel.get_length(unit=config.unit_cm, decimal_places=0))
        width.text = str(parcel.get_width(unit=config.unit_cm, decimal_places=0))
        height.text = str(parcel.get_height(unit=config.unit_cm, decimal_places=0))
        unit_code_wgh.text = (config.unit_kg.abbreviation + 's').upper() # UPS uses a non-standard plural form
        weight.text = str(parcel.get_weight(unit=config.unit_kg, decimal_places=1))
    return package

def rate_information(shipment_element, negotiated_rates=True):
    rate_information = ET.SubElement(shipment_element, 'RateInformation')
    if negotiated_rates:
        negotiated_rates = ET.SubElement(rate_information, 'NegotiatedRatesIndicator')

def service(shipment_element, service_code):
    service = ET.SubElement(shipment_element, 'Service')
    code = ET.SubElement(service, 'Code')
    code.text = service_code

def shipment_service_options(shipment_element, saturday_delivery=False):
    shipment_service_options = ET.SubElement(shipment_element, 'ShipmentServiceOptions')
    if saturday_delivery:
        saturday_deliver = ET.SubElement(shipment_service_options, 'SaturdayDelivery')

def package_service_options(package_element, parcel, config):
    if config.apiuser.address.country:
        parcel_value = parcel.get_parcel_value(currency = config.apiuser.address.country.currency)
    else:
        parcel_value = parcel.get_parcel_value(currency = config.currency_usd)
    if parcel_value > 0:
        package_service_options = ET.SubElement(package_element, 'PackageServiceOptions')
        insured_value = ET.SubElement(package_service_options, 'InsuredValue')
        currency_code = ET.SubElement(insured_value, 'CurrencyCode')
        if config.apiuser.address.country:
            currency_code.text = config.apiuser.address.country.currency.iso  # ...in the SHIPPER's country
        else:
            currency_code.text = config.currency_usd.iso
        monetary_value = ET.SubElement(insured_value, 'MonetaryValue')
        monetary_value.text = parcel_value

def label_specification(shipment_confirm_request_element):
    label_specification = ET.SubElement(shipment_confirm_request_element, 'LabelSpecification')
    label_print_method = ET.SubElement(label_specification, 'LabelPrintMethod')
    label_print_code = ET.SubElement(label_print_method, 'Code')
    label_print_code.text = 'GIF'
    user_agent = ET.SubElement(label_specification, 'HTTPUserAgent')
    user_agent.text = 'Mozilla/4.5'
    label_image_format = ET.SubElement(label_specification, 'LabelImageFormat')
    label_image_format_code = ET.SubElement(label_image_format, 'Code')
    label_image_format_code.text = 'GIF'

def payment_info(shipment_element, config):
    payment = ET.SubElement(shipment_element, 'PaymentInformation')
    prepaid = ET.SubElement(payment, 'Prepaid')
    bill_shipper = ET.SubElement(prepaid, 'BillShipper')
    account_no = ET.SubElement(bill_shipper, 'AccountNumber')
    account_no.text = config.account.lstrip('0')
    
def origin_address(locator_request_element, address):
    origin_address_element = ET.SubElement(locator_request_element, 'OriginAddress')
    address_key_format = ET.SubElement(origin_address_element, 'AddressKeyFormat')
    address_line = ET.SubElement(address_key_format, 'AddressLine')
    address_line.text = (address.street_no + ' ' + address.street1)[:100]
    if address.street2:
        address_line_2 = ET.SubElement(address_key_format, 'AddressLine2')
        address_line_2.text = address.street2[:64]
        
    political_division_2 = ET.SubElement(address_key_format, 'PoliticalDivision2')
    political_division_2.text = address.city[:50]
    political_division_1 = ET.SubElement(address_key_format, 'PoliticalDivision1')
    political_division_1.text = address.state[:50]
    
    postcode_primary_low = ET.SubElement(address_key_format, 'PostcodePrimaryLow')
    postcode_primary_low.text = address.zip[:10]
    country_code = ET.SubElement(address_key_format, 'CountryCode')
    country_code.text = address.country.iso2
    
    maximum_list_size = ET.SubElement(origin_address_element, 'MaximumListSize')
    maximum_list_size.text = '25'
    
def translate(locator_request_element, english_system=False):
    translate_element = ET.SubElement(locator_request_element, 'Translate')
    language_code = ET.SubElement(translate_element, 'LanguageCode')
    language_code.text = 'eng'
    
    unit_of_measurement = ET.SubElement(locator_request_element, 'UnitOfMeasurement')
    code = ET.SubElement(unit_of_measurement, 'Code')
    if english_system:
        code.text = 'MI'
    else:
        code.text = 'KM'
        
def location_search_criteria(locator_request_element):
    location_element = ET.SubElement(locator_request_element, 'LocationSearchCriteria')
    search_option = ET.SubElement(location_element, 'SearchOption')
    option_type = ET.SubElement(search_option, 'OptionType')
    option_type_code = ET.SubElement(option_type, 'Code')
    option_type_code.text = '05' # + 02 # 05
    option_code_01 = ET.SubElement(search_option, 'OptionCode')
    # using Servicelevel codes, not location codes, see doc p. 57
    option_code_01_code = ET.SubElement(option_code_01, 'Code')
    option_code_01_code.text = '01'
    option_code_02 = ET.SubElement(search_option, 'OptionCode')
    option_code_02_code = ET.SubElement(option_code_02, 'Code')
    option_code_02_code.text = '02'
    option_code_03 = ET.SubElement(search_option, 'OptionCode')
    option_code_03_code = ET.SubElement(option_code_03, 'Code')
    option_code_03_code.text = '03'
    option_code_04 = ET.SubElement(search_option, 'OptionCode')
    option_code_04_code = ET.SubElement(option_code_04, 'Code')
    option_code_04_code.text = '04'
    option_code_05 = ET.SubElement(search_option, 'OptionCode')
    option_code_05_code = ET.SubElement(option_code_05, 'Code')
    option_code_05_code.text = '05'
    option_code_06 = ET.SubElement(search_option, 'OptionCode')
    option_code_06_code = ET.SubElement(option_code_06, 'Code')
    option_code_06_code.text = '06'
    option_code_07 = ET.SubElement(search_option, 'OptionCode')
    option_code_07_code = ET.SubElement(option_code_07, 'Code')
    option_code_07_code.text = '07'
    
    maximum_list_size = ET.SubElement(location_element, 'MaximumListSize')
    maximum_list_size.text = '25'