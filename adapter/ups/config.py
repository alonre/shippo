# -*- coding: utf-8 -*-
from django.conf import settings

from adapter.models import Provider, Adapter
from api.models import Currency, Country, Unit

class UPSConfig:
    def __init__(self, user):
        self.provider = Provider.objects.get(name='UPS')
        self.adapter = Adapter.objects.get(name='UPS')
        self.user = user
        self.apiuser = user.apiuser
        self.settings = settings.ADAPTERS['UPS']
        self.label_lifetime = settings.LABEL_LIFETIME
        self.xml_declaration = u'<?xml version="1.0" encoding="UTF-8"?>'
        
        # Setup units & currency
        self.country_us = Country.objects.get(iso2='US')
        self.currency_usd = Currency.objects.get(iso='USD')
        self.unit_kg = Unit.objects.get(abbreviation='kg')
        self.unit_cm = Unit.objects.get(abbreviation='cm')
        self.unit_lb = Unit.objects.get(abbreviation='lb')
        self.unit_in = Unit.objects.get(abbreviation='in')
        
        self.account = self.apiuser.ups_account
        self.user_id = self.apiuser.ups_user_id
        self.password = self.apiuser.ups_password

        self.access_license_number = self.settings['ACCESS_LICENSE_NUMBER']
        
        if self.apiuser.ups_production:
            self.base_url = self.settings['BASE_URL_PRODUCTION']
            self.production = True
        else:
            self.base_url = self.settings['BASE_URL_TEST']
            self.production = False
            
    def credentials_valid(self, transaction = None, *args, **kwargs):
        if self.account and self.user_id and self.password:
            return True
        else:
            if transaction:
                transaction.credentials_missing()
            return False

    def __unicode__(self):
        return 'UPSConfig'