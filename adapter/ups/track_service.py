from decimal import *
from sys import stderr
import datetime
        
import xml.etree.ElementTree as ET
from django.conf import settings

from adapter.models import Endpoint, Provider, Adapter, Zone, CountryZone, Servicelevel, Tier, Duration, Price
from api.models import Currency, Country, Rate, Location, Unit, Track

from base import UPSBaseService
import elements

class UPSTrackService(UPSBaseService):
    def __init__(self, config, *args, **kwargs):
        super(UPSTrackService, self).__init__(config, *args, **kwargs)

    def get_status(self, transaction):
        if self.config.credentials_valid():
            track_request = self.generate_track_request(transaction)
            response = self.send_request(request=track_request, url_appendix='Track')

            if response is not None and len(response):
                track = self.generate_track(response, transaction)
            else:
                return None
    
            if track is not None:
                return track
            else:
                return None

    def generate_track_request(self, transaction):
        track_request_element = ET.Element('TrackRequest')
        request_element = ET.SubElement(track_request_element, 'Request')
        request_action = ET.SubElement(request_element, 'RequestAction')
        request_action.text = 'Track'
        request_option = ET.SubElement(request_element, 'RequestOption')
        request_option.text = '0' # Last Activity only
        
        # use destination information to make sure only one result is returned (else "CandidateBookmark" elements may be returned, which would need a separate transaction to receive the actual tracking information."
        destination_postal_code = ET.SubElement(request_element, 'DestinationPostalCode')
        destination_postal_code.text = transaction.shipment.address_to.zip
        destination_country_code = ET.SubElement(request_element, 'DestinationCountryCode')
        destination_country_code.text = transaction.shipment.address_to.country.iso2
        
        tracking_number = ET.SubElement(track_request_element, 'TrackingNumber')
        tracking_number.text = transaction.tracking_number

        return track_request_element
    
    def element_exists(self, element, path):
        if ET.iselement(element.find(path)):
            return True
        else:
            return False
   
    def get_element_content(self, element, path):
        if self.element_exists(element, path):
            element_list = element.findall(path)
            return element.findall(path)[len(element_list)-1].text
        else:
            return ''
               
    def generate_track(self, response, transaction):
        track = Track(transaction=transaction, provider=self.config.provider, adapter=self.config.adapter, object_owner=self.config.user)
        
        # pickup_date = when was the shipment picked up?
        if self.element_exists(response, './Shipment/PickupDate'):
            track.pickup_date = self.parse_datetimestamp(response, './Shipment/PickupDate', None)
        
        # at delivery, who signed for the shipment?
        if self.element_exists(response, './Shipment/SignedForByName'):
            track.signed_for = self.get_element_content(response, './Shipment/SignedForByName')

        # what is the status of the shipment overall
        if self.element_exists(response, './Shipment/Activity/Description'):
            track.status = self.get_element_content(response, './Shipment/Activity/Description')
        if self.element_exists(response, './Shipment/CurrentStatus/Description'):
            track.status = self.get_element_content(response, './Shipment/CurrentStatus/Description')
        
        # what is the delivery date, and what is the delivery status? 
        if self.element_exists(response, './Shipment/DeliveryDetails/DeliveryDate/Date'):
            track.delivery_date = self.parse_datetimestamp(response, './Shipment/DeliveryDetails/DeliveryDate/Date', './Shipment/DeliveryDetails/DeliveryDate/Time')
            track.delivery_date_status = Track.DELIVERY_DATE_CHOICE.DELIVERED
        elif self.element_exists(response, './Shipment/Package/RescheduledDeliveryDate'):
            track.delivery_date = self.parse_datetimestamp(response, './Shipment/Package/RescheduledDeliveryDate', './Shipment/Package/RescheduledDeliveryTime')            
            track.delivery_date_status = Track.DELIVERY_DATE_CHOICE.RESCHEDULED
        elif self.element_exists(response, './Shipment/EstimatedDeliveryDetails/Date'):
            track.delivery_date = self.parse_datetimestamp(response, './Shipment/EstimatedDeliveryDetails/Date', './Shipment/EstimatedDeliveryDetails/Time')
            track.delivery_date_status = Track.DELIVERY_DATE_CHOICE.ESTIMATED
        elif self.element_exists(response, './Shipment/ScheduledDeliveryDate'):
            track.delivery_date = self.parse_datetimestamp(response, './Shipment/ScheduledDeliveryDate', None)
            track.delivery_date_status = Track.DELIVERY_DATE_CHOICE.SCHEDULED
        
        if self.element_exists(response, './Shipment/Package/Message/Description'):
            track.enroute = self.get_element_content(response, './Shipment/Package/Message/Description')
        
        # Parse the activity always in the end, to make sure that specific information overwrites general information
        track = self.parse_activity(response, transaction.tracking_number, track)

        track.save()
        return track
    
    def parse_activity(self, response, tracking_no, track):
        activity = None
        shipment = response.findall('./Shipment')
        
        # first: choose right package
        for package in response.findall('./Shipment/Package'):
            # check whether the package is actually the one that should be tracked
            if package.find('./TrackingNumber').text == tracking_no:
                # we've found the right package, now find the latest activity
                latest_date = datetime.datetime(1970, 1, 1)
                latest_activity = 0
                i = 0
                
                # First, try per package activity
                if self.element_exists(package, './Activity'):
                    activities = package.findall('./Activity')
                # If there was no package activity, maybe there was shipment activity?
                elif self.element_exists(shipment, './Activity'):
                    activities = shipment.findall('./Activity')
                    
                for activity in activities:
                    date = self.parse_datetimestamp(activity, './Date', './Time')
                    if date > latest_date:
                        latest_activity = i
                        latest_date = date
                    i += 1
    
                activity = activities[latest_activity]
        
        # the latest activity is now in activity. Use this activity to fill the related Track object's attributes        
        if activity is not None:
            if self.element_exists(activity, './ActivityLocation/Address/City'):
                track.location_city = self.get_element_content(activity, './ActivityLocation/Address/City')
            if self.element_exists(activity, './ActivityLocation/Address/StateProvinceCode'):
                track.location_state = self.get_element_content(activity, './ActivityLocation/Address/StateProvinceCode')
            if self.element_exists(activity, './ActivityLocation/Address/PostalCode'):
                track.location_zip = self.get_element_content(activity, './ActivityLocation/Address/PostalCode')
            if self.element_exists(activity, './ActivityLocation/Address/CountryCode'):
                track.location_country = Country.objects.get(iso2=self.get_element_content(activity, './ActivityLocation/Address/CountryCode'))
            if self.element_exists(activity, './Status/StatusType/Description'):
                track.status = self.get_element_content(activity, './Status/StatusType/Description')
            if self.element_exists(activity, './ActivityLocation/Description'):
                track.location_details = self.get_element_content(activity, './ActivityLocation/Description')
            if self.element_exists(activity,  './ActivityLocation/SignedForByName'):
                track.signed_for = self.get_element_content(activity,  './ActivityLocation/SignedForByName')
                
            if self.element_exists(activity, './Date'):
                track.status_date = self.parse_datetimestamp(activity, './Date', './Time')
          
            if self.get_element_content(activity,  './Status/StatusType/Code') == 'D': # DELIVERED
                if self.element_exists(activity, './Date'):
                    track.delivery_date = self.parse_datetimestamp(activity, './Date', './Time')
                track.delivery_date_status = Track.DELIVERY_DATE_CHOICE.DELIVERED
                
            if self.get_element_content(activity,  './Status/StatusType/Code') == 'P': # PICKUP SCAN
                if self.element_exists(activity, './Date'):
                    track.pickup_date = self.parse_datetimestamp(activity, './Date', './Time')
            
        return track
    
    def parse_datetimestamp(self, response, date_element, time_element):
        try: # when parsing dates there is so much that can go wrong
            result = datetime
            if date_element and self.element_exists(response, date_element):
                if time_element and self.element_exists(response, time_element):
                    time_formatted = (self.get_element_content(response, time_element)[:2] + ' ' + self.get_element_content(response, time_element)[2:4] + ' ' + self.get_element_content(response, time_element)[-2:])
                    date_formatted = (self.get_element_content(response, date_element)[:4] + ' ' + self.get_element_content(response, date_element)[4:6] + ' ' + self.get_element_content(response, date_element)[-2:])
                    result = datetime.datetime.strptime(time_formatted + ' ' + date_formatted, "%H %M %S %Y %m %d")
                else:
                    date_formatted = (self.get_element_content(response, date_element)[:4] + ' ' + self.get_element_content(response, date_element)[4:6] + ' ' + self.get_element_content(response, date_element)[-2:])
                    result = datetime.datetime.strptime(date_formatted, "%Y %m %d")
    
            return result
        except:
            return None

    def __unicode__(self):
        return 'UPSTrackService'