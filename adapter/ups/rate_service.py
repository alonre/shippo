from decimal import *
from sys import stderr

import xml.etree.ElementTree as ET
from django.conf import settings

from adapter.models import Endpoint, Provider, Adapter, Zone, CountryZone, Servicelevel, Tier, Duration, Price
from api.models import Currency, Country, Rate

from base import UPSBaseService
import elements

class UPSRateService(UPSBaseService):
    def __init__(self, config, *args, **kwargs):
        super(UPSRateService, self).__init__(config, *args, **kwargs)

    def get_rates(self, shipment):
        if self.config.credentials_valid():
            rate_request = self.generate_rate_request(shipment)
            response = self.send_request(request=rate_request, url_appendix='Rate')
    
            if response is not None and len(response):
                self.messages = self.get_messages(response)
                rates = self.generate_rates(response, shipment, self.config.user)
            else:
                return None
    
            if rates is not None and len(rates) > 0:
                return rates
            else:
                return None

    def generate_rate_request(self, shipment):
        english_system = self.use_english_system(shipment.address_from)

        rating_service_selection_request_element = ET.Element('RatingServiceSelectionRequest')
        elements.request_rating_and_service(rating_service_selection_request_element)
        elements.pickup_type(rating_service_selection_request_element)

        shipment_element = ET.SubElement(rating_service_selection_request_element, 'Shipment')

        elements.shipper(shipment_element, self.config, shipment.address_from)
        elements.ship_to(shipment_element, self.config, shipment.address_to)
        elements.ship_from(shipment_element, self.config, shipment.address_from)
        elements.package(shipment_element, self.config, shipment.parcel, english_system, '02')

        # TODO: Insurance!

        elements.rate_information(shipment_element, True)

        return rating_service_selection_request_element

    def generate_rates(self, response, shipment, user):
        rates = []

        for rate_raw in response.findall('./RatedShipment'):

            rate = Rate(shipment = shipment, adapter = self.config.adapter, provider=self.config.provider, object_owner=user)
            rate.object_api_record = ET.tostring(rate_raw)

            # to get the servicelevel, first get the related zones
            outbound_country = shipment.address_from.country
            inbound_country = shipment.address_to.country

            # get all zones of UPS and the specific mapping for this country
            zone = Zone.objects.filter(provider=self.config.provider)
            countryzone = None
            if zone:
                try:
                    countryzone = CountryZone.objects.get(zone=zone, outbound_country=outbound_country, country=inbound_country)
                except Exception as ex:
                    print >>stderr, "UPS ADAPTER: Exception when querying countryzone for zone, outbound_country, inbound_country Exception was: " + str(ex)
                    pass
                
            if not countryzone:
                try:
                    countryzone = CountryZone.objects.filter(zone=zone, outbound_country=outbound_country)[0]
                except Exception as ex:
                    print >>stderr, "UPS ADAPTER: Exception when querying countryzone for zone, outbound_country. Exception was: " + str(ex)
                    pass
            
            if countryzone:
                # now, build the token used to query the servicelevel. Format is OUTBOUNDCOUNTRYISO_INBOUNDCOUNTRYISO_IDPROVIDEDBYUPS.
                # Exception: EU(w/o PL)->abroad: EU_IDPROVIDEDBYUPS
                if countryzone.zone == Zone.objects.get(name='UPS Special Servicelevel EU w/o Poland'):
                    servicelevel_token = 'EU'
                else:
                    servicelevel_token = countryzone.outbound_country.iso2

                servicelevel_token += '_'

                if countryzone.country:
                    servicelevel_token += countryzone.country.iso2
                    servicelevel_token += '_'

            else:
                servicelevel_token = '_'

            servicelevel_token += rate_raw.find('./Service/Code').text

            print >>stderr, "UPS servicelevel token was %s" % servicelevel_token

            # now, look up the servicelevel
            rate.servicelevel = Servicelevel.objects.get(servicelevel_token = servicelevel_token)
            rate.fill_from_servicelevel_object(rate.servicelevel)

            print >>stderr, "UPS found SL %s." % (rate.servicelevel.servicelevel_name)

            # fill days after servicelevel to make sure that empty servicelevel days does not overwrite the actual days.
            guaranteed_days = rate_raw.find('./GuaranteedDaysToDelivery')
            if guaranteed_days.text:
                rate.days = int(guaranteed_days.text)
                print >>stderr, "UPS duration time of %s for SL %s came from GUARANTEED_DAYS tag." % (guaranteed_days.text, rate.servicelevel.servicelevel_name)
            else:
                try:
                    rate.duration = Duration.objects.get(servicelevel=rate.servicelevel)
                    rate.fill_from_duration_object(rate.duration)
                except Exception as ex:
                    print >>stderr, "UPS ADAPTER: Exception when getting Duration for Servicelevel. Exception was: " + str(ex)
                    pass

            rate.currency = Currency.objects.get(iso=rate_raw.find('./TotalCharges/CurrencyCode').text)
            rate.amount = Decimal(rate_raw.find('./TotalCharges/MonetaryValue').text)

            rate.outbound_endpoint = rate.servicelevel.outbound_endpoint
            rate.inbound_endpoint = rate.servicelevel.inbound_endpoint

            rate.save() # make sure we get an ID that can be used to save the messages later on

            self.save_messages_for_object(messages=self.messages, rate=rate)

            rates.append(rate)

        return rates

    def __unicode__(self):
        return 'UPSRateService'