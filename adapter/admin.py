from django.contrib import admin
from cms.models import *

class ProviderAdmin(admin.ModelAdmin):
	list_display = ('name', 'website')

class ZoneAdmin(admin.ModelAdmin):
	list_display = ('name', 'provider', 'type')
	list_editable = ('provider', 'type')
	list_filter = ('provider', 'type')
	save_on_top = True

class CountryZoneAdmin(admin.ModelAdmin):
	list_display = ('zone', 'outbound_country', 'country')
	list_editable = ('outbound_country', 'country')
	list_filter = ('zone__provider',)
	list_per_page = 25
	save_on_top = True

class TierAdmin(admin.ModelAdmin):
	list_display = ('tier_name', 'provider', 'max_length', 'max_width', 'max_height', 'distance_unit', 'max_weight', 'mass_unit')
	list_editable = ('max_length', 'max_width', 'max_height', 'distance_unit', 'max_weight', 'mass_unit')
	list_filter = ('provider',)
	list_per_page = 50
	save_on_top = True

class ServicelevelAdmin(admin.ModelAdmin):
	list_display = ('servicelevel_name', 'servicelevel_token', 'provider', 'insurance_amount', 'trackable', 'available_shippo', 'outbound_endpoint', 'inbound_endpoint')
	list_editable = ('servicelevel_token', 'provider', 'insurance_amount', 'trackable', 'available_shippo', 'outbound_endpoint', 'inbound_endpoint')
	list_filter = ('provider',)
	list_per_page = 50
	save_on_top = True

class DurationAdmin(admin.ModelAdmin):
	list_display = ('servicelevel', 'days', 'outbound_country', 'outbound_zone', 'inbound_country', 'inbound_zone',)
	list_editable = ('days', 'outbound_country', 'outbound_zone', 'inbound_country', 'inbound_zone',)
	list_filter = ('servicelevel__provider',)
	list_per_page = 25
	save_on_top = True

class PriceAdmin(admin.ModelAdmin):
	list_display = ('provider', 'tier', 'servicelevel', 'outbound_country', 'outbound_zone', 'inbound_country', 'inbound_zone', 'amount', 'outbound_endpoint', 'inbound_endpoint')
	list_editable = ('amount',)
	list_filter = ('provider',)
	list_per_page = 25
	save_on_top = True


# Register your models here.
from models import *

admin.site.register(Endpoint)
admin.site.register(Provider, ProviderAdmin)
admin.site.register(Adapter)
admin.site.register(Zone, ZoneAdmin)
admin.site.register(CountryZone, CountryZoneAdmin)
admin.site.register(Servicelevel, ServicelevelAdmin)
admin.site.register(Tier, TierAdmin)
admin.site.register(Duration, DurationAdmin)
admin.site.register(Price, PriceAdmin)

'''
list_display = ('name', 'provider', 'type')
search_fields = ['title', 'body_markdown']
prepopulated_fields = {"slug" : ('title',)}
list_editable = ('provider', 'type')
list_filter = ('name', 'provider', 'type')
list_per_page = 100
filter_horizontal = ('provider', 'type') #or vtcl
save_on_top = True
'''