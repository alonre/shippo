# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Adapter'
        db.create_table(u'adapter_adapter', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('object_state', self.gf('django.db.models.fields.CharField')(default='DISABLED', max_length=20)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'adapter', ['Adapter'])

        # Adding M2M table for field provider_served on 'Adapter'
        m2m_table_name = db.shorten_name(u'adapter_adapter_provider_served')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('adapter', models.ForeignKey(orm[u'adapter.adapter'], null=False)),
            ('provider', models.ForeignKey(orm[u'adapter.provider'], null=False))
        ))
        db.create_unique(m2m_table_name, ['adapter_id', 'provider_id'])

        # Deleting field 'Price.price'
        db.delete_column(u'adapter_price', 'price')

        # Adding field 'Price.amount'
        db.add_column(u'adapter_price', 'amount',
                      self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=8, decimal_places=2),
                      keep_default=False)

        # Deleting field 'Duration.duration'
        db.delete_column(u'adapter_duration', 'duration')

        # Adding field 'Duration.days'
        db.add_column(u'adapter_duration', 'days',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'Adapter'
        db.delete_table(u'adapter_adapter')

        # Removing M2M table for field provider_served on 'Adapter'
        db.delete_table(db.shorten_name(u'adapter_adapter_provider_served'))

        # Adding field 'Price.price'
        db.add_column(u'adapter_price', 'price',
                      self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=8, decimal_places=2),
                      keep_default=False)

        # Deleting field 'Price.amount'
        db.delete_column(u'adapter_price', 'amount')

        # Adding field 'Duration.duration'
        db.add_column(u'adapter_duration', 'duration',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Deleting field 'Duration.days'
        db.delete_column(u'adapter_duration', 'days')


    models = {
        u'adapter.adapter': {
            'Meta': {'object_name': 'Adapter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'object_state': ('django.db.models.fields.CharField', [], {'default': "'DISABLED'", 'max_length': '20'}),
            'provider_served': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['adapter.Provider']", 'symmetrical': 'False'})
        },
        u'adapter.countryzone': {
            'Meta': {'object_name': 'CountryZone'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country_zone_country_relation'", 'null': 'True', 'to': u"orm['api.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'outbound_country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country_zone_outbound_country_relation'", 'null': 'True', 'to': u"orm['api.Country']"}),
            'zone': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country_zone_zone_relation'", 'to': u"orm['adapter.Zone']"})
        },
        u'adapter.duration': {
            'Meta': {'object_name': 'Duration'},
            'days': ('django.db.models.fields.IntegerField', [], {}),
            'duration_details': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inbound_country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'duration_inbound_country_relation'", 'null': 'True', 'to': u"orm['api.Country']"}),
            'inbound_zone': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'duration_inbound_zone_relation'", 'null': 'True', 'to': u"orm['adapter.Zone']"}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'outbound_country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'duration_outbound_country_relation'", 'null': 'True', 'to': u"orm['api.Country']"}),
            'outbound_zone': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'duration_outbound_zone_relation'", 'null': 'True', 'to': u"orm['adapter.Zone']"}),
            'servicelevel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Servicelevel']"})
        },
        u'adapter.endpoint': {
            'Meta': {'object_name': 'Endpoint'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'ANY'", 'max_length': '20'})
        },
        u'adapter.price': {
            'Meta': {'object_name': 'Price'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'price_currency_relation'", 'to': u"orm['api.Currency']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inbound_country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'price_inbound_country_relation'", 'null': 'True', 'to': u"orm['api.Country']"}),
            'inbound_endpoint': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'price_inbound_endpoint_relation'", 'null': 'True', 'to': u"orm['adapter.Endpoint']"}),
            'inbound_zone': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'price_inbound_zone_relation_relation'", 'null': 'True', 'to': u"orm['adapter.Zone']"}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'outbound_country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'price_outbound_country_relation'", 'null': 'True', 'to': u"orm['api.Country']"}),
            'outbound_endpoint': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'price_outbound_endpoint_relation'", 'null': 'True', 'to': u"orm['adapter.Endpoint']"}),
            'outbound_zone': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'price_outbound_zone_relation'", 'null': 'True', 'to': u"orm['adapter.Zone']"}),
            'provider': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Provider']"}),
            'servicelevel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Servicelevel']"}),
            'tier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Tier']"})
        },
        u'adapter.provider': {
            'Meta': {'object_name': 'Provider'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'adapter.servicelevel': {
            'Meta': {'object_name': 'Servicelevel'},
            'arrives_by': ('django.db.models.fields.TimeField', [], {'null': 'True'}),
            'available_shippo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'delivery_attempts': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'insurance': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'insurance_amount': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2'}),
            'insurance_currency': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'servicelevel_insurance_currency_relation'", 'null': 'True', 'to': u"orm['api.Currency']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'provider': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Provider']"}),
            'terms': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'trackable': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'adapter.tier': {
            'Meta': {'object_name': 'Tier'},
            'distance_unit': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'tier_distance_unit_relation'", 'to': u"orm['api.Unit']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mass_unit': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'tier_mass_unit_relation'", 'to': u"orm['api.Unit']"}),
            'max_height': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'max_length': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'max_length_plus_girth': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'max_length_plus_height': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'max_length_plus_width_plus_height': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'max_weight': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'max_width': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'min_height': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'min_length': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'min_length_plus_girth': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'min_length_plus_height': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'min_length_plus_width_plus_height': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'min_weight': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'min_width': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'provider': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Provider']"})
        },
        u'adapter.zone': {
            'Meta': {'object_name': 'Zone'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'provider': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Provider']"}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'ANY'", 'max_length': '50'})
        },
        u'api.country': {
            'Meta': {'object_name': 'Country'},
            'capital': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.Currency']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'iso3': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'language': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.Language']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'requires_zip': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'tld': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '3', 'null': 'True', 'blank': 'True'})
        },
        u'api.currency': {
            'Meta': {'object_name': 'Currency'},
            'base': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['api.Currency']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rate': ('django.db.models.fields.DecimalField', [], {'default': '1.0', 'max_digits': '15', 'decimal_places': '6'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'api.language': {
            'Meta': {'object_name': 'Language'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'api.unit': {
            'Meta': {'object_name': 'Unit'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'base': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['api.Unit']"}),
            'dimension': ('django.db.models.fields.CharField', [], {'default': "'DISTANCE'", 'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'plural': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'rate': ('django.db.models.fields.DecimalField', [], {'default': '1.0', 'max_digits': '20', 'decimal_places': '10'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '5'})
        }
    }

    complete_apps = ['adapter']