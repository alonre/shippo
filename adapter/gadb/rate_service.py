from django.conf import settings

from django.db.models import Q

from adapter.models import Endpoint, Provider, Adapter, Zone, CountryZone, Servicelevel, Tier, Duration, Price
from api.models import Rate

from base import GADBBaseService
from custom_rate_service import GADBCustomRateService

class GADBRateService(GADBBaseService):
    def __init__(self, config, *args, **kwargs):
        super(GADBRateService, self).__init__(config, *args, **kwargs)
        self.custom_rate_service = GADBCustomRateService(config)
        
    def get_rates(self, shipment):
        response = self.query(shipment)
        
        if response is not None and len(response):
            rates = self.generate_rates(response, shipment, self.config.user)
        else:
            return None
        
        if rates is not None and len(rates) > 0:
            return rates
        else:
            return None
        
    def generate_rates(self, response, shipment, user):
        rates = []
    
        for result in response:
            price = response.get(result)
            if price[0].provider in self.config.enabled_providers():
                rate = Rate(shipment = shipment, object_purpose = shipment.object_purpose, adapter = self.config.adapter)
                rate.fill_from_price_object(price[0])   # first get price object
                rate.fill_from_duration_object(price[1])    # now get duration object
    
                rate.object_owner = user
                rates.append(rate)
            
        rates = self.custom_rate_service.get_rates(rates, shipment, user)
        
        for rate in rates:
            if rate.servicelevel:
                rate.fill_from_servicelevel_object(rate.servicelevel)
                rate.fill_from_duration_object(self.get_durations(shipment = shipment, servicelevel = rate.servicelevel, outbound_country = shipment.address_from.country, inbound_country = shipment.address_to.country, single=True))
            elif rate.price.servicelevel:
                rate.fill_from_servicelevel_object(rate.price.servicelevel)
                rate.fill_from_duration_object(self.get_durations(shipment = shipment, servicelevel = rate.price.servicelevel, outbound_country = shipment.address_from.country, inbound_country = shipment.address_to.country, single=True))
            
            if rate.servicelevel.available_shippo:
                rate.object_purpose = shipment.object_purpose
            else:
                rate.object_purpose = Rate.OBJECT_PURPOSE_CHOICE.QUOTE
        
        return rates
            
    def query(self, shipment):
        # get dimensions beforehand. else it's too long a call @ tiers. TODO: Refactor get_xxx to use unit, not abbrev
        length = shipment.parcel.get_length(unit=self.config.unit_cm, decimal_places=2)
        width = shipment.parcel.get_width(unit=self.config.unit_cm, decimal_places=2)
        height = shipment.parcel.get_height(unit=self.config.unit_cm, decimal_places=2)
        weight = shipment.parcel.get_weight(unit=self.config.unit_g, decimal_places=2)
        
        length_plus_height = shipment.parcel.get_length_plus_height(unit=self.config.unit_cm, decimal_places=2)
        length_plus_width_plus_height = shipment.parcel.get_length_plus_width_plus_height(unit=self.config.unit_cm, decimal_places=2)
        length_plus_girth = shipment.parcel.get_length_plus_girth(unit=self.config.unit_cm, decimal_places=2)
        
        outbound_country = shipment.address_from.country
        inbound_country = shipment.address_to.country
    
        # get relevant zones
        outbound_zones = CountryZone.objects.filter(
            (Q(outbound_country = outbound_country) & Q(zone__isnull=False) & Q(country__isnull=True))
        )
        inbound_zones = CountryZone.objects.filter(
            (Q(outbound_country = outbound_country) | Q(outbound_country__isnull=True)) & (Q(country = inbound_country) | Q(country__isnull=True))
        )
        
        # get relevant tiers
        tiers = Tier.objects.filter(
            # The applicable tiers' minimum values must be smaller/equal to the actual parcel
            (Q(min_length__lte=length) | Q(min_length__isnull=True)) & (Q(min_width__lte=width) | Q(min_width__isnull=True)) & (Q(min_height__lte=height) | Q(min_height__isnull=True))
          & (Q(min_length_plus_height__lte=length_plus_height) | Q(min_length_plus_height__isnull=True)) & (Q(min_length_plus_girth__lte=length_plus_girth) | Q(min_length_plus_girth__isnull=True))
          & (Q(min_length_plus_width_plus_height__lte=length_plus_width_plus_height) | Q(min_length_plus_width_plus_height__isnull=True)) & (Q(min_weight__lte=weight) | Q(min_weight__isnull=True)) 
            # The applicable tiers' maximum values must larger/equal to the actual parcel
          & (Q(max_length__gte=length) | Q(max_length__isnull=True)) & (Q(max_width__gte=width) | Q(max_width__isnull=True)) & (Q(max_height__gte=height) | Q(max_height__isnull=True))
          & (Q(max_length_plus_height__gte=length_plus_height) | Q(max_length_plus_height__isnull=True)) & (Q(max_length_plus_girth__gte=length_plus_girth) | Q(max_length_plus_girth__isnull=True))
          & (Q(max_length_plus_width_plus_height__gte=length_plus_width_plus_height) | Q(max_length_plus_width_plus_height__isnull=True)) & (Q(max_weight__gte=weight) | Q(max_weight__isnull=True))
        )
        
        prices = Price.objects.filter(
            (Q(tier__in=tiers) | Q(tier__isnull=True))
          & (
                (Q(outbound_country=outbound_country) & Q(inbound_country=inbound_country))
              | (Q(outbound_country=outbound_country) & Q(inbound_zone__in=inbound_zones.values('zone_id')))
              | (Q(outbound_zone__in=outbound_zones.values('zone_id')) & Q(inbound_zone__in=inbound_zones.values('zone_id')))
              | (Q(outbound_zone__in=outbound_zones.values('zone_id')) & Q(inbound_country=inbound_country))
            )
        ).order_by('provider', 'servicelevel', 'inbound_endpoint', 'outbound_endpoint', 'amount').distinct('provider', 'servicelevel', 'inbound_endpoint', 'outbound_endpoint')
    
        durations = self.get_durations(shipment = shipment, servicelevel_ids = prices.values('servicelevel_id'), outbound_country = outbound_country, inbound_country = inbound_country)
        
        results = {}
        
        # writing prices + corresponding duration into a list ("concatenated")
        for p in prices:
            results.setdefault(p.pk, []).append(p)
            for d in durations.filter(servicelevel=p.servicelevel):
                results[p.pk].append(d)
        
        return results
        
    def get_durations(self, shipment, outbound_country, inbound_country, servicelevel_ids = None, servicelevel = None, single = False, *args, **kwargs):
        outbound_country = shipment.address_from.country
        inbound_country = shipment.address_to.country
    
        # get relevant zones
        outbound_zones = CountryZone.objects.filter(
            (Q(outbound_country = outbound_country) & Q(zone__isnull=False) & Q(country__isnull=True))
        )
        inbound_zones = CountryZone.objects.filter(
            (Q(outbound_country = outbound_country) | Q(outbound_country__isnull=True)) & (Q(country = inbound_country) | Q(country__isnull=True))
        )
        
        durations = None
        
        if servicelevel:
            durations = Duration.objects.filter(
                Q(servicelevel=servicelevel)
              & (
                    ((Q(outbound_country=outbound_country)) & (Q(inbound_country=inbound_country)))
                  | ((Q(outbound_country=outbound_country)) & (Q(inbound_zone__in=inbound_zones.values('zone_id'))))     #  | Q(outbound_country__isnull=True) in first part removed
                  | ((Q(outbound_zone__in=outbound_zones.values('zone_id'))) & (Q(inbound_zone__in=inbound_zones.values('zone_id'))))
                  | ((Q(outbound_zone__in=outbound_zones.values('zone_id'))) & (Q(inbound_country=inbound_country)))
                  | ((Q(outbound_country=outbound_country)) & (Q(inbound_country__isnull=True)) & (Q(inbound_zone__isnull=True))) # Wildcard scenario I: outbound_country to any
                  | ((Q(outbound_zone__in=outbound_zones.values('zone_id'))) & (Q(inbound_country__isnull=True)) & (Q(inbound_zone__isnull=True))) # Wildcard scenario II: outbound_zone to any
                )
            ).distinct()
        elif servicelevel_ids:
            durations = Duration.objects.filter(
                Q(servicelevel__in=servicelevel_ids)
              & (
                    ((Q(outbound_country=outbound_country)) & (Q(inbound_country=inbound_country)))
                  | ((Q(outbound_country=outbound_country)) & (Q(inbound_zone__in=inbound_zones.values('zone_id'))))     #  | Q(outbound_country__isnull=True) in first part removed
                  | ((Q(outbound_zone__in=outbound_zones.values('zone_id'))) & (Q(inbound_zone__in=inbound_zones.values('zone_id'))))
                  | ((Q(outbound_zone__in=outbound_zones.values('zone_id'))) & (Q(inbound_country=inbound_country)))
                  | ((Q(outbound_country=outbound_country)) & (Q(inbound_country__isnull=True)) & (Q(inbound_zone__isnull=True))) # Wildcard scenario I: outbound_country to any
                  | ((Q(outbound_zone__in=outbound_zones.values('zone_id'))) & (Q(inbound_country__isnull=True)) & (Q(inbound_zone__isnull=True))) # Wildcard scenario II: outbound_zone to any
                )
            ).distinct()
            
        if single:
            durations_result = list(durations)
            return durations_result[0]
        else:
            results = durations
            
        return results
    
    def __unicode__(self):
        return 'GADBRateService'