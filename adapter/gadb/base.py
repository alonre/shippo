from adapter.models import Zone, CountryZone
from api.models import Message

class GADBBaseService(object):
    def __init__(self, config, *args, **kwargs):
        self.config = config
            
    def __unicode__(self):
        return 'GADBBaseService'