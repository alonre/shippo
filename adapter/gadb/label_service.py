import requests
from django.conf import settings

from api.models import Currency, Country, Transaction, Message
from base import GADBBaseService
from .. import dispatch

class GADBLabelService(GADBBaseService):
    def __init__(self, config, *args, **kwargs):
        super(GADBLabelService, self).__init__(config, *args, **kwargs)

    def get_label(self, transaction):
        if transaction.rate.is_purchasable(transaction, self.config.adapter) and self.config.credentials_valid(transaction): 
            production = self.config.production(transaction)
            transaction.set_was_test(production)
            transaction.save(trigger_generation=False)
                
            if production:
                try:
                    headers = {'Content-Type': 'application/json', 'Authorization': 'Token ' + settings.LABEL_GENERATION_API_TOKEN}

                    r = requests.get(settings.LABEL_GENERATION_API_CALL + str(transaction.object_id) + '/', headers=headers)

                    if r.status_code != 200:
                        transaction.add_message(code='', text = "Internal error generating label. Please contact info@goshippo.com with the Object ID.", source=self.config.adapter, stack_trace=r.content, new_status=Transaction.OBJECT_STATUS_CHOICE.ERROR)
                        dispatch.send_transaction_error_mail(transaction)
                except:
                    import traceback
                    e = traceback.format_exc()
                    transaction.add_message(code='', text = "Internal error generating label. Please contact info@goshippo.com with the Object ID.", source=self.config.adapter, stack_trace=e, new_status=Transaction.OBJECT_STATUS_CHOICE.ERROR)

            else:
                self.generate_sample_label(transaction)

    def generate_sample_label(self, transaction):
            from django.templatetags.static import static
            #we provide a sample label
            transaction.label_url = static('providers/label_samples/gadb_label_sample.pdf')
            transaction.object_status = transaction.OBJECT_STATUS_CHOICE.SUCCESS # TODO: better as validation function in model?
            transaction.save(trigger_generation=False)

    def charge_transaction(self, transaction, *args, **kwargs):
        if self.config.apiuser.billing.payment_type == self.config.apiuser.billing.PAYMENT_CHOICE.STRIPE:
            payment = StripePaymentAdapter(adapter=self.config.adapter, transaction=transaction)
            # add more payment adapters here
            if payment:
                charge = payment.charge()

                if charge:
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False

    def __unicode__(self):
        return 'GADBLabelService'