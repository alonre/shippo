from decimal import *
from math import ceil
from copy import copy

from base import GADBBaseService
from adapter.models import Endpoint, Provider, Adapter, Zone, CountryZone, Servicelevel, Tier, Duration, Price
from api.models import Rate, Country

class GADBCustomRateService(GADBBaseService):
    def __init__(self, config, *args, **kwargs):
        super(GADBCustomRateService, self).__init__(config, *args, **kwargs)

    def get_rates(self, rates, shipment, user):
        for rate in rates:
            self.calculate_posten_norge_prices(rate, shipment)
            self.calculate_colissimo_prices(rate, shipment)
            self.calculate_chronopost_prices(rate, shipment)
            self.calculate_posten_ab_prices(rate, shipment)

        self.calculate_royal_mail_prices(rates, shipment)
        self.duplicate_chronopost_prices(rates, shipment)
        self.duplicate_deutsche_post_DHL_prices(rates, shipment)
        self.duplicate_posten_ab_prices(rates, shipment)

        return rates

    # TESTED
    def calculate_posten_norge_prices(self, rate, shipment, *args, **kwargs):
        provider = Provider.objects.get(name='Posten Norge')

        if rate.provider == provider:
            zone_europe = Zone.objects.get(provider=provider, name__contains='Europe')
            zone_world = Zone.objects.get(provider=provider, name__contains='Rest of the World')

            price_base = 270.0
            value_insurance_per_kg = 4.5
            price_europe_per_kg = 17.0
            price_world_per_kg = 44.0

            price_final = price_base
            weight_rounded = ceil(shipment.parcel.get_weight(unit=self.config.unit_kg, decimal_places = 1))

            if rate.price.inbound_zone == zone_europe:
                price_final += weight_rounded * price_europe_per_kg

            if rate.price.inbound_zone == zone_world:
                price_final += weight_rounded * price_world_per_kg

            insurance_value_final = weight_rounded * value_insurance_per_kg

            rate.amount = price_final
            rate.insurance_amount = insurance_value_final

            rate.object_custom_logic_applied = True

    def calculate_colissimo_prices(self, rate, shipment, *args, **kwargs):
        provider = Provider.objects.get(name='Colissimo')

        if rate.provider == provider:

            length = shipment.parcel.get_length(unit=self.config.unit_cm, decimal_places=2)
            length_plus_width_plus_height = shipment.parcel.get_length_plus_width_plus_height(unit=self.config.unit_cm, decimal_places=0)

            if length > 150.0 or (150.0 < length_plus_width_plus_height <= 200.0):
                rate.amount += 6
                rate.object_custom_logic_applied = True

    # TESTED
    def calculate_chronopost_prices(self, rate, shipment, *args, **kwargs):
        provider = Provider.objects.get(name='Chronopost')

        if rate.provider == provider:
            sl_chrono_relais = Servicelevel.objects.get(provider=provider, servicelevel_name='Relais')
            sl_chrono_10 = Servicelevel.objects.get(provider=provider, servicelevel_name='10')
            sl_chrono_13 = Servicelevel.objects.get(provider=provider, servicelevel_name='13')
            sl_chrono_express = Servicelevel.objects.get(provider=provider, servicelevel_name='Express')
            sl_chrono_classic = Servicelevel.objects.get(provider=provider, servicelevel_name='Classic')

            weight = shipment.parcel.get_weight(unit=self.config.unit_kg, decimal_places=1)

            zone_express_classic_1 = Zone.objects.get(provider=provider, name='Express & Classic 1')
            zone_express_classic_2 = Zone.objects.get(provider=provider, name='Express & Classic 2')
            zone_classic_3 = Zone.objects.get(provider=provider, name='Classic 3')
            zone_classic_4 = Zone.objects.get(provider=provider, name='Classic 4')
            zone_express_3 = Zone.objects.get(provider=provider, name='Express 3')
            zone_express_4 = Zone.objects.get(provider=provider, name='Express 4')
            zone_express_5 = Zone.objects.get(provider=provider, name='Express 5')
            zone_express_6 = Zone.objects.get(provider=provider, name='Express 6')
            zone_express_7 = Zone.objects.get(provider=provider, name='Express 7')
            zone_express_8 = Zone.objects.get(provider=provider, name='Express 8')
            zone_express_9 = Zone.objects.get(provider=provider, name='Express 9')
            
            if rate.price.servicelevel == sl_chrono_relais:
                if 5.0 < weight <= 20.0:
                    weight_difference = ceil(weight-5)
                    price_add = round((weight_difference*0.65), 2)
                    rate.amount += Decimal(price_add)
            
            if rate.price.servicelevel == sl_chrono_10:
                if 7.0 < weight <= 30.0:
                    weight_difference = ceil(weight-7)
                    price_add = round((weight_diff * 0.9), 2)
                    rate.amount += Decimal(price_add)
                    
            if rate.price.servicelevel == sl_chrono_13:
                if 7.0 < weight <= 30.0:
                    weight_difference = ceil(weight-7)
                    price_add = round((weight_diff * 0.65), 2)
                    rate.amount += Decimal(price_add)
                    
            if rate.price.servicelevel == sl_chrono_express:
                length = shipment.parcel.get_length(unit=self.config.unit_cm, decimal_places=1)
                width = shipment.parcel.get_width(unit=self.config.unit_cm, decimal_places=1)
                height = shipment.parcel.get_height(unit=self.config.unit_cm, decimal_places=1)
                
                weight_actual = weight
                
                volumetric = (length * width * height) / 5000
                
                if volumetric > weight:
                    weight = volumetric
                   
                if rate.price.inbound_zone == zone_express_classic_1:
                    price5 = Price.objects.get(id=1327)
                    extra_to10 = 4.3
                    extra_to30 = 2.5
                
                if rate.price.inbound_zone == zone_express_classic_2:
                    price5 = Price.objects.get(id=1336)
                    extra_to10 = 4.7
                    extra_to30 = 3.4
                    
                if rate.price.inbound_zone == zone_express_3:
                    price5 = Price.objects.get(id=1229)
                    extra_to10 = 10.7
                    extra_to30 = 7.6
                    
                if rate.price.inbound_zone == zone_express_4:
                    price5 = Price.objects.get(id=1239)
                    extra_to10 = 3.6
                    extra_to30 = 3.6
                    
                if rate.price.inbound_zone == zone_express_5:
                    price5 = Price.objects.get(id=1249)
                    extra_to10 = 4.1
                    extra_to30 = 6.5
                    
                if rate.price.inbound_zone == zone_express_6:
                    price5 = Price.objects.get(id=1259)
                    extra_to10 = 9.3
                    extra_to30 = 10.6
                    
                if rate.price.inbound_zone == zone_express_7:
                    price5 = Price.objects.get(id=1269)
                    extra_to10 = 9.8
                    extra_to30 = 11.1
                    
                if rate.price.inbound_zone == zone_express_8:
                    price5 = Price.objects.get(id=1279)
                    extra_to10 = 4.1
                    extra_to30 = 5.5
                    
                if rate.price.inbound_zone == zone_express_9:
                    price5 = Price.objects.get(id=1289)
                    extra_to10 = 12.4
                    extra_to30 = 13.9
                    
                if 5 < weight <= 30:
                    weight_difference = ceil((weight-5) / 0.5)
                    rate.price = price5
                    rate.amount = price5.amount
                    
                    if weight > 10:
                        price_add = round((10*extra_to10 + (weight_difference-10) * extra_to30), 2)
                    else:
                        price_add = round((weight_difference * extra_to10), 2)
                        
                    rate.amount += Decimal(price_add)
                elif weight <= 5 and volumetric > weight_actual:
                    diff_vw = ceil(volumetric/0.5) - ceil(weight_actual/0.5)
                    
                    if diff_vw > 0:
                        tier_id = rate.price.tier.id + diff_vw
                        new_price = Price.objects.get(tier_id = tier_id, inbound_zone = rate.price.inbound_zone, provider=provider)
                        rate.price = Decimal(new_price)
                        
            if rate.price.servicelevel == sl_chrono_classic:
                if 1 < weight <= 30:
                    if rate.price.inbound_zone == zone_express_classic_1:
                        extra_to10 = 2.2
                        extra_to30 = 2.3
                
                    if rate.price.inbound_zone == zone_express_classic_2:
                        extra_to10 = 2.5
                        extra_to30 = 2.6
                        
                    if rate.price.inbound_zone == zone_classic_3:
                        extra_to10 = 2.8
                        extra_to30 = 2.9
                        
                    if rate.price.inbound_zone == zone_classic_4:
                        extra_to10 = 2.6
                        extra_to30 = 2.8
                        
                    weight_diff = ceil(weight-1)
                    
                    if weight > 10:
                        price_add = round((9*extra_to10 + (weight_diff-9) * extra_to30), 2)
                    else:
                        price_add = round((weight_diff * extra_to10), 2)
                        
                    rate.amount += Decimal(price_add)
                    
    # TESTED
    def calculate_posten_ab_prices(self, rate, shipment, *args, **kwargs):
        provider = Provider.objects.get(name='Posten AB')
        country_sweden = Country.objects.get(iso2='SE')
        
        if rate.provider == provider:
            weight = shipment.parcel.get_weight(unit=self.config.unit_kg, decimal_places=2)
            length = shipment.parcel.get_length(unit=self.config.unit_cm, decimal_places=2)
            width = shipment.parcel.get_width(unit=self.config.unit_cm, decimal_places=2)
            height = shipment.parcel.get_height(unit=self.config.unit_cm, decimal_places=2)
            
            weight_volume_servicelevels = Servicelevel.objects.filter(id__in=[45,47,48,49])
            
            if rate.price.servicelevel in weight_volume_servicelevels:
                volume = length/100 * width/100 * height/100 * 280
                
                price_weight = ceil(weight)
                
                if weight < volume:
                    price_weight = ceil(volume)
                    
                if rate.price.inbound_country == country_sweden:
                    sl_postpaket = Servicelevel.objects.get(provider=provider, servicelevel_name='Postpaket Domestic')
                    sl_hempaket = Servicelevel.objects.get(provider=provider, servicelevel_name='Hempaket Domestic')
                    sl_expresspaket = Servicelevel.objects.get(provider=provider, servicelevel_name='Expresspaket Domestic')
                    
                    if rate.price.servicelevel == sl_postpaket:
                        if price_weight <= 3:
                            rate.price = Price.objects.get(id=860)
                        elif 3 < price_weight <= 5:
                            rate.price = Price.objects.get(id=861)
                        elif 5 < price_weight <= 10:
                            rate.price = Price.objects.get(id=862)
                        elif 10 < price_weight <= 15:
                            rate.price = Price.objects.get(id=863)
                        elif 15 < price_weight <= 20:
                            rate.price = Price.objects.get(id=864)
                        elif 20 < price_weight <= 25:
                            rate.price = Price.objects.get(id=865)
                        elif 25 < price_weight <= 30:
                            rate.price = Price.objects.get(id=866)
                        elif 30 < price_weight <= 35:
                            rate.price = Price.objects.get(id=867)
                        elif 35 < price_weight <= 50:
                            rate.price = Price.objects.get(id=868)
                    elif rate.price.servicelevel == sl_expresspaket:
                        if price_weight <= 3:
                            rate.price = Price.objects.get(id=851)
                        elif 3 < price_weight <= 5:
                            rate.price = Price.objects.get(id=852)
                        elif 5 < price_weight <= 10:
                            rate.price = Price.objects.get(id=853)
                        elif 10 < price_weight <= 15:
                            rate.price = Price.objects.get(id=854)
                        elif 15 < price_weight <= 20:
                            rate.price = Price.objects.get(id=855)
                        elif 20 < price_weight <= 25:
                            rate.price = Price.objects.get(id=856)
                        elif 25 < price_weight <= 30:
                            rate.price = Price.objects.get(id=857)
                        elif 30 < price_weight <= 35:
                            rate.price = Price.objects.get(id=858)
                        elif 35 < price_weight <= 50:
                            rate.price = Price.objects.get(id=859)
                    elif rate.price.servicelevel == sl_hempaket:
                        if price_weight <= 3:
                            rate.price = Price.objects.get(id=869)
                        elif 3 < price_weight <= 5:
                            rate.price = Price.objects.get(id=870)
                        elif 5 < price_weight <= 10:
                            rate.price = Price.objects.get(id=871)
                        elif 10 < price_weight <= 15:
                            rate.price = Price.objects.get(id=872)
                        elif 15 < price_weight <= 20:
                            rate.price = Price.objects.get(id=873)
                        elif 20 < price_weight <= 25:
                            rate.price = Price.objects.get(id=874)
                        elif 25 < price_weight <= 30:
                            rate.price = Price.objects.get(id=875)
                        elif 30 < price_weight <= 35:
                            rate.price = Price.objects.get(id=876)
                        elif 35 < price_weight <= 50:
                            rate.price = Price.objects.get(id=877)
                    
                    rate.fill_from_price_object(rate.price)
                
                elif price_weight > 1:
                    factor = ceil(price_weight-1)
                    
                    if rate.price == Price.objects.get(id=878):
                        rate.amount += Decimal(factor * 40)
                    elif rate.price == Price.objects.get(id=879):
                        rate.amount += Decimal(factor * 32)
                    elif rate.price == Price.objects.get(id=880):
                        rate.amount += Decimal(factor * 85)
                        
            length_overcharge_bottom = 120
            overcharge_amount = 145
            overcharge_servicelevels = Servicelevel.objects.filter(id__in=[45, 46, 47, 48])
            
            if length > length_overcharge_bottom and rate.servicelevel in overcharge_servicelevels:
                rate.amount += overcharge_amount
            
            rate.object_custom_logic_applied = True

    # TESTED
    def duplicate_posten_ab_prices(self, rates, shipment, *args, **kwargs):
        provider = Provider.objects.get(name='Posten AB')
        
        addtl_rates = []
        
        weight_volume_tiers = []
        begin_tier = 111
        end_tier = 145
        
        i = 0
        while i <= end_tier:
            weight_volume_tiers.append(i)
            i += 1
            
        weight_volume_tiers.append(155)
        
        weight = shipment.parcel.get_weight(unit=self.config.unit_kg, decimal_places=1)
        
        for rate in rates:
            if rate.price.tier.id in weight_volume_tiers:
                if weight > 20:
                    rates.remove(rate)
            
            if rate.provider == provider:
                servicelevel_firstclass_intl = Servicelevel.objects.get(id=38)
                
                if rate.price.servicelevel == servicelevel_firstclass_intl:
                
                    country_sweden = Country.objects.get(iso2='SE')
                    country_inbound = shipment.address_to.country
                    
                    servicelevel_express_mail_posten = Servicelevel.objects.get(id=41)
                    reg_mail_500_posten = Servicelevel.objects.get(id=39)
                    reg_mail_10000_posten = Servicelevel.objects.get(id=40)
                    
                    addtl_servicelevels = [{'servicelevel': servicelevel_express_mail_posten, 'price_add': 89}, {'servicelevel': reg_mail_500_posten, 'price_add': 64}, {'servicelevel': reg_mail_10000_posten, 'price_add': 94}]
                    
                    for item in addtl_servicelevels:
                        new_rate = copy(rate)
                        new_rate.servicelevel = item['servicelevel']
                        new_rate.amount += item['price_add']
                        
                        new_rate.object_custom_logic_applied = True
                        addtl_rates.append(new_rate)
        
        rates += addtl_rates
    
    # TESTED
    def calculate_royal_mail_prices(self, rates, shipment, *args, **kwargs):
        provider_royal_mail = Provider.objects.get(name='Royal Mail')
        country_gb = Country.objects.get(iso2='GB')
        servicelevel_air_mail = Servicelevel.objects.get(servicelevel_name='Airmail')
        
        addtl_rates = []
        
        for rate in rates:
            if rate.provider == provider_royal_mail and shipment.address_to.country != country_gb and rate.price.servicelevel == servicelevel_air_mail:
                zone_europe_pk = 31     # Attn: != EU
                zone_world_1_pk = 32
                zone_world_2_pk = 33

                price_europe_base = 3.50
                price_europe_per250g = 1.45
                price_world_1_base = 4.50
                price_world_1_per250g = 2.70
                price_world_2_base = 4.70
                price_world_2_per250g = 2.85

                weight = shipment.parcel.get_weight(unit=self.config.unit_g, decimal_places=2)
                weight_true = ceil(weight / 250)           # stueckelung = 250g
                price_final = 0
                
                if weight > 100 and rate.price.inbound_zone:
                    if rate.price.inbound_zone == Zone.objects.get(id=zone_europe_pk):
                        price_final += price_europe_base
                        price_final += (weight_true - 1) * price_europe_per250g
                    elif rate.price.inbound_zone == Zone.objects.get(id=zone_world_1_pk):
                        price_final += price_world_1_base
                        price_final += (weight_true - 1) * price_europe_per250g
                    elif rate.price.inbound_zone == Zone.objects.get(id=zone_world_2_pk):
                        price_final += price_world_2_base
                        price_final += (weight_true - 1) * price_world_2_per250g
                        
                    rate.amount = price_final
                    rate.object_custom_logic_applied = True
                    
                addtl_rates = self.duplicate_royal_mail_service(rate, shipment)
            
        rates += addtl_rates

    # TESTED
    def duplicate_royal_mail_service(self, rate, shipment, *args, **kwargs):
        addtl_rates = []
        
        zone_eu = Zone.objects.get(id=41)
        zone_world = Zone.objects.get(id=42)
        country_gb = Country.objects.get(iso2="GB")
        country_inbound = shipment.address_to.country
        
        vat_uk = 0.2
        price_airsure_eu_base = 5.00 * (1 + vat_uk)
        price_airsure_eu_compensation = 7.60 * (1 + vat_uk)
        price_airsure_world_base = 5.40
        price_airsure_world_compensation = 8.00
        price_signedfor_eu_base = 5.30 * (1 + vat_uk)
        price_signedfor_eu_compensation = 7.90 * (1 + vat_uk)
        price_signedfor_world_base = 5.30
        price_signedfor_world_compensation = 7.90
        
        servicelevel_airsure = Servicelevel.objects.get(id=139)
        servicelevel_airsure_compensation = Servicelevel.objects.get(id=140)
        servicelevel_signedfor = Servicelevel.objects.get(id=142)
        servicelevel_signedfor_compentation = Servicelevel.objects.get(id=141)
        
        servicelevels = [{'servicelevel': servicelevel_airsure, 'price_eu': price_airsure_eu_base, 'price_world': price_airsure_world_base},
                            {'servicelevel': servicelevel_airsure_compensation, 'price_eu': price_airsure_eu_compensation, 'price_world': price_airsure_world_compensation},
                            {'servicelevel': servicelevel_signedfor, 'price_eu': price_signedfor_eu_base, 'price_world': price_signedfor_world_base},
                            {'servicelevel': servicelevel_signedfor_compentation, 'price_eu': price_signedfor_eu_compensation, 'price_world':price_signedfor_world_compensation}]
        
        countryzone = CountryZone.objects.get(zone__in=[zone_eu, zone_world], country=country_inbound)
        
        for item in servicelevels:
            new_rate = copy(rate)
            new_rate.amount = Decimal(new_rate.amount)
            new_rate.servicelevel = item['servicelevel']
            
            if countryzone.zone == zone_eu:
                new_rate.amount += Decimal(item['price_eu'])
            elif countryzone.zone == zone_world:
                new_rate.amount += Decimal(item['price_world'])
            
            new_rate.object_custom_logic_applied = True
            addtl_rates.append(new_rate)
            
        return addtl_rates
    
    # TESTED
    def duplicate_chronopost_prices(self, rates, shipment, *args, **kwargs):
        addtl_rates = []
        
        provider = Provider.objects.get(name='Chronopost')
        servicelevel_premium = Servicelevel.objects.get(id=157)
        country_fr = Country.objects.get(iso2='FR')
        country_inbound = shipment.address_to.country
        
        residential_surcharge = Decimal(2.9)
        vat_fr = Decimal(0.196)
        co2_air = Decimal(0.2)
        co2_ground = Decimal(0.15)
        
        for rate in rates:
            if rate.provider == provider:
                if rate.price.servicelevel in Servicelevel.objects.filter(id__in=[152, 153, 154, 156]):
                    rate.amount += rate.amount * co2_ground
                    
                if rate.price.servicelevel == Servicelevel.objects.get(id=155) and rate.price.inbound_zone in Zone.objects.filter(id__in=[80,81,89,90]):
                    addtl_rate = copy(rate)
                    addtl_rate.servicelevel = servicelevel_premium
                    addtl_rate.amount += 35
                    addtl_rate.amount += addtl_rate.amount * co2_air
                    addtl_rate.amount += residential_surcharge
                    addtl_rate.amount += addtl_rate.amount * vat_fr
                    addtl_rate.object_custom_logic_applied = True
                    addtl_rates.append(addtl_rate)
                    
                    rate.amount += rate.amount * co2_air
                
                rate.amount += Decimal(residential_surcharge)
                rate.amount += Decimal(rate.amount * Decimal(vat_fr))
                rate.object_custom_logic_applied = True
                
        rates += addtl_rates
    
    # NOT TESTED
    def calculate_deutsche_post_DHL_prices(self, rate, shipment, *args, **kwargs):
        # currently not used becuase not available in all countries
        provider = Provider.objects.get(name='Deutsche Post DHL')
        servicelevel = Servicelevel.objects.get(id=159)
        
        if rate.provider == provider and rate.price.servicelevel == servicelevel:
            length = shipment.parcel.get_length(unit=self.config.unit_cm, decimal_places=2)
            length_plus_girth = shipment.parcel.get_length_plus_girth(unit=self.config.unit_cm, decimal_places=2)
            weight = shipment.parcel.get_weight(unit=self.config.unit_kg, decimal_places=2)
            
            if length > 120 or length_plus_girth > 300:
                if rate.price.inbound_zone == Zone.objects.get(id=91) and (weight > 10 or length_plus_girth > 300):
                    rate.amount += 23.8
                else:
                    rate.amount += 20
                    
                rate.servicelevel_name += " (Sperrgut)"
                rate.object_custom_logic_applied = True
    
    # TESTED
    def duplicate_deutsche_post_DHL_prices(self, rates, shipment, *args, **kwargs):
        provider = Provider.objects.get(name='Deutsche Post DHL')
        addtl_rates = []
        
        for rate in rates:
            if rate.provider == provider:
                # Add Pickups
                new_rate = copy(rate)
                new_rate.amount += Decimal(3.0)
                new_rate.outbound_endpoint = Endpoint.objects.get(endpoint_name='door', endpoint_type='ANY')
                new_rate.object_custom_logic_applied = True
                addtl_rates.append(new_rate)
                
        rates += addtl_rates
        
    def __unicode__(self):
        return 'GADBCustomRateService'