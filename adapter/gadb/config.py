from django.conf import settings

from adapter.models import Adapter, Provider
from api.models import Unit, Transaction

class GADBConfig:
    def __init__(self, user):
        self.adapter = Adapter.objects.get(name='GADB')
        self.user = user
        self.apiuser = user.apiuser
        self.settings = settings.ADAPTERS['GADB']

        # setup units
        self.unit_kg = Unit.objects.get(abbreviation='kg')
        self.unit_cm = Unit.objects.get(abbreviation='cm')
        self.unit_g = Unit.objects.get(abbreviation='g')

        # setup providers
        self.provider_royalmail = Provider.objects.get(name='Royal Mail')
        self.provider_deutsche_post = Provider.objects.get(name='Deutsche Post')
        self.provider_dp_dhl = Provider.objects.get(name='Deutsche Post DHL')
        self.provider_hermes = Provider.objects.get(name='Hermes Logistik Group')
        self.provider_gls = Provider.objects.get(name='GLS Germany')
        self.provider_posten_no = Provider.objects.get(name='Posten Norge')
        self.provider_posti = Provider.objects.get(name='Posti')
        self.provider_laposte = Provider.objects.get(name='La Poste')
        self.provider_colissimo = Provider.objects.get(name='Colissimo')
        self.provider_chronopost = Provider.objects.get(name='Chronopost')
        self.provider_posten_ab = Provider.objects.get(name='Posten AB')
        
    def credentials_valid(self, transaction, *args, **kwargs):
        if self.credentials_valid_for_provider(provider=transaction.rate.provider):
            return True
        else:
            transaction.credentials_missing()
            return False

    def production(self, transaction, *args, **kwargs):
        if transaction.rate.provider == self.provider_royalmail and self.apiuser.royalmail_production and self.credentials_valid_for_provider(provider=self.provider_royalmail):
            return True
        elif transaction.rate.provider == self.provider_deutsche_post and self.apiuser.deutsche_post_production and self.credentials_valid_for_provider(provider=self.provider_deutsche_post):
            return True
        elif transaction.rate.provider == self.provider_dp_dhl and self.apiuser.dp_dhl_production and self.credentials_valid_for_provider(provider=self.provider_dp_dhl):
            return True
        elif transaction.rate.provider == self.provider_hermes and self.apiuser.hermes_production and self.credentials_valid_for_provider(provider=self.provider_hermes):
            return True
        elif transaction.rate.provider == self.provider_gls and self.apiuser.gls_de_production and self.credentials_valid_for_provider(provider=self.provider_gls):
            return True
        elif transaction.rate.provider == self.provider_posten_no and self.apiuser.posten_no_production and self.credentials_valid_for_provider(provider=self.provider_posten_no):
            return True
        elif transaction.rate.provider == self.provider_posti and self.apiuser.posti_production and self.credentials_valid_for_provider(provider=self.provider_posti):
            return True
        elif transaction.rate.provider == self.provider_laposte and self.apiuser.laposte_production and self.credentials_valid_for_provider(provider=self.provider_laposte):
            return True
        elif transaction.rate.provider == self.provider_colissimo and self.apiuser.colissimo_production and self.credentials_valid_for_provider(provider=self.provider_colissimo):
            return True
        elif transaction.rate.provider == self.provider_chronopost and self.apiuser.chronopost_production and self.credentials_valid_for_provider(provider=self.provider_chronopost):
            return True
        elif transaction.rate.provider == self.provider_posten_ab and self.apiuser.posten_ab_production and self.credentials_valid_for_provider(provider=self.provider_posten_ab):
            return True
        else:
            return False

    def enabled_providers(self, *args, **kwargs):
        providers = []
        for provider in self.adapter.provider_served.all():
            if self.credentials_valid_for_provider(provider=provider):
                providers.append(provider)

        return providers

    def credentials_valid_for_provider(self, provider, *args, **kwargs):
        if provider == self.provider_royalmail:
            if self.apiuser.royalmail_username and self.apiuser.royalmail_password:
                return True
        elif provider == self.provider_deutsche_post:
            if self.apiuser.portokasse_mail and self.apiuser.portokasse_password:
                return True
            else:
                return False
        elif provider == self.provider_dp_dhl:
            if self.apiuser.postpay_mail and self.apiuser.postpay_password:
                return True
            else:
                return False
        elif provider == self.provider_hermes:
            if self.apiuser.hermes_dummy_credential:
                return True
            else:
                return False
        elif provider == self.provider_gls:
            if self.apiuser.gls_de_dummy_credential:
                return True
            else:
                return False
        elif provider == self.provider_posten_no:
            if self.apiuser.posten_no_dummy_credential:
                return True
            else:
                return False
        elif provider == self.provider_posti:
            if self.apiuser.posti_dummy_credential:
                return True
            else:
                return False
        elif provider == self.provider_laposte:
            if self.apiuser.laposte_dummy_credential:
                return True
            else:
                return False
        elif provider == self.provider_colissimo:
            if self.apiuser.colissimo_dummy_credential:
                return True
            else:
                return False
        elif provider == self.provider_chronopost:
            if self.apiuser.chronopost_dummy_credential:
                return True
            else:
                return False
        elif provider == self.provider_posten_ab:
            if self.apiuser.posten_ab_dummy_credential:
                return True
            else:
                return False
        else:
            return False

    def __unicode__(self):
        return 'GADBConfig'