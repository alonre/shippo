import requests
import copy
import xml.etree.ElementTree as ET

from adapter.models import Zone, CountryZone
from api.models import Message
import elements

class Parcel2GoBaseService(object):
    def __init__(self, config, *args, **kwargs):
        self.config = config

    def send_request(self, request, soap_action='', transaction=None, is_tracking_request=False):
        headers = self.config.headers
        headers['SOAPAction'] += soap_action
        
        import sys
        print >>sys.stderr, str(request)
        
        if not is_tracking_request:
            post = requests.post(self.config.base_url_parcel_service, data=str(request), headers=headers)
        else:
            post = requests.post(self.config.base_url_track_service, data=str(request), headers=headers)
        
        response = ET.fromstring(post.text)
        
        import sys
        print >>sys.stderr, ET.tostring(response)
        # TODO
        if transaction:
            transaction.object_api_response += ET.tostring(response)
            transaction.save(trigger_generation=False)
            self.messages = self.get_messages(response)
            self.save_messages_for_object(messages=self.messages, transaction=transaction) # TODO: for rates as well

        return self.check_for_error(response)

    def check_for_error(self, response):
        body = response.find('ns0:Body', namespaces=self.config.namespaces)
        quotes_response = body.find('ns1:GetQuotesResponse', namespaces=self.config.namespaces)
        book_response = body.find('ns1:BookCollectionResponse', namespaces=self.config.namespaces)
        track_response = body.find('ns1:GetTrackingHistoryByOrderNoResponse', namespaces=self.config.namespaces)
        element = None
        success_string = ''
        success = False
        
        if quotes_response is not None:
            element = quotes_response.find('ns1:GetQuotesResult', namespaces=self.config.namespaces)
        elif book_response is not None:
            element = book_response.find('ns1:BookCollectionResult', namespaces=self.config.namespaces)
        elif track_response is not None:
            element = track_response.find('ns1:GetTrackingHistoryByOrderNoResult', namespaces=self.config.namespaces)

        if element is not None:
            success_string = element.find('ns1:Success', namespaces=self.config.namespaces).text
        
        if success_string == 'true':
            success = True
        
        if success:
            return response
        else:
            return None # QUESTIONABLE

    def get_messages(self, response):
        messages = []
        
        body = response.find('ns0:Body', namespaces=self.config.namespaces)
        quotes_response = body.find('ns1:GetQuotesResponse', namespaces=self.config.namespaces)
        book_response = body.find('ns1:BookCollectionResponse', namespaces=self.config.namespaces)
        element = None
        
        if quotes_response is not None:
            element = quotes_response.find('ns1:GetQuotesResult', namespaces=self.config.namespaces)
        elif book_response is not None:
            element = book_response.find('ns1:BookCollectionResult', namespaces=self.config.namespaces)

        if element is not None and element.find('ns0:ErrorMessage', namespaces=self.config.namespaces_results) is not None:
            message = Message(source=self.config.adapter)

            if element.find('ns0:ErrorMessage', namespaces=self.config.namespaces).text is not None:
                message.text = element.find('ns0:ErrorMessage', namespaces=self.config.namespaces_results).text

            messages.append(message)

        return messages

    def save_messages_for_object(self, messages, rate=None, transaction=None):
        for message in messages:
            message = copy.copy(message)

            # TODO: Fatal errors are not saved since there is no rate for them.
            if rate:
                message.rate = rate
            if transaction:
                message.transaction = transaction

            message.save()

    def __unicode__(self):
        return 'Parcel2GoBaseService'