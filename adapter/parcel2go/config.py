from django.conf import settings

from adapter.models import Provider, Adapter
from api.models import Currency, Country, Unit

class Parcel2GoConfig:
    def __init__(self, user):
        self.adapter = Adapter.objects.get(name='Parcel2Go')
        self.currency_gbp = Currency.objects.get(iso='GBP')
        self.country_gb = Country.objects.get(iso2='GB')
        self.user = user
        self.apiuser = user.apiuser
        self.settings = settings.ADAPTERS['Parcel2Go']
        self.base_url_parcel_service = self.settings['BASE_URL_PARCEL_SERVICE']
        self.base_url_track_service = self.settings['BASE_URL_TRACK_SERVICE']
        self.label_lifetime = settings.LABEL_LIFETIME

        # Setup country & units
        self.unit_kg = Unit.objects.get(abbreviation='kg')
        self.unit_cm = Unit.objects.get(abbreviation='cm')

        # SOAPAction Header is customized by the different services depending on the action to be executed.
        self.headers = {'content-type': 'text/xml; charset=utf-8', 'SOAPAction': 'https://v3.api.parcel2go.com/'}
        self.namespaces = {'ns0': 'http://schemas.xmlsoap.org/soap/envelope/', 'ns1':'https://v3.api.parcel2go.com/'}
        self.namespaces_results = {'ns0': 'https://v3.api.parcel2go.com/'}
        self.xmlns = 'https://v3.api.parcel2go.com/'

        self.parcel2go_api_key = self.apiuser.parcel2go_api_key
        self.parcel2go_card_reference = self.apiuser.parcel2go_card_reference

        self.production = self.apiuser.parcel2go_production

    def credentials_valid(self, transaction = None, *args, **kwargs):
        if self.parcel2go_api_key:
            return True
        else:
            if transaction:
                transaction.credentials_missing()
            return False

    def __unicode__(self):
        return 'Parcel2GoConfig'