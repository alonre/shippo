import xml.etree.ElementTree as ET

from django.conf import settings

from api.models import Currency, Country, Transaction

from base import Parcel2GoBaseService
from .. helpers.s3upload import S3Delivery
import elements

class Parcel2GoLabelService(Parcel2GoBaseService):
    def __init__(self, config, *args, **kwargs):
        super(Parcel2GoLabelService, self).__init__(config, *args, **kwargs)

    def get_label(self, transaction):
        if transaction.rate.is_purchasable(transaction, self.config.adapter) and self.config.credentials_valid(transaction=transaction):
            collection_request = self.generate_book_collection_request(transaction)
            transaction.object_api_message = str(collection_request)
            transaction.set_was_test(self.config.production)
            
            transaction.save(trigger_generation=False)

            response = self.send_request(collection_request, soap_action='BookCollection', transaction=transaction)

            if response is not None and len(response):
                transaction.tracking_number = self.get_tracking_number(response)
                transaction.label_url = self.get_and_upload_label(response, transaction)
                # TODO: changed pickup date?!
                transaction.object_status = transaction.OBJECT_STATUS_CHOICE.SUCCESS # TODO: better as validation function in model?
            else:
                transaction.object_status = transaction.OBJECT_STATUS_CHOICE.ERROR

            transaction.save(trigger_generation=False)

    def generate_book_collection_request(self, transaction):
        rate = transaction.rate
        shipment = rate.shipment

        # Create the elements
        soap_envelope = elements.soap_envelope()
        soap_body = elements.soap_body()
        book_collection = elements.book_collection_tag(self.config)
        api_key = elements.api_key(self.config)

        shipment_tag = elements.shipment_tag()
        parcels_tag = elements.parcels_tag()
        parcel_tag = elements.parcel_tag(shipment.parcel, self.config)
        value = elements.value(shipment.parcel, self.config)
        collection_address = elements.collection_address(shipment.address_from)
        delivery_address = elements.delivery_address(shipment.address_to)

        service_id = elements.service_id(rate.servicelevel.servicelevel_token)

        if transaction.pickup_date:
            collection = elements.collection(transaction.pickup_date)
        else:
            import datetime
            collection = elements.collection(datetime.datetime.now() + datetime.timedelta(days=1))

        if self.config.production:
            if self.config.parcel2go_card_reference:
                payment_method = elements.payment_method('StoredCard', self.config.parcel2go_card_reference)
            else:
                payment_method = elements.payment_method('Prepay')
        else:
            payment_method = elements.payment_method('DoNotPay')

        # Build the XML tree in-->out
        parcels_tag.append(parcel_tag)
        shipment_tag.append(collection_address)
        shipment_tag.append(delivery_address)
        shipment_tag.append(parcels_tag)
        shipment_tag.append(value)

        book_collection.append(shipment_tag)
        book_collection.append(api_key)
        book_collection.append(service_id)
        book_collection.append(collection)
        book_collection.append(payment_method)

        soap_body.append(book_collection)
        soap_envelope.append(soap_body)
       
        return soap_envelope

    def get_tracking_number(self, response):
        body = response.find('ns0:Body', namespaces=self.config.namespaces)
        label_response = body.find('ns1:BookCollectionResponse', namespaces=self.config.namespaces)
        label_result = label_response.find('ns1:BookCollectionResult', namespaces=self.config.namespaces)
        
        # using order number as tracking number makes tracking easier later on
        return label_result.find('ns1:OrderNumber', namespaces=self.config.namespaces).text
    
        #tracking_numbers = label_result.find('ns1:OrderNumber', namespaces=self.config.namespaces)
        #try:
        #    return tracking_numbers.find('ns1:string', namespaces=self.config.namespaces).text
        #except:
        #    return ''

    def get_and_upload_label(self, response, transaction):
        rate = transaction.rate
        if self.config.production:
            body = response.find('ns0:Body', namespaces=self.config.namespaces)
            label_response = body.find('ns1:BookCollectionResponse', namespaces=self.config.namespaces)
            label_result = label_response.find('ns1:BookCollectionResult', namespaces=self.config.namespaces)

            download_url = label_result.find('ns1:OrderLabelUrl', namespaces=self.config.namespaces).text

            extension = '.pdf'
            url = S3Delivery().download_and_upload(url=download_url, filename=str(transaction.object_id) + extension, contenttype='application/pdf', expiration_sec=self.config.label_lifetime)

        else:
            from django.templatetags.static import static
            #we provide a sample because P2G returns a HTML error page
            url = static('providers/label_samples/p2g_label_sample.pdf')

        return url

    def __unicode__(self):
        return 'Parcel2GoLabelService'