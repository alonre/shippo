from decimal import *
from sys import stderr
from suds.sax.element import Element
import xml.etree.ElementTree as ET 

from django.conf import settings

from adapter.models import Endpoint, Provider, Adapter, Zone, CountryZone, Servicelevel, Tier, Duration, Price
from api.models import Currency, Country, Rate

from base import Parcel2GoBaseService
import elements

class Parcel2GoRateService(Parcel2GoBaseService):
    def __init__(self, config, *args, **kwargs):
        super(Parcel2GoRateService, self).__init__(config, *args, **kwargs)

    def get_rates(self, shipment):
        if self.config.credentials_valid():
            quotes_request = self.generate_get_quotes_request(shipment)
            response = self.send_request(quotes_request, 'GetQuotes')
    
            if response is not None and len(response):
                self.messages = self.get_messages(response)
                rates = self.generate_rates(response, shipment, self.config.user)
            else:
                return None
    
            if rates is not None and len(rates) > 0:
                return rates
            else:
                return None

    def generate_get_quotes_request(self, shipment):
        # Create the elements
        soap_envelope = elements.soap_envelope()
        soap_body = elements.soap_body()
        get_quotes = elements.get_quotes_tag(self.config)
        api_key = elements.api_key(self.config)
        
        shipment_tag = elements.shipment_tag()
        parcels_tag = elements.parcels_tag()
        parcel_tag = elements.parcel_tag(shipment.parcel, self.config)
        value = elements.value(shipment.parcel, self.config)
        collection_address = elements.collection_address(shipment.address_from)
        delivery_address = elements.delivery_address(shipment.address_to)
        
        # Build the XML tree in-->out
        parcels_tag.append(parcel_tag)
        shipment_tag.append(collection_address)
        shipment_tag.append(delivery_address)
        shipment_tag.append(parcels_tag)
        shipment_tag.append(value)
        
        get_quotes.append(shipment_tag)
        get_quotes.append(api_key)

        soap_body.append(get_quotes)
        soap_envelope.append(soap_body)

        return soap_envelope

    def generate_rates(self, response, shipment, user):
        rates = []

        body = response.find('ns0:Body', namespaces=self.config.namespaces)
        quotes_response = body.find('ns1:GetQuotesResponse', namespaces=self.config.namespaces)
        quotes_result = quotes_response.find('ns1:GetQuotesResult', namespaces=self.config.namespaces)
        quotes = quotes_result.find('ns1:Quotes', namespaces=self.config.namespaces)
        
        if quotes is not None:
            for quote in quotes.findall('ns1:Quote', namespaces=self.config.namespaces):
                rate = Rate(shipment = shipment, object_purpose = shipment.object_purpose, adapter = self.config.adapter)
                rate.object_api_record = ET.tostring(quote)
                
                servicelevel_token = None
                servicelevel_pretty_name = None
    
                try:
                    servicelevel_token = 'P2G_' + quote.find('ns0:ServiceId', namespaces=self.config.namespaces_results).text
                    servicelevel_pretty_name = quote.find('ns0:ServiceName', namespaces=self.config.namespaces_results).text
                    rate.servicelevel = Servicelevel.objects.get(servicelevel_token = servicelevel_token) # ServiceId for Token-match (WITH PREFIX)
                except:
                    import sys
                    print >>sys.stderr, "TRIED SERVICELEVEL TOKEN, BUT NOT FUOND:" + str(servicelevel_token) + "Pretty NAme: " + str(servicelevel_pretty_name)
                    pass
    
                # we did not find the corresponding servicelevel in the DB, so we are falling back...
                if not rate.servicelevel:
                    try:
                        rate.servicelevel_name = quote.find('ns0:ServiceName', namespaces=self.config.namespaces_results).text
                    except:
                        rate.servicelevel_name = None
    
                    carrier_office = bool(quote.find('ns0:IsDropOffService', namespaces=self.config.namespaces_results).text)
                    if carrier_office:
                        rate.outbound_endpoint = Endpoint.objects.get(endpoint_name='carrier office')
                        rate.inbound_endpoint = rate.outbound_endpoint
    
    
                    min_days = int(quote.find('ns0:EstimatedMinTransitDays', namespaces=self.config.namespaces_results).text)
                    max_days = int(quote.find('ns0:EstimatedMaxTransitDays', namespaces=self.config.namespaces_results).text)
    
                    average_days = int(round((max_days+min_days)/2, 0)) # Generate average transit time based on min, max
    
                    rate.days = average_days
    
                    # Get the provider. Since the response is not uniformed (e.g., TNT vs. TNT Europost), we have to do some string operations first
                    provider_name = provider_name = quote.find('ns0:CourierName', namespaces=self.config.namespaces_results).text
                    provider_list = provider_name.split()
    
                    for item in provider_list:
                        try:
                            rate.provider = Provider.objects.get(Q(name__in=provider_list))
                            if rate.provider:
                                break
                        except:
                            rate.provider = Provider.objects.get(id=23)

                else:
                    rate.fill_from_servicelevel_object(rate.servicelevel)
                    try:
                        rate.duration = Duration.objects.get(servicelevel=rate.servicelevel)
                    except:
                        pass

                    if rate.duration:
                        rate.fill_from_duration_object(rate.duration)

                    rate.outbound_endpoint = rate.servicelevel.outbound_endpoint
                    rate.inbound_endpoint = rate.servicelevel.inbound_endpoint
                    rate.provider = rate.servicelevel.provider

                rate.amount = Decimal(quote.find('ns0:TotalPrice', namespaces=self.config.namespaces_results).text)
                rate.currency = self.config.currency_gbp

                rate.object_owner = user
                #append Parcel2Go note in order to make it clear for users
                if rate.servicelevel_name:
                    rate.servicelevel_name += ' (via Parcel2Go)'
                rates.append(rate)

        return rates

    def __unicode__(self):
        return 'Parcel2GoRateService'