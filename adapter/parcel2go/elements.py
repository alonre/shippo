from api.models import Unit
from suds.sax.element import Element
import xml.etree.ElementTree as ET

def soap_envelope():
    soap_envelope = Element('soap:Envelope')
    soap_envelope.set('xmlns:xsi', "http://www.w3.org/2001/XMLSchema-instance")
    soap_envelope.set('xmlns:xsd', "http://www.w3.org/2001/XMLSchema")
    soap_envelope.set('xmlns:soap', "http://schemas.xmlsoap.org/soap/envelope/")
    return soap_envelope

def soap_body():
    soap_body = Element('soap:Body')
    return soap_body

def get_quotes_tag(config):
    get_quotes_tag = Element('GetQuotes')
    get_quotes_tag.set('xmlns', config.xmlns)
    return get_quotes_tag

def book_collection_tag(config):
    book_collection_tag = Element('BookCollection')
    book_collection_tag.set('xmlns', config.xmlns)
    return book_collection_tag

def tracking_history_order(config):
    tracking_tag = Element('GetTrackingHistoryByOrderNo')
    tracking_tag.set('xmlns', config.xmlns)
    return tracking_tag

def order_no_tag(transaction):
    order_tag = Element('orderNo')
    order_tag.setText(transaction.tracking_number)
    return order_tag

def api_key(config):
    api_key = Element('apiKey')
    api_key.setText(config.parcel2go_api_key)
    return api_key

def shipment_tag():
    shipment_tag = Element('shipment')
    return shipment_tag

def parcels_tag():
    parcels_tag = Element('Parcels')
    return parcels_tag

def parcel_tag(parcel, config):
    parcel_tag = Element('Parcel')

    contents = Element('Contents').setText('Clothing')

    weight = Element('Weight').setText(str(parcel.get_weight(unit=config.unit_kg, decimal_places=1)))
    length = Element('Length').setText(str(parcel.get_length(unit=config.unit_cm, decimal_places=0)))
    width = Element('Width').setText(str(parcel.get_width(unit=config.unit_cm, decimal_places=0)))
    height = Element('Height').setText(str(parcel.get_height(unit=config.unit_cm, decimal_places=0)))

    parcel_tag.append(contents)
    parcel_tag.append(weight)
    parcel_tag.append(length)
    parcel_tag.append(width)
    parcel_tag.append(height)
    
    return parcel_tag

def value(parcel, config):
    value = parcel.get_parcel_value(currency=config.currency_gbp)

    if value:
        total_value = Element('TotalValue').setText(str(value))
    else:
        total_value = Element('TotalValue').setText('10')

    return total_value

def collection_address(address):
    collection_address_tag = address_tag('CollectionAddress', address)
    return collection_address_tag

def delivery_address(address):
    delivery_address_tag = address_tag('DeliveryAddress', address)
    return delivery_address_tag

def address_tag(sub_element, address):
    address_tag = Element(sub_element)

    addressee = Element('Addressee').setText(address.name)
    contact_telephone = Element('ContactTelephone').setText(address.phone)
    company_name = Element('CompanyName').setText(address.company)
    property_name = Element('PropertyNameOrNumber').setText(address.street_no)
    street = Element('Street').setText(address.street1)   # TODO: Street 2?
    locality = Element('Locality').setText(address.street2)
    town_or_city = Element('TownOrCity').setText(address.city)
    county = Element('County').setText(address.state)
    country_code = Element('CountryCode').setText(address.country.iso2)
    country_subdivision_code = Element('CountrySubdivisionCode').setText(address.state)
    postal_code = Element('PostalCode').setText(address.zip)

    address_tag.append(addressee)
    address_tag.append(contact_telephone)
    address_tag.append(company_name)
    address_tag.append(property_name)
    address_tag.append(street)
    address_tag.append(locality)
    address_tag.append(town_or_city)
    address_tag.append(county)
    address_tag.append(country_code)
    address_tag.append(country_subdivision_code)
    address_tag.append(postal_code)

    return address_tag

def service_id(servicelevel_token):
    #servicelevel P2G ID
    try:
        p2g_servicelevel_id = servicelevel_token
        if p2g_servicelevel_id:
            p2g_servicelevel_id = p2g_servicelevel_id[4:] #cut off "P2G_"
    except:
        import sys
        print >>sys.stderr, "TRIED TO GET SERVICELEVEL TOKEN, BUT NOT FOUND"
        pass
    
    service_id = Element('serviceId').setText(p2g_servicelevel_id) #int
    return service_id

def collection(date):
    import datetime
    collection_date = Element('collectionDate').setText(date.strftime('%Y-%m-%d')) #datetime
    return collection_date

def payment_method(type, reference = None):
    payment_method = Element('paymentMethod')
    payment_method_type = Element('PaymentMethodType').setText(type) #StoredCard, DoNotPay (=Test), Prepay, ShopWithReference
    payment_method.append(payment_method_type)
    if reference is not None:
        payment_method_reference = Element('Reference').setText(reference) #Debit Shippo Simon for StoredCard
    else:
        payment_method_reference = Element('Reference').setText('')
    payment_method.append(payment_method_reference)
    
    return payment_method