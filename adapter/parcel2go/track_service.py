from decimal import *
from sys import stderr
import datetime
        
import xml.etree.ElementTree as ET
from django.conf import settings

from adapter.models import Endpoint, Provider, Adapter, Zone, CountryZone, Servicelevel, Tier, Duration, Price
from api.models import Currency, Country, Rate, Location, Unit, Track

from base import Parcel2GoBaseService
import elements

class Parcel2GoTrackService(Parcel2GoBaseService):
    def __init__(self, config, *args, **kwargs):
        super(Parcel2GoTrackService, self).__init__(config, *args, **kwargs)

    def get_status(self, transaction):
        if self.config.credentials_valid():
            track_request = self.generate_track_request(transaction)
            response = self.send_request(request=track_request, soap_action='GetTrackingHistoryByOrderNo', transaction=None, is_tracking_request=True)

            if response is not None and len(response):
                track = self.generate_track(response, transaction)
            else:
                return None
    
            if track is not None:
                return track
            else:
                return None

    def generate_track_request(self, transaction):
        # Create the elements
        soap_envelope = elements.soap_envelope()
        soap_body = elements.soap_body()
        tracking_tag = elements.tracking_history_order(self.config)
        api_key = elements.api_key(self.config)
        order_no_tag = elements.order_no_tag(transaction)
        
        # Build the XML tree in-->out
        tracking_tag.append(api_key)
        tracking_tag.append(order_no_tag)
        soap_body.append(tracking_tag)
        soap_envelope.append(soap_body)

        return soap_envelope
    
    def element_exists(self, element, path):
        if ET.iselement(element.find(path, namespaces=self.config.namespaces)):
            return True
        else:
            return False
   
    def get_element_content(self, element, path):
        if self.element_exists(element, path):
            element_list = element.findall(path)
            return element.findall(path)[len(element_list)-1].text
        else:
            return ''
               
    def generate_track(self, response, transaction):
        # get the element with the latest timestamp
        latest_event = self.get_latest_tracking_event(response)
        if latest_event is not None:
            track = Track(transaction=transaction, provider=transaction.rate.provider, adapter=self.config.adapter, object_owner=self.config.user)
            latest_event_date = self.parse_datetimestamp(latest_event, './ns1:Timestamp')
            latest_event_description = self.get_element_content(latest_event, './ns1:Description')
            latest_event_details = self.get_element_content(latest_event, './ns1:Details')
            
            track.status_date = latest_event_date
            track.status = latest_event_description
            track.status_details = latest_event_details
            
            collection_event = self.get_collection_event(response)
            if collection_event is not None:
                track.pickup_date = self.parse_datetimestamp(collection_event, './ns1:Timestamp')
                
            delivery_event = self.get_delivery_event(response)
            if delivery_event is not None:
                track.delivery_date = self.parse_datetimestamp(delivery_event, './ns1:Timestamp')
                track.delivery_date_status = Track.DELIVERY_DATE_CHOICE.DELIVERED
    
            track.save()
            return track

    def get_latest_tracking_event(self, response):
        events = response.find('./ns0:Body/ns1:GetTrackingHistoryByOrderNoResponse/ns1:GetTrackingHistoryByOrderNoResult/ns1:Events', namespaces=self.config.namespaces)
        
        if events is not None and self.element_exists(events, './ns1:TrackingEvent'):
            latest_date = datetime.datetime(1970, 1, 1)
            latest_event = 0
            i = 0

            for event in events.findall('./ns1:TrackingEvent'):
                date = self.parse_datetimestamp(event, './ns1:Timestamp')
                if date > latest_date:
                    latest_event = i
                    latest_date = date
                i += 1

            event = events[latest_event]
        
            return event
        else:
            return None
    
    def get_tracking_event(self, response, keywords):
        events = response.find('./ns0:Body/ns1:GetTrackingHistoryByOrderNoResponse/ns1:GetTrackingHistoryByOrderNoResult/ns1:Events', namespaces=self.config.namespaces)
        found_event = None
        
        if self.element_exists(events, './ns1:TrackingEvent'):
            for event in events.findall('./ns1:TrackingEvent'):
                status = self.get_element_content(event, './ns1:Description').lower()
                
                for keyword in keywords:
                    if keyword in status:
                        found_event = event
            
        return found_event
    
    def get_collection_event(self, response):
        return self.get_tracking_event(response, ('collected', 'picked', ))
    
    def get_delivery_event(self, response):
        return self.get_tracking_event(response, ('delivered', ))
    
    def parse_datetimestamp(self, element, timestamp_path):
        try: # when parsing dates there is so much that can go wrong
            result = datetime
            if element is not None and self.element_exists(element, timestamp_path):
                timestamp_string = self.get_element_content(element, timestamp_path)
                result = datetime.datetime.strptime(timestamp_string[:19], "%Y-%m-%dT%H:%M:%S")
            return result
        except:
            return None

    def __unicode__(self):
        return 'Parcel2GoTrackService'