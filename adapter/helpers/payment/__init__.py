from decimal import *
from django.conf import settings
import stripe

from api.models import Currency, Country, Transaction
from adapter.models import Adapter

class Stripe:
    def charge_stripe(self, price, currency, *args, **kwargs):
        stripe_token = self.user.apiuser.billing.get_stripe_customer()
        if stripe_token:
            try:
                stripe_charge = stripe.Charge.create(
                    amount=price,
                    currency=currency.iso,
                    customer=stripe_token,
                    description='goshippo.com Postage (Shipping Label %s)' % self.transaction.rate.provider.name
                )
                return True
            except stripe.CardError, e:
                body = e.json_body
                err  = body['error']
                self.transaction.add_message(code=err['code'], text=err['message'], source=self.adapter_gadb, stack_trace=body)
                error = err['message']
            except (stripe.InvalidRequestError, stripe.AuthenticationError, stripe.APIConnectionError, stripe.StripeError) as e:
                self.transaction.add_message(code='payment_error', text = "Error when handling payment. Please contact info@goshippo.com with the Object ID.", stack_trace=e.json_body)
            except:
                print >>sys.stderr, "Error when creating or saving stripe customer id data for user " + apiuser.user.username
                error = 'There was a problem saving your credit card data. Please try again or contact info@goshippo.com for help. Sorry for the trouble!'
        return False
                    
        def calculate_price(self, *args, **kwargs):
            price = self.rate.get_amount_in_currency()
            fees = self.calculate_fees(price, currency)
            
            return round(fees + price, 2)
            
        def calculate_fees(self, amount, currency, *args, **kwargs):
            fee_fixed_currency = self.adapter_gadb.fee_fixed_currency
            fee_fixed = self.adapter_gadb.fee_fixed
            fee_relative = self.adapter_gadb.fee_relative
            
            fee_fixed_transaction = self.convert_amount(fee_fixed, fee_fixed_currency, currency)
            fee_relative_transaction = fee_relative * Decimal(amount)
            
            return fee_fixed_transaction + fee_relative_transaction
            
        def convert_amount(self, amount, currency_from, currency_to, *args, **kwargs):
            if not currency_from == currency_to: 
                #base_factor = currency_from.base.rate / currency_to.base.rate 
                #rate_from = currency_from.rate / currency_from.base.rate
                #rate_to = currency_to.rate / currency_to.base.rate
                #rate_total = (rate_to / rate_from) * base_factor
                #return Decimal(amount) * rate_total
                return Decimal(amount) * (currency_to.rate/currency_from.rate)
            else:
                return amount