from decimal import *
from django.conf import settings
import stripe

from api.models import Currency, Country, Transaction
from adapter.models import Adapter
from base import BasePaymentAdapter

class StripePaymentAdapter(BasePaymentAdapter):
    def __init__(self, adapter, transaction, *args, **kwargs):
        super(StripePaymentAdapter, self).__init__(adapter, transaction, *args, **kwargs)
        self.stripe_secret_key = settings.STRIPE_SECRET_KEY_PRODUCTION
        
    # must be implemented with this signature, but trigger payment via charge() of BasePaymentAdapter.
    # takes a final amount which already includes all fees.
    def execute_payment(self, final_amount, currency, *args, **kwargs):
        stripe_token = self.user.apiuser.billing.get_stripe_token()
        
        if stripe_token:
            try:
                stripe.api_key = self.stripe_secret_key
                stripe_charge = stripe.Charge.create(
                    amount=final_amount,
                    currency=self.currency.iso,
                    customer=stripe_token,
                    description=str(self.transaction.object_id)
                )
                return True
            except stripe.CardError as e:
                body = e.json_body
                err  = body['error']
                self.transaction.add_message(code=err['code'], text=err['message'], source=self.adapter, stack_trace=body, new_status=Transaction.OBJECT_STATUS_CHOICE.ERROR)
                error = err['message']
            except (stripe.InvalidRequestError, stripe.AuthenticationError, stripe.APIConnectionError, stripe.StripeError) as e:
                self.transaction.add_message(code='payment_error', text = "Error when handling payment. Please contact info@goshippo.com with the Object ID.", source=self.adapter, stack_trace=e.json_body, new_status=Transaction.OBJECT_STATUS_CHOICE.ERROR)
            except:
                import sys 
                e = sys.exc_info()[0]
                self.transaction.add_message(code='payment_error', text = "Error when handling payment. Please contact info@goshippo.com with the Object ID.", source=self.adapter, stack_trace=e, new_status=Transaction.OBJECT_STATUS_CHOICE.ERROR)
        return False