from decimal import *
from math import ceil
from django.conf import settings
import stripe

from api.models import Currency, Country, Transaction
from adapter.models import Adapter

class BasePaymentAdapter(object):
    def __init__(self, adapter, transaction, *args, **kwargs):
        self.adapter = adapter
        self.currency_usd = Currency.objects.get(iso='USD')
        self.transaction = transaction
        #self.currency = self.transaction.rate.currency
        self.currency = Currency.objects.get(iso=settings.STRIPE_CURRENCIES_ACCEPTED[0])
        self.user = transaction.object_owner

    def charge(self, *args, **kwargs):
        '''Method to trigger a payment. First calculates prices incl. fees, then charges the instantiated payment adapter.'''
        result = False

        final_amount = self.calculate_price()
        result = self.execute_payment(final_amount, self.currency)

        return result

    def calculate_price(self, *args, **kwargs):
        price = Decimal(self.transaction.rate.get_amount_in_currency(currency=self.currency))
        fees = Decimal(self.calculate_fees(price, self.currency))
        
        return int(round((fees + price) * 100, 0))

    def calculate_fees(self, amount, currency, *args, **kwargs):
        # TODO: adapter-specific AND Shippo fees
        fee_fixed_currency = self.adapter.fee_fixed_currency
        fee_fixed = self.adapter.fee_fixed
        fee_relative = self.adapter.fee_relative
        
        # OK here comes the trick.
        # (!!!) If we INCLUDE PAYMENT FEES so that WE DON'T HAVE TO PAY 'EM, we need to calculate: 
        # RELATIVE PAYMENT FEE = RELATIVE PAYMENT FEE x (1 + RELATIVE PAYMENT FEE), e.g., 0.029 x 1.029 = 0.029841,
        # else we would pay relative fees on the fees that we actually want to collect, which would result in 1/1.029 less revenue
        # + FIXED PAYMENT FEE x (1+RELATIVE PAYMENT FEE)
        # and then ceil() each one of them for the remainder (since the sequence continues forever but becomes insignificant for our amounts after two calculation rounds)
        # TODO: clean solution based on "ewige rente mit zinseszinseffekt" (? without the time component, of course)
        
        fee_relative_transaction = Decimal(Decimal(fee_relative) * Decimal(amount) * (1+Decimal(fee_relative)))
        fee_fixed_transaction = Decimal(self.convert_amount(fee_fixed, fee_fixed_currency, currency) * (1+Decimal(fee_relative)))
        
        fee_relative_transaction = Decimal(ceil(fee_relative_transaction * 100) / 100)
        fee_fixed_transaction = Decimal(ceil(fee_fixed_transaction * 100) / 100)
        
        fee_complete_transaction = Decimal(fee_relative_transaction + fee_fixed_transaction) 
        
        return fee_complete_transaction
        
    def convert_amount(self, amount, currency_from, currency_to, *args, **kwargs):
        # TODO: support for different base currencies
        if not currency_from == currency_to: 
            #base_factor = currency_from.base.rate / currency_to.base.rate 
            #rate_from = currency_from.rate / currency_from.base.rate
            #rate_to = currency_to.rate / currency_to.base.rate
            #rate_total = (rate_to / rate_from) * base_factor
            #return Decimal(amount) * rate_total
            return Decimal(amount) * (currency_to.rate/currency_from.rate)
        else:
            return amount