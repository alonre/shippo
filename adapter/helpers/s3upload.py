# -*- coding: utf-8 -*-
from boto.s3.connection import S3Connection
from boto.s3.key import Key
from django.conf import settings
import sys, base64, os
from urlparse import urlsplit
from os.path import basename
import requests as HTTPRequest

class S3Delivery:
	def __init__(self, *args, **kwargs):
		self.ACCESS_KEY = settings.AWS_ACCESS_KEY_ID_DELIVERY
		self.SECRET_KEY = settings.AWS_SECRET_ACCESS_KEY_DELIVERY
		self.BUCKET_NAME = settings.AWS_DELIVERY_BUCKET_NAME

	def save_and_upload(self, base64content, filename, contenttype, expiration_sec = 2592000):
		base64_label = base64.decodestring(base64content)
		filepath = self.save_to_tmp(base64_label, filename)
		url = self.upload_string_to_S3(filepath, filename, contenttype, expiration_sec)
		return url

	def download_and_upload(self, url, filename, contenttype, expiration_sec):
		base64_label = self.download_from_url(url)
		filepath = self.save_to_tmp(base64_label, filename)
		url = self.upload_string_to_S3(filepath, filename, contenttype, expiration_sec)
		return url

	def get_base_path(self):
		path = settings.BASE_DIR + "/tmp";
		if os.path.isdir(path) == False:
			os.mkdir(path)
		return path

	def save_to_tmp(self, base64_label, filename):
		path = self.get_base_path()
		local_label = path + '/' + filename
		fh = open(local_label, "wb")
		fh.write(base64_label)
		fh.close()
		return local_label

	def upload_string_to_S3(self, filepath, filename, contenttype, expiration_sec):
		connection = S3Connection(self.ACCESS_KEY, self.SECRET_KEY)
		bucket = connection.get_bucket(self.BUCKET_NAME)
		upload = Key(bucket)
		upload.key = filename
		upload.set_contents_from_filename(filepath)
		url = upload.generate_url(int(expiration_sec), method='GET')
		print >>sys.stderr, "AWS URL: " + url
		return url

	def url_to_name(self, url):
		return basename(urlsplit(url)[2])

	def download_from_url(self, url):
		local_label = self.url_to_name(url)
		req = HTTPRequest.get(url)
		if req.headers and 'content-disposition' in req.headers:
			local_label = req.headers['content-disposition'].split('filename=')[1]
			if local_label[0] == '"' or local_label[0] == "'":
				local_label = local_label[1:-1]
		return req.content