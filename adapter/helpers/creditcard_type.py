def get_cc_type(cc_number):
	cc_initial_1 = int(cc_number[:1])
	cc_initial_2 = int(cc_number[:2])
	cc_initial_3 = int(cc_number[:3])
	cc_initial_4 = int(cc_number[:4])
	if 4 == cc_initial_1: cc_type = 'V'
	if (51 <= cc_initial_2) and (cc_initial_2 <= 55): cc_type = 'M'
	if 34 == cc_initial_2: cc_type = 'A'
	if 37 == cc_initial_2: cc_type = 'A'
	if (300 <= cc_initial_3) and (cc_initial_3 <= 305): cc_type = 'B'
	if 6011 == cc_initial_4: cc_type = 'N'
	if 65 == cc_initial_2: cc_type = 'N'
	if 36 == cc_initial_2: cc_type = 'D'
	if 38 == cc_initial_2: cc_type = 'D'
	return cc_type