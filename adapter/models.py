from django.db import models
from model_utils import Choices
#from api.models import Country
#from api.models import Currency, Country, Language, Unit, Parcel, Address, Shipment
from django.conf import settings
from urlparse import urljoin

class EndpointMixIn(models.Model):
    # Choices
    OBJECT_TYPE_CHOICE = Choices('RESIDENTIAL', 'COMMERCIAL', 'ANY')

    # Data
    endpoint_name = models.CharField(max_length=50)
    endpoint_type = models.CharField(max_length=20, choices=OBJECT_TYPE_CHOICE, default=OBJECT_TYPE_CHOICE.ANY)

    class Meta:
        abstract = True

class Endpoint(EndpointMixIn):
    """Defines an Endpoint, a physical location where a Parcel can be submitted or received."""

    # Object-related Metadata
    object_created = models.DateTimeField(auto_now_add=True, editable=True)
    object_updated = models.DateTimeField(auto_now=True, editable=True)
    
    def __unicode__(self):
        return '%s (%s)' % (self.endpoint_name, self.endpoint_type)
        
    def natural_key(self):
        return self.__unicode__()
        
class Provider(models.Model):
    """Defines a provider, which is a company executing Shipments."""

    # Data
    name = models.CharField(max_length=100)
    website = models.URLField(max_length=200)
    image_filename = models.CharField(max_length=50, blank=True)
    
    # Object-related Metadata
    object_created = models.DateTimeField(auto_now_add=True, editable=True)
    object_updated = models.DateTimeField(auto_now=True, editable=True)
    
    def get_image_75_url(self, *args, **kwargs):
        return self.get_image_url(width=75)
        
    def get_image_200_url(self, *args, **kwargs):
        return self.get_image_url(width=200)
    
    def get_image_url(self, width, *args, **kwargs):
        if self.image_filename:
            return urljoin(settings.PROVIDER_IMAGE_URL, str(width) + '/' + self.image_filename)
        else:
            return None
    
    def __unicode__(self):
        return '%s' % (self.name)
        
    def natural_key(self):
        return self.__unicode__()
        
class Adapter(models.Model):
    """An Adapter is a sending and receiving data to/from APIs, such as GADB or USPS API."""

    enabled = models.BooleanField(default=False)
    name = models.CharField(max_length=100)
    provider_served = models.ManyToManyField(Provider, blank=True, null=True)
    country_served = models.ManyToManyField('api.Country', blank=True, null=True)
    # For Adapters that have extra fees (such as Endicia for some Servicelevels)
    fee_fixed = models.DecimalField(max_digits=8, decimal_places=4, default=0.0)
    fee_fixed_currency = models.ForeignKey('api.Currency', blank=True, null=True)
    fee_relative = models.DecimalField(max_digits=5, decimal_places=4, default=0.0)

    def __unicode__(self):
        return self.name

    def natural_key(self):
        return self.__unicode__()

class Zone(models.Model):
    """Defines a Zone, which is a bundle of countries used by Providers to calculate Prices."""

    # Choices
    OBJECT_TYPE_CHOICE = Choices('PROVIDER-DEFINED', 'DURATION', 'CUSTOM-DEFINED', 'ANY')

    # Data
    name = models.CharField(max_length=200)
    provider = models.ForeignKey(Provider)
    type = models.CharField(max_length=50, choices=OBJECT_TYPE_CHOICE, default=OBJECT_TYPE_CHOICE.ANY)
    
    # Object-related Metadata
    object_created = models.DateTimeField(auto_now_add=True, editable=True)
    object_updated = models.DateTimeField(auto_now=True, editable=True)
    
    def __unicode__(self):
        return '%s (%s) (%s)' % (self.name, self.provider.name, self.type)
        
    def natural_key(self):
        return self.__unicode__()
        
class CountryZone(models.Model):
    """Associates Countries with Zones, based on the outbound Country. Workaround because Django does not support this type of relationship natively."""
    
    # Data
    outbound_country = models.ForeignKey('api.Country', related_name='country_zone_outbound_country_relation', null=True, blank=True)
    zone = models.ForeignKey(Zone, related_name='country_zone_zone_relation')
    country = models.ForeignKey('api.Country', related_name='country_zone_country_relation', null=True, blank=True)
    
    # Object-related Metadata
    object_created = models.DateTimeField(auto_now_add=True, editable=True)
    object_updated = models.DateTimeField(auto_now=True, editable=True)
    
    def __unicode__(self):
        if self.country:
            return '%s of %s (from %s to %s) (Type: %s)' % (self.zone.name, self.zone.provider.name, self.outbound_country.name, self.country.name, self.zone.type)
        else:
            return '%s of %s (from %s to anywhere) (Type: %s)' % (self.zone.name, self.zone.provider.name, self.outbound_country.name, self.zone.type)
        
    def natural_key(self):
        return self.__unicode__()
        
class ServicelevelMixIn(models.Model):
    servicelevel_name = models.CharField(max_length=200)
    servicelevel_terms = models.TextField(blank=True)  
    
    insurance = models.BooleanField(default=False)
    insurance_amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    trackable = models.BooleanField(default=False)
    delivery_attempts = models.IntegerField(null=True, blank=True)
    arrives_by = models.TimeField(null=True, blank=True)
    available_shippo = models.BooleanField(default=False)   
    
    servicelevel_token = models.CharField(max_length=200, blank=True)

    class Meta:
        abstract = True
        
class Servicelevel(ServicelevelMixIn):
    """Defines a given level of service for Shipments."""
    
    # Data
    provider = models.ForeignKey(Provider)
    insurance_currency = models.ForeignKey('api.Currency', null=True, blank=True, related_name='servicelevel_insurance_currency_relation')
    
    # Endpoints
    outbound_endpoint = models.ForeignKey(Endpoint, related_name='servicelevel_outbound_endpoint_relation', null=True, blank=True)
    inbound_endpoint = models.ForeignKey(Endpoint, related_name='servicelevel_inbound_endpoint_relation', null=True, blank=True)

    # Object-related Metadata
    object_created = models.DateTimeField(auto_now_add=True, editable=True)
    object_updated = models.DateTimeField(auto_now=True, editable=True)
    
    def __unicode__(self):
        return '%s (Provider: %s)' % (self.servicelevel_name, self.provider.name)
        
    def natural_key(self):
        return self.__unicode__()
        
class TierMixIn(models.Model):
    tier_name = models.CharField(max_length=50)
    
    class Meta:
        abstract = True        
        
class Tier(TierMixIn):
    """Defines how big and how heavy parcels may be for one price level."""
    
    # Data
    provider = models.ForeignKey(Provider)
    max_length = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    max_width = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    max_height = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    max_length_plus_height = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    max_length_plus_width_plus_height = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    max_length_plus_girth = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    min_length = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    min_width = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    min_height = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    min_length_plus_height = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    min_length_plus_width_plus_height = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    min_length_plus_girth = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    distance_unit = models.ForeignKey('api.Unit', related_name='tier_distance_unit_relation')
    max_weight = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    min_weight = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    mass_unit = models.ForeignKey('api.Unit', related_name='tier_mass_unit_relation')

    # Object-related Metadata
    object_created = models.DateTimeField(auto_now_add=True, editable=True)
    object_updated = models.DateTimeField(auto_now=True, editable=True)

    def __unicode__(self):
        return '%s (Provider: %s)' % (self.tier_name, self.provider.name)

    def natural_key(self):
        return self.__unicode__()

class DurationMixIn(models.Model):
    days = models.IntegerField(null=True)
    duration_terms = models.TextField(blank=True)

    class Meta:
        abstract = True

class Duration(DurationMixIn):
    """Defines how long a Shipment takes if sent in a Servicelevel from a Country or Zone to a Country or Zone."""

    # TODO: Integrity check: does zone and servicelevel belong to same provider?
    # Data
    servicelevel = models.ForeignKey(Servicelevel)
    outbound_country = models.ForeignKey('api.Country', related_name='duration_outbound_country_relation', blank=True, null=True)
    outbound_zone = models.ForeignKey(Zone, related_name='duration_outbound_zone_relation', blank=True, null=True)
    inbound_country = models.ForeignKey('api.Country', related_name='duration_inbound_country_relation', blank=True, null=True)
    inbound_zone = models.ForeignKey(Zone, related_name='duration_inbound_zone_relation', blank=True, null=True)

    # Object-related Metadata
    object_created = models.DateTimeField(auto_now_add=True, editable=True)
    object_updated = models.DateTimeField(auto_now=True, editable=True)

    def __unicode__(self):
        return '%s day(s) as %s (Provider: %s)' % (self.days, self.servicelevel.servicelevel_name, self.servicelevel.provider.name)

    def natural_key(self):
        return self.__unicode__()

class PriceMixIn(models.Model):
    amount = models.DecimalField(max_digits=8, decimal_places=2)

    class Meta:
        abstract = True

class Price(PriceMixIn):
    """Defines what it costs to send a Parcel which is in a specific Tier from one Country/Zone
        to another Country/Zone at a specific Servicelevel from/to specific Endpoints."""

    # TODO: Integrity check: does zone and servicelvel belong to same provider?
    # Data
    provider = models.ForeignKey(Provider)
    tier = models.ForeignKey(Tier)
    servicelevel = models.ForeignKey(Servicelevel)
    outbound_country = models.ForeignKey('api.Country', related_name='price_outbound_country_relation', null=True, blank=True)
    outbound_zone = models.ForeignKey(Zone, related_name='price_outbound_zone_relation', null=True, blank=True)
    inbound_country = models.ForeignKey('api.Country', related_name='price_inbound_country_relation', null=True, blank=True)
    inbound_zone = models.ForeignKey(Zone, related_name='price_inbound_zone_relation_relation', null=True, blank=True)
        
    outbound_endpoint = models.ForeignKey(Endpoint, related_name='price_outbound_endpoint_relation', null=True, blank=True)
    inbound_endpoint = models.ForeignKey(Endpoint, related_name='price_inbound_endpoint_relation', null=True, blank=True)
    
    currency = models.ForeignKey('api.Currency', related_name='price_currency_relation')
    
    # Object-related Metadata
    object_created = models.DateTimeField(auto_now_add=True, editable=True)
    object_updated = models.DateTimeField(auto_now=True, editable=True)
   
    def __unicode__(self):
        return '%s %s with %s as %s' % (self.currency.iso, self.amount, self.provider.name, self.tier.tier_name)
        
    def natural_key(self):
        return self.__unicode__()