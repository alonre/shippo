from api.models import Unit

def shipper(request, apiuser, address_from):
    # the shipper address is NOT the account owner's address, but the "responsible" party of the shipment; in 99% of cases = ShipFrom
    # return shipments usually go to this address
    generate_address(request.RequestedShipment.Shipper, apiuser, address_from, shipper=True)

def origin(request, apiuser, address):
    generate_address(request.RequestedShipment.Origin, apiuser, address)

def recipient(request, apiuser, address):
    generate_address(request.RequestedShipment.Recipient, apiuser, address)

def package(request, parcel, config):
    package_weight = request.create_wsdl_object_of_type('Weight')
    package_weight.Value = parcel.get_weight(config.unit_lb, 1)
    package_weight.Units = 'LB'

    package_dimensions = request.create_wsdl_object_of_type('Dimensions')
    package_dimensions.Length = int(parcel.get_length(config.unit_in, 0))
    package_dimensions.Width = int(parcel.get_width(config.unit_in, 0))
    package_dimensions.Height = int(parcel.get_height(config.unit_in, 0))
    package_dimensions.Units = 'IN'

    package_value = value(request, parcel, config.currency_usd)

    #rate_request.RequestedShipment.InternationalDetail.CustomsValue = package_value
    #rate_request.RequestedShipment.InternationalDetail.CustomsValue.Amount = shipment.parcel.get_parcel_value(currency = currency_usd)

    package = request.create_wsdl_object_of_type('RequestedPackageLineItem')

    package.Weight = package_weight
    package.Dimensions = package_dimensions

    package.PhysicalPackaging = 'BOX'
    package.InsuredValue = package_value

    package.GroupPackageCount = 1

    return package

def value(request, parcel, currency):
    package_value = request.create_wsdl_object_of_type('Money')

    package_value.Currency = currency.iso

    if parcel.value_amount > 0:
        package_value.Amount = parcel.get_parcel_value(currency = config.currency_usd)
    else:
        package_value.Amount = 10.0

    return package_value

def generate_address(element, apiuser, address, shipper=False):
    if address.name:
        element.Contact.PersonName = address.name
    else:
        element.Contact.PersonName = apiuser.user.first_name + ' ' + apiuser.user.last_name
    element.Contact.CompanyName = address.company
    element.Contact.PhoneNumber = address.get_cleaned_phone(max_digits=15)
    if address.email:
        element.Contact.EMailAddress = address.email
    else:
        element.Contact.EMailAddress = address.email
    element.Address.StreetLines = [address.street_no + " " + address.street1, address.street2]
    element.Address.City = address.city
    element.Address.PostalCode = address.zip
    if address.state and len(address.state) <= 2:   # State abbreviation may only be 2 characters long for FedEx API
        element.Address.StateOrProvinceCode = address.state
    else:
        element.Address.StateOrProvinceCode = ''
    if address.country:
        element.Address.CountryCode = address.country.iso2
        element.Address.CountryName = address.country.name
    if not shipper:
        element.Address.Residential = address.residential

def payment(request, apiuser, account_number, address_from):
    request.RequestedShipment.ShippingChargesPayment.PaymentType = 'SENDER' # RECIPIENT, SENDER or THIRD_PARTY
    payor = request.create_wsdl_object_of_type('Party')
    payor.AccountNumber = account_number
    if apiuser.address:
        address = apiuser.address
    else:
        address = address_from
    generate_address(payor, apiuser, address, shipper=True)

    request.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty = payor

def customs_clearance_detail(request, parcel, address, config):
    customs_clearance_element = request.create_wsdl_object_of_type('CustomsClearanceDetail')

    customs_value = request.create_wsdl_object_of_type('Money')

    if parcel.value_currency:
        customs_value.Currency = parcel.value_currency.fedex_iso
    else:
        customs_value.Currency = config.currency_usd.fedex_iso

    if parcel.value_amount:
        customs_value.Amount = parcel.value_amount
    else:
        customs_value.Amount = 10.0

    customs_clearance_element.CustomsValue = customs_value
    customs_clearance_element.ClearanceBrokerage = 'BROKER_UNASSIGNED'
    customs_clearance_element.DocumentContent = 'NON_DOCUMENTS'
    customs_clearance_element.FreightOnValue = 'CARRIER_RISK'
    customs_clearance_element.DutiesPayment = duties_payment(request, config.apiuser, address, config)
    customs_clearance_element.Commodities = []
    customs_clearance_element.Commodities.append(commodity(request, parcel, address, config))

    request.RequestedShipment.CustomsClearanceDetail = customs_clearance_element

def commodity(request, parcel, address, config):
    commodity_element = request.create_wsdl_object_of_type('Commodity')
    commodity_element.NumberOfPieces = 1
    commodity_element.Quantity = 1
    commodity_element.QuantityUnits = 'Pieces'
    commodity_element.CountryOfManufacture = address.country.iso2
    commodity_element.Description = "Various Personal Gifts"

    weight = request.create_wsdl_object_of_type('Weight')
    weight.Value = parcel.get_weight(config.unit_lb, 1)
    weight.Units = 'LB'

    commodity_element.Weight = weight
    commodity_element.CustomsValue = value(request, parcel, config.currency_usd)
    commodity_element.UnitPrice = value(request, parcel, config.currency_usd)

    #measures = request.create_wsdl_object_of_type('Measure')
    #measures.Quantity = 1
    #measures.Units = 'Pieces'
    #commodity_element.AdditionalMeasures = measures

    return commodity_element

def duties_payment(request, apiuser, address_from, config):
    payment_element = request.create_wsdl_object_of_type('Payment')
    payment_element.PaymentType = 'SENDER'

    payor = request.create_wsdl_object_of_type('Party')
    payor.AccountNumber = config.fedex_account
    if apiuser.address:
        address = apiuser.address
    else:
        address = address_from
    generate_address(payor, apiuser, address, shipper=True)

    payment_element.Payor.ResponsibleParty = payor

    return payment_element