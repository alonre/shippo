import requests
import copy
import xml.etree.ElementTree as ET

from adapter.models import Zone, CountryZone
from api.models import Message
import elements

#import logging
#logging.basicConfig(level=logging.INFO)
#logging.getLogger('suds.client').setLevel(logging.DEBUG)
#logging.getLogger('suds.transport').setLevel(logging.DEBUG)
#logging.getLogger('suds.xsd.schema').setLevel(logging.DEBUG)
#logging.getLogger('suds.wsdl').setLevel(logging.DEBUG)

class FedExBaseService(object):
    def __init__(self, config, *args, **kwargs):
        self.config = config

    def send_request(self, request, transaction=None):
        request.send_request()

        if transaction:
            transaction.object_api_response += str(request.response)
            transaction.save(trigger_generation=False)
            self.messages = self.get_messages(request.response)
            self.save_messages_for_object(messages=self.messages, transaction=transaction) # TODO: for rates as well

        return self.check_for_error(request.response)

    def check_for_error(self, response):
        if response.HighestSeverity == 'SUCCESS' or response.HighestSeverity == 'NOTE' or response.HighestSeverity == 'WARNING':
            return response
        else:
            return None # QUESTIONABLE

    def get_messages(self, response):
        messages = []

        if not response.HighestSeverity == 'SUCCESS':
            for notification in response.Notifications:
                message = Message(source=self.config.adapter)

                if notification.Severity is not None:
                    message.text = notification.Severity
                    message.text += ': '

                if notification.Code is not None:
                    message.code = notification.Code

                if notification.Message is not None:
                    message.text += notification.Message

                messages.append(message)

        return messages

    def save_messages_for_object(self, messages, rate=None, transaction=None):
        for message in messages:
            message = copy.copy(message)

            if rate:
                message.rate = rate
            if transaction:
                message.transaction = transaction

            message.save()

    def __unicode__(self):
        return 'FedExBaseService'