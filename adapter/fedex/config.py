from django.conf import settings
from fedex.config import FedexConfig as FC

from adapter.models import Provider, Adapter
from api.models import Currency, Country, Unit

class FedExConfig:
    def __init__(self, user):
        self.provider = Provider.objects.get(name='FedEx')
        self.adapter = Adapter.objects.get(name='FedEx')
        self.user = user
        self.apiuser = user.apiuser
        self.settings = settings.ADAPTERS['FedEx']
        self.label_lifetime = settings.LABEL_LIFETIME
        self.wsdl_path = self.settings['WSDL_PATH']
        
        # Setup country & units
        self.country_us = Country.objects.get(iso2='US')
        self.currency_usd = Currency.objects.get(iso='USD')
        self.unit_lb = Unit.objects.get(abbreviation='lb')
        self.unit_in = Unit.objects.get(abbreviation='in')

        self.fedex_account = self.apiuser.fedex_account
        self.fedex_meter = self.apiuser.fedex_meter

        if self.apiuser.fedex_production:
            self.fedex_key = self.settings['PRODUCTION']['KEY']
            self.fedex_password = self.settings['PRODUCTION']['PASSWORD']
            self.production = True
        else:
            self.fedex_key = self.settings['TEST']['KEY']
            self.fedex_password = self.settings['TEST']['PASSWORD']
            self.production = False
            
    def credentials_valid(self, transaction = None, *args, **kwargs):
        if self.fedex_account and self.fedex_meter:
            return True
        else:
            if transaction:
                transaction.credentials_missing()
            return False

    def get_config_object(self, *args, **kwargs):
        config_object = FC(key=self.fedex_key, password=self.fedex_password, account_number=self.fedex_account, meter_number=self.fedex_meter,
                                    use_test_server=not self.production, wsdl_path=self.wsdl_path)
        return config_object

    def __unicode__(self):
        return 'FedExConfig'