import xml.etree.ElementTree as ET

from fedex.services.ship_service import FedexProcessShipmentRequest
from django.conf import settings

from api.models import Currency, Country, Transaction

from base import FedExBaseService
from .. helpers.s3upload import S3Delivery
import elements

class FedExLabelService(FedExBaseService):
    def __init__(self, config, *args, **kwargs):
        super(FedExLabelService, self).__init__(config, *args, **kwargs)

    def get_label(self, transaction):
        if transaction.rate.is_purchasable(transaction, self.config.adapter) and self.config.credentials_valid(transaction=transaction):
            shipment_request = self.generate_shipment_request(transaction.rate)
            transaction.object_api_message = str(shipment_request)
            transaction.set_was_test(self.config.production)
        
            transaction.save(trigger_generation=False)
    
            response = self.send_request(shipment_request, transaction=transaction)
    
            if response is not None and len(response):
                transaction.tracking_number = self.get_tracking_number(response)
                transaction.label_url = self.get_and_upload_label(response, transaction)
                transaction.object_status = transaction.OBJECT_STATUS_CHOICE.SUCCESS
            else:
                transaction.object_status = transaction.OBJECT_STATUS_CHOICE.ERROR

            transaction.save(trigger_generation=False)

    def generate_shipment_request(self, rate):
        shipment = rate.shipment
        international = False
        if not shipment.address_from.country == shipment.address_to.country:
            international = True

        shipment_request = FedexProcessShipmentRequest(self.config.get_config_object())

        # REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER or STATION
        shipment_request.RequestedShipment.DropoffType = 'REGULAR_PICKUP'
        shipment_request.RequestedShipment.ServiceType = self.get_service_type(rate.object_api_record)
        shipment_request.RequestedShipment.PackagingType = 'YOUR_PACKAGING'

        elements.shipper(shipment_request, self.config.apiuser, shipment.address_from)
        elements.origin(shipment_request, self.config.apiuser, shipment.address_from)
        elements.recipient(shipment_request, self.config.apiuser, shipment.address_to)

        #include estimated duties and taxes in rate quote, can be ALL or NONE
        shipment_request.RequestedShipment.EdtRequestType = 'NONE'  # TBD

        elements.payment(shipment_request, self.config.apiuser, self.config.fedex_account, shipment.address_from)

        if international:
            elements.customs_clearance_detail(shipment_request, shipment.parcel, shipment.address_from, self.config)

        shipment_request.RequestedShipment.LabelSpecification.LabelFormatType = 'COMMON2D'
        shipment_request.RequestedShipment.LabelSpecification.ImageType = 'PDF'
        shipment_request.RequestedShipment.LabelSpecification.LabelStockType = 'PAPER_LETTER'
        shipment_request.RequestedShipment.LabelSpecification.LabelPrintingOrientation = 'BOTTOM_EDGE_OF_TEXT_FIRST'

        package = elements.package(shipment_request, shipment.parcel, self.config)
        shipment_request.add_package(package)

        return shipment_request

    def get_service_type(self, object_api_record):
        service_type_match = 'ServiceType = "'
        packaging_type_match = '"\n   PackagingType'
        index_begin = object_api_record.find(service_type_match) + len(service_type_match)
        index_end = object_api_record.find(packaging_type_match)

        return object_api_record[index_begin:index_end]

    def get_tracking_number(self, response):
        return response.CompletedShipmentDetail.CompletedPackageDetails[0].TrackingIds[0].TrackingNumber

    def get_and_upload_label(self, response, transaction):
        base64_label = response.CompletedShipmentDetail.CompletedPackageDetails[0].Label.Parts[0].Image
        extension = '.pdf'
        url = S3Delivery().save_and_upload(base64content = base64_label, filename = str(transaction.object_id) + extension, contenttype = 'application/pdf', expiration_sec = self.config.label_lifetime)
        return url

    def __unicode__(self):
        return 'FedExLabelService'