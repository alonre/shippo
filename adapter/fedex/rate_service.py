from decimal import *
from sys import stderr

from fedex.services.rate_service import FedexRateServiceRequest
from django.conf import settings

from adapter.models import Endpoint, Provider, Adapter, Zone, CountryZone, Servicelevel, Tier, Duration, Price
from api.models import Currency, Country, Rate

from base import FedExBaseService
import elements

class FedExRateService(FedExBaseService):
    def __init__(self, config, *args, **kwargs):
        super(FedExRateService, self).__init__(config, *args, **kwargs)

    def get_rates(self, shipment):
        if self.config.credentials_valid():
            rate_request = self.generate_rate_request(shipment)
            response = self.send_request(rate_request)
    
            if response is not None and len(response):
                self.messages = self.get_messages(response)
                rates = self.generate_rates(response, shipment, self.config.user)
            else:
                return None
    
            if rates is not None and len(rates) > 0:
                return rates
            else:
                return None

    def generate_rate_request(self, shipment):
        rate_request = FedexRateServiceRequest(self.config.get_config_object())

        rate_request.RequestedShipment.DropoffType = None
        rate_request.RequestedShipment.ServiceType = None
        rate_request.RequestedShipment.PackagingType = 'YOUR_PACKAGING'

        elements.shipper(rate_request, self.config.apiuser, shipment.address_from)         # TODO: sobald User Accuont mehr Details hat (Adrfesse usw. des Users ) --> zwischen Sender und Origin diskriminieren
        elements.origin(rate_request, self.config.apiuser, shipment.address_from)
        elements.recipient(rate_request, self.config.apiuser, shipment.address_to)

        #include estimated duties and taxes in rate quote, can be ALL or NONE
        rate_request.RequestedShipment.EdtRequestType = 'NONE'
        rate_request.RequestedShipment.ShippingChargesPayment.PaymentType = 'SENDER' #'THIRD_PARTY'

        package = elements.package(rate_request, shipment.parcel, self.config)

        rate_request.add_package(package)

        return rate_request

    def generate_rates(self, response, shipment, user):
        rates = []
        
        # there might be no rate_reply_detail if an error is returned by the API.
        try:
            for rate_reply_detail in response.RateReplyDetails:
                rate = Rate(shipment = shipment, adapter = self.config.adapter, provider=self.config.provider)
                rate.object_api_record = str(rate_reply_detail)
    
                rate.servicelevel = Servicelevel.objects.get(servicelevel_token = rate_reply_detail.ServiceType)
                rate.duration = Duration.objects.get(servicelevel = rate.servicelevel)
    
                rate.currency = Currency.objects.get(fedex_iso=rate_reply_detail.RatedShipmentDetails[0].ShipmentRateDetail.TotalNetCharge.Currency)
                rate.amount = Decimal(rate_reply_detail.RatedShipmentDetails[0].ShipmentRateDetail.TotalNetCharge.Amount)
    
                rate.fill_from_duration_object(rate.duration)
    
                # overwrite insurance details if parcel is worth more than $100 or it's not an intra-US shipment
                if shipment.parcel.get_parcel_value(currency = self.config.currency_usd) > 100:
                    rate.servicelevel.insurance_amount = shipment.parcel.value_amount
                    rate.servicelevel.insurance_currency = shipment.parcel.value_currency
                else:
                    rate.servicelevel.insurance_amount = 100.00
                    rate.servicelevel.insurance_currency = self.config.currency_usd
    
                rate.fill_from_servicelevel_object(rate.servicelevel)
                    
                rate.outbound_endpoint = self.get_endpoint(shipment.address_from)
                rate.inbound_endpoint = self.get_endpoint(shipment.address_to)
                    
                rate.object_owner = user
                rates.append(rate)
        except:
            pass
            
        return rates

    
    def get_endpoint(self, address, *args, **kwargs):
        if address.residential:
            return Endpoint.objects.get(endpoint_name='door', endpoint_type='RESIDENTIAL')
        else:
            return Endpoint.objects.get(endpoint_name='door', endpoint_type='COMMERCIAL')
    
    def __unicode__(self):
        return 'FedExRateService'