from decimal import *
from sys import stderr

import xml.etree.ElementTree as ET
from django.conf import settings

from adapter.models import Endpoint, Provider, Adapter, Zone, CountryZone, Servicelevel, Tier, Duration, Price
from api.models import Currency, Country, Rate

from base import EndiciaBaseService
import elements

class EndiciaUserService(EndiciaBaseService):
    def __init__(self, config, *args, **kwargs):
        super(EndiciaUserService, self).__init__(config, *args, **kwargs)

    def get_account_status(self):
        status_request = self.generate_status_request()
        request = {'accountStatusRequestXML': ET.tostring(status_request)}
        response = self.send_request(data=request, xml_url=self.config.account_url)

        return self.get_status(response)

    def generate_status_request(self):
        #build XML tree
        status_request = ET.Element('AccountStatusRequest')
        requester_id = ET.SubElement(status_request, 'RequesterID')
        requester_id.text = self.config.requester_id
        request_id = ET.SubElement(status_request, 'RequestID')
        request_id.text = 'ASR-Shippo'
        certified_intermediary = ET.SubElement(status_request, 'CertifiedIntermediary')
        account_id = ET.SubElement(certified_intermediary, 'AccountID')
        account_id.text = self.config.account_id
        pass_phrase = ET.SubElement(certified_intermediary, 'PassPhrase')
        pass_phrase.text = self.config.passphrase
        
        return status_request
    
    def get_status(self, response):
        if response.find(self.config.prefix + 'Status').text == '0':
            account = results.find(self.config.prefix + 'CertifiedIntermediary')
            return account.find(self.config.prefix + 'AccountStatus').text
        else:
            return results.find(self.config.prefix + 'Status').text

    def change_passphrase(self, new_passphrase):
        passphrase_request = self.generate_passphrase_request(new_passphrase)
        request = {'changePassPhraseRequestXML': ET.tostring(passphrase_request)}
        response = self.send_request(data=request, xml_url=self.config.passphrase_url)
        
        # Check for errors has already been done in base
        if response:
            return True
        else:
            return False
    
    def generate_passphrase_request(self, new_passphrase):
        passphrase_request = ET.Element('ChangePassPhraseRequest')
        requester_id = ET.SubElement(passphrase_request, 'RequesterID')
        requester_id.text = self.config.requester_id
        request_id = ET.SubElement(passphrase_request, 'RequestID')
        request_id.text = 'CPR-Shippo'
        certified_intermediary = ET.SubElement(passphrase_request, 'CertifiedIntermediary')
        account_id = ET.SubElement(certified_intermediary, 'AccountID')
        account_id.text = self.config.account_id
        pass_phrase = ET.SubElement(certified_intermediary, 'PassPhrase')
        pass_phrase.text = self.config.passphrase
        new_pass_phrase = ET.SubElement(passphrase_request, 'NewPassPhrase')
        new_pass_phrase.text = new_passphrase

        return passphrase_request

    def refill_balance(self, amount):
        refill_request = self.refill_balance_request(amount)
        request = {'recreditRequestXML': ET.tostring(refill_request)}
        import sys
        print >>sys.stderr, 'request: ' + ET.tostring(refill_request)
        response = self.send_request(data=request, xml_url=self.config.postage_url)

        # Check for errors has already been done in base
        if response is not None and len(response):
            return response
        else:
            return False

    def refill_balance_request(self, amount):
        refill_request = ET.Element('RecreditRequest')
        requester_id = ET.SubElement(refill_request, 'RequesterID')
        requester_id.text = self.config.requester_id
        request_id = ET.SubElement(refill_request, 'RequestID')
        request_id.text = 'RBR-Shippo'
        certified_intermediary = ET.SubElement(refill_request, 'CertifiedIntermediary')
        account_id = ET.SubElement(certified_intermediary, 'AccountID')
        account_id.text = self.config.account_id
        pass_phrase = ET.SubElement(certified_intermediary, 'PassPhrase')
        pass_phrase.text = self.config.passphrase
        recredit_amount = ET.SubElement(refill_request, 'RecreditAmount')
        recredit_amount.text = str(int(amount)) #round to int first, then serialize to string

        return refill_request

    def generate_account(self, endicia_user):
        signup_request = self.generate_signup_request(endicia_user)
        request = {'XMLInput': ET.tostring(signup_request)}
        response = self.send_request(data=request, xml_url=self.config.service_url + '&method=UserSignup', bypass_check=True, use_service_url=True)
        if not response.find('ErrorMsg').text:
            return True
        else:
            return response.find('ErrorMsg').text

    def generate_signup_request(self, endicia_user):
        #get creditcard type
        import adapter.helpers.creditcard_type as cc
        customer_cc_type = cc.get_cc_type(endicia_user['cc_number'])
        #build XML tree
        create_account_request = ET.Element('UserSignupRequest')
        partner_id = ET.SubElement(create_account_request, 'PartnerId')
        partner_id.text = self.config.requester_id
        product_type = ET.SubElement(create_account_request, 'ProductType')
        product_type.text = 'LABELSERVER'
        test = ET.SubElement(create_account_request, 'Test')
        if settings.DEBUG:
            test.text = 'Y'
        else:
            test.text = 'N'
        first_name = ET.SubElement(create_account_request, 'FirstName')
        first_name.text = endicia_user['first_name']
        last_name = ET.SubElement(create_account_request, 'LastName')
        last_name.text = endicia_user['last_name']
        email = ET.SubElement(create_account_request, 'EmailAddress')
        email.text = self.config.apiuser.address.email
        email_confirm = ET.SubElement(create_account_request, 'EmailConfirm')
        email_confirm.text = self.config.apiuser.address.email
        phone = ET.SubElement(create_account_request, 'PhoneNumber')
        phone.text = endicia_user['phone']
        certify = ET.SubElement(create_account_request, 'ICertify')
        certify.text = 'Y'
        mailing_street = ET.SubElement(create_account_request, 'PhysicalAddress')
        mailing_street.text = endicia_user['physical_street']
        mailing_city = ET.SubElement(create_account_request, 'PhysicalCity')
        mailing_city.text = endicia_user['physical_city']
        mailing_state = ET.SubElement(create_account_request, 'PhysicalState')
        mailing_state.text = endicia_user['physical_state']
        mailing_zip = ET.SubElement(create_account_request, 'PhysicalZipCode')
        mailing_zip.text = endicia_user['physical_zip']
        passphrase = ET.SubElement(create_account_request, 'PassPhrase')
        passphrase.text = endicia_user['passphrase']
        password = ET.SubElement(create_account_request, 'WebPassword')
        password.text = endicia_user['webpassword']
        challenge_question = ET.SubElement(create_account_request, 'ChallengeQuestion')
        challenge_question.text = endicia_user['challenge_question']
        challenge_answer = ET.SubElement(create_account_request, 'ChallengeAnswer')
        challenge_answer.text = endicia_user['challenge_answer']
        billing_type = ET.SubElement(create_account_request, 'BillingType')
        billing_type.text = 'TS'
        billing_street = ET.SubElement(create_account_request, 'CreditCardAddress')
        billing_street.text = endicia_user['cc_street']
        billing_city = ET.SubElement(create_account_request, 'CreditCardCity')
        billing_city.text = endicia_user['cc_city']
        billing_state = ET.SubElement(create_account_request, 'CreditCardState')
        billing_state.text = endicia_user['cc_state']
        billing_zip = ET.SubElement(create_account_request, 'CreditCardZipCode')
        billing_zip.text = endicia_user['cc_zip']
        cc_number = ET.SubElement(create_account_request, 'CreditCardNumber')
        cc_number.text = endicia_user['cc_number']
        cc_type = ET.SubElement(create_account_request, 'CreditCardType')
        cc_type.text = customer_cc_type
        cc_month = ET.SubElement(create_account_request, 'CreditCardExpMonth')
        cc_month.text = endicia_user['cc_exp_mm']
        cc_year = ET.SubElement(create_account_request, 'CreditCardExpYear')
        cc_year.text = endicia_user['cc_exp_yy']
        payment_type = ET.SubElement(create_account_request, 'PaymentType')
        payment_type.text = 'CC'
        override_email = ET.SubElement(create_account_request, 'OverrideEmailCheck')
        override_email.text = 'Y'
        
        return create_account_request
    
    def __unicode__(self):
        return 'EndiciaUserService'