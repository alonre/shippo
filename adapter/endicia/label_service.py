import xml.etree.ElementTree as ET

from django.conf import settings

from api.models import Currency, Country, Transaction, Rate

from base import EndiciaBaseService
from user_service import EndiciaUserService
from adapter.dispatch import queue_generate_label
import elements

class EndiciaLabelService(EndiciaBaseService):
    def __init__(self, config, *args, **kwargs):
        super(EndiciaLabelService, self).__init__(config, *args, **kwargs)

    def get_label(self, transaction):
        if transaction.rate.is_purchasable(transaction, self.config.adapter) and self.config.credentials_valid(transaction=transaction):

            label_request = self.generate_label_request(transaction)
            request = {'labelRequestXML': ET.tostring(label_request)}

            transaction.object_api_message = ET.tostring(label_request)
            transaction.set_was_test(self.config.production)

            transaction.save(trigger_generation=False)

            response = self.send_request(data=request, xml_url=self.config.label_url, transaction=transaction)

            if response is not None and len(response):
                if response == 'REFILL':
                    #if insufficient funds, refill account
                    refill_response = EndiciaUserService(self.config).refill_balance(self.config.refill_amount)
                    #then try to get label again
                    if refill_response is not None and len(response):
                        self.get_label(transaction)
                    else:
                        transaction.object_status = transaction.OBJECT_STATUS_CHOICE.ERROR
                        return None
                else:
                    transaction.tracking_number = self.get_tracking_number(response)
                    transaction.label_url = self.get_and_upload_label(response, self.config.prefix + 'Base64LabelImage', transaction)
                    transaction.object_status = transaction.OBJECT_STATUS_CHOICE.SUCCESS
                    transaction.save(trigger_generation=False)
            else:
                transaction.object_status = transaction.OBJECT_STATUS_CHOICE.ERROR
                return None

    def generate_label_request(self, transaction):
        label_request = ET.Element('LabelRequest')
        elements.label_request(label_request, transaction, self.config)

        elements.mail_class(label_request, transaction.rate.object_api_record, self.config)

        if not self.config.production:
            test = ET.SubElement(label_request, 'Test')
            test.text = 'YES'

        elements.mailpiece_value_dimensions(label_request, transaction.rate.shipment.parcel, self.config)

        elements.address_from(label_request, transaction.rate.shipment.address_from, self.config)
        elements.address_to(label_request, transaction.rate.shipment.address_to, self.config)
        elements.label_type(label_request, transaction.rate.shipment.address_to, self.config)

        elements.date_time_description(label_request)

        return label_request

    def get_tracking_number(self, response):
        return response.find(self.config.prefix + 'TrackingNumber').text

    def __unicode__(self):
        return 'EndiciaLabelService'