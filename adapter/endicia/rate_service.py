from decimal import *
from sys import stderr

import xml.etree.ElementTree as ET
from django.conf import settings

from adapter.models import Endpoint, Provider, Adapter, Zone, CountryZone, Servicelevel, Tier, Duration, Price
from api.models import Currency, Country, Rate

from base import EndiciaBaseService
import elements

class EndiciaRateService(EndiciaBaseService):
    def __init__(self, config, *args, **kwargs):
        super(EndiciaRateService, self).__init__(config, *args, **kwargs)
        
    def get_rates(self, shipment):
        if self.config.credentials_valid():
            rate_request = self.generate_rate_request(shipment)
            
            request = {'postageRatesRequestXML': ET.tostring(rate_request)}
            
            response = self.send_request(data=request, xml_url=self.config.rates_url)
           
            if response is not None and len(response):
                self.messages = self.get_messages(response)
                rates = self.generate_rates(response, shipment, self.config.user)
            else:
                return None
            
            if rates is not None and len(rates) > 0:
                return rates
            else:
                return None
            
    def generate_rate_request(self, shipment):
        postage_rates_request = ET.Element('PostageRatesRequest')
        requester_id = ET.SubElement(postage_rates_request, 'RequesterID')
        requester_id.text = self.config.requester_id
        certified_intermediary = ET.SubElement(postage_rates_request, 'CertifiedIntermediary')
        account_id = ET.SubElement(certified_intermediary, 'AccountID')
        account_id.text = self.config.account_id
        pass_phrase = ET.SubElement(certified_intermediary, 'PassPhrase')
        pass_phrase.text = self.config.passphrase

        mail_class = ET.SubElement(postage_rates_request, 'MailClass')

        if shipment.address_to.country == self.config.country_us:
            mail_class.text = 'Domestic'
        else:
            mail_class.text = 'International'

        weight_oz = ET.SubElement(postage_rates_request, 'WeightOz')
        weight_oz.text = str(shipment.parcel.get_weight(unit=self.config.unit_oz, decimal_places=1))

        mailpiece_dimensions = ET.SubElement(postage_rates_request, 'MailpieceDimensions')
        length = ET.SubElement(mailpiece_dimensions, 'Length')
        length.text = str(shipment.parcel.get_length(unit=self.config.unit_in, decimal_places=1))
        width = ET.SubElement(mailpiece_dimensions, 'Width')
        width.text = str(shipment.parcel.get_width(unit=self.config.unit_in, decimal_places=1))
        height = ET.SubElement(mailpiece_dimensions, 'Height')
        height.text = str(shipment.parcel.get_height(unit=self.config.unit_in, decimal_places=1))

        machinable = ET.SubElement(postage_rates_request, 'Machinable')
        machinable.text = 'TRUE'

        delivery_time = ET.SubElement(postage_rates_request, 'DeliveryTimeDays')
        delivery_time.text = 'TRUE'

        # TODO: We could add separate insurance here. look at documentation, there are two variants available
        # check insurance HERE
        #services = ET.SubElement(postage_rates_request, 'Services')
        #insured_mail = ET.SubElement(services, 'InsuredMail')
        #insured_mail.text = 'UspsOnline' # only for domestic
        #signature_confirmation = ET.SubElement(services, 'SignatureConfirmation')
        #signature_confirmation.text = 'ON'
        #insured_value = ET.SubElement(postage_rates_request, 'InsuredValue')
        #insured_value.text = shipment.parcel.get_value(currency=self.currency_usd, decimal_places=1)

        from_zip = ET.SubElement(postage_rates_request, 'FromPostalCode')
        from_zip.text = shipment.address_from.zip
        to_zip = ET.SubElement(postage_rates_request, 'ToPostalCode')
        to_zip.text = shipment.address_to.zip

        if not shipment.address_to.country == self.config.country_us:
            to_country = ET.SubElement(postage_rates_request, 'ToCountryCode')
            to_country.text = shipment.address_to.country.iso2
            
        return postage_rates_request
        
    def generate_rates(self, response, shipment, user):
        rates = []

        for postage_price in response.iter(self.config.prefix + 'PostagePrice'):
            rate = Rate(shipment = shipment, adapter = self.config.adapter, provider=self.config.provider, object_owner=user)
            rate.object_api_record = ET.tostring(postage_price)

            postage = postage_price.find(self.config.prefix + 'Postage')

            servicelevel = None

            sl_token = postage_price.find(self.config.prefix + 'MailClass').text

            try:
                servicelevel = Servicelevel.objects.get(servicelevel_token = sl_token)
            except:
                import sys
                print >>sys.stderr, "USPS ADAPTER: EXCEPTION: Servicelevel not found for token " + str(postage.find(self.config.prefix + 'MailService').text)
                pass

            if servicelevel:
                rate.servicelevel = servicelevel
                rate.fill_from_servicelevel_object(rate.servicelevel)

                rate.outbound_endpoint = rate.servicelevel.outbound_endpoint
                rate.inbound_endpoint = rate.servicelevel.inbound_endpoint
            else:
                rate.servicelevel_name = postage.find(self.config.prefix + 'MailService').text

            days = int(postage_price.find(self.config.prefix + 'DeliveryTimeDays').text)

            if (not days or days == 0) and servicelevel:
                try:
                    rate.duration = Duration.objects.get(servicelevel = rate.servicelevel)
                    rate.fill_from_duration_object(rate.duration)
                except:
                    print >>sys.stderr, "USPS ADAPTER: EXCEPTION: Duration not found for token " + sl_token + " which is pk= " + rate.servicelevel.id
                    pass
            else:
                rate.duration_days = days

            rate.currency = self.config.currency_usd
            rate.amount = Decimal(postage_price.get('TotalAmount'))

            self.save_messages_for_object(messages=self.messages, rate=rate)

            rates.append(rate)

        return rates
    
    def __unicode__(self):
        return 'EndiciaRateService'