from django.conf import settings
from adapter.models import Provider, Adapter
from api.models import Currency, Country, Unit

class EndiciaConfig:
    def __init__(self, user):
        self.provider = Provider.objects.get(name='USPS')
        self.adapter = Adapter.objects.get(name='Endicia')
        self.user = user
        self.apiuser = user.apiuser
        self.settings = settings.ADAPTERS['Endicia']
        self.label_lifetime = settings.LABEL_LIFETIME

        # setup units & currency
        self.country_us = Country.objects.get(iso2='US')
        self.currency_usd = Currency.objects.get(iso='USD')
        self.unit_in = Unit.objects.get(abbreviation='in')
        self.unit_oz = Unit.objects.get(abbreviation='oz')

        # if user settings exist and are complete, use them
        self.account_id = self.apiuser.endicia_account_id
        self.passphrase = self.apiuser.endicia_passphrase
        self.refill_amount = self.apiuser.endicia_refill_amount
        self.requester_id = self.settings['REQUESTER_ID']
        self.production = self.apiuser.endicia_production

        self.production_base_url = self.settings['PRODUCTION_BASE_URL_XML']
        self.test_base_url = self.settings['TEST_BASE_URL_XML']
        self.service_url = self.settings['SERVICE_URL_XML']
        self.rates_url = self.settings['RATES_URL_XML']
        self.label_url = self.settings['LABEL_PURCHASE_URL_XML']
        self.account_url = self.settings['ACCOUNT_STATUS_URL_XML']
        self.passphrase_url = self.settings['CHANGE_PASSPHRASE_URL_XML']
        self.postage_url = self.settings['BUY_POSTAGE_URL_XML']
        self.prefix = self.settings['PREFIX']

    def credentials_valid(self, transaction = None, *args, **kwargs):
        if self.account_id and self.passphrase:
            return True
        else:
            if transaction:
                transaction.credentials_missing()
            return False

    def __unicode__(self):
        return 'EndiciaConfig'