import requests
import copy
import xml.etree.ElementTree as ET

from adapter.models import Zone, CountryZone
from api.models import Message
import elements
from .. helpers.s3upload import S3Delivery


class EndiciaBaseService(object):
    def __init__(self, config, *args, **kwargs):
        self.config = config

    def send_request(self, data, xml_url, transaction=None, bypass_check=False, use_service_url=False):
        headers = {'content-type': 'application/x-www-form-urlencoded'}

        if use_service_url:
            base_url = self.config.service_url
        else:
            if self.config.production == True:
                base_url = self.config.production_base_url
            else:
                base_url = self.config.test_base_url

        post = requests.post(base_url + xml_url, data=data, headers=headers)
        response = ET.fromstring(post.text)

        if transaction:
            transaction.object_api_response = post.text
            transaction.save(trigger_generation=False)
            self.messages = self.get_messages(response)
            self.save_messages_for_object(messages=self.messages, transaction=transaction)

        if bypass_check:
            return response
        else:
            return self.check_for_error(response)

    def check_for_error(self, response):
        if response.find(self.config.prefix + 'Status').text == '0':
            return response
        elif response.find(self.config.prefix + 'Status').text == '12104' or response.find(self.config.prefix + 'Status').text == '12503':
            #if insufficient funds, return response to refill & retry
            return 'REFILL'
        else:
            return None # QUESTIONABLE

    def get_messages(self, response):
        messages = []

        error_message = response.find(self.config.prefix + 'ErrorMessage')

        if error_message is not None:
            message = Message(source=self.config.adapter, text=error_message.text)
            messages.append(message)

        return messages

    def save_messages_for_object(self, messages, rate=None, transaction=None):
        for message in messages:
            message = copy.copy(message)
            
            if rate:
                message.rate = rate
            if transaction:
                message.transaction = transaction
        
            message.save()
            
    def get_and_upload_label(self, response, xpath, transaction, extension='gif'):
        base64_label = response.find(xpath).text
        url = S3Delivery().save_and_upload(base64content = base64_label, filename = str(transaction.object_id) + '.' + extension, contenttype = 'image/' + extension, expiration_sec = self.config.label_lifetime)
        return url
    
    def __unicode__(self):
        return 'EndiciaBaseService'