import re, time
import xml.etree.ElementTree as ET
from api.models import Unit

def label_request(label_request_element, transaction, config):
    requester_id = ET.SubElement(label_request_element, 'RequesterID')
    requester_id.text = config.requester_id
    account_id = ET.SubElement(label_request_element, 'AccountID')
    account_id.text = config.account_id
    pass_phrase = ET.SubElement(label_request_element, 'PassPhrase')
    pass_phrase.text = config.passphrase

    #internal customer and transaction id
    customerid = ET.SubElement(label_request_element, 'PartnerCustomerID')
    customerid.text = str(config.user.id)
    transactionid = ET.SubElement(label_request_element, 'PartnerTransactionID')
    transactionid.text = str(transaction.object_id)[:8]

def mail_class(label_request_element, object_api_record, config):
    record = ET.fromstring(object_api_record)
    mailclass_text = record.find('./' + config.prefix + 'MailClass').text #'./' + config.prefix + 'PostagePrice' +
    mailclass = ET.SubElement(label_request_element, 'MailClass')
    mailclass.text = mailclass_text

    return mailclass

def mailpiece_value_dimensions(label_request_element, parcel, config):
        value = ET.SubElement(label_request_element, 'Value')
        parcel_value = parcel.get_parcel_value(currency=config.currency_usd)

        if parcel_value < 1.0:
            value.text = '1.0'
        else:
            value.text = str(parcel_value)

        #dateadvance
        #todo: how many days should the label be advanced --> possible in date_time_description?

        #dimensions
        mailpiece_dimensions = ET.SubElement(label_request_element, 'MailpieceDimensions')
        length = ET.SubElement(mailpiece_dimensions, 'Length')
        length.text = str(parcel.get_length(unit=config.unit_in, decimal_places=1))
        width = ET.SubElement(mailpiece_dimensions, 'Width')
        width.text = str(parcel.get_width(unit=config.unit_in, decimal_places=1))
        height = ET.SubElement(mailpiece_dimensions, 'Height')
        height.text = str(parcel.get_height(unit=config.unit_in, decimal_places=1))
        weight_oz = ET.SubElement(label_request_element, 'WeightOz')
        weight_oz.text = str(parcel.get_weight(unit=config.unit_oz, decimal_places=1))

def address_from(label_request_element, address_from, config):
    address(label_request_element, address_from, config, 'From', 'Return')

def address_to(label_request_element, address_to, config):
    address(label_request_element, address_to, config, 'To')

def address(label_request_element, address, config, prefix_all, prefix_address=None):
    if not prefix_address:
        prefix_address=prefix_all

    name = ET.SubElement(label_request_element, prefix_all + 'Name')
    name.text = address.name
    company = ET.SubElement(label_request_element, prefix_all + 'Company')
    company.text = address.company
    address1 = ET.SubElement(label_request_element, prefix_address + 'Address1')
    address1.text = address.street_no + ' ' + address.street1
    address2 = ET.SubElement(label_request_element, prefix_address + 'Address2')
    address2.text = address.street2
    city = ET.SubElement(label_request_element, prefix_all + 'City')
    city.text = address.city
    state = ET.SubElement(label_request_element, prefix_all + 'State')
    state.text = address.state
    postal_code = ET.SubElement(label_request_element, prefix_all + 'PostalCode')
    postal_code.text = address.zip

    country = ET.SubElement(label_request_element, prefix_all + 'Country')
    if address.country == config.country_us:
        country.text = ''
    else:
        country.text = str(address.country.name)

    phone = ET.SubElement(label_request_element, prefix_all + 'Phone')
    #strip all but digits from phone number
    phone_number = re.sub("\D", "", address.phone)
    #phone number has to be exactly 10 digits
    if address.country == config.country_us:
        if len(phone_number) > 10:
            if(phone_number[:1] == '1'):
                phone_number = phone_number[1:]
            phone_number = phone_number[:10]
        elif len(phone_number) < 10:
            while len(phone_number) < 10:
                phone_number += '0'
    else:
        if len(phone_number) > 30:
            phone_number = phone_number[:30]

    phone.text = str(phone_number)

def label_type(label_request_element, address_to, config):
    label_type_element = ET.SubElement(label_request_element, 'LabelType')

    if address_to.country == config.country_us:
        label_type_element.text = 'Default'
    else:
        label_type_element.text = 'International'

    # might also come after date_time_description, if order matters for endicia parser
    #labelsubtype = ET.SubElement(label_request_element, 'LabelSubtype')
    #labelsubtype.text = ''
    imageformat = ET.SubElement(label_request_element, 'ImageFormat')
    imageformat.text = 'GIF'

def date_time_description(label_request_element):
    shipdate = ET.SubElement(label_request_element, 'ShipDate')
    shipdate.text = time.strftime("%m/%d/%Y") #priority mail express only, mm/dd/yyyy
    shiptime = ET.SubElement(label_request_element, 'ShipTime')
    shiptime.text = '12:00 PM' #priority mail express only, HH:MM AM
    description = ET.SubElement(label_request_element, 'Description')
    description.text = 'Goods or Documents' # REPLACE WITH REASONABLE TEXT HERE