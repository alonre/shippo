import sys
from datetime import datetime
from datetime import timedelta

from django.utils import timezone
from django.conf import settings
from rq import Queue
from worker import conn

from models import Adapter
from api.models import Rate, ShipmentRateQuery, Transaction, Message, Address, Location, AddressLocationQuery
import api.tracking.mixpanel_track as mixp

def queue_generate_rates(shipment, currency_local, *args, **kwargs):
    """Queues the Rate generation job for a given Shipment & local Currency. Extra short so the synchronous web process is finished quickly."""
    q = Queue(connection=conn)
    q.enqueue_call(func=dispatch_generate_rates, args=(shipment, currency_local), result_ttl=0)

def dispatch_generate_rates(shipment, currency_local, *args, **kwargs):
    """Triggers the generation of rates by first querying all adapters from the database,
    and then queuing the query task for each adapter separately. This method also takes care
    that a rates generation cannot be triggered multiple times."""

    current_rates = Rate.objects.filter(shipment = shipment, currency_local=currency_local)

    outbound_country = shipment.address_from.country

    if ShipmentRateQuery.objects.filter(shipment=shipment, currency=currency_local).count() > 0:
        shipment_rate_query = ShipmentRateQuery.objects.get(shipment=shipment, currency=currency_local)
        queue_timeout = shipment_rate_query.entered_queue + timedelta(seconds=settings.SHIPMENT_QUEUE_TIMEOUT)
    else:
        shipment_rate_query = ShipmentRateQuery(shipment=shipment, currency=currency_local)
        queue_timeout = timezone.now() + timedelta(seconds=settings.SHIPMENT_QUEUE_TIMEOUT)

    # Several possibilities:
    # 1. query did not enter queue yet --> new query
    # 2. query never left queue and is above lifetime --> new query
    if (not shipment_rate_query.entered_queue
        or (not shipment_rate_query.left_queue and queue_timeout < timezone.now())):

        shipment_rate_query.entered_queue = timezone.now()
        shipment_rate_query.save()

        q = Queue(connection=conn)
        jobs = []
        generate_rates_attributes_job = None

        adapter_list = Adapter.objects.all()
        for adapter_record in adapter_list:
            if adapter_record.enabled and outbound_country in adapter_record.country_served.all():
                job = q.enqueue_call(func=generate_rates, args=(adapter_record, shipment, currency_local), result_ttl=0)
                jobs.append(job)
                
    return True

def generate_rates(adapter_record, shipment, currency_local, *args, **kwargs):
    """Handles the process of querying Adapters to generate Rates for a Shipment Object.
    Adapter names are given as string to decrease memory footprint."""

    rates = None
    
    config = get_config(adapter_record, shipment.object_owner)
    rate_service = get_rate_service(config)

    if rate_service:
        print >>sys.stderr, "%s generated" % str(rate_service)
        rates = rate_service.get_rates(shipment)
        save_rates(rates, currency_local)
        
    generate_rates_attributes(shipment=shipment)
    left_queue(shipment_rate_query=ShipmentRateQuery.objects.get(shipment=shipment, currency=currency_local))
    
    return True

def save_rates(rates, currency_local, *args, **kwargs):
    if rates and len(rates) > 0:
        for rate in rates:
            rate.fill_with_local_currency(currency_local)       # TODO: check
            rate.save()

def generate_rates_attributes(shipment, *args, **kwargs):
    #wrapped in try/except since it would break rate saving otherwise
    try:
        shipment.generate_rates_attributes()
    except:
        pass

def queue_generate_label(transaction, *args, **kwargs):
    """Queues the Label generation job for a given Transaction. Extra short so the synchronous web process is finished quickly."""
    q = Queue(connection=conn)
    q.enqueue_call(func=dispatch_generate_label, args=(transaction,), result_ttl=0)

def dispatch_generate_label(transaction, *args, **kwargs):
    q = Queue(connection=conn)
    
    config = get_config(transaction.rate.adapter, transaction.rate.object_owner)
    label_service = get_label_service(config)
    
    if transaction.entered_queue:
        queue_timeout = transaction.entered_queue + timedelta(seconds=settings.SHIPMENT_QUEUE_TIMEOUT)
    else:
        queue_timeout = timezone.now() + timedelta(seconds=settings.SHIPMENT_QUEUE_TIMEOUT)

    if not transaction.rate.adapter.enabled:
        transaction.add_message('not_available', 'Currently, no labels can be bought from this shipping provider. Please try again later.', transaction.rate.adapter, stack_trace='', new_status = Transaction.OBJECT_STATUS_CHOICE.ERROR)

    if (label_service and (not transaction.entered_queue or (not transaction.left_queue and queue_timeout < timezone.now()))
        and transaction.object_status in (Transaction.OBJECT_STATUS_CHOICE.WAITING, Transaction.OBJECT_STATUS_CHOICE.QUEUED)
        and not transaction.label_url and not transaction.tracking_number):
        transaction.entered_queue = timezone.now()
        transaction.object_status = Transaction.OBJECT_STATUS_CHOICE.QUEUED
        q.enqueue_call(func=generate_label, args=(transaction, label_service), result_ttl=0)
        
    transaction.save(trigger_generation=False)
    return True
               
def generate_label(transaction, label_service, *args, **kwargs):
    try:
        label_service.get_label(transaction)
    except Exception as ex:
        import traceback
        print >>sys.stderr, "EXCEPTION: %s threw exception when trying to print a label for rate %s and shipment %s. Exception message was %s. " % (str(label_service), str(transaction.rate.object_id), str(transaction.rate.shipment.object_id), str(ex))
        trace_back = traceback.format_exc()
        transaction.add_message(code="", text = "Internal Error. Please contact Shippo support with the Object ID.", source=label_service.config.adapter, stack_trace=str(trace_back), new_status=Transaction.OBJECT_STATUS_CHOICE.ERROR)
    
    left_queue(transaction=transaction)
    
    return True
    
def queue_generate_locations(address, *args, **kwargs):
    """Queues the Location generation job for a given Address. Extra short so the synchronous web process is finished quickly."""
    q = Queue(connection=conn)
    q.enqueue_call(func=dispatch_generate_locations, args=(address,), result_ttl=0)

def dispatch_generate_locations(address, *args, **kwargs):
    """Triggers the generation of locations by first querying all adapters from the database,
    and then queuing the query task for each adapter separately. This method also takes care
    that a location generation cannot be triggered multiple times."""

    if AddressLocationQuery.objects.filter(address=address).exists():
        address_location_query = AddressLocationQuery.objects.get(address=address)
        queue_timeout = address_location_query.entered_queue + timedelta(seconds=settings.SHIPMENT_QUEUE_TIMEOUT)
    else:
        address_location_query = AddressLocationQuery(address=address)
        queue_timeout = timezone.now() + timedelta(seconds=settings.SHIPMENT_QUEUE_TIMEOUT)

    if (not address_location_query.entered_queue
        or (not address_location_query.left_queue and queue_timeout < timezone.now())):

        address_location_query.entered_queue = timezone.now()
        address_location_query.save()

        jobs = []
        q = Queue(connection=conn)

        adapter_list = Adapter.objects.filter(name='UPS')
        for adapter_record in adapter_list:
            if adapter_record.enabled and address.country in adapter_record.country_served.all():
                job = q.enqueue_call(func=generate_locations, args=(adapter_record, address), result_ttl=0)
                jobs.append(job)
                
    return True

def generate_locations(adapter_record, address, *args, **kwargs):
    """Handles the process of querying Adapters to generate Locations for an Address Object.
    Adapter names are given as string to decrease memory footprint."""

    config = get_config(adapter_record, address.object_owner)
    location_service = get_location_service(config)

    if location_service:
        location_addresses = location_service.get_locations(address)
        
    left_queue(address_location_query=AddressLocationQuery.objects.get(address=address))
        
    return True

# TODO: Migrate to models?
def left_queue(shipment_rate_query = None, transaction = None, address_location_query = None):
    if shipment_rate_query:
        shipment_rate_query.left_queue = timezone.now()
        shipment_rate_query.shipment.object_updated = timezone.now()
        shipment_rate_query.save()
        shipment_rate_query.shipment.save(trigger_generation=False)
        print >>sys.stderr, "DONE: Shipment %s left queue." % shipment_rate_query.shipment.object_id
    elif transaction:
        transaction.left_queue = timezone.now()
        transaction.save(trigger_generation=False)
        print >>sys.stderr, "DONE: Transaction %s left queue." % transaction.object_id
        # Trigger Mixpanel tracking
        mixp.track_event(mixp.track_API_transaction, transaction.object_owner, transaction)
        if transaction.object_status == 'ERROR':
            q = Queue('low', connection=conn)
            q.enqueue_call(func=send_transaction_error_mail, args=(transaction,), result_ttl=0)
    elif address_location_query:
        address_location_query.left_queue = timezone.now()
        address_location_query.save()
        print >>sys.stderr, "DONE: Location Query for Address %s left queue." % address_location_query.address.object_id

def send_transaction_error_mail(transaction):
    from django.core.mail import send_mail
    from django.core import serializers
    subject = 'Shippo: Error in Transaction Generation Attempt'
    message = 'Transaction: ' + serializers.serialize("json", [transaction])
    to_mail = settings.BACKEND_ERROR_CONTACT_EMAIL
    send_mail(subject, message, 'error@goshippo.com', [to_mail], fail_silently=True)

def get_config(adapter_record, user):
    adapter_gadb = Adapter.objects.get(name='GADB')
    adapter_dhl = Adapter.objects.get(name='DHL')
    adapter_endicia = Adapter.objects.get(name='Endicia')
    adapter_fedex = Adapter.objects.get(name='FedEx')
    adapter_parcel2go = Adapter.objects.get(name='Parcel2Go')
    adapter_ups = Adapter.objects.get(name='UPS')

    if adapter_record and user:
        if adapter_record == adapter_gadb:
            from gadb.config import GADBConfig
            config = GADBConfig(user)
        elif adapter_record == adapter_dhl:
            from dhl.config import DHLConfig
            config = DHLConfig(user)
        elif adapter_record == adapter_endicia:
            from endicia.config import EndiciaConfig
            config = EndiciaConfig(user)
        elif adapter_record == adapter_parcel2go:
            from parcel2go.config import Parcel2GoConfig
            config = Parcel2GoConfig(user)
        elif adapter_record == adapter_fedex:
            from fedex.config import FedExConfig
            config = FedExConfig(user)
        elif adapter_record == adapter_ups:
            from ups.config import UPSConfig
            config = UPSConfig(user)
    
        if config:
            return config
        else:
            return None
    else:
        return None

def get_rate_service(config):
    adapter_gadb = Adapter.objects.get(name='GADB')
    adapter_dhl = Adapter.objects.get(name='DHL')
    adapter_endicia = Adapter.objects.get(name='Endicia')
    adapter_fedex = Adapter.objects.get(name='FedEx')
    adapter_parcel2go = Adapter.objects.get(name='Parcel2Go')
    adapter_ups = Adapter.objects.get(name='UPS')
    
    if config and config.adapter:
        if config.adapter == adapter_gadb:
            from gadb.rate_service import GADBRateService
            rate_service = GADBRateService(config)
        elif config.adapter == adapter_dhl:
            from dhl.rate_service import DHLRateService
            rate_service = DHLRateService(config)
        elif config.adapter == adapter_endicia:
            from endicia.rate_service import EndiciaRateService
            rate_service = EndiciaRateService(config)
        elif config.adapter == adapter_parcel2go:
            from parcel2go.rate_service import Parcel2GoRateService
            rate_service = Parcel2GoRateService(config)
        elif config.adapter == adapter_fedex:
            from fedex.rate_service import FedExRateService
            rate_service = FedExRateService(config)
        elif config.adapter == adapter_ups:
            from ups.rate_service import UPSRateService
            rate_service = UPSRateService(config)
        
        if rate_service:
            return rate_service
        else:
            return None
    else:
        return None

def get_label_service(config):
    adapter_gadb = Adapter.objects.get(name='GADB')
    adapter_dhl = Adapter.objects.get(name='DHL')
    adapter_endicia = Adapter.objects.get(name='Endicia')
    adapter_fedex = Adapter.objects.get(name='FedEx')
    adapter_parcel2go = Adapter.objects.get(name='Parcel2Go')
    adapter_ups = Adapter.objects.get(name='UPS')
    
    if config and config.adapter:
        if config.adapter == adapter_gadb:
            from gadb.label_service import GADBLabelService
            label_service = GADBLabelService(config)
        elif config.adapter == adapter_dhl:
            from dhl.label_service import DHLLabelService
            label_service = DHLLabelService(config)
        elif config.adapter == adapter_endicia:
            from endicia.label_service import EndiciaLabelService
            label_service = EndiciaLabelService(config)
        elif config.adapter == adapter_parcel2go:
            from parcel2go.label_service import Parcel2GoLabelService
            label_service = Parcel2GoLabelService(config)
        elif config.adapter == adapter_fedex:
            from fedex.label_service import FedExLabelService
            label_service = FedExLabelService(config)
        elif config.adapter == adapter_ups:
            from ups.label_service import UPSLabelService
            label_service = UPSLabelService(config)
            
        if label_service:
            return label_service
        else:
            return None
    else:
        return None

def get_location_service(config):
    adapter_gadb = Adapter.objects.get(name='GADB')
    adapter_dhl = Adapter.objects.get(name='DHL')
    adapter_endicia = Adapter.objects.get(name='Endicia')
    adapter_fedex = Adapter.objects.get(name='FedEx')
    adapter_parcel2go = Adapter.objects.get(name='Parcel2Go')
    adapter_ups = Adapter.objects.get(name='UPS')
    
    if config and config.adapter:
        if config.adapter == adapter_gadb:
            location_service = None
        elif config.adapter == adapter_dhl:
            location_service = None
        elif config.adapter == adapter_endicia:
            location_service = None
        elif config.adapter == adapter_parcel2go:
            location_service = None
        elif config.adapter == adapter_fedex:
            location_service = None
        elif config.adapter == adapter_ups:
            from ups.location_service import UPSLocationService
            location_service = UPSLocationService(config)
        
        if location_service:
            return location_service
        else:
            return None
    else:
        return None
    
def get_track_service(config):
    adapter_gadb = Adapter.objects.get(name='GADB')
    adapter_dhl = Adapter.objects.get(name='DHL')
    adapter_endicia = Adapter.objects.get(name='Endicia')
    adapter_fedex = Adapter.objects.get(name='FedEx')
    adapter_parcel2go = Adapter.objects.get(name='Parcel2Go')
    adapter_ups = Adapter.objects.get(name='UPS')
    
    if config and config.adapter:
        if config.adapter == adapter_gadb:
            track_service = None
        elif config.adapter == adapter_dhl:
            track_service = None
        elif config.adapter == adapter_endicia:
            track_service = None
        elif config.adapter == adapter_parcel2go:
            from parcel2go.track_service import Parcel2GoTrackService
            track_service = Parcel2GoTrackService(config)        
        elif config.adapter == adapter_fedex:
            track_service = None
        elif config.adapter == adapter_ups:
            from ups.track_service import UPSTrackService
            track_service = UPSTrackService(config)
        
        if track_service:
            return track_service
        else:
            return None
    else:
        return None
