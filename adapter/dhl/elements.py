from api.models import Unit
from suds.sax.element import Element
import xml.etree.ElementTree as ET

def soap_envelope():
    soap_envelope = Element('soapenv:Envelope')
    soap_envelope.set('xmlns:soapenv', "http://schemas.xmlsoap.org/soap/envelope/")
    soap_envelope.set('xmlns:cis', "http://dhl.de/webservice/cisbase")
    soap_envelope.set('xmlns:is', "http://dhl.de/webservice/is_base_de")
    soap_envelope.set('xmlns:de', "http://de.ws.intraship")
    return soap_envelope

def soap_header():
    soap_header = Element('soapenv:Header')
    return soap_header

def soap_authentification(config):
    #https://entwickler.dhl.de/group/ep/geschaeftskundenversand/authentifizierung
    soap_authentification_tag = Element('cis:Authentification')

    user = Element('cis:user').setText(config.dhl_intraship_username)
    signature = Element('cis:signature').setText(config.dhl_intraship_password)
    type = Element('cis:type').setText(0)

    soap_authentification_tag.append(user)
    soap_authentification_tag.append(signature)
    soap_authentification_tag.append(type)

    return soap_authentification_tag

def soap_dev_authentification(config):
    #https://entwickler.dhl.de/group/ep/geschaeftskundenversand/authentifizierung
    soap_dev_authentification_tag = Element('de:DeveloperAuthentification')

    devid = Element('DEVID').setText(config.dhl_intraship_username)
    appid = Element('APPID').setText(config.dhl_intraship_password)
    certid = Element('CERTID').setText(0)

    soap_dev_authentification_tag.append(devid)
    soap_dev_authentification_tag.append(appid)
    soap_dev_authentification_tag.append(certid)

    return soap_dev_authentification_tag

def soap_body():
    #https://entwickler.dhl.de/group/ep/apis/grosskundenversand/operationen/createshipmentdd
    soap_body = Element('soapenv:Body')
    return soap_body

def create_shipment_dd_tag():
    create_shipment_dd_tag = Element('de:CreateShipmentDDRequest')
    return create_shipment_dd_tag

def book_pickup_dd_tag():
    book_pickup_dd_tag = Element('de:BookPickupRequest')
    return book_pickup_dd_tag

def version_tag(config):
    version_tag = Element('cis:Version')

    majorrelease = Element('cis:majorRelease').setText(config.create_shipment_dd_version_major)
    minorrelease = Element('cis:minorRelease').setText(config.create_shipment_dd_version_minor)

    version_tag.append(majorrelease)
    version_tag.append(minorrelease)

    return version_tag

def shipment_order_tag():
    shipment_order_tag = Element('ShipmentOrder')
    return shipment_order_tag

def sequence_number_tag():
    #change that to be counted dynamically if we want to send more than 1 shipment per request
    sequence_number_tag = Element('SequenceNumber').setText(1)
    return sequence_number_tag

def shipment_tag():
    shipment = Element('Shipment')
    return shipment

def shipment_details_tag():
    shipment_details_tag = Element('ShipmentDetails')
    return shipment_details_tag

def product_code_tag(product_code):
    product_code_tag = Element('ProductCode').setText(product_code)
    return product_code_tag

def shipment_date_tag(shipment_date):
    date = str(shipment_date)[:10] #uncool way to parse date format
    shipment_date_tag = Element('ShipmentDate').setText(date)
    return shipment_date_tag

def ekp_tag(config):
    ekp_tag = Element('cis:EKP').setText(config.dhl_account_number[:10]) #EKP number are first 10 digits of 14digit DHL account number
    return ekp_tag

def attendance_tag(config):
    attendance_tag = Element('Attendance')
    partner_id = Element('cis:partnerID').setText(config.dhl_account_number[12:]) #Partner ID is last 2 digits of 14digit DHL account number
    attendance_tag.append(partner_id)
    return attendance_tag

def shipment_item_tag(parcel, config):
    shipment_item_tag = Element('ShipmentItem')

    weight = Element('WeightInKG').setText(str(parcel.get_weight(unit=config.unit_kg, decimal_places=0)))
    length = Element('LengthInCM').setText(str(parcel.get_length(unit=config.unit_cm, decimal_places=0)))
    width = Element('WidthInCM').setText(str(parcel.get_width(unit=config.unit_cm, decimal_places=0)))
    height = Element('HeightInCM').setText(str(parcel.get_height(unit=config.unit_cm, decimal_places=0)))
    package_type = Element('PackageType').setText('PK') # tbd not sure what PackageType is

    shipment_item_tag.append(weight)
    shipment_item_tag.append(length)
    shipment_item_tag.append(width)
    shipment_item_tag.append(height)
    shipment_item_tag.append(package_type)

    return shipment_item_tag

def notification_tag(address):
    notification_tag = Element('Notification')

    recipient_name = Element('RecipientName').setText(address.name)
    recipient_email = Element('RecipientEmailAddress').setText(address.email)

    notification_tag.append(recipient_name)
    notification_tag.append(recipient_email)

    return notification_tag

def shipper_tag():
    shipper_tag = Element('Shipper')
    return shipper_tag

def receiver_tag():
    receiver_tag = Element('Receiver')
    return receiver_tag

def company_outer_tag():
    company_outer_tag = Element('Company')
    return company_outer_tag

def company_tag(address):
    company_tag = Element('cis:Company')

    company_name1_tag = Element('cis:name1').setText(address.company[:50]) #field must be less than 30 or 50 chars. docs give two possible values.

    company_tag.append(company_name1_tag)
    return company_tag

def person_tag(address):
    person_tag = Element('cis:Person')

    names = address.name.split()
    person_first_name_tag = Element('cis:firstname').setText(names[0])
    person_last_name_tag = Element('cis:lastname').setText(names[1])

    person_tag.append(person_first_name_tag)
    person_tag.append(person_last_name_tag)
    return person_tag

def address_tag(address):
    address_tag = Element('Address')

    street_name_tag = Element('cis:streetName').setText(address.street1)
    street_no_tag = Element('cis:streetNumber').setText(address.street_no)
    street_addon_tag = Element('cis:careOfName').setText(address.street2)

    zip_tag = Element('cis:Zip')
    zip_country_tag_text = ''
    try:
        #use try/except wrapper if Shipper Country is not set --> exception of DHL will be sent, not an internal server error
        #DHL differentiates only between germany, england and other
        if address.country.iso2 == 'DE':
            zip_country_tag_text = 'cis:germany'
        elif address.country.iso2 == 'GB':
            zip_country_tag_text = 'cis:england'
        else:
            zip_country_tag_text = 'cis:other'
    except:
        pass
    zip_country_tag = Element(zip_country_tag_text).setText(address.zip)
    zip_tag.append(zip_country_tag)

    city_tag = Element('cis:city').setText(address.city)

    origin_tag = Element('cis:Origin')
    try:
        isocode_tag = Element('cis:countryISOCode').setText(address.country.iso2)
    except:
        pass
    state_tag = Element('cis:state').setText(address.state)
    origin_tag.append(isocode_tag)
    origin_tag.append(state_tag)

    address_tag.append(street_name_tag)
    address_tag.append(street_no_tag)
    address_tag.append(street_addon_tag)
    address_tag.append(zip_tag)
    address_tag.append(city_tag)
    address_tag.append(origin_tag)

    return address_tag

def communication_tag(address, phone=False):
    communication_tag = Element('Communication')

    email_tag = Element('cis:email').setText(address.email[:30])
    phone_tag = Element('cis:phone').setText(address.phone[:20])
    contact_person_tag = Element('cis:contactPerson').setText(address.name[:30])

    if phone:
        communication_tag.append(phone_tag)
    communication_tag.append(email_tag)
    communication_tag.append(contact_person_tag)

    return communication_tag

def booking_information_tag(product_code, config, transaction):
    booking_information_tag = Element('BookingInformation')

    product_id = Element('ProductID').setText(product_code)
    account = Element('Account').setText(config.dhl_account_number[:10]) #EKP number are first 10 digits of 14digit DHL account number
    attendance = Element('Attendance').setText(config.dhl_account_number[12:]) #Partner ID is last 2 digits of 14digit DHL account number
    date = str(transaction.pickup_date)[:10] #uncool way to parse date format
    pickup_date = Element('PickupDate').setText(date)
    ready_by = Element('ReadyByTime').setText('09:00')
    closing_by = Element('ClosingTime').setText('17:00')
    weight = Element('WeightInKG').setText(str(transaction.rate.shipment.parcel.get_weight(unit=config.unit_kg, decimal_places=0)))
    count_shipments = Element('CountOfShipments').setText(1) #change if we allow more
    length = Element('MaxLengthInCM').setText(str(transaction.rate.shipment.parcel.get_length(unit=config.unit_cm, decimal_places=0)))
    width = Element('MaxWidthInCM').setText(str(transaction.rate.shipment.parcel.get_width(unit=config.unit_cm, decimal_places=0)))
    height = Element('MaxHeightInCM').setText(str(transaction.rate.shipment.parcel.get_height(unit=config.unit_cm, decimal_places=0)))

    booking_information_tag.append(product_id)
    booking_information_tag.append(account)
    booking_information_tag.append(attendance)
    booking_information_tag.append(pickup_date)
    booking_information_tag.append(ready_by)
    booking_information_tag.append(closing_by)
    booking_information_tag.append(weight)
    booking_information_tag.append(count_shipments)
    booking_information_tag.append(length)
    booking_information_tag.append(width)
    booking_information_tag.append(height)

    return booking_information_tag

def pickup_address_tag():
    pickup_address_tag = Element('PickupAddress')
    return pickup_address_tag

def contact_orderer_tag():
    contact_orderer_tag = Element('ContactOrderer')
    return contact_orderer_tag

def tracking_data_tag(config, transactions):
    tracking_data_tag = Element('data')
    tracking_data_tag.set('appname', config.dhl_tracking_account)
    tracking_data_tag.set('password', config.dhl_tracking_password)
    tracking_data_tag.set('request', 'd-get-piece')
    tracking_data_tag.set('language-code', 'en')
    piece_codes = ''
    for transaction in transactions:
        piece_codes += transaction.tracking_number + ';'
    tracking_data_tag.set('piece-code', piece_codes)
    return tracking_data_tag