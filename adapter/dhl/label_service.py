import xml.etree.ElementTree as ET

from django.conf import settings

from api.models import Currency, Country, Transaction

from base import DHLBaseService
from .. helpers.s3upload import S3Delivery
import elements

class DHLLabelService(DHLBaseService):
    def __init__(self, config, *args, **kwargs):
        super(DHLLabelService, self).__init__(config, *args, **kwargs)

    def get_label(self, transaction):
        if transaction.rate.is_purchasable(transaction, self.config.adapter) and self.config.credentials_valid(transaction=transaction):
            shipment_dd_request = self.create_shipment_dd_request(transaction)
            transaction.object_api_message = str(shipment_dd_request)
            transaction.set_was_test(self.config.production)
                
            transaction.save(trigger_generation=False)

            response = self.send_request(shipment_dd_request, soap_action='createShipmentDD', transaction=transaction)

            if response is not None and len(response):
                transaction.tracking_number = self.get_tracking_number(response)
                transaction.label_url = self.get_and_upload_label(response, transaction)
                if not transaction.pickup_date or not self.config.production:
                    transaction.object_status = transaction.OBJECT_STATUS_CHOICE.SUCCESS # TODO: better as validation function in model?
                else:
                    #DHL Pickup currently only works in production
                    book_pickup = self.book_pickup(transaction)
            else:
                transaction.object_status = transaction.OBJECT_STATUS_CHOICE.ERROR

            transaction.save(trigger_generation=False)

    def create_shipment_dd_request(self, transaction):
        rate = transaction.rate
        shipment = rate.shipment

        # Create the elements
        soap_envelope = elements.soap_envelope()

        soap_header = elements.soap_header()
        soap_authentification = elements.soap_authentification(self.config)
        soap_header.append(soap_authentification)

        soap_body = elements.soap_body()

        create_shipment_dd = elements.create_shipment_dd_tag()

        version = elements.version_tag(self.config)
        shipment_order = elements.shipment_order_tag()

        sequence_number = elements.sequence_number_tag()
        dhl_shipment = elements.shipment_tag()

        shipment_details = elements.shipment_details_tag()

        product_code = elements.product_code_tag('EPN') #tbd producttype parameter

        if transaction.pickup_date:
            shipment_date = elements.shipment_date_tag(transaction.pickup_date)
        else:
            import datetime
            shipment_date = elements.shipment_date_tag(datetime.datetime.now() + datetime.timedelta(days=1))

        ekp = elements.ekp_tag(self.config)
        attendance = elements.attendance_tag(self.config)
        shipment_item = elements.shipment_item_tag(shipment.parcel, self.config)

        shipment_details.append(product_code)
        shipment_details.append(shipment_date)
        shipment_details.append(ekp)
        shipment_details.append(attendance)
        shipment_details.append(shipment_item)

        if transaction.notification_email_to:
            notification = elements.notification_tag(shipment.address_to)
            shipment_details.append(notification)

        shipper = elements.shipper_tag() #shipper seems to be the account holder, not the sender

        shipper_company_outer = elements.company_outer_tag()
        shipper_company = elements.company_tag(self.config.apiuser.address)
        shipper_company_outer.append(shipper_company)
        shipper_address = elements.address_tag(self.config.apiuser.address)
        shipper_communication = elements.communication_tag(self.config.apiuser.address)

        shipper.append(shipper_company_outer)
        shipper.append(shipper_address)
        shipper.append(shipper_communication)

        receiver = elements.receiver_tag()

        receiver_company_outer = elements.company_outer_tag()
        receiver_person = elements.person_tag(shipment.address_to)
        receiver_company_outer.append(receiver_person)
        receiver_address = elements.address_tag(shipment.address_to)
        receiver_communication = elements.communication_tag(shipment.address_to)

        receiver.append(receiver_company_outer)
        receiver.append(receiver_address)
        receiver.append(receiver_communication)

        dhl_shipment.append(shipment_details)
        dhl_shipment.append(shipper)
        dhl_shipment.append(receiver)

        shipment_order.append(sequence_number)
        shipment_order.append(dhl_shipment)

        create_shipment_dd.append(version)
        create_shipment_dd.append(shipment_order)

        soap_body.append(create_shipment_dd)

        soap_envelope.append(soap_header)
        soap_envelope.append(soap_body)

        return soap_envelope

    def get_tracking_number(self, response):
        body = response.find('ns0:Body', namespaces=self.config.namespaces)
        label_response = body.find('ns1:CreateShipmentResponse', namespaces=self.config.namespaces)
        label_creation_state = label_response.find('CreationState', namespaces=self.config.namespaces)
        label_shipment_number_outer = label_creation_state.find('ShipmentNumber', namespaces=self.config.namespaces)
        label_shipment_number = label_shipment_number_outer.find('ns2:shipmentNumber', namespaces=self.config.namespaces)

        try:
            return label_shipment_number.text
        except:
            return ''

    def get_and_upload_label(self, response, transaction):
        body = response.find('ns0:Body', namespaces=self.config.namespaces)
        label_response = body.find('ns1:CreateShipmentResponse', namespaces=self.config.namespaces)
        label_creation_state = label_response.find('CreationState', namespaces=self.config.namespaces)
        label_url = label_creation_state.find('Labelurl', namespaces=self.config.namespaces)

        #try:
        download_url = label_url.text
        import sys
        print >>sys.stderr, str(download_url)
        extension = '.pdf'
        url = S3Delivery().download_and_upload(url=download_url, filename=str(transaction.object_id) + extension, contenttype='application/pdf', expiration_sec=self.config.label_lifetime)
        return url
        #except:
         #   return ''

    def book_pickup(self, transaction):
        book_pickup_request = self.create_book_pickup_request(transaction)
        transaction.object_api_message += str(book_pickup_request)
        transaction.save(trigger_generation=False)

        response = self.send_request(book_pickup_request, soap_action='bookPickup', transaction=transaction)

        if response is not None and len(response):
            transaction.object_status = transaction.OBJECT_STATUS_CHOICE.SUCCESS # TODO: better as validation function in model?
        else:
            transaction.object_status = transaction.OBJECT_STATUS_CHOICE.ERROR

    def create_book_pickup_request(self, transaction):
        rate = transaction.rate
        shipment = rate.shipment

        # Create the elements
        soap_envelope = elements.soap_envelope()

        soap_header = elements.soap_header()
        soap_authentification = elements.soap_authentification(self.config)
        #soap_dev_authentification = elements.soap_dev_authentification(self.config)
        soap_header.append(soap_authentification)
        #soap_header.append(soap_dev_authentification)

        soap_body = elements.soap_body()

        book_pickup_dd = elements.book_pickup_dd_tag()

        version = elements.version_tag(self.config)
        booking_information = elements.booking_information_tag('DDN', self.config, transaction)

        pickup_address_outer = elements.pickup_address_tag()

        pickup_company_outer = elements.company_outer_tag()
        pickup_person = elements.person_tag(shipment.address_from)
        pickup_company_outer.append(pickup_person)
        pickup_address = elements.address_tag(shipment.address_from)
        pickup_communication = elements.communication_tag(shipment.address_from, phone=True)

        pickup_address_outer.append(pickup_company_outer)
        pickup_address_outer.append(pickup_address)
        pickup_address_outer.append(pickup_communication)

        contact_orderer_outer = elements.contact_orderer_tag()

        contact_orderer_company_outer = elements.company_outer_tag()
        contact_orderer_company = elements.company_tag(self.config.apiuser.address)
        contact_orderer_company_outer.append(contact_orderer_company)
        contact_orderer_address = elements.address_tag(self.config.apiuser.address)
        contact_orderer_communication = elements.communication_tag(self.config.apiuser.address, phone=True)

        contact_orderer_outer.append(contact_orderer_company_outer)
        contact_orderer_outer.append(contact_orderer_address)
        contact_orderer_outer.append(contact_orderer_communication)

        book_pickup_dd.append(version)
        book_pickup_dd.append(booking_information)
        book_pickup_dd.append(pickup_address_outer)
        book_pickup_dd.append(contact_orderer_outer)

        soap_body.append(book_pickup_dd)

        soap_envelope.append(soap_header)
        soap_envelope.append(soap_body)

        return soap_envelope

    def __unicode__(self):
        return 'DHLLabelService'