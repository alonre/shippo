import requests
from requests.auth import HTTPBasicAuth
import copy
import xml.etree.ElementTree as ET

from adapter.models import Zone, CountryZone
from api.models import Message
import elements

class DHLBaseService(object):
    def __init__(self, config, *args, **kwargs):
        self.config = config

    def send_request(self, request, soap_action='', transaction=None):
        headers = self.config.headers
        headers['SOAPAction'] += soap_action

        import sys
        print >>sys.stderr, str(request)

        post = requests.post(self.config.base_url_soap, data=str(request), headers=headers, auth=HTTPBasicAuth(self.config.api_user, self.config.api_password))
        response = ET.fromstring(post.text)

        print >>sys.stderr, ET.tostring(response)

        if transaction:
            transaction.object_api_response += ET.tostring(response)
            transaction.save(trigger_generation=False)
            self.messages = self.get_messages(response)
            self.save_messages_for_object(messages=self.messages, transaction=transaction)

        return self.check_for_error(response)

    def send_rest_request(self, request, appendix='', data=False):
        headers = self.config.rest_headers
        url = self.config.base_url_rest + appendix

        response = requests.get(url, params=data, auth=HTTPBasicAuth(self.config.api_user, self.config.api_password))

        import sys
        print >>sys.stderr, str(response.text)

        return response.text

    def check_for_error(self, response):
        try:
            body = response.find('ns0:Body', namespaces=self.config.namespaces)
            label_response = body.find('ns1:CreateShipmentResponse', namespaces=self.config.namespaces)
            label_status = label_response.find('status', namespaces=self.config.namespaces)
            label_status_code = label_status.find('StatusCode', namespaces=self.config.namespaces)
            try:
                if label_status_code.text == '0':
                    return response
            except:
                pass
        except:
            return None

    # TODO
    def get_messages(self, response):
        messages = []
        try:
            body = response.find('ns0:Body', namespaces=self.config.namespaces)
            label_response = body.find('ns1:CreateShipmentResponse', namespaces=self.config.namespaces)
            label_creationstate = label_response.find('CreationState', namespaces=self.config.namespaces)
            error_code = None
            try:
                error_code = label_creationstate.find('StatusCode', namespaces=self.config.namespaces).text
            except:
                pass
            for error in label_creationstate.iter('StatusMessage'):
                try:
                    message = Message(source=self.config.adapter)
                    message.text = error.text
                    message.code = error_code
                    messages.append(message)
                except:
                    pass
        except:
            pass
        try:
            body = response.find('ns0:Body', namespaces=self.config.namespaces)
            fault = body.find('ns0:Fault', namespaces=self.config.namespaces)
            faultstring = fault.find('faultstring', namespaces=self.config.namespaces)
            try:
                message = Message(source=self.config.adapter)
                message.text = faultstring.text
                message.code = 'Request Data'
                messages.append(message)
            except:
                pass
        except:
            pass
        return messages

    def save_messages_for_object(self, messages, rate=None, transaction=None):
        for message in messages:
            message = copy.copy(message)

            # TODO: Fatal errors are not saved since there is no rate for them.
            if rate:
                message.rate = rate
            if transaction:
                message.transaction = transaction

            message.save()

    def __unicode__(self):
        return 'DHLBaseService'