import xml.etree.ElementTree as ET

from django.conf import settings

from adapter.models import Endpoint, Provider, Adapter, Zone, CountryZone, Servicelevel, Tier, Duration, Price
from api.models import Currency, Country, Rate

from base import DHLBaseService
import elements

class DHLRateService(DHLBaseService):
    def __init__(self, config, *args, **kwargs):
        super(DHLRateService, self).__init__(config, *args, **kwargs)

    def get_rates(self, shipment):
        if self.config.credentials_valid():
            rates = self.query(shipment)
    
            if rates is not None and len(rates) > 0:
                return rates
            else:
                return None

    def query(self, shipment):
        rates = []
        in_tiers = self.check_tiers(shipment.parcel)
        inbound_country = shipment.address_to.country

        if in_tiers and inbound_country == self.config.country_de:
            #only national shipments as Paket (EPN) supported atm
            rate = Rate(shipment=shipment, object_purpose=shipment.object_purpose, adapter=self.config.adapter, provider=self.config.provider)
            rate.servicelevel = Servicelevel.objects.get(servicelevel_token='DHL_EPN')
            try:
                rate.duration = Duration.objects.get(servicelevel = rate.servicelevel)
            except:
                pass
            rate.currency = self.config.currency_eur
            rate.amount = self.get_price(shipment.parcel)
            if rate.duration:
                rate.fill_from_duration_object(rate.duration)
            rate.fill_from_servicelevel_object(rate.servicelevel)

            #fill_from_servicelevel does not fill endpoints
            rate.outbound_endpoint = rate.servicelevel.outbound_endpoint
            rate.inbound_endpoint = rate.servicelevel.inbound_endpoint

            rate.object_owner = self.config.user
            rates.append(rate)

        return rates

    def check_tiers(self, parcel):
        length = parcel.get_length(unit=self.config.unit_cm, decimal_places=2)
        width = parcel.get_width(unit=self.config.unit_cm, decimal_places=2)
        height = parcel.get_height(unit=self.config.unit_cm, decimal_places=2)
        weight = parcel.get_weight(unit=self.config.unit_kg, decimal_places=2)

        min_dimensions = False
        max_dimensions = False
        max_weight = False

        if length >= 15 and width >= 11 and height >= 1: min_dimensions = True
        if length <= 120 and width <= 60 and height <= 60: max_dimensions = True
        if weight <= 31.5: max_weight = True

        if min_dimensions and max_dimensions and max_weight:
            return True
        else:
            return False

    def get_price(self, parcel):
        weight = parcel.get_weight(unit=self.config.unit_kg, decimal_places=2)

        if weight <= 1:
            return 3.8
        if weight <= 3:
            return 4.2
        if weight <= 5:
            return 5.1
        if weight <= 10:
            return 5.8
        if weight <= 20:
            return 8
        if weight <= 31.5:
            return 9

    def __unicode__(self):
        return 'DHLRateService'