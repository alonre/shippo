import xml.etree.ElementTree as ET

from django.conf import settings

from api.models import Currency, Country, Transaction

from base import DHLBaseService
import elements
from urllib import urlencode

class DHLTrackingService(DHLBaseService):
    def __init__(self, config, *args, **kwargs):
        super(DHLTrackingService, self).__init__(config, *args, **kwargs)

    def get_tracking(self, transactions):
        tracking_request = '<?xml version="1.0" encoding="ISO-8859-1" standalone="no" ?>'
        tracking_element = self.create_tracking_request(transactions)
        #transaction.object_api_message = str(shipment_dd_request)
        #transaction.save(trigger_generation=False)
        tracking_request += str(tracking_element)

        import sys
        print >>sys.stderr, tracking_request

        data = urlencode({'xml': tracking_request})

        response = self.send_rest_request(tracking_request, appendix='/sendungsverfolgung', data=data)

        if response is not None and len(response):
            #do something
            return response
        else:
            print 'error'
            return None

        transaction.save(trigger_generation=False)

    def create_tracking_request(self, transactions):
        tracking_request = elements.tracking_data_tag(self.config, transactions)
        return tracking_request

    def __unicode__(self):
        return 'DHLTrackingService'