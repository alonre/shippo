from django.conf import settings
import base64

from adapter.models import Provider, Adapter
from api.models import Currency, Country, Unit

class DHLConfig:
    def __init__(self, user):
        self.provider = Provider.objects.get(name='Deutsche Post DHL')
        self.adapter = Adapter.objects.get(name='DHL')
        self.currency_eur = Currency.objects.get(iso='EUR')
        self.country_de = Country.objects.get(iso2='DE')
        self.user = user
        self.apiuser = user.apiuser
        self.settings = settings.ADAPTERS['DHL']
        self.label_lifetime = settings.LABEL_LIFETIME

        # Setup country & units
        self.unit_kg = Unit.objects.get(abbreviation='kg')
        self.unit_cm = Unit.objects.get(abbreviation='cm')

        # API Version to use
        self.create_shipment_dd_version_major = 1
        self.create_shipment_dd_version_minor = 0

        # SOAP Header
        self.headers = {
            'content-type': 'text/xml; charset=utf-8',
            'SOAPAction': 'http://de.ws.intrashipservice/',
            }
        self.namespaces = {
            'ns0': 'http://schemas.xmlsoap.org/soap/envelope/',
            'ns1': 'http://de.ws.intraship',
            'ns2':'http://dhl.de/webservice/cisbase',
            }
        # REST Header
        self.rest_headers = {
            'content-type': 'application/x-www-form-urlencoded; charset=utf-8',
            }
        
        self.dhl_intraship_username = self.apiuser.dhl_intraship_username
        self.dhl_intraship_password = self.apiuser.dhl_intraship_password
        self.dhl_account_number = self.apiuser.dhl_account_number
        self.production = self.apiuser.dhl_production

        if self.production:
            self.base_url_rest = self.settings['BASE_URL_REST_PRODUTION']
            self.base_url_soap = self.settings['BASE_URL_SOAP_PRODUCTION']
            self.api_user = self.settings['USER_PRODUCTION']
            self.api_password = self.settings['PASSWORD_PRODUCTION']
        else:
            self.base_url_rest = self.settings['BASE_URL_REST_TEST']
            self.base_url_soap = self.settings['BASE_URL_SOAP_TEST']
            self.api_user = self.settings['USER_TEST']
            self.api_password = self.settings['PASSWORD_TEST']
            
    def credentials_valid(self, transaction = None, *args, **kwargs):
        if self.dhl_intraship_username and self.dhl_intraship_password and self.dhl_account_number and self.api_user and self.api_password:
            import sys
            print >>sys.stderr, "DHL Credentials checked. VALID!"
            return True
        else:
            import sys
            print >>sys.stderr, "DHL Credentials checked. VALID!"
            
            if transaction:
                transaction.credentials_missing()
            return False

    def __unicode__(self):
        return 'DHLConfig'