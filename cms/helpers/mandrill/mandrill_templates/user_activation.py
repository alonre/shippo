from api.models import APIUser
from django.core.urlresolvers import reverse
from django.utils.http import urlquote

def getParams(apiuser):
    email_encoded = urlquote(apiuser.user.email)
    #activation_path = reverse('user:useractivation', args=[email_encoded, apiuser.shippo_activation_code])
    activation_path = '/activate/' + email_encoded + '/' + apiuser.shippo_activation_code + '/'
    activation_url = 'https://goshippo.com' + activation_path
    return {
        'subject': 'Activate Your Shippo Account',
        'from_email': 'info@goshippo.com',
        'from_name': 'Shippo',
        'to': [{'email': apiuser.user.email,
             'name': apiuser.user.first_name,
             'type': 'to'}],
        'headers': {'Reply-To': 'info@goshippo.com'},
        'auto_html': None,
        'auto_text': None,
        'track_opens': True,
        'track_clicks': True,
        'auto_text': None,
        'auto_html': None,
        'inline_css': None,
        'url_strip_qs': None,
        'preserve_recipients': None,
        'view_content_link': None,
        'tracking_domain': None,
        'signing_domain': None,
        'return_path_domain': None,
        'merge': True,
        'metadata': {'website': 'goshippo.com'},
        'global_merge_vars': [
            {'name': 'ACTIVATION_URL', 'content': activation_url}
        ]
    }