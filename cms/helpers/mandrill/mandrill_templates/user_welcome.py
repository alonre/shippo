from api.models import APIUser

def getParams(apiuser):
    return {
        'subject': 'Set up your shipping account',
        'from_email': 'laura@goshippo.com',
        'from_name': 'Laura Behrens Wu',
        'to': [{'email': apiuser.user.email,
             'name': apiuser.user.first_name,
             'type': 'to'}],
        'headers': {'Reply-To': 'laura@goshippo.com'},
        'auto_html': None,
        'auto_text': None,
        'track_opens': True,
        'track_clicks': True,
        'auto_text': None,
        'auto_html': None,
        'inline_css': None,
        'url_strip_qs': None,
        'preserve_recipients': None,
        'view_content_link': None,
        'tracking_domain': None,
        'signing_domain': None,
        'return_path_domain': None,
        'merge': True,
        'metadata': {'website': 'goshippo.com'},
        'global_merge_vars': [
            {'name': 'FIRSTNAME', 'content': apiuser.user.first_name}
        ]
    }