import mandrill
from django.conf import settings
from django.core.mail import send_mail
from mandrill_templates import user_activation, user_welcome

class MandrillMailService:
    def __init__(self, *args, **kwargs):
        self.api_key = settings.MANDRILL_API_KEY

    def sendTemplateMail(self, template_name, params=''):
        mandrill_client = mandrill.Mandrill(self.api_key)
        template_content = [{'content': '', 'name': ''}]
        try:
            return mandrill_client.messages.send_template(
                template_name=template_name,
                template_content=template_content,
                message=params,
                async=False,
                ip_pool='Main Pool')
            return True

        except Exception as e:
            import sys
            print >>sys.stderr, 'Mandrill Sending Error: ' + str(e)
            subject = 'Mandrill: Sending Error'
            message = "Mandrill Sending Error for Template " + str(template_name) + ": " + str(e) + ". Params were: " + str(params)
            to_mail = settings.FRONTEND_ERROR_CONTACT_EMAIL
            send_mail(subject, message, 'error@goshippo.com', [to_mail], fail_silently=True)
            raise Exception(str(e))


    def ActivationMail(self, apiuser):
        template_name = 'shippo-user-activation'
        params = user_activation.getParams(apiuser)
        return self.sendTemplateMail(template_name, params)

    def WelcomeMail(self, apiuser):
        template_name = 'laura-registration-welcome'
        params = user_welcome.getParams(apiuser)
        return self.sendTemplateMail(template_name, params)