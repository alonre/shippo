# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'SEOPage.body_markdown'
        db.alter_column(u'cms_seopage', 'body_markdown', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Blogpost.body_markdown'
        db.alter_column(u'cms_blogpost', 'body_markdown', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'BlogImage.file'
        db.alter_column(u'cms_blogimage', 'file', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'SEOPage.body_markdown'
        raise RuntimeError("Cannot reverse this migration. 'SEOPage.body_markdown' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'SEOPage.body_markdown'
        db.alter_column(u'cms_seopage', 'body_markdown', self.gf('django.db.models.fields.TextField')())

        # User chose to not deal with backwards NULL issues for 'Blogpost.body_markdown'
        raise RuntimeError("Cannot reverse this migration. 'Blogpost.body_markdown' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Blogpost.body_markdown'
        db.alter_column(u'cms_blogpost', 'body_markdown', self.gf('django.db.models.fields.TextField')())

        # Changing field 'BlogImage.file'
        db.alter_column(u'cms_blogimage', 'file', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True))

    models = {
        u'adapter.provider': {
            'Meta': {'object_name': 'Provider'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_filename': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'cms.blogimage': {
            'Meta': {'object_name': 'BlogImage'},
            'blogpost': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cms.Blogpost']", 'null': 'True', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'cms.blogpost': {
            'Meta': {'ordering': "('-pub_date',)", 'object_name': 'Blogpost'},
            'body_html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'body_markdown': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'cms.carriercontent': {
            'Meta': {'ordering': "('slug',)", 'object_name': 'CarrierContent'},
            'available_countries': ('django.db.models.fields.TextField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'available_services': ('django.db.models.fields.TextField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'carrier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Provider']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description_markdown': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label_manual_instruction_html': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'label_manual_instruction_markdown': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'overview_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'overview_region': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'cms.seopage': {
            'Meta': {'ordering': "('-pub_date',)", 'object_name': 'SEOPage'},
            'body_html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'body_markdown': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'button_text': ('django.db.models.fields.CharField', [], {'default': "'Find the best shipping rates now'", 'max_length': '100'}),
            'carrier_content': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cms.CarrierContent']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['cms']