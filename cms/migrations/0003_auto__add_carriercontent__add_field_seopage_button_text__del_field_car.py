# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CarrierContent'
        db.create_table(u'cms_carriercontent', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('carrier', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['adapter.Provider'], null=True, blank=True)),
            ('description_html', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('description_markdown', self.gf('django.db.models.fields.TextField')()),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'cms', ['CarrierContent'])

        # Adding field 'SEOPage.button_text'
        db.add_column(u'cms_seopage', 'button_text',
                      self.gf('django.db.models.fields.CharField')(default='Find the best shipping rates now', max_length=100),
                      keep_default=False)

        # Deleting field 'CarrierPage.provider'
        db.delete_column(u'cms_carrierpage', 'provider_id')

        # Adding field 'CarrierPage.carrier'
        db.add_column(u'cms_carrierpage', 'carrier',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['adapter.Provider'], null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'CarrierContent'
        db.delete_table(u'cms_carriercontent')

        # Deleting field 'SEOPage.button_text'
        db.delete_column(u'cms_seopage', 'button_text')

        # Adding field 'CarrierPage.provider'
        db.add_column(u'cms_carrierpage', 'provider',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['adapter.Provider'], null=True, blank=True),
                      keep_default=False)

        # Deleting field 'CarrierPage.carrier'
        db.delete_column(u'cms_carrierpage', 'carrier_id')


    models = {
        u'adapter.provider': {
            'Meta': {'object_name': 'Provider'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_filename': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'cms.carriercontent': {
            'Meta': {'object_name': 'CarrierContent'},
            'carrier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Provider']", 'null': 'True', 'blank': 'True'}),
            'description_html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description_markdown': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'cms.carrierpage': {
            'Meta': {'ordering': "('-pub_date',)", 'object_name': 'CarrierPage'},
            'body_html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'body_markdown': ('django.db.models.fields.TextField', [], {}),
            'carrier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Provider']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'cms.seopage': {
            'Meta': {'ordering': "('-pub_date',)", 'object_name': 'SEOPage'},
            'body_html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'body_markdown': ('django.db.models.fields.TextField', [], {}),
            'button_text': ('django.db.models.fields.CharField', [], {'default': "'Find the best shipping rates now'", 'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['cms']