from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse
from cms.models import *

class SEOPagesSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.5

    def items(self):
        return SEOPage.objects.all()

    def lastmod(self, obj):
        return obj.pub_date

class BlogPostsSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.5

    def items(self):
        return Blogpost.objects.all()

    def lastmod(self, obj):
        return obj.pub_date

class CarrierContentSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return CarrierContent.objects.all()

    def lastmod(self, obj):
        return obj.pub_date

class DesignPagesSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.8

    def items(self):
        return ['cms:homepage',
                'cms:apilandingpage',
                'cms:demopage',
                'cms:pricingpage',
                'cms:becomepartnerpage',
                'cms:courierspage',
                'cms:faqpage',
                'cms:contactuspage',
                'cms:sitemappage',
                'cms:jobspage',
                'cms:termsservicepage',
                'cms:privacypolicypage',
                'cms:prohibiteditemspage',
                'docs:overview',
                'docs:addresses',
                'docs:parcels',
                'docs:shipments',
                'docs:rates',
                'docs:transactions',
                'docs:billing',
                'docs:testing',
                'user:userlogin',
                ]

    def location(self, item):
        return reverse(item)
