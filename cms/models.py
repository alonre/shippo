from django.db import models
from django.contrib.sitemaps import Sitemap
import markdown
from django.template.defaultfilters import slugify
from model_utils import Choices
from django.conf import settings

class PageMixIn(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique_for_date='pub_date', help_text='Automatically built from title.', max_length=100)
    body_html = models.TextField(blank=True, help_text='Will be automatically filled if left blank.')
    body_markdown = models.TextField(blank=True, null=True)
    pub_date = models.DateTimeField('Date published', auto_now=True)

    class Meta:
        abstract = True

class CarrierContent(models.Model):
    '''Main content page for a carrier, linked on supported carrier page'''

    # Choices
    REGION_CHOICE = Choices('NORTH_AMERICA', 'EUROPE', 'ASIA', 'AFRICA', 'SOUTH_AMERICA', 'GLOBAL')

    carrier = models.ForeignKey('adapter.Provider', blank=True, null=True)
    description_html = models.TextField(blank=True)
    description_markdown = models.TextField()
    title = models.CharField(max_length=200)
    slug = models.SlugField(blank=True, null=True)
    description = models.TextField(max_length=1000, blank=True, null=True)
    available_services = models.TextField(max_length=500, blank=True, null=True)
    available_countries = models.TextField(max_length=500, blank=True, null=True)
    overview_enabled = models.BooleanField(default=False)
    overview_region = models.CharField(max_length=50, choices=REGION_CHOICE, blank=True, null=True)
    label_manual_instruction_markdown = models.TextField(blank=True, null=True)
    label_manual_instruction_html = models.TextField(blank=True, null=True)
    pub_date = models.DateTimeField('Date published', auto_now=True, blank=True, null=True)

    class Meta:
        ordering = ('slug',)
        get_latest_by = 'carrier'
        verbose_name_plural = 'Carrier Contents'

    def __unicode__(self):
        return u'%s' %(self.carrier.name)

    def get_absolute_url(self):
        return "/%s/" %(self.slug)

    def save(self):
        self.slug = slugify(self.carrier.name)
        self.description_html = markdown.markdown(self.description_markdown, safe_mode = False)
        if not self.label_manual_instruction_html:
            self.label_manual_instruction_html = markdown.markdown(self.label_manual_instruction_markdown, safe_mode = False)
        super(CarrierContent, self).save()

class SEOPage(PageMixIn):
    '''Carrier-unspecific SEO pages linked only in sitemap'''
    button_text = models.CharField(max_length=100, default='Find the best shipping rates now')
    carrier_content = models.ForeignKey('cms.CarrierContent', blank=True, null=True, help_text='Link this page to a carrier overview page.')

    class Meta:
        ordering = ('-pub_date',)
        get_latest_by = 'pub_date'
        verbose_name_plural = 'SEO Pages'

    def __unicode__(self):
        return u'%s' %(self.title)

    def get_absolute_url(self):
        return "/%s/" %(self.slug)

    def save(self):
        if not self.body_html:
            self.body_html = markdown.markdown(self.body_markdown, safe_mode = False)
        super(SEOPage, self).save()

class Blogpost(PageMixIn):
    '''A Shippo Blog Post'''

    class Meta:
        ordering = ('-pub_date',)
        get_latest_by = 'pub_date'
        verbose_name_plural = 'Blogposts'

    def __unicode__(self):
        return u'%s' %(self.title)

    def get_absolute_url(self):
        return "/blog/%s/" %(self.slug)

    def save(self):
        if not self.body_html:
            self.body_html = markdown.markdown(self.body_markdown, safe_mode = False)

        super(Blogpost, self).save()

class BlogImage(models.Model):
    file = models.FileField(upload_to=settings.BLOG_IMAGE_URL, blank=True, null=True)
    title =  models.CharField(max_length=200, blank=True, null=True)
    blogpost = models.ForeignKey('Blogpost', blank=True, null=True)

    def __unicode__(self):
        return u'Image ID: %s' % self.id
