from django.shortcuts import render
from django.template import Template, Context
from cms.models import *
from adapter.models import Provider
from django.shortcuts import redirect, get_object_or_404
from django.template.defaultfilters import slugify
from django.core.mail import send_mail, BadHeaderError
from django.conf import settings
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def displayHomePage(request):
	context = {}
	return render(request, 'page_singledesigns/home.html', context)

def displayCarrierAdLandingPge(request, carrier):
	context = {'carrier':carrier}
	return render(request, 'page_singledesigns/home.html', context)

def displayDemoPage(request):
	context = {}
	return render(request, 'page_singledesigns/demo.html', context)

def displayPricingPage(request):
	context = {}
	return render(request, 'page_singledesigns/pricing.html', context)

def displaySEOPage(request, slug):
	all_carriers = Provider.objects.all()
	for carrier in all_carriers:
		if slugify(slug) == slugify(carrier.name):
			carrier_content = CarrierContent.objects.get(carrier=carrier)
			seopages = SEOPage.objects.filter(carrier_content=carrier_content)
			context = {'carrier_content': carrier_content, 'seopages': seopages}
			return render(request, 'page_types/carrier-overview.html', context)
	seo_page = get_object_or_404(SEOPage, slug=slug)
	context = {'seopage': seo_page}
	return render(request, 'page_types/seopage.html', context)

def displayBlogpage(request):
	all_posts = Blogpost.objects.order_by('-pub_date')
	paginator = Paginator(all_posts, 10)
	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		posts = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		posts = paginator.page(paginator.num_pages)
	context = {'posts': posts,}
	return render(request, 'page_types/blog.html', context)

def displayBlogpost(request, slug):
	blog_post = get_object_or_404(Blogpost, slug=slug)
	content_as_template = Template('{% load staticfiles %}' + blog_post.body_html)
	content = content_as_template.render(Context(''))
	context = {'post': blog_post, 'content': content}
	return render(request, 'page_types/blogpost.html', context)

def displayAPILandingPage(request):
	mail_subject = 'Shippo: Your API Access Information'
	message_body = 'New API registration (/api):'
	success, error = mail_name_inline_form(request, mail_subject, message_body)
	context = {'success': success, 'error': error}
	return render(request, 'page_singledesigns/api.html', context)

def displayBecomePartnerPage(request):
	mail_subject = 'Shippo: Your Carrier Access Information'
	message_body = 'New registration as carrier (/partner):'
	success, error = mail_name_inline_form(request, mail_subject, message_body)
	context = {'success': success, 'error': error}
	return render(request, 'page_singledesigns/become_partner.html', context)

def displayCouriersPage(request):
	all_carriers = CarrierContent.objects.filter(overview_enabled=True)
	context = {'carriers': all_carriers}
	return render(request, 'page_singledesigns/couriers.html', context)

def mail_name_inline_form(request, mail_subject, message_body=''):
	success = False
	error = False
	if request.method == 'POST':
		try:
			fullname = request.POST['fullname']
			email = request.POST['email']
			if email:
				try:
					subject = mail_subject
					message = message_body + ' Full name: ' + str(fullname) + ', E-mail: ' + str(email)
					to_mail = settings.DEFAULT_CONTACT_EMAIL
					send_mail(subject, message, email, [to_mail], fail_silently=True)
				except BadHeaderError:
					error = 'There was a problem submitting your registration. Please try again or contact us directly at info@goshippo.com. Sorry for the trouble!'
			else:
				error = 'It seems like you have not entered all required data. Please try again or contact us directly at info@goshippo.com. Sorry for the trouble!'
			if error == False:
				success = True
		except KeyError:
			error = 'There was a problem submitting your registration. Please try again or contact us directly at info@goshippo.com. Sorry for the trouble!'
	return success, error

def sendAJAXMail(request, from_email='ajaxmail@goshippo.com'):
	if request.method == 'POST':
		subject = request.POST['subject']
		message = request.POST['message']
		from_mail = from_email
		to_mail = settings.FRONTEND_ERROR_CONTACT_EMAIL
		send_mail(subject, message, from_mail, [to_mail], fail_silently=True)
		return HttpResponse('Success')

def displaySitemapPage(request):
	content_carriers = CarrierContent.objects.filter(overview_enabled=True)
	seopages = SEOPage.objects.all()
	context = {'content_carriers': content_carriers, 'seopages': seopages}
	return render(request, 'page_singledesigns/sitemap.html', context)

def displayContactUsPage(request):
	success = False
	error = False
	to_mail = settings.DEFAULT_CONTACT_EMAIL
	if request.method == 'POST':
		try:
			fullname = request.POST['fullname']
			email = request.POST['email']
			message = request.POST['message']
			if message and email:
				try:
					subject = 'Shippo: New Contact Form Submission'
					send_mail(subject, message, email, [to_mail], fail_silently=True)
				except BadHeaderError:
					error = 'There was a problem sending your message. Please try again or contact us directly at info@goshippo.com. Sorry for the trouble!'
			else:
				error = 'It seems like you have not entered all required data. Please try again or contact us directly at info@goshippo.com. Sorry for the trouble!'
			if error == False:
				success = True
		except KeyError:
			error = 'There was a problem sending your message. Please try again or contact us directly at info@goshippo.com. Sorry for the trouble!'
	context = {'success': success,'error': error}
	return render(request, 'page_singledesigns/contact-us.html', context)

def display404Page(request):
	context = {}
	return render(request, 'cms-404.html', context)

def display500Page(request):
	context = {}
	return render(request, 'cms-500.html', context)

