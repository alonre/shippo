from django.contrib import admin
from cms.models import *

class SEOPageAdmin(admin.ModelAdmin):
	list_display = ('title', 'pub_date', 'carrier_content', 'slug')
	search_fields = ['title', 'body_markdown']
	prepopulated_fields = {"slug" : ('title',)}
	fieldsets = (
		(None, {'fields': ('title', 'body_markdown', 'button_text', 'carrier_content', 'slug')}),
		('Advanced options', {
			'fields': ('body_html',)
		}),
	)

class BlogImageInline(admin.TabularInline):
	model = BlogImage

class BlogpostAdmin(admin.ModelAdmin):
	list_display = ('title', 'pub_date', 'slug')
	search_fields = ['title', 'body_markdown']
	prepopulated_fields = {"slug" : ('title',)}
	inlines = [BlogImageInline,]
	fieldsets = (
		(None, {'fields': ('title', 'body_markdown', 'slug')}),
		('Advanced options', {
			'fields': ('body_html',)
		}),
	)

class CarrierContentAdmin(admin.ModelAdmin):
	list_display = ('carrier', 'title')
	search_fields = ['title', 'description_markdown', 'carrier']
	fieldsets = (
		(None, {'fields': (('carrier', 'title'), ('description_markdown', 'description', 'available_services', 'available_countries'), ('overview_enabled', 'overview_region'), ('label_manual_instruction_markdown', 'label_manual_instruction_html'))}),
	)

admin.site.register(SEOPage, SEOPageAdmin)
admin.site.register(Blogpost, BlogpostAdmin)
admin.site.register(CarrierContent, CarrierContentAdmin)