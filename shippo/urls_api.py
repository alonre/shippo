from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic.base import RedirectView
from django.shortcuts import redirect
from django.http import HttpResponse

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/?', include(admin.site.urls)),
    url(r'^docs/', lambda x: redirect('https://goshippo.com/docs')), #we need the redirect to goshippo.com since api.goshippo.com/docs would throw an import error
    url(r'^v1/', include('api.urls')),
    url(r'^robots\.txt$', lambda r: HttpResponse("User-agent: *\nDisallow: /", mimetype="text/plain")),
)

#Do not add a general redirect. Apparently, this is NOT best-practice for APIs.
