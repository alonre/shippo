from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic.base import RedirectView
from cms.sitemap import *
from cms import views
from django.views.generic import TemplateView

admin.autodiscover()

# a dictionary of sitemaps
sitemaps = {
    'design_pages': DesignPagesSitemap,
    'carrier_pages': CarrierContentSitemap,
    'logistics_pages': SEOPagesSitemap,
    'blog_posts': BlogPostsSitemap,
}

handler404 = 'cms.views.display404Page'
handler500 = 'cms.views.display500Page'

urlpatterns = patterns('',
    url(r'^admin/?', include(admin.site.urls)),
    url(r'^shopify/', include('shopify_app.urls', namespace="shopify_app")),
    url(r'^etsy/', include('etsy_app.urls', namespace="etsy_app")),
    url(r'^', include('user.urls', namespace="user")),
    url(r'^', include('cms.urls', namespace="cms")),
    url(r'^', include('websearch.urls', namespace="websearch")),
    #url(r'^v1/', include('api.urls')),
    url(r'^docs/', include('docs.urls', namespace="docs")),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
)

# General redirect to seopage for all others
urlpatterns += patterns('',
    url(r'^(?P<slug>[a-zA-Z0-9_.-]+)/$', views.displaySEOPage, name='seopage'),
)

#General redirect to docs from root
#urlpatterns += patterns('',
    #url(r'^$', lambda x: redirect('https://goshippo.com')),
#)