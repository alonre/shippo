"""
Django settings for shippo project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

ROOT_URLCONF = 'shippo.urls_api'
WSGI_APPLICATION = 'shippo.wsgi.application'

import os
from django.conf.urls import patterns, url
from os import environ

# Directory settings
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
here = lambda * x: os.path.join(os.path.abspath(os.path.dirname(__file__)), *x)
PROJECT_ROOT = here('..')
# root() gives us file paths from the root of the system to whatever
# folder(s) we pass it starting at the parent directory of the current file.
root = lambda * x: os.path.join(os.path.abspath(PROJECT_ROOT), *x)

# Revenue settings
DEFAULT_FEE_PAYMENT_FIXED = 0.3000
DEFAULT_FEE_PAYMENT_RELATIVE = 0.0290
DEFAULT_FEE_SHIPPO_FIXED = 0.0
DEFAULT_FEE_SHIPPO_RELATIVE = 0.0

# Definitions of directories
DATA_DIR = root('..', 'data/')

# Staticfiles
STATICFILES_DIRS = (
    os.path.join(PROJECT_ROOT, '..', 'static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

if os.environ.get('SHIPPO_PRODUCTION') or os.environ.get('SHIPPO_STAGING'):
    #user-env-compile need to be enabled in heroku app: "heroku labs:enable user-env-compile -a myapp"
    DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
    STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

AWS_STORAGE_BUCKET_NAME = 'shippo-static'
STATIC_URL = "https://%s.s3.amazonaws.com/" % 'shippo-static'
if os.environ.get('SHIPPO_STAGING'):
    AWS_STORAGE_BUCKET_NAME = 'shippo-static-staging'
    STATIC_URL = "https://%s.s3.amazonaws.com/" % 'shippo-static-staging'

MEDIA_ROOT = ''
STATIC_ROOT = ''
MEDIA_URL = STATIC_URL + 'media/'
PROVIDER_IMAGE_URL = STATIC_URL + 'providers/'
AWS_ACCESS_KEY_ID = 'AKIAJR2WEQI3QMHE2OEA'
AWS_SECRET_ACCESS_KEY = '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'

BLOG_IMAGE_URL = 'blog/img/'

AWS_DELIVERY_BUCKET_NAME = 'shippo-delivery'
AWS_ACCESS_KEY_ID_DELIVERY = "AKIAJTHP3LLFMYAWALIA"
AWS_SECRET_ACCESS_KEY_DELIVERY = "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"

# Database settings for Heroku
import dj_database_url
DATABASES = {}
DATABASES['default'] =  dj_database_url.config()

# URL settings
from urlparse import urljoin
BASE_URL = "https://api.goshippo.com"
VERSION = "v1/"
CURRENT_API_URL = urljoin(BASE_URL, VERSION)
RATES_URL = "rates/"

LABEL_GENERATION_API_CALL = 'https://label.goshippo.com/trigger-transaction/'
LABEL_GENERATION_API_TOKEN = '95479d9b50b5050b27f1915f9d9f9699c60c7575' # token of user labelprinting

# API keys and stuff
GOOGLE_API_KEY = 'AIzaSyApyi-SY1KRXebyGfi49NYDy5rW3uPKheQ' # mind IP locking
OPENEXCHANGERATES_API_KEY = '26674602c112472a89b60afaa7933a0d' # mind 1,000 requests per month
GEOIP_CITY_IP4_DAT_URL = 'http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz' # TODO: Make a list
GEOIP_CITY_IP6_DAT_URL = 'http://geolite.maxmind.com/download/geoip/database/GeoLiteCityv6-beta/GeoLiteCityv6.dat.gz'
GEOIP_CITY_BASE_DIR = os.path.join(DATA_DIR, 'geoip')
GEOIP_CITY_IP4_DAT_FILE = os.path.join(GEOIP_CITY_BASE_DIR, 'GeoLiteCity.dat')
GEOIP_CITY_IP6_DAT_FILE = os.path.join(GEOIP_CITY_BASE_DIR, 'GeoLiteCityv6.dat')
GOOGLE_ANALYTICS_CODE = 'UA-42378660-2'
MIXPANEL_FRONTEND_DEV_CODE = '7b971ab659019e9ac69a84d9d3e3bf2a'
MIXPANEL_FRONTEND_PROD_CODE = '7ca05d902f1ce5ffbe82341cd6be0222'
MIXPANEL_API_CODE = 'd46ec377b95222a71f9a9d23bfed1217'
MIXPANEL_EXCLUDE_USERNAME = ['admin', 'test@goshippo.com', 'loadtest', 'unittest',]
STRIPE_PUBLISHABLE_KEY_PRODUCTION = 'pk_live_pC8KQk7ohpNtMVCi0aRQzPYI'
STRIPE_PUBLISHABLE_KEY_TEST = 'pk_test_OGjlgIe4swnl7wTrMLiFQmMe'
STRIPE_SECRET_KEY_PRODUCTION = 'sk_live_xZFMQPJp7ZjrvZvZ48OiXsjp'
STRIPE_SECRET_KEY_TEST = 'sk_test_LnpoT3w9kGTmIgfFueEcQ2kS'
STRIPE_CURRENCIES_ACCEPTED = ('USD',)
MANDRILL_API_KEY = 'E1H1HlanxLqYyq4-L2o0hA'

ADAPTERS = {
    'FedEx': {'WSDL_PATH' : os.path.join(DATA_DIR, 'fedex-wsdl'),
              'TEST': {'KEY' : 'P5oLidUwMiAThSPG', 'PASSWORD' : 'JwKbOsoaEvkfo663l4I3nG3lt', 'ACCOUNT' : '510087763', 'METER' : '118588325', 'TESTSERVER':True},
              'PRODUCTION': {'KEY' : 'xyCaxqwF33mliEpf', 'PASSWORD' : 'd2Q9igdv3bAZMRaPdhbdfRWgH', 'ACCOUNT' : '496398041', 'METER' : '105736362', 'TESTSERVER':False}},
    'UPS' : {
             'ACCESS_LICENSE_NUMBER': 'DCC318FA3974FD95',
             'USER_ID': 'diez-it',
             'PASSWORD': '9KX18q37w',
             'ACCOUNT': '0000T25T93',
             'BASE_URL_PRODUCTION': 'https://onlinetools.ups.com/ups.app/xml/',
             'BASE_URL_TEST': 'https://wwwcie.ups.com/ups.app/xml/'},
    'GADB' : {},
    'Endicia' : {
            'TEST_BASE_URL_XML': 'https://www.envmgr.com/LabelService/EwsLabelService.asmx/',
            'PRODUCTION_BASE_URL_XML': 'https://LabelServer.Endicia.com/LabelService//EwsLabelService.asmx/',
            'SERVICE_URL_XML': 'https://www.endicia.com/ELS/ELSServices.cfc?wsdl/',
            'RATES_URL_XML': 'CalculatePostageRatesXML',
            'LABEL_PURCHASE_URL_XML': 'GetPostageLabelXML',
            'ACCOUNT_STATUS_URL_XML': 'GetAccountStatusXML',
            'CHANGE_PASSPHRASE_URL_XML': 'ChangePassPhraseXML',
            'BUY_POSTAGE_URL_XML': 'BuyPostageXML',
            'PREFIX': '{www.envmgr.com/LabelService}',
            'REQUESTER_ID': 'lpop',
            'ACCOUNT_ID': '912333',
            'PASSPHRASE': 'CaptainAmerica2013!'},
    'Parcel2Go' : {
            'BASE_URL_PARCEL_SERVICE' : 'https://v3.api.parcel2go.com/ParcelService.asmx?WSDL',
            'BASE_URL_TRACK_SERVICE' : 'https://v3.api.parcel2go.com/TrackingService.asmx?WSDL',
            'API_KEY': 'bzeLNtvi3nfOHMT1DV0lDK2lj6VeHvdn9s34fAQsAUfd/A06VNEZ1cOFHHgBS34W',
            'CARD_REFERENCE': 'Shippo Debit Simon'
            },
    'DHL' : {
            'BASE_URL_REST_TEST': 'https://cig.dhl.de/services/sandbox/rest',
            'BASE_URL_REST_PRODUTION': 'https://cig.dhl.de/services/production/rest',
            'BASE_URL_SOAP_TEST': 'https://cig.dhl.de/services/sandbox/soap',
            'BASE_URL_SOAP_PRODUCTION': 'https://cig.dhl.de/services/production/soap',
            'USER_TEST': 'shippodhl',
            'PASSWORD_TEST': 'SanFrancisco2013!',
            'USER_PRODUCTION': 'Shippo_1',
            'PASSWORD_PRODUCTION': '0u8bTocxyaSl5tSZtEHgH3WgMyDPiC',
            'BUSINESS_USER_TEST': 'geschaeftskunden_api',
            'BUSINESS_SIGNATURE_TEST': 'Dhl_ep_test1',
            'BUSINESS_EKP_TEST': '50000000007201',
            'BUSINESS_TRACKING_USER_TEST': 'dhl_entwicklerportal',
            'BUSINESS_TRACKING_PASSWORD_TEST': 'Dhl_123!',
            'BUSINESS_TRACKING_USER_PRODUCTION': 'Custom ID we need to request at DHL',
            'BUSINESS_TRACKING_USER_PRODUCTION': 'Password for this ID',
            'BUSINESS_USER_PRODUCTION': 'Intraship-Benutzername',
            'BUSINESS_SIGNATURE_PRODUCTION': 'Intraship-Passwort',
            'BUSINESS_EKP_PRODUCTION': 'DHL Kundennummer',
    }
}

LABEL_LIFETIME = 60 * 60 * 24 * 30  # 30 days

HEROKU = {
    'API_KEY': '8ec5f4de-19c7-4a39-87ab-3ef3e472cc01',
    'APP_NAME': 'shippo-api'
}

# Set the amount of seconds after which rates are newly queried when they are requested.
# There is only a new attempt if the previous attempt did not generate any rates,
# either (a) because there are none or (b) because the queue was temporarily full or not available.
SHIPMENT_QUEUE_TIMEOUT = 180
# Set the amount of minutes after which the request to /transactions/<object_id>/tracking triggers a new API call
TRACKING_RESULT_LIFETIME = 60

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'in9=9c3m!b%c@)mxh+s#ltjt*s^%x78j-^63f459#)-0v7ra-5'

# Application definition
INSTALLED_APPS = (
    'api',
    'adapter',
    'cms',
    'user',
    'docs',
    'websearch',
    'south',
    'storages',
    'corsheaders',
    'rest_framework',
    'rest_framework.authtoken',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'shopify_app',
    'etsy_app',
)

# Middlewares
MIDDLEWARE_CLASSES = ()

if os.environ.get('SHIPPO_PRODUCTION'):
    #fwd all non-https requests; needs to be (one of) the first
    MIDDLEWARE_CLASSES += (
        'shippo.middleware.SSLMiddleware',
    )

MIDDLEWARE_CLASSES += (
    'django.middleware.gzip.GZipMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'subdomains.middleware.SubdomainURLRoutingMiddleware', #must come before django.middleware.common.CommonMiddleware
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'shopify_app.middleware.LoginProtection',
)

# CORS (Cross-Origin Resource Sharing) settings
# According to https://github.com/ottoyiu/django-cors-headers/
CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = (
    'goshippo.com',
    'www.goshippo.com',
    'dev.goshippo.com',
    'test.goshippo.com',
    'stage.goshippo.com',
)

CORS_URLS_REGEX = r'^/v1/.*$'

CORS_ALLOW_METHODS = (
    'GET',
    'POST',
    'OPTIONS'
)

CORS_ALLOW_HEADERS = (
    'x-requested-with',
    'content-type',
    'authorization',
    'x-csrftoken'
)

CORS_EXPOSE_HEADERS = ()
CORS_ALLOW_CREDENTIALS = False
CORS_PREFLIGHT_MAX_AGE = 86400

try:
    CACHES = {
        'default': {
            'BACKEND': 'django_bmemcached.memcached.BMemcached',
            'LOCATION': os.environ.get('MEMCACHEDCLOUD_SERVERS').split(','),
            'OPTIONS': {
                        'username': os.environ.get('MEMCACHEDCLOUD_USERNAME'),
                        'password': os.environ.get('MEMCACHEDCLOUD_PASSWORD')
                }
        }
    }
except:
    CACHES = {
          'default': {
              'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
          }
    }

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.UnicodeJSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'DEFAULT_THROTTLE_CLASSES': (
        'rest_framework.throttling.UserRateThrottle',
    ),
    'DEFAULT_THROTTLE_RATES':{
        'user': '5000/day',
        'user': '10/second',
    },
    'PAGINATE_BY': 5
}

# Internationalization
USE_TZ = False
TIME_ZONE = None

LANGUAGE_CODE = 'en-us'
USE_I18N = False # changed
USE_L10N = False # changed

DATETIME_FORMAT = 'Y-m-d H:i:s.u'
SHORT_DATETIME_FORMAT = 'Y-m-d H:i:s'
SHORT_DATE_FORMAT = 'Y-m-d'
MONTH_DAY_FORMAT = 'm-d'
YEAR_MONTH_FORMAT = 'Y-m'
TIME_FORMAT = 'H:i:s.u'

DATETIME_INPUT_FORMATS = (
    '%Y-%m-%d %H:%M:%S',
    '%Y-%m-%d %H:%M:%S.%f',
    '%Y-%m-%d %H:%M',
    '%Y-%m-%d',
)
DATE_INPUT_FORMATS = ('%Y-%m-%d',)
TIME_INPUT_FORMATS = ('%H:%M:%S.%f', '%H:%M:%S',)

FIRST_DAY_OF_WEEK = 0 # Sunday

NUMBER_GROUPING = 0
THOUSAND_SEPARATOR = None
USE_THOUSAND_SEPARATOR = False

# E-Mail Settings
DEFAULT_FROM_EMAIL = 'api@goshippo.com'
SERVER_EMAIL = 'api@goshippo.com'

DEFAULT_CONTACT_EMAIL = 'info@goshippo.com'
FRONTEND_ERROR_CONTACT_EMAIL = 'simon@goshippo.com'
BACKEND_ERROR_CONTACT_EMAIL = 'tech@goshippo.com'

EMAIL_HOST = 'w010c1a2.kasserver.com'
EMAIL_HOST_USER = 'm02babb7'
EMAIL_HOST_PASSWORD = 'CgHURs5KcnSypSqD'
EMAIL_PORT = 587
EMAIL_SUBJECT_PREFIX = '[shippo] '
EMAIL_USE_TLS = True

# Added for Heroku compatibility - Honor the 'X-Forwarded-Proto' Header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
ALLOWED_HOSTS = ['*']
APPEND_SLASH = True

#Custom Template Context Processor
TEMPLATE_CONTEXT_PROCESSORS = ( 'user.context_processors.resolve_urlname',
                                'user.context_processors.access_settings',
                                'user.context_processors.lookup_apiuser',
                                'django.contrib.auth.context_processors.auth',
                                'django.core.context_processors.debug',
                                'django.core.context_processors.i18n',
                                'django.core.context_processors.media',
                                'django.core.context_processors.static',
                                'django.core.context_processors.tz',
                                'django.contrib.messages.context_processors.messages',
                                'shopify_app.context_processors.shopify_connected',
                                )

TEMPLATE_LOADERS = ('django.template.loaders.filesystem.Loader',
                    'django.template.loaders.app_directories.Loader')

#Custom Login URL for User frontend
LOGIN_URL = 'user:userlogin'

#Shippo Frontend Websearch Username & Password
SHIPPO_API_FRONTEND = {
    'username': 'frontend@goshippo.com',
    'password': 'FrontendShippo2014!',
    'token': 'dbba158a43aacbed571c7329461ab72502097c1a',
    'http_type': 'https',
    'base_url': 'api.goshippo.com',
    'addresses_url_ext': '/v1/addresses/',
    'parcels_url_ext': '/v1/parcels/',
    'shipments_url_ext': '/v1/shipments/',
    'rates_url_ext': '/rates/',
    'transactions_url_ext': '/v1/transactions/',
    'test_user': 'unittest',
    'test_password': 'unittest'
}

#Set a SITE ID to allow Sitemap creation
SITE_ID = 1

#http://django-subdomains.readthedocs.org/en/latest/
# A dictionary of urlconf module paths, keyed by their subdomain.
SUBDOMAIN_URLCONFS = {
    None: 'shippo.urls',  # no subdomain, e.g. ``example.com``
    'www': 'shippo.urls',
    'test': 'shippo.urls',
    'stage': 'shippo.urls',
    'api': 'shippo.urls_api',
    'api-test': 'shippo.urls_api',
    'api-stage': 'shippo.urls_api',
}

TEST_RUNNER = 'api.testrunner.DatabaselessTestRunner'

if os.environ.get('SHIPPO_PRODUCTION'):
    DEBUG = False
    TEMPLATE_DEBUG = False
    #send error mails to the following admins
    ADMINS = (('Max Diez', 'max@goshippo.com'), ('Simon Kreuz', 'simon@goshippo.com'))

if os.environ.get('SHIPPO_STAGING'):
    DEBUG = False
    TEMPLATE_DEBUG = False

    #use stripe test keys
    STRIPE_PUBLISHABLE_KEY_PRODUCTION = 'pk_test_OGjlgIe4swnl7wTrMLiFQmMe'
    STRIPE_SECRET_KEY_PRODUCTION = 'sk_test_LnpoT3w9kGTmIgfFueEcQ2kS'

    #use admin user in staging
    SHIPPO_API_FRONTEND = {
        'username': 'frontend@goshippo.com',
        'password': 'frontend',
        'token': '6776df969cdc049ee445d37122ccc5581dffc604',
        'http_type': 'http',
        'base_url': 'api-stage.goshippo.com',
        'addresses_url_ext': '/v1/addresses/',
        'parcels_url_ext': '/v1/parcels/',
        'shipments_url_ext': '/v1/shipments/',
        'rates_url_ext': '/rates/',
        'transactions_url_ext': '/v1/transactions/',
        'test_user': 'unittest',
        'test_password': 'unittest'
        }

    #disable GA
    GOOGLE_ANALYTICS_CODE = False
    #disable mixpanel
    MIXPANEL_FRONTEND_PROD_CODE = 'DISABLED7b971ab659019e9ac69a84d9d3e3bf2a'
    MIXPANEL_API_CODE = 'DISABLEDd46ec377b95222a71f9a9d23bfed1217'
    #send error mails to the following admins
    ADMINS = (('Simon Kreuz', 'simon@goshippo.com'),)
