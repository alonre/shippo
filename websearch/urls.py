from django.conf.urls import patterns, url
from websearch import views

urlpatterns = patterns('',
    url(r'^results/$', views.displayShipmentResults, name='shipmentresults'),
    url(r'^results/internal/getshipmentid/$', views.getShipmentID, name='getshipmentid'),
    url(r'^results/internal/submitnoresultsform/$', views.submitNoresultsForm, name='submitnoresultsform'),
    url(r'^results/get-label/$', views.getManualLabelInstructionsPage, name='manuallabelinstructions'),
)