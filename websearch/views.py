from django.shortcuts import render
from django.shortcuts import redirect
from django.core.mail import send_mail, BadHeaderError
from django.conf import settings
from shippo_api.shippo_api import ShippoAPI
import sys
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

def displayShipmentResults(request):
    http_type = settings.SHIPPO_API_FRONTEND['http_type']
    base_url = settings.SHIPPO_API_FRONTEND['base_url']
    rates_url_ext = settings.SHIPPO_API_FRONTEND['rates_url_ext']
    shipments_url_ext = settings.SHIPPO_API_FRONTEND['shipments_url_ext']
    api_token = settings.SHIPPO_API_FRONTEND['token']
    context = {
        'base_url': base_url,
        'rates_url_ext': rates_url_ext,
        'api_token': api_token,
        'shipments_url_ext': shipments_url_ext,
        'http_type': http_type
        }
    return render(request, 'websearch/results.html', context)

def getManualLabelInstructionsPage(request):
    rate_id = request.GET.get("id")
    if not rate_id:
        #if no id GETable, redirect to homepage
        return HttpResponseRedirect(reverse('cms:homepage'))
    from api.models import Rate, Currency
    from cms.models import CarrierContent
    rate = Rate.objects.get(object_id=rate_id)
    currency = Currency.objects.get(iso='USD')
    rate.fill_with_local_currency(currency=currency)
    try:
        carrier_content = CarrierContent.objects.get(carrier=rate.provider)
    except:
        carrier_content = False
    success = False
    error = False
    if request.method == 'POST':
        from cms.views import mail_name_inline_form
        mail_subject = 'Shippo: Your API Access Information'
        message_body = 'New registration (/get-label). Rate was ' + rate.provider.name + ' ' + rate.servicelevel_name + ' (Rate Object ID: ' + str(rate.object_id) + '); Registration:'
        success, error = mail_name_inline_form(request, mail_subject, message_body)
    context = {
        'rate': rate,
        'carrier_content': carrier_content,
        'success': success,
        'error': error,
        }
    return render(request, 'websearch/label_manual_instructions.html', context)

def getShipmentID(request):
    shipment_id = ''
    dimension_unit = request.GET['dimensions-unit']
    weight_unit = request.GET['weight-unit']
    if weight_unit == 'lbs': weight_unit = 'lb'
    if dimension_unit == 'inch': dimension_unit = 'in'
    try:
        from_address_id = ShippoAPI().saveAddress(
            city=request.GET['from-city'],
            state=request.GET['from-state'],
            zip=request.GET['from-ZIP'],
            country=request.GET['from-country']
        )
        if from_address_id:
            to_address_id = ShippoAPI().saveAddress(
                city=request.GET['to-city'],
                state=request.GET['to-state'],
                zip=request.GET['to-ZIP'],
                country=request.GET['to-country']
            )
            if from_address_id and to_address_id:
                parcel_id = ShippoAPI().saveParcel(
                    length=float(request.GET['dimensions-length']),
                    width=float(request.GET['dimensions-width']),
                    height=float(request.GET['dimensions-height']),
                    distance_unit=dimension_unit,
                    weight=float(request.GET['weight-value']),
                    mass_unit=weight_unit,
                )
                if from_address_id and to_address_id and parcel_id:
                    shipment_id = ShippoAPI().saveShipment(
                        address_from=from_address_id,
                        address_to=to_address_id,
                        parcel=parcel_id,
                    )
    except:
        pass
    return HttpResponse(shipment_id)

def submitNoresultsForm(request):
    if request.method == 'POST':
        try:
            fullname = request.POST['visitor_name']
            email = request.POST['visitor_email']
            query = request.POST['visitor_query']
            if email and fullname:
                try:
                    subject = 'Shippo: Your API Access Information'
                    message = 'New registration via search without any rates found:' + ' Full name: ' + str(fullname) + ', E-mail: ' + str(email) + ', Query: ' + str(query)
                    to_mail = settings.DEFAULT_CONTACT_EMAIL
                    send_mail(subject, message, email, [to_mail], fail_silently=True)
                except BadHeaderError:
                    raise Exception('E-Mail could not be send')
            else:
                raise Exception('Not all fields filled out')
        except KeyError:
            raise Exception('Key Error')
    return HttpResponse('Success')