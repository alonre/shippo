# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth.models import User
from api.models import Country, Currency, Unit, APIUser, Address, Parcel, Shipment, Transaction, Rate
import sys, base64, urllib2, httplib, json

class ShippoAPI:
    def __init__(self, *args, **kwargs):
        self.USERNAME = settings.SHIPPO_API_FRONTEND['username']
        self.PASSWORD = settings.SHIPPO_API_FRONTEND['password']
        self.BASE64AUTHKEY = base64.encodestring('%s:%s' % (self.USERNAME, self.PASSWORD))[:-1]

        self.HTTP_TYPE = settings.SHIPPO_API_FRONTEND['http_type']
        self.BASE_URL = settings.SHIPPO_API_FRONTEND['base_url']
        self.ADDRESSES_URL_EXT = settings.SHIPPO_API_FRONTEND['addresses_url_ext']
        self.PARCELS_URL_EXT = settings.SHIPPO_API_FRONTEND['parcels_url_ext']
        self.SHIPMENTS_URL_EXT = settings.SHIPPO_API_FRONTEND['shipments_url_ext']
        self.RATES_URL_EXT = settings.SHIPPO_API_FRONTEND['rates_url_ext']
        self.TRANSACTIONS_URL_EXT = settings.SHIPPO_API_FRONTEND['transactions_url_ext']

    def saveAddress(self,
            purpose='QUOTE',
            name='',
            company='',
            street1='',
            street_no='',
            street2='',
            city='',
            zip='',
            state='',
            country='',
            phone='',
            email='',
            ip='',
            user=False
        ):
        try:
            address = Address()
            if not user:
                user = User.objects.get(username=self.USERNAME)
            address.object_owner = user
            address.object_purpose = purpose
            address.name = name
            address.company = company
            address.street1 = street1
            address.street_no = street_no
            address.street2 = street2
            address.city = city
            address.zip = zip
            address.state = state
            country_object = Country.objects.get(iso2=country)
            address.country = country_object
            address.phone = phone
            address.email = email
            address.ip = ip
            address.save()
            return str(address.object_id)
        except Exception as e:
            raise Exception('Address error: address data missing or invalid (' + str(e.message) + ').')

    def saveParcel(self,
            length='30',
            width='20',
            height='20',
            distance_unit='cm',
            weight='5',
            mass_unit='kg',
            user=False
        ):
        try:
            parcel = Parcel()
            if not user:
                user = User.objects.get(username=self.USERNAME)
            parcel.object_owner = user
            parcel.length = length
            parcel.width = width
            parcel.height = height
            distance_unit_object = Unit.objects.get(abbreviation=distance_unit)
            parcel.distance_unit = distance_unit_object
            parcel.weight = weight
            mass_unit_object = Unit.objects.get(abbreviation=mass_unit)
            parcel.mass_unit = mass_unit_object
            parcel.save()
            return str(parcel.object_id)
        except Exception as e:
            raise Exception('Parcel error: parcel data missing or invalid (' + str(e.message) + ').')

    def saveShipment(self,
            object_purpose='QUOTE',
            address_from='',
            address_to='',
            parcel='',
            user=False
        ):
        try:
            shipment = Shipment()
            if not user:
                user = User.objects.get(username=self.USERNAME)
            shipment.object_owner = user
            shipment.object_purpose = object_purpose
            address_from_object = Address.objects.get(object_id=address_from)
            shipment.address_from = address_from_object
            address_to_object = Address.objects.get(object_id=address_to)
            shipment.address_to = address_to_object
            parcel_object = Parcel.objects.get(object_id=parcel)
            shipment.parcel = parcel_object
            shipment.save()
            return str(shipment.object_id)
        except Exception as e:
            raise Exception('Shipment error: shipment data missing or invalid (' + str(e.message) + ').')

    def saveTransaction(self,
            rate='',
            notification_email_from=False,
            notification_email_to=False,
            notification_email_other='',
            pickup_date='',
            user=False
        ):
        try:
            transaction = Transaction()
            if not user:
                user = User.objects.get(username=self.USERNAME)
            transaction.object_owner = user
            rate_object = Rate.objects.get(object_id=rate)
            transaction.rate = rate_object
            transaction.notification_email_from = notification_email_from
            transaction.notification_email_to = notification_email_to
            transaction.notification_email_other = notification_email_other
            transaction.pickup_date = pickup_date
            transaction.save()
            return str(transaction.object_id)
        except Exception as e:
            raise Exception('Transaction error: transaction data missing or invalid (' + str(e.message) + ').')