from django.shortcuts import render_to_response, redirect, render
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail
from django.template import RequestContext
from django.conf import settings
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from api.models import APIUser, Rate, Transaction
from rest_framework.authtoken.models import Token
from models import *
import datetime, sys, urllib
from user.views import loginFunction as cms_login
from websearch.shippo_api.shippo_api import ShippoAPI
import requests, os
from requests_oauthlib import OAuth1
from urlparse import parse_qs

def login(request):
    #if user is not logged in, we first need to get him an account
    if not request.user.is_authenticated():
        return redirect(reverse('etsy_app:etsyaccountlandingpage'))
    #if user is logged in, authenticate = get & redirect him to auth_url
    else:
        return authenticate(request)

def account_landingpage(request):
    error = False
    if request.method == 'POST':
        success, error = cms_login(request)
        if success:
            return HttpResponseRedirect(reverse('etsy_app:login'))
    context = {'error': error}
    return render(request, 'etsy_app/account_landingpage.html', context)

def authenticate(request):
    #authentication is NOT yet connected to a specific etsy user
    request_scope = settings.ETSY_API_SCOPE
    if os.environ.get('SHIPPO_PRODUCTION'):
        request_callback_url = 'https://goshippo.com' + reverse('etsy_app:finalize')
    elif os.environ.get('SHIPPO_STAGING'):
        request_callback_url = 'http://stage.goshippo.com' + reverse('etsy_app:finalize')
    else:
        request_callback_url = 'http://test-local.goshippo.com:5000' + reverse('etsy_app:finalize')
    oauth = OAuth1(settings.ETSY_API_KEY, client_secret=settings.ETSY_API_SECRET)
    params = {
        'scope': request_scope,
        'oauth_callback': request_callback_url,
    }
    r = requests.post(url=settings.ETSY_URLS['base_url'] + settings.ETSY_URLS['request_token'], params=params, auth=oauth)
    credentials = parse_qs(r.content)
    resource_owner_key = credentials.get('oauth_token')[0]
    resource_owner_secret = credentials.get('oauth_token_secret')[0]
    etsy_url = credentials.get('login_url')[0]
    #save to database
    apiuser = APIUser.objects.get(user=request.user)
    apiuser.etsy_oauth_token = resource_owner_key
    apiuser.etsy_oauth_secret = resource_owner_secret
    apiuser.save()
    return redirect(etsy_url)

def finalize(request):
    current_user = request.user
    apiuser = APIUser.objects.get(user=current_user)
    oauth = OAuth1(settings.ETSY_API_KEY,
                   client_secret=settings.ETSY_API_SECRET,
                   resource_owner_key=apiuser.etsy_oauth_token,
                   resource_owner_secret=apiuser.etsy_oauth_secret,
                   verifier=request.REQUEST.get('oauth_verifier'))
    try:
        r = requests.post(url=settings.ETSY_URLS['base_url'] + settings.ETSY_URLS['access_token'], auth=oauth)
        credentials = parse_qs(r.content)
        resource_owner_key = credentials.get('oauth_token')[0]
        resource_owner_secret = credentials.get('oauth_token_secret')[0]
    except:
        print >>sys.stderr, "Etsy views.finalize: ValidationException when trying to get session token after temp auth"
        subject = 'Etsy views.finalize: ValidationException'
        message = 'Etsy views.finalize: ValidationException when trying to get session token after temp auth; User: ' + current_user.username
        to_mail = settings.ETSY_ERROR_CONTACT_EMAIL
        send_mail(subject, message, 'error@goshippo.com', [to_mail], fail_silently=True)
        return redirect(reverse('cms:homepage'))
    apiuser.etsy_oauth_token = resource_owner_key
    apiuser.etsy_oauth_secret = resource_owner_secret
    apiuser.etsy_connected = True
    apiuser.save()
    return HttpResponseRedirect(reverse('user:userorders'))