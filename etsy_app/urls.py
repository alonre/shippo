from django.conf.urls import patterns, url
from etsy_app import views

urlpatterns = patterns('',
    url(r'^$', views.login, name='login'),
    url(r'^account/$', views.account_landingpage, name='accountlandingpage'),
    url(r'^authenticate/$', views.authenticate, name='authenticate'),
    url(r'^finalize/$', views.finalize, name='finalize'),
)