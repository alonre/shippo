from django.core.mail import send_mail
import shopify
from django.conf import settings
from requests_oauthlib import OAuth1

def start_session(apiuser):
	try:
		oauth = OAuth1(settings.ETSY_API_KEY,
                      client_secret=settings.ETSY_API_SECRET,
                      resource_owner_key=apiuser.etsy_oauth_token,
                      resource_owner_secret=apiuser.etsy_oauth_secret)
		return oauth
	except:
		import sys
		print >>sys.stderr, "Could not establish an Etsy Session for APIUser " + str(apiuser.user.email)
        message = "Could not establish a Etsy Session for APIUser " + str(apiuser.user.email)
        send_mail('Shippo: Etsy Session Error', message, 'error@goshippo.com', [settings.ETSY_ERROR_CONTACT_EMAIL], fail_silently=True)