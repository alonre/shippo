from user.models import Order as Order
from user.models import Item as Item
from api.models import Address, APIUser, Currency, Country
from django.conf import settings
from django.contrib.auth.models import User
from django.core.mail import send_mail
from urlparse import parse_qs
import requests, json, etsy_session, datetime

def import_orders(apiuser):
    oauth = etsy_session.start_session(apiuser)
    shops = get_user_shops(oauth, apiuser)
    for shop in shops:
        return get_unimported_orders(oauth, apiuser, shop)

def get_user_shops(oauth, apiuser):
    url = settings.ETSY_URLS['base_url'] + '/users/__SELF__/shops'
    r = requests.get(url=url, auth=oauth)
    response = r.json()
    shops = []
    for shop in response['results']:
        shops.append(shop['shop_id'])
    return shops

def get_unimported_orders(oauth, apiuser, shop):
    #get new orders
    existing_orders = Order.objects.filter(object_owner=apiuser.user, shop_app='Etsy')
    existing_order_ids = []
    if existing_orders:
        for order in existing_orders:
            existing_order_ids.append(order.object_id)
    url = settings.ETSY_URLS['base_url'] + '/shops/' + str(shop) + '/receipts'
    r = requests.get(url=url, auth=oauth)
    response = r.json()
    i = 0
    for order in response['results']:
        if order['receipt_id'] not in existing_order_ids:
            try:
                url = settings.ETSY_URLS['base_url'] + '/receipts/' + str(order['receipt_id']) + '/transactions'
                r2 = requests.get(url=url, auth=oauth)
                transactions = r2.json()
                save_data(oauth, order, transactions, apiuser.user)
                i = i + 1
            except:
                pass
    import sys
    print >>sys.stderr, 'Etsy Import: Found ' + str(len(response['results'])) + ' orders, imported ' + str(i) + '.'
    return True

def save_data(oauth, order, transactions, current_user):
    user_order = save_order(order, current_user)
    for transaction in transactions['results']:
        item = save_item(user_order, transaction)
    try:
        to_address = save_customer_address(oauth, order, user_order)
        user_order.to_address = to_address
        user_order.save()
    except:
        pass
    return user_order

def save_order(order, current_user):
    user_order = Order()
    user_order.object_owner = current_user
    user_order.shop_app = 'Etsy'
    user_order.object_id = order['receipt_id']
    user_order.created_at = datetime.datetime.fromtimestamp(int(order['creation_tsz'])).strftime('%Y-%m-%dT%H:%M:%SZ')
    user_order.currency = Currency.objects.get(iso=order['currency_code'])
    user_order.order_number = order['receipt_id']
    user_order.subtotal_price = order['subtotal']
    user_order.total_price = order['grandtotal']
    if 'total_tax_cost' in order and order['total_tax_cost']:
        user_order.total_tax = order['total_tax_cost']
    if 'total_shipping_cost' in order and order['total_shipping_cost']:
        user_order.shipping_cost = order['total_shipping_cost']
    user_order.shipping_cost_currency = user_order.currency
    #Etsy provides no weight or shipping method info
    #order.total_weight = order.total_weight
    #order.shipping_method = order.shipping_lines[0].title
    user_order.save()
    return user_order

def save_item(order, transaction):
    item = Item()
    item.order = order
    item.object_id = transaction['transaction_id']
    #Etsy provides no weight
    #item.grams = transaction['transaction_id']
    item.price = transaction['price']
    item.currency = Currency.objects.get(iso=transaction['currency_code'])
    item.product_id = transaction['listing_id']
    item.quantity = transaction['quantity']
    item.title = transaction['title']
    item.save()
    return item

def save_customer_address(oauth, order, user_order):
    address = Address()
    address.object_owner = user_order.object_owner
    address.object_purpose = 'PURCHASE'
    address.name = order['name']
    address.company = ''
    address.street1 = order['first_line']
    address.street_no = ''
    #for second line, let's check if it exists to prevent exception. we can ship without second line.
    if 'second_line' in order and order['second_line']:
        address.street2 = order['second_line']
    else:
        address.street2 = ''
    address.city = order['city']
    if 'state' in order and order['state']:
        address.state = order['state']
    else:
        address.state = ''
    address.zip = order['zip']
    #Countries are provided by ID only, i.e. we need to call Etsy again, except hardcoded US=209 to limit queries
    if str(order['country_id']) == '209':
        country_code = 'US'
    else:
        url = settings.ETSY_URLS['base_url'] + '/countries/' + str(order['country_id']) + '?api_key=' + settings.ETSY_API_KEY
        r = requests.get(url=url)
        response = r.json()
        country_code = response['results'][0]['iso_country_code']
    address.country = Country.objects.get(iso2=country_code)
    #Etsy provides no phone numbers
    address.phone = '4151234567'
    if 'buyer_email' in order and order['buyer_email']:
        address.email = order['buyer_email']
    else:
        address.email = 'info@goshippo.com'
    address.ip = ''
    address.save()
    return address
