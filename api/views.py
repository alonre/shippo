from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.http import Http404
from rest_framework import generics
from rest_framework import permissions
from rest_framework import authtoken

from api.models import Address, Parcel, Shipment, Rate, Currency, Transaction, Location, Track
from api.serializers import AddressSerializer, ParcelSerializer, ShipmentSerializer, RateSerializer, TransactionSerializer, LocationSerializer, TrackSerializer
from api.permissions import IsOwner

from adapter.dispatch import queue_generate_rates, queue_generate_locations

class AddressList(generics.ListCreateAPIView):
    """Defines Addresses from which a Shipment is submitted, to which a Shipment is directed, or which is used for billing purposes."""
    lookup_field = ('object_id')
    serializer_class = AddressSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)
    
    def get_queryset(self):
        user = self.request.user

        # Basic authentication allows listing the items; Token authentication does not.
        if not self.request.auth:
            return Address.objects.filter(object_owner=user).order_by('-object_created')
        else:
            raise PermissionDenied()

    def pre_save(self, obj):
        obj.object_owner = self.request.user
    
class AddressDetail(generics.RetrieveAPIView):
    """An Address from which a Shipment is submitted, to which a Shipment is directed, or which is used for billing purposes."""
    queryset = Address.objects.all()
    lookup_field = ('object_id')
    serializer_class = AddressSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)
        
class ParcelList(generics.ListCreateAPIView):
    """Defines a Parcel, which is an object that is shipped from A to B."""
    lookup_field = ('object_id')
    serializer_class = ParcelSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)
    
    def get_queryset(self):
        user = self.request.user
        # Basic authentication allows listing the items; Token authentication does not.
        if not self.request.auth:
            return Parcel.objects.filter(object_owner=user).order_by('-object_created')
        else:
            raise PermissionDenied()

    def pre_save(self, obj):
        obj.object_owner = self.request.user
        
class ParcelDetail(generics.RetrieveAPIView):
    """A Parcel, which is an object that is shipped from A to B."""
    queryset = Parcel.objects.all()
    lookup_field = ('object_id')
    serializer_class = ParcelSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)

class ShipmentList(generics.ListCreateAPIView):
    """Defines a Shipment, which is a transaction in which a Parcel is shipped from an Address to another Address."""
    lookup_field = ('object_id')
    serializer_class = ShipmentSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)
    
    def get_queryset(self):
        user = self.request.user
        
        # Basic authentication allows listing the items; Token authentication does not.
        if not self.request.auth:
            return Shipment.objects.filter(object_owner=user).order_by('-object_created')
        else:
            raise PermissionDenied()
        
    def pre_save(self, obj):
        obj.object_owner = self.request.user
        
class ShipmentDetail(generics.RetrieveAPIView):
    """A Shipment, which is a transaction in which a Parcel is shipped from an Address to another Address."""
    queryset = Shipment.objects.all()
    lookup_field = ('object_id')
    serializer_class = ShipmentSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)
    
class RateList(generics.ListAPIView):
    """Defines a Rate, which is the calculated Price for a Shipment along with all relevant meta-data."""

    paginate_by = 25

    queryset = Rate.objects.all()
    serializer_class = RateSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)
    
    def get_queryset(self):
        user = self.request.user
        shipment = None
        local_currency = None
        
        if 'shipment_id' in self.kwargs and self.kwargs['shipment_id']:
            shipment_id = self.kwargs['shipment_id']
            try:
                shipment = Shipment.objects.get(object_id=shipment_id, object_owner=user)
            except:
                raise Http404()
        if 'currency_iso' in self.kwargs and self.kwargs['currency_iso']:
            currency_iso = self.kwargs['currency_iso']
            currency_iso = currency_iso.upper()
            try:
                local_currency = Currency.objects.get(iso = currency_iso)
            except:
                raise Http404()
        elif shipment:
            local_currency = shipment.address_to.country.currency

        # is the view associated with a Shipment?
        if shipment:
            existing_rates = Rate.objects.filter(object_owner=user, object_current=True, shipment=shipment, currency_local=local_currency) # filter to show only CURRENT Rates for the LOGGEDIN User and the requested SHIPMENT
            if not existing_rates.exists():
                queue_generate_rates(shipment, local_currency)
                return_rates = []
            else:
                return_rates = existing_rates
        # or has it been triggered from /rates/?
        else:
            return_rates = Rate.objects.filter(object_owner=user, object_current=True).order_by('-object_created')

        return return_rates

class RateDetail(generics.RetrieveAPIView):
    """Defines a Rate, which is the calculated Price for a Shipment along with all relevant meta-data."""
    queryset = Rate.objects.all()
    lookup_field = ('object_id')
    serializer_class = RateSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)

class TransactionList(generics.ListCreateAPIView):
    """Defines a Transaction, which is a Label for a given Rate."""
    lookup_field = ('object_id')
    serializer_class = TransactionSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)

    def get_queryset(self):
        # Basic authentication allows listing the items; Token authentication does not.
        if not self.request.auth:
            return Transaction.objects.filter(object_owner=self.request.user).order_by('-object_created')
        else:
            raise PermissionDenied()

    def pre_save(self, obj):
        # only basic authentication lets users create transactions (and thus spend money)
        if not self.request.auth:
            obj.object_owner = self.request.user
        else:
            raise PermissionDenied()

class TransactionDetail(generics.RetrieveAPIView):
    """Defines a Transaction, which is a Label for a given Rate."""
    queryset = Transaction.objects.all()
    lookup_field = ('object_id')
    serializer_class = TransactionSerializer
    permissions_classes = (permissions.IsAuthenticated, IsOwner,)
    
class LocationList(generics.ListAPIView):
    """Locations are places near a given Address where Parcels can be dropped off or picked up. Usually, one Location provides service for one or more Providers."""
    serializer_class = LocationSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)
    
    def get_queryset(self):
        user = self.request.user

        # Everyone can list Locations for a given Address, we don't mind.
        if 'address_id' in self.kwargs and self.kwargs['address_id']:
            address_id = self.kwargs['address_id']
            try:
                source_address = Address.objects.get(object_id=address_id)
            except Exception as e:
                raise Http404()
            
            if source_address and not Location.objects.filter(address=source_address).filter(object_owner=self.request.user).exists():
                queue_generate_locations(source_address)
            
            try:
                locations = Location.objects.filter(address=source_address).filter(object_owner=self.request.user).order_by('distance_amount')
            except:
                locations = []
                raise Http404()
        else:
            locations = Location.objects.filter(object_owner=user).order_by('-object_created')
            
        return locations
    
class LocationDetail(generics.RetrieveAPIView):
    """A Location is a place where Parcels can be dropped off or picked up. Usually, one Location provides service for one or more Providers."""
    queryset = Location.objects.all()
    lookup_field = ('object_id')
    serializer_class = LocationSerializer
    permissions_classes = (permissions.IsAuthenticated, IsOwner,)
    
class TrackingList(generics.ListAPIView):
    """Trace the state of a Transaction, usually a Shipment in transit."""
    serializer_class = TrackSerializer
    permissions_classes = (permissions.IsAuthenticated, IsOwner,)
    
    def get_queryset(self):
        user = self.request.user
    
        # TODO: proper authentication here
        if 'transaction_id' in self.kwargs and self.kwargs['transaction_id']:
            transaction_id = self.kwargs['transaction_id']
            
            try:
                transaction = Transaction.objects.get(object_id=transaction_id)
            except Exception as e:
                raise Http404()
          
            if transaction:
                transaction.refresh_track()
                tracks = Track.objects.filter(transaction=transaction).order_by('-object_created')
            else:
                raise Http404()

        else:
            tracks = Track.objects.filter(object_owner=user).order_by('-object_created')
            
        return tracks
    
class TrackingDetail(generics.RetrieveAPIView):
    """Trace the state of a Transaction, usually a Shipment in transit."""
    queryset = Track.objects.all()
    lookup_field = ('object_id')
    serializer_class = TrackSerializer
    permissions_classes = (permissions.IsAuthenticated, IsOwner,)