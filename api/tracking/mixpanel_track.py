# -*- coding: utf-8 -*-
from rq import Queue
from worker import conn
from mixpanel import Mixpanel
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from decimal import *

from adapter.models import Provider

def track_event(mp_event, mp_owner, mp_object):
	q = Queue('low', connection=conn)
	q.enqueue_call(func=mp_event, args=(mp_owner, mp_object), result_ttl=0)

def identify_mp_user(mp_user):
	from api.models import APIUser
	mp = Mixpanel(settings.MIXPANEL_API_CODE)
	if mp_user.is_authenticated():
		if mp_user.username not in settings.MIXPANEL_EXCLUDE_USERNAME:
			apiuser = APIUser.objects.get(user=mp_user)
			if apiuser.mixpanel_user_id:
				return apiuser.mixpanel_user_id
			else:
				mp.people_set(mp_user.username, {
					'$first_name'    : mp_user.first_name,
					'$last_name'     : mp_user.last_name,
					'$email'         : mp_user.email
				})
				apiuser.mixpanel_user_id = mp_user.username
				apiuser.save()
				return apiuser.mixpanel_user_id

def track_API_address(mp_user, address):
	mp = Mixpanel(settings.MIXPANEL_API_CODE)
	mp_user_id = identify_mp_user(mp_user)
	mp_event = 'Address created'
	if mp_user_id is not None:
		mp.track(mp_user_id, mp_event, {
			'Object ID': str(address.object_id),
			'Object Purpose': address.object_purpose,
			'Object Source': address.object_source,
			'Object State': address.object_state,
			'Name': address.name,
			'Company': address.company,
			'Street1': address.street1,
			'Street No': address.street_no,
			'Street2': address.street2,
			'ZIP': address.zip,
			'City': address.city,
			'State': address.state,
			'Country': address.country.iso2,
			'IP': address.ip,
			'Phone': address.phone,
			'Email': address.email
			})

def track_API_parcel(mp_user, parcel):
	mp = Mixpanel(settings.MIXPANEL_API_CODE)
	mp_user_id = identify_mp_user(mp_user)
	mp_event = 'Parcel created'
	if mp_user_id is not None:
		mp.track(mp_user_id, mp_event, {
			'Object ID': str(parcel.object_id),
			'Object State': parcel.object_state,
			'Length': float(parcel.length),
			'Width': float(parcel.width),
			'Height': float(parcel.height),
			'Distance Unit': parcel.distance_unit.name,
			'Weight': float(parcel.weight),
			'Mass Unit': parcel.mass_unit.name
			})

def track_API_shipment(mp_user, shipment):
	mp = Mixpanel(settings.MIXPANEL_API_CODE)
	mp_user_id = identify_mp_user(mp_user)
	mp_event = 'Shipment created'
	if mp_user_id is not None:
		mp.track(mp_user_id, mp_event, {
			'Object ID': str(shipment.object_id),
			'Object Purpose': shipment.object_purpose,
			'Object State': shipment.object_state,
			'Address From ID': str(shipment.address_from.object_id),
			'Address To ID': str(shipment.address_to.object_id),
			'Parcel ID': str(shipment.parcel.object_id),
			'Length': float(shipment.parcel.length),
			'Width': float(shipment.parcel.width),
			'Height': float(shipment.parcel.height),
			'Distance Unit': shipment.parcel.distance_unit.name,
			'Weight': float(shipment.parcel.weight),
			'Mass Unit': shipment.parcel.mass_unit.name,
			'From ZIP': shipment.address_from.zip,
			'From City': shipment.address_from.city,
			'From State': shipment.address_from.state,
			'From Country': shipment.address_from.country.iso2,
			'To ZIP': shipment.address_to.zip,
			'To City': shipment.address_to.city,
			'To State': shipment.address_to.state,
			'To Country': shipment.address_to.country.iso2
			})

def track_API_rate(mp_user, rate):
	fedex = Provider.objects.get(name='FedEx')
	mp = Mixpanel(settings.MIXPANEL_API_CODE)
	mp_user_id = identify_mp_user(mp_user)
	rate, price, insurance, endpoint_outbound, endpoint_inbound = parse_rate(rate)
	mp_event = 'Rate created'

	if mp_user_id is not None:
		mp.track(mp_user_id, mp_event, {
			"Object State": rate.object_state,
			"Object Purpose": rate.object_purpose,
			"Object ID": str(rate.object_id),
			"Available Shippo": rate.available_shippo,
			"Price": price,
			"Carrier": rate.provider.name,
			"Adapter": rate.adapter.name,
			"Servicelevel": rate.servicelevel_name,
			"Duration": rate.days,
			"Trackable": rate.trackable,
			"Insurance": insurance,
			"Outbound": endpoint_outbound,
			"Inbound": endpoint_inbound,
			'Shipment Object ID': str(rate.shipment.object_id),
			'Length': float(rate.shipment.parcel.length),
			'Width': float(rate.shipment.parcel.width),
			'Height': float(rate.shipment.parcel.height),
			'Distance Unit': rate.shipment.parcel.distance_unit.name,
			'Weight': float(rate.shipment.parcel.weight),
			'Mass Unit': rate.shipment.parcel.mass_unit.name,
			'From ZIP': rate.shipment.address_from.zip,
			'From City': rate.shipment.address_from.city,
			'From State': rate.shipment.address_from.state,
			'From Country': rate.shipment.address_from.country.iso2,
			'To ZIP': rate.shipment.address_to.zip,
			'To City': rate.shipment.address_to.city,
			'To State': rate.shipment.address_to.state,
			'To Country': rate.shipment.address_to.country.iso2
			})

def parse_rate(rate):
	#convert insurance & price to $
	from api.models import Currency
	currency = Currency.objects.get(iso="USD")
	if rate.amount:
		unrounded_price = Decimal(rate.amount) * (Decimal(currency.rate) / Decimal(rate.currency.rate))
		price = str(round(unrounded_price, 2))
	else:
		price = 0.00
	if rate.insurance_amount:
		unrounded_insurance = Decimal(rate.insurance_amount) * (Decimal(currency.rate) / Decimal(rate.insurance_currency.rate))
		insurance = str(round(unrounded_insurance, 2))
	else:
		insurance = 0.00
	#check if all objects exist
	endpoint_outbound = None
	endpoint_inbound = None
	if rate.outbound_endpoint: endpoint_outbound = rate.outbound_endpoint.endpoint_name
	if rate.inbound_endpoint: endpoint_inbound = rate.inbound_endpoint.endpoint_name
	return rate, price, insurance, endpoint_outbound, endpoint_inbound

def track_API_transaction(mp_user, transaction):
	mp = Mixpanel(settings.MIXPANEL_API_CODE)
	mp_user_id = identify_mp_user(mp_user)
	transaction.rate, price, insurance, endpoint_outbound, endpoint_inbound = parse_rate(transaction.rate)
	if transaction.was_test == True:
		mode = 'Test'
	else:
		mode = 'Production'
	mp_event = 'Transaction created'
	if mp_user_id is not None:
		mp.track(mp_user_id, mp_event, {
			"Object State": transaction.object_state,
			"Object Status": transaction.object_status,
			"Object ID": str(transaction.object_id),
			"Label URL": str(transaction.label_url),
			"Tracking Number": str(transaction.tracking_number),
			"Price": price,
			"Carrier": transaction.rate.provider.name,
			"Adapter": transaction.rate.adapter.name,
			"Servicelevel": transaction.rate.servicelevel_name,
			"Duration": transaction.rate.days,
			"Trackable": transaction.rate.trackable,
			"Insurance": insurance,
			"Outbound": endpoint_outbound,
			"Inbound": endpoint_inbound,
			'Shipment Object ID': str(transaction.rate.shipment.object_id),
			'Rate Object ID': str(transaction.rate.object_id),
			'Length': float(transaction.rate.shipment.parcel.length),
			'Width': float(transaction.rate.shipment.parcel.width),
			'Height': float(transaction.rate.shipment.parcel.height),
			'Distance Unit': transaction.rate.shipment.parcel.distance_unit.name,
			'Weight': float(transaction.rate.shipment.parcel.weight),
			'Mass Unit': transaction.rate.shipment.parcel.mass_unit.name,
			'From ZIP': transaction.rate.shipment.address_from.zip,
			'From City': transaction.rate.shipment.address_from.city,
			'From State': transaction.rate.shipment.address_from.state,
			'From Country': transaction.rate.shipment.address_from.country.iso2,
			'To ZIP': transaction.rate.shipment.address_to.zip,
			'To City': transaction.rate.shipment.address_to.city,
			'To State': transaction.rate.shipment.address_to.state,
			'To Country': transaction.rate.shipment.address_to.country.iso2,
			'Mode': mode
			})

		#no revenue tracking now, too diverse rev model
		#if transaction.was_test == False and transaction.object_status == 'SUCCESS':
			#track_API_transaction_revenue(mp_user_id, transaction, price, insurance, endpoint_outbound, endpoint_inbound)

def track_API_transaction_revenue(mp_user_id, transaction, price, insurance, endpoint_outbound, endpoint_inbound):
	mp = Mixpanel(settings.MIXPANEL_API_CODE)
	mp.people_track_charge(mp_user_id, price, {
		"Object ID": str(transaction.object_id),
		"Label URL": str(transaction.label_url),
		"Tracking Number": str(transaction.tracking_number),
		"Price": price,
		"Carrier": transaction.rate.provider.name,
		"Adapter": transaction.rate.adapter.name,
		"Servicelevel": transaction.rate.servicelevel_name,
		"Duration": transaction.rate.days,
		"Trackable": transaction.rate.trackable,
		"Insurance": insurance,
		"Outbound": endpoint_outbound,
		"Inbound": endpoint_inbound,
		'Shipment Object ID': str(transaction.rate.shipment.object_id),
		'Rate Object ID': str(transaction.rate.object_id),
		'Length': float(transaction.rate.shipment.parcel.length),
		'Width': float(transaction.rate.shipment.parcel.width),
		'Height': float(transaction.rate.shipment.parcel.height),
		'Distance Unit': transaction.rate.shipment.parcel.distance_unit.name,
		'Weight': float(transaction.rate.shipment.parcel.weight),
		'Mass Unit': transaction.rate.shipment.parcel.mass_unit.name,
		'From ZIP': transaction.rate.shipment.address_from.zip,
		'From City': transaction.rate.shipment.address_from.city,
		'From State': transaction.rate.shipment.address_from.state,
		'From Country': transaction.rate.shipment.address_from.country.iso2,
		'To ZIP': transaction.rate.shipment.address_to.zip,
		'To City': transaction.rate.shipment.address_to.city,
		'To State': transaction.rate.shipment.address_to.state,
		'To Country': transaction.rate.shipment.address_to.country.iso2
		})