# -*- coding: utf-8 -*-
from django.utils import unittest
from django.test import TestCase
from decimal import Decimal
from math import ceil
import sys

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from api.models import Currency, Language, Country, Unit, Parcel, Address, Shipment

class ShipmentTestCase(TestCase):
    def setUp(self):
        """Creating necessary units & parcels"""
        self.create_countries()
        self.create_users()
        self.create_units()
        self.create_parcels()
        self.create_addresses()
        
    def test_shipment_1_purchase(self):
        """Tests a Shipment consisting of two VALID PURCHASE\'able ADDRESSES."""
        shipment = Shipment.objects.create(object_owner=self.user, object_purpose=Shipment.OBJECT_PURPOSE_CHOICE.PURCHASE, address_from=self.address_1_fully_purchase, address_to=self.address_1_fully_purchase_no_zip, parcel=self.parcel_valid)
        
        self.assertEqual(shipment.object_state, shipment.OBJECT_STATE_CHOICE.VALID)
        self.assertEqual(shipment.object_purpose, shipment.OBJECT_PURPOSE_CHOICE.PURCHASE)
        
    def test_shipment_2_quote_purchasable(self):
        """Tests a Shipment consisting of two COMPLETED QUOTE Addresses. Goal: create a PURCHASE\'able Shipment."""
        with self.assertRaises(ValidationError) as error:
            shipment = Shipment.objects.create(object_owner=self.user, object_purpose=Shipment.OBJECT_PURPOSE_CHOICE.PURCHASE, address_from=self.address_5_completed_city, address_to=self.address_6_completed_zip, parcel=self.parcel_sorted)
        self.assertEqual(error.exception.message, 'PURCHASE\'able Shipments can only be created from PURCHASE\'able and VALID Addresses.')
        
    def test_shipment_2_quote_quote(self):
        """Tests a Shipment consisting of two COMPLETED QUOTE Addresses. Goal: create a QUOTE Shipment."""
        shipment = Shipment.objects.create(object_owner=self.user, object_purpose=Shipment.OBJECT_PURPOSE_CHOICE.QUOTE, address_from=self.address_5_completed_city, address_to=self.address_6_completed_zip, parcel=self.parcel_sorted)
        
        self.assertEqual(shipment.object_state, shipment.OBJECT_STATE_CHOICE.COMPLETED)
        self.assertEqual(shipment.object_purpose, shipment.OBJECT_PURPOSE_CHOICE.QUOTE)
        
    def test_shipment_3_quote_purchasable(self):
        """Tests a Shipment consisting a COMPLETED QUOTE and a VALID PURCHASE\'able Address. Goal: create a PURCHASE\'able Shipment."""
        with self.assertRaises(ValidationError) as error:
            shipment = Shipment.objects.create(object_owner=self.user, object_purpose=Shipment.OBJECT_PURPOSE_CHOICE.PURCHASE, address_from=self.address_1_fully_purchase, address_to=self.address_6_completed_zip, parcel=self.parcel_units)
        self.assertEqual(error.exception.message, 'PURCHASE\'able Shipments can only be created from PURCHASE\'able and VALID Addresses.')

    def test_shipment_3_quote_quote(self):
        """Tests a Shipment consisting a COMPLETED QUOTE and a VALID PURCHASE\'able Address. Goal: create a QUOTE Shipment."""
        shipment = Shipment.objects.create(object_owner=self.user, object_purpose=Shipment.OBJECT_PURPOSE_CHOICE.QUOTE, address_from=self.address_1_fully_purchase, address_to=self.address_6_completed_zip, parcel=self.parcel_units)
        
        self.assertEqual(shipment.object_state, shipment.OBJECT_STATE_CHOICE.COMPLETED)
        self.assertEqual(shipment.object_purpose, shipment.OBJECT_PURPOSE_CHOICE.QUOTE)

    def test_shipment_4_quote_purchasable(self):
        """Tests a Shipment consisting a COMPLETED QUOTE and a VALID PURCHASE\'able Address. Goal: create a PURCHASE\'able Shipment. Other permutation."""
        with self.assertRaises(ValidationError) as error:
            shipment = Shipment.objects.create(object_owner=self.user, object_purpose=Shipment.OBJECT_PURPOSE_CHOICE.PURCHASE, address_from=self.address_6_completed_zip, address_to=self.address_1_fully_purchase, parcel=self.parcel_units)
        self.assertEqual(error.exception.message, 'PURCHASE\'able Shipments can only be created from PURCHASE\'able and VALID Addresses.')
        
    def test_shipment_4_quote_quote(self):
        """Tests a Shipment consisting a COMPLETED QUOTE and a VALID PURCHASE\'able Address. Goal: create a QUOTE Shipment. Other permutation."""
        shipment = Shipment.objects.create(object_owner=self.user, object_purpose=Shipment.OBJECT_PURPOSE_CHOICE.QUOTE, address_from=self.address_6_completed_zip, address_to=self.address_1_fully_purchase, parcel=self.parcel_units)
        
        self.assertEqual(shipment.object_state, shipment.OBJECT_STATE_CHOICE.COMPLETED)
        self.assertEqual(shipment.object_purpose, shipment.OBJECT_PURPOSE_CHOICE.QUOTE)
        
    def test_shipment_5_empty_from(self):
        """Tests a Shipment consisting of an empty FROM Address."""
        
        with self.assertRaises(ValueError) as error:
            shipment = Shipment.objects.create(object_owner=self.user, object_purpose=Shipment.OBJECT_PURPOSE_CHOICE.PURCHASE, address_from=None, address_to=self.address_1_fully_purchase, parcel=self.parcel_units)
            assert True
        #self.assertEqual(error.exception.message, 'A From Address, a To Address and a Parcel must be given to create a shipment.')
        
    def test_shipment_6_empty_to(self):
        """Tests a Shipment consisting of an empty TO Address."""
        
        with self.assertRaises(ValueError) as error:
            shipment = Shipment.objects.create(object_owner=self.user, object_purpose=Shipment.OBJECT_PURPOSE_CHOICE.PURCHASE, address_from=self.address_1_fully_purchase, address_to=None, parcel=self.parcel_units)
            assert True
        #self.assertEqual(error.exception.message, 'A From Address, a To Address and a Parcel must be given to create a shipment.')
        
    def test_shipment_7_empty_parcel(self):
        """Tests a Shipment consisting of an empty TO Address."""
        
        with self.assertRaises(ValueError) as error:
            shipment = Shipment.objects.create(object_owner=self.user, object_purpose=Shipment.OBJECT_PURPOSE_CHOICE.PURCHASE, address_from=self.address_1_fully_purchase, address_to=self.address_6_completed_zip, parcel=None)
            assert True
        #self.assertEqual(error.exception.message, 'A From Address, a To Address and a Parcel must be given to create a shipment.')
    
    def test_shipment_8_same_address(self):
        """Tests a Shipment consisting of identical Addresses."""
        
        with self.assertRaises(ValidationError) as error:
            shipment = Shipment.objects.create(object_owner=self.user, object_purpose=Shipment.OBJECT_PURPOSE_CHOICE.PURCHASE, address_from=self.address_1_fully_purchase, address_to=self.address_1_fully_purchase, parcel=self.parcel_units)
        self.assertEqual(error.exception.message, 'From and To Addresses are identical. Addresses must be different to create a valid shipment.')
        
    def create_units(self):
        self.centimetres = Unit.objects.create(name="centimetre", abbreviation="cm", symbol="cm", dimension=Unit.DIMENSION_CHOICE.DISTANCE, rate=1.000000000000, plural="centimetres")
        self.yards = Unit.objects.create(name="yard", abbreviation="yd", symbol="yd", dimension=Unit.DIMENSION_CHOICE.DISTANCE, rate=0.0109361330, base=self.centimetres, plural="yard")
        self.grams = Unit.objects.create(name="gram", abbreviation="g", symbol="g", dimension=Unit.DIMENSION_CHOICE.MASS, rate=1.000000000000, plural="grams")
        self.pounds = Unit.objects.create(name="pound", abbreviation="lb", symbol="lb", dimension=Unit.DIMENSION_CHOICE.MASS, rate=0.0022046226, base=self.grams, plural="pound")

    def create_countries(self):
        euro_symbol= 'E' # Postgres does not support UTF8 in testing
        self.euro = Currency.objects.create(name="Euro", symbol=euro_symbol, iso="EUR", rate=1.0)
        self.dollar = Currency.objects.create(name="U.S. Dollar", symbol="$", iso="USD", rate=1.2)
        self.german = Language.objects.create(name="German", iso2="DE")
        self.english = Language.objects.create(name="English", iso2="EN")
        self.germany = Country.objects.create(name="Germany", iso2="DE", tld="DE", currency=self.euro, language=self.german, capital="Berlin")        
        self.usa = Country.objects.create(name="United States", iso2="US", tld="", currency=self.dollar, language=self.english, capital="Washington D.C.", requires_zip = False)
        
    def create_users(self):
        self.user = User.objects.create(username='test')
        
    def create_parcels(self):
        self.parcel_valid = Parcel.objects.create(object_owner = self.user, length=30, width=20, height=10, distance_unit = self.centimetres, weight=2500, mass_unit = self.grams)
        self.parcel_sorted = Parcel.objects.create(object_owner = self.user, length=10, width=5, height=30, distance_unit = self.centimetres, weight=2500, mass_unit = self.grams)
        self.parcel_units = Parcel.objects.create(object_owner = self.user, length=0.8, width=0.9, height=0.3, distance_unit = self.yards, weight=3.5, mass_unit = self.pounds)
        
    def create_addresses(self):
        self.address_1_fully_purchase = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.PURCHASE, name = "  Max Diez  ", company = "Diez IT GmbH & Co. KG", street1 = "Heidenkamer Str.", street_no = "2", city="Tiefenbach", zip="84184", country=self.germany, phone="+49 (80709) 9261928", email="max@diez-it.de")
        self.address_1_fully_purchase_no_zip = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.PURCHASE, name = "Simon Kreuz", company = "Popout, Inc.", street1 = "Indian Summer Ct               ", street_no = "1092", city="San Jose", state="CA", country = self.usa, phone="+1 (415) 741 9393", email="simon@goshippo.com")
        self.address_2_fully_purchase = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.PURCHASE, name = "Simon Kreuz", company = "Popout, Inc.", street1 = "Indian Summer Ct               ", street_no = "1092", city="San Jose", zip="95122", state="CA", country = self.usa, phone="+1 (415) 741 9393", email="simon@goshippo.com")
        self.address_3_fully_quote = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.QUOTE, name = "Kathrina Baumann", street1 = "Bettinger Str.", zip="75113", city = "Grenzach-Wyhlen", country = self.germany, email="kmmbaumann@gmail.com")
        self.address_4_ip = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.QUOTE, ip = "50.152.187.67")
        self.address_5_completed_city = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.QUOTE, city = u'Köln', country=self.germany)
        self.address_6_completed_zip = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.QUOTE, zip="95122", country=self.usa)
        self.address_7_completed_country = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.QUOTE, country=self.usa)