from rest_framework import permissions
from django.contrib.auth.models import User

class IsOwner(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to retrieve or edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Write permissions are only allowed to the owner of the snippet
        return obj.object_owner == request.user