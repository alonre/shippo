# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Shipment.rates_queried'
        db.add_column(u'api_shipment', 'rates_queried',
                      self.gf('django.db.models.fields.DateTimeField')(null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Shipment.rates_queried'
        db.delete_column(u'api_shipment', 'rates_queried')


    models = {
        u'adapter.adapter': {
            'Meta': {'object_name': 'Adapter'},
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'provider_served': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['adapter.Provider']", 'symmetrical': 'False'})
        },
        u'adapter.duration': {
            'Meta': {'object_name': 'Duration'},
            'days': ('django.db.models.fields.IntegerField', [], {}),
            'duration_terms': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inbound_country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'duration_inbound_country_relation'", 'null': 'True', 'to': u"orm['api.Country']"}),
            'inbound_zone': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'duration_inbound_zone_relation'", 'null': 'True', 'to': u"orm['adapter.Zone']"}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'outbound_country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'duration_outbound_country_relation'", 'null': 'True', 'to': u"orm['api.Country']"}),
            'outbound_zone': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'duration_outbound_zone_relation'", 'null': 'True', 'to': u"orm['adapter.Zone']"}),
            'servicelevel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Servicelevel']"})
        },
        u'adapter.endpoint': {
            'Meta': {'object_name': 'Endpoint'},
            'endpoint_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'endpoint_type': ('django.db.models.fields.CharField', [], {'default': "'ANY'", 'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'adapter.price': {
            'Meta': {'object_name': 'Price'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'price_currency_relation'", 'to': u"orm['api.Currency']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inbound_country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'price_inbound_country_relation'", 'null': 'True', 'to': u"orm['api.Country']"}),
            'inbound_endpoint': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'price_inbound_endpoint_relation'", 'null': 'True', 'to': u"orm['adapter.Endpoint']"}),
            'inbound_zone': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'price_inbound_zone_relation_relation'", 'null': 'True', 'to': u"orm['adapter.Zone']"}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'outbound_country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'price_outbound_country_relation'", 'null': 'True', 'to': u"orm['api.Country']"}),
            'outbound_endpoint': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'price_outbound_endpoint_relation'", 'null': 'True', 'to': u"orm['adapter.Endpoint']"}),
            'outbound_zone': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'price_outbound_zone_relation'", 'null': 'True', 'to': u"orm['adapter.Zone']"}),
            'provider': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Provider']"}),
            'servicelevel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Servicelevel']"}),
            'tier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Tier']"})
        },
        u'adapter.provider': {
            'Meta': {'object_name': 'Provider'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'adapter.servicelevel': {
            'Meta': {'object_name': 'Servicelevel'},
            'arrives_by': ('django.db.models.fields.TimeField', [], {'null': 'True'}),
            'available_shippo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'delivery_attempts': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'insurance': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'insurance_amount': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2'}),
            'insurance_currency': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'servicelevel_insurance_currency_relation'", 'null': 'True', 'to': u"orm['api.Currency']"}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'provider': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Provider']"}),
            'servicelevel_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'servicelevel_terms': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'trackable': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'adapter.tier': {
            'Meta': {'object_name': 'Tier'},
            'distance_unit': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'tier_distance_unit_relation'", 'to': u"orm['api.Unit']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mass_unit': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'tier_mass_unit_relation'", 'to': u"orm['api.Unit']"}),
            'max_height': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'max_length': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'max_length_plus_girth': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'max_length_plus_height': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'max_length_plus_width_plus_height': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'max_weight': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'max_width': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'min_height': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'min_length': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'min_length_plus_girth': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'min_length_plus_height': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'min_length_plus_width_plus_height': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'min_weight': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'min_width': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'provider': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Provider']"}),
            'tier_name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'adapter.zone': {
            'Meta': {'object_name': 'Zone'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'provider': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['adapter.Provider']"}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'ANY'", 'max_length': '50'})
        },
        u'api.address': {
            'Meta': {'object_name': 'Address'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'company': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.Country']", 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'default': "'api@goshippo.com'", 'max_length': '254', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39', 'null': 'True', 'blank': 'True'}),
            'lat': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '15', 'decimal_places': '10'}),
            'lng': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '15', 'decimal_places': '10'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'S. Hippo'", 'max_length': '100', 'blank': 'True'}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_gcoder_record': ('django.db.models.fields.TextField', [], {'max_length': '10000', 'null': 'True', 'blank': 'True'}),
            'object_geoip_record': ('django.db.models.fields.TextField', [], {'max_length': '5000', 'null': 'True', 'blank': 'True'}),
            'object_id': ('uuidfield.fields.UUIDField', [], {'db_index': 'True', 'unique': 'True', 'max_length': '32', 'blank': 'True'}),
            'object_owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'address_owner'", 'to': u"orm['auth.User']"}),
            'object_purpose': ('django.db.models.fields.CharField', [], {'default': "'QUOTE'", 'max_length': '20'}),
            'object_source': ('django.db.models.fields.CharField', [], {'default': "'INVALID'", 'max_length': '20'}),
            'object_state': ('django.db.models.fields.CharField', [], {'default': "'INVALID'", 'max_length': '20'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'default': "'+1 (415) 741 9407'", 'max_length': '50', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'street1': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'street2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'street_no': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        u'api.attribute': {
            'Meta': {'object_name': 'Attribute'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'})
        },
        u'api.country': {
            'Meta': {'object_name': 'Country'},
            'capital': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.Currency']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'iso3': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'language': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.Language']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'requires_zip': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'tld': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '3', 'null': 'True', 'blank': 'True'})
        },
        u'api.currency': {
            'Meta': {'object_name': 'Currency'},
            'base': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['api.Currency']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rate': ('django.db.models.fields.DecimalField', [], {'default': '1.0', 'max_digits': '15', 'decimal_places': '6'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'api.language': {
            'Meta': {'object_name': 'Language'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'api.parcel': {
            'Meta': {'object_name': 'Parcel'},
            'distance_unit': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'parcel_distance_unit'", 'to': u"orm['api.Unit']"}),
            'height': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '4'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'length': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '4'}),
            'mass_unit': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'parcel_mass_unit'", 'to': u"orm['api.Unit']"}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_id': ('uuidfield.fields.UUIDField', [], {'db_index': 'True', 'unique': 'True', 'max_length': '32', 'blank': 'True'}),
            'object_owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'parcel_owner'", 'to': u"orm['auth.User']"}),
            'object_state': ('django.db.models.fields.CharField', [], {'default': "'INVALID'", 'max_length': '20'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '4'}),
            'width': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '4'})
        },
        u'api.rate': {
            'Meta': {'object_name': 'Rate'},
            'adapter': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rate_adapter_relation'", 'to': u"orm['adapter.Adapter']"}),
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            'arrives_by': ('django.db.models.fields.TimeField', [], {'null': 'True'}),
            'attributes': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['api.Attribute']", 'null': 'True', 'symmetrical': 'False'}),
            'available_shippo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rate_currency_relation'", 'to': u"orm['api.Currency']"}),
            'days': ('django.db.models.fields.IntegerField', [], {}),
            'delivery_attempts': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'duration': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rate_duration_relation'", 'null': 'True', 'to': u"orm['adapter.Duration']"}),
            'duration_terms': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inbound_endpoint': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rate_inbound_endpoint_relation'", 'null': 'True', 'to': u"orm['adapter.Endpoint']"}),
            'insurance': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'insurance_amount': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2'}),
            'insurance_currency': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rate_insurance_currency_relation'", 'null': 'True', 'to': u"orm['api.Currency']"}),
            'local_amount': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'local_currency': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rate_local_currency_relation'", 'null': 'True', 'to': u"orm['api.Currency']"}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_id': ('uuidfield.fields.UUIDField', [], {'db_index': 'True', 'unique': 'True', 'max_length': '32', 'blank': 'True'}),
            'object_owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rate_owner'", 'to': u"orm['auth.User']"}),
            'object_purpose': ('django.db.models.fields.CharField', [], {'default': "'QUOTE'", 'max_length': '20'}),
            'object_state': ('django.db.models.fields.CharField', [], {'default': "'INVALID'", 'max_length': '20'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'outbound_endpoint': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rate_outbound_endpoint_relation'", 'null': 'True', 'to': u"orm['adapter.Endpoint']"}),
            'price': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rate_price_relation'", 'null': 'True', 'to': u"orm['adapter.Price']"}),
            'provider': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rate_provider_relation'", 'to': u"orm['adapter.Provider']"}),
            'servicelevel': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rate_servicelevel_relation'", 'null': 'True', 'to': u"orm['adapter.Servicelevel']"}),
            'servicelevel_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'servicelevel_terms': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'shipment': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rate_shipment_relation'", 'to': u"orm['api.Shipment']"}),
            'tier': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rate_tier_relation'", 'null': 'True', 'to': u"orm['adapter.Tier']"}),
            'tier_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'trackable': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'api.shipment': {
            'Meta': {'object_name': 'Shipment'},
            'address_from': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'address_from'", 'to': u"orm['api.Address']"}),
            'address_to': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'address_to'", 'to': u"orm['api.Address']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_id': ('uuidfield.fields.UUIDField', [], {'db_index': 'True', 'unique': 'True', 'max_length': '32', 'blank': 'True'}),
            'object_owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'shipment_owner'", 'to': u"orm['auth.User']"}),
            'object_purpose': ('django.db.models.fields.CharField', [], {'default': "'QUOTE'", 'max_length': '20'}),
            'object_state': ('django.db.models.fields.CharField', [], {'default': "'INVALID'", 'max_length': '20'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'parcel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.Parcel']"}),
            'rates_queried': ('django.db.models.fields.DateTimeField', [], {'null': 'True'})
        },
        u'api.unit': {
            'Meta': {'object_name': 'Unit'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'base': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['api.Unit']"}),
            'dimension': ('django.db.models.fields.CharField', [], {'default': "'DISTANCE'", 'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'plural': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'rate': ('django.db.models.fields.DecimalField', [], {'default': '1.0', 'max_digits': '20', 'decimal_places': '10'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '5'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['api']