# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Country'
        db.create_table(u'api_country', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('iso2', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('iso3', self.gf('django.db.models.fields.CharField')(max_length=3)),
        ))
        db.send_create_signal(u'api', ['Country'])

        # Deleting field 'Address.created'
        db.delete_column(u'api_address', 'created')

        # Deleting field 'Address.modified'
        db.delete_column(u'api_address', 'modified')

        # Adding field 'Address.name'
        db.add_column(u'api_address', 'name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Address.company'
        db.add_column(u'api_address', 'company',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Address.street1'
        db.add_column(u'api_address', 'street1',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Address.street2'
        db.add_column(u'api_address', 'street2',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Address.city'
        db.add_column(u'api_address', 'city',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Address.state'
        db.add_column(u'api_address', 'state',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Address.zip'
        db.add_column(u'api_address', 'zip',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=20, blank=True),
                      keep_default=False)

        # Adding field 'Address.phone'
        db.add_column(u'api_address', 'phone',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=50, blank=True),
                      keep_default=False)

        # Adding field 'Address.email'
        db.add_column(u'api_address', 'email',
                      self.gf('django.db.models.fields.EmailField')(default='', max_length=254, blank=True),
                      keep_default=False)

        # Adding field 'Address.ip'
        db.add_column(u'api_address', 'ip',
                      self.gf('django.db.models.fields.GenericIPAddressField')(max_length=39, null=True, blank=True),
                      keep_default=False)

        # Adding index on 'Address', fields ['object_id']
        db.create_index(u'api_address', ['object_id'])


    def backwards(self, orm):
        # Removing index on 'Address', fields ['object_id']
        db.delete_index(u'api_address', ['object_id'])

        # Deleting model 'Country'
        db.delete_table(u'api_country')

        # Adding field 'Address.created'
        db.add_column(u'api_address', 'created',
                      self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now),
                      keep_default=False)

        # Adding field 'Address.modified'
        db.add_column(u'api_address', 'modified',
                      self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now),
                      keep_default=False)

        # Deleting field 'Address.name'
        db.delete_column(u'api_address', 'name')

        # Deleting field 'Address.company'
        db.delete_column(u'api_address', 'company')

        # Deleting field 'Address.street1'
        db.delete_column(u'api_address', 'street1')

        # Deleting field 'Address.street2'
        db.delete_column(u'api_address', 'street2')

        # Deleting field 'Address.city'
        db.delete_column(u'api_address', 'city')

        # Deleting field 'Address.state'
        db.delete_column(u'api_address', 'state')

        # Deleting field 'Address.zip'
        db.delete_column(u'api_address', 'zip')

        # Deleting field 'Address.phone'
        db.delete_column(u'api_address', 'phone')

        # Deleting field 'Address.email'
        db.delete_column(u'api_address', 'email')

        # Deleting field 'Address.ip'
        db.delete_column(u'api_address', 'ip')


    models = {
        u'api.address': {
            'Meta': {'object_name': 'Address'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'company': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'object_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'object_id': ('uuidfield.fields.UUIDField', [], {'db_index': 'True', 'unique': 'True', 'max_length': '32', 'blank': 'True'}),
            'object_purpose': ('django.db.models.fields.CharField', [], {'default': "'UNKNOWN'", 'max_length': '20'}),
            'object_source': ('django.db.models.fields.CharField', [], {'default': "'UNKNOWN'", 'max_length': '20'}),
            'object_state': ('django.db.models.fields.CharField', [], {'default': "'INVALID'", 'max_length': '20'}),
            'object_type': ('django.db.models.fields.CharField', [], {'default': "'addr_'", 'max_length': '5'}),
            'object_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'street1': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'street2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        u'api.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'iso3': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['api']