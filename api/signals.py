from django.db.models.signals import post_save
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from api.models import APIUser, APIAddress, APIBilling

def create_profile(sender, **kw):
	user = kw["instance"]
	#if kw["created"]:
		#api_user = APIUser(user=user)
		#api_user.save()
		#deactivated by Simon on Dec 27 since it apparently creates a duplicate APIUser since APIuser already is created via /admin

def create_auth_token(sender, **kw):
	user = kw["instance"]
	if kw["created"]:
		Token.objects.create(user=user)

def create_apiuser_objects(sender, **kw):
	apiuser = kw["instance"]
	if kw["created"]:
		current_user = apiuser.user
		#check if an APIAddress has also been created
		if not apiuser.address:
			api_address = APIAddress()
			api_address.save()
			apiuser.address = api_address
			apiuser.save()
		#for APIBilling, we only need to create a new, empty object if not already created
		if not apiuser.billing:
			api_billing = APIBilling()
			api_billing.save()
			apiuser.billing = api_billing
			apiuser.save()

def update_apiaddress(sender, **kw):
	#fill APIAddress name and email from User object
	#will be triggered when USER instance is updated
	if not kw["created"]:
		current_user = kw["instance"]
		try:
			api_user = APIUser.objects.get(user=current_user)
			api_address = api_user.address
			if api_address:
				if not api_address.name:
					api_address.name = ''
					if current_user.first_name:
						api_address.name += current_user.first_name + ' '
					if current_user.last_name:
						api_address.name += current_user.last_name
				if not api_address.email:
					if current_user.email:
						api_address.email = current_user.email
				api_address.save()
		except:
			pass

post_save.connect(create_profile, sender=User, dispatch_uid="users-profilecreation-signal")
post_save.connect(create_auth_token, sender=User, dispatch_uid="users-tokencreation-signal")
post_save.connect(create_apiuser_objects, sender=APIUser, dispatch_uid="apiusers-addressbillingcreation-signal")
post_save.connect(update_apiaddress, sender=User, dispatch_uid="users-apiaddressupdate-signal")