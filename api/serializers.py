from rest_framework import serializers
from api.models import Address, Parcel, Shipment, Country, Unit, Rate, Transaction, Message, Attribute, Location, Track
from uuidfield import UUIDField
from django.contrib.auth.models import User

# TODO EVERYWHERE: replace XXX.objects.all.... by address = XXX.objects.get(

class APIObjectIDField(serializers.SlugRelatedField):    
    def __init__(self, model = None, *args, **kwargs):
        self.model = model
        super(APIObjectIDField, self).__init__(*args, **kwargs)

    def to_native(self, obj):
        if obj:
            return str(obj.object_id)
        else:
            raise serializers.ValidationError('No object given.')
        
    def from_native(self, data):
        data = str(data)

        if self.model == Address:
            try:
                address = Address.objects.get(object_id=data)
                return address
            except:
                raise serializers.ValidationError('Address with supplied object_id not found.')
        elif self.model == Parcel:
            try:
                parcel = Parcel.objects.get(object_id=data)
                return parcel
            except:
                raise serializers.ValidationError('Parcel with supplied object_id not found.')
        elif self.model == Rate:
            try:
                rate = Rate.objects.get(object_id=data)
                return rate
            except:
                raise serializers.ValidationError('Rate with supplied object_id not found.')

class AddressSerializer(serializers.ModelSerializer):
    object_owner = serializers.Field(source='object_owner.username')
    country = serializers.SlugRelatedField(slug_field='iso2', required=False, queryset=Country.objects.all())

    class Meta:
        model = Address
        fields = ('object_state', 'object_purpose', 'object_source', 'object_created', 'object_updated', 'object_id', 'object_owner', 'name', 'company', 'street1', 'street_no', 'street2', 'city', 'state', 'zip', 'country', 'phone', 'email', 'ip', 'metadata')
        read_only_fields = ('object_state', 'object_source', 'object_created', 'object_updated', 'object_id', )

class ParcelSerializer(serializers.ModelSerializer):
    object_owner = serializers.Field(source='object_owner.username')
    mass_unit = serializers.SlugRelatedField(slug_field='abbreviation', queryset=Unit.objects.all())
    distance_unit = serializers.SlugRelatedField(slug_field='abbreviation', queryset=Unit.objects.all())

    class Meta:
        model = Parcel
        fields = ('object_state', 'object_created', 'object_updated', 'object_id', 'object_owner', 'length', 'width', 'height', 'distance_unit', 'weight', 'mass_unit', 'metadata')
        read_only_fields = ('object_state', 'object_created', 'object_updated', 'object_id',)

class ShipmentSerializer(serializers.ModelSerializer):
    object_owner = serializers.Field(source='object_owner.username')
    address_from = APIObjectIDField(slug_field='object_id', model = Address, queryset=Address.objects.all()) # QUERYSET 
    address_to = APIObjectIDField(slug_field='object_id', model = Address, queryset=Address.objects.all())
    parcel = APIObjectIDField(slug_field='object_id', model = Parcel, queryset=Parcel.objects.all())
    rates_url = serializers.Field(source='get_rates_url')
    object_status = serializers.Field(source='get_object_status')

    # it feels wrong to filter the entries shown in the list here and not in the view, but it works
    def get_fields(self, *args, **kwargs):
        fields = super(ShipmentSerializer, self).get_fields(*args, **kwargs)
        view = self.context['view']
        #object_id = view.kwargs.get('object_id')
        fields['address_from'].queryset = fields['address_from'].queryset.filter(object_owner=view.request.user).order_by('-object_created')[:25]
        fields['address_to'].queryset = fields['address_to'].queryset.filter(object_owner=view.request.user).order_by('-object_created')[:25]
        fields['parcel'].queryset = fields['parcel'].queryset.filter(object_owner=view.request.user).order_by('-object_created')[:25]
        return fields
    
    class Meta:
        model = Shipment
        fields = ('object_state', 'object_status', 'object_purpose', 'object_created', 'object_updated', 'object_id', 'object_owner', 'address_from', 'address_to', 'parcel', 'rates_url', 'metadata')
        read_only_fields = ('object_state', 'object_created', 'object_updated', 'object_id',)
        
class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('code', 'text')
        read_only_fields = ('code', 'text')
        
class RateSerializer(serializers.ModelSerializer):
    object_owner = serializers.Field(source='object_owner.username')
    attributes = serializers.RelatedField(many=True)
    insurance_currency = serializers.Field(source='insurance_currency.iso')
    insurance_currency_local = serializers.Field(source='insurance_currency_local.iso')
    currency = serializers.Field(source='shipment.address_from.country.currency.iso')
    currency_local = serializers.Field(source='currency_local.iso')
    provider = serializers.Field(source='provider.name')
    provider_image_75 = serializers.Field(source='provider.get_image_75_url')
    provider_image_200 = serializers.Field(source='provider.get_image_200_url')
    outbound_endpoint = serializers.Field(source='outbound_endpoint.endpoint_name')
    inbound_endpoint = serializers.Field(source='inbound_endpoint.endpoint_name')
    amount = serializers.Field(source='get_amount_in_currency')
    shipment = serializers.Field(source='shipment.object_id')
    messages = MessageSerializer(source='message_rate_relation', read_only=True)

    class Meta:
        model = Rate
        fields = ('object_state', 'object_purpose', 'object_created', 'object_updated', 'object_id', 'object_owner', 'shipment', 'available_shippo',
            'attributes', 'amount', 'currency', 'amount_local', 'currency_local', 'provider', 'provider_image_75', 'provider_image_200',
            'servicelevel_name', 'servicelevel_terms',  'days', 'arrives_by', 'duration_terms', 'trackable',
            'insurance', 'insurance_amount_local', 'insurance_currency_local', 'insurance_amount', 'insurance_currency', 'delivery_attempts',
            'tier_name', 'outbound_endpoint', 'inbound_endpoint', 'messages')
        
class TransactionSerializer(serializers.ModelSerializer):
    object_owner = serializers.Field(source='object_owner.username')
    rate = APIObjectIDField(slug_field='object_id', model=Rate, queryset=Rate.objects.all())
    messages = MessageSerializer(source='message_transaction_relation', read_only=True)
    
    # it feels wrong to filter the entries shown in the list here and not in the view, but it works
    def get_fields(self, *args, **kwargs):
        fields = super(TransactionSerializer, self).get_fields(*args, **kwargs)
        view = self.context['view']
        #object_id = view.kwargs.get('object_id')
        #fields['rate'].queryset = fields['rate'].queryset.filter(object_owner=view.request.user, object_purpose=Rate.OBJECT_PURPOSE_CHOICE.PURCHASE, object_state=Rate.OBJECT_STATE_CHOICE.VALID).order_by('-object_created')[:25]
        fields['rate'].queryset = fields['rate'].queryset.filter(object_owner=view.request.user, object_state=Rate.OBJECT_STATE_CHOICE.VALID).order_by('-object_created')[:25]
        return fields

    class Meta:
        model = Transaction
        fields = ('object_state', 'object_status', 'object_created', 'object_updated', 'object_id', 'object_owner', 'was_test', 'rate', 'notification_email_from', 'notification_email_to', 'notification_email_other', 'pickup_date', 'tracking_number', 'label_url', 'messages', 'metadata')
        read_only_fields = ('object_state', 'object_status', 'object_created', 'object_updated', 'object_id', 'was_test', 'tracking_number', 'label_url',)
        
class LocationSerializer(serializers.ModelSerializer):
    object_owner = serializers.Field(source='object_owner.username')
    address = APIObjectIDField(slug_field='object_id', model=Address, queryset=Address.objects.all())
    provider = serializers.Field(source='provider.name')
    country = serializers.Field(source='country.iso2')
    distance_unit = serializers.Field(source='distance_unit.abbreviation')
    
    class Meta:
        model = Location
        fields = ('object_created', 'object_updated', 'object_id', 'object_owner', 'address', 'provider', 'company', 'street1', 'street_no', 'street2', 'city', 'state', 'zip', 'country', 'phone', 'email', 'distance_amount', 'distance_unit', 'lat', 'lng', 'further_information')
    
class TrackSerializer(serializers.ModelSerializer):
    object_owner = serializers.Field(source='object_owner.username')
    transaction = serializers.Field(source='transaction.object_id')
    provider = serializers.Field(source='provider.name')
    location_country = serializers.Field(source='location_country.iso2')
    
    class Meta:
        model = Track
        fields = ('object_created', 'object_updated', 'object_id', 'object_owner', 'transaction', 'provider', 'status', 'status_details', 'status_date', 'pickup_date', 'enroute', 'delivery_date_status', 'delivery_date', 'location_street1', 'location_street2', 'location_street_no', 'location_city', 'location_state', 'location_zip', 'location_country', 'location_details', 'signed_for')