from django.utils import unittest
from django.test import TestCase
from decimal import Decimal
from math import ceil
import sys

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from api.models import Currency, Language, Country, Unit, Parcel, Address, Shipment

class ParcelTestCase(TestCase):
    def setUp(self):
        """Creating necessary units & parcels"""
        self.create_units()
        self.create_users()
        
        self.valid_parcel = Parcel.objects.create(object_owner = self.user, length=30, width=20, height=10, distance_unit = self.centimetres, weight=2500, mass_unit = self.grams)
        self.sorted_parcel = Parcel.objects.create(object_owner = self.user, length=10, width=5, height=30, distance_unit = self.centimetres, weight=2500, mass_unit = self.grams)
        self.units_parcel = Parcel.objects.create(object_owner = self.user, length=0.8, width=0.9, height=0.3, distance_unit = self.yards, weight=3.5, mass_unit = self.pounds)
        
    def test_parcels_valid(self):
        """Test whether parcels are valid"""
        self.assertEqual(self.valid_parcel.object_state, self.valid_parcel.OBJECT_STATE_CHOICE.VALID)
        self.assertEqual(self.sorted_parcel.object_state, self.valid_parcel.OBJECT_STATE_CHOICE.VALID)
        self.assertEqual(self.units_parcel.object_state, self.valid_parcel.OBJECT_STATE_CHOICE.VALID)
        
    def test_parcels_sort(self):
        """Tests parcel whether dimensions are sorted properly."""
        self.assertEqual(self.sorted_parcel.length, 30)
        self.assertEqual(self.sorted_parcel.width, 10)
        self.assertEqual(self.sorted_parcel.height, 5)
        
        self.assertEqual(self.sorted_parcel.get_value(attribute=self.sorted_parcel.length, unit = self.centimetres, decimal_places=0), 30)
        self.assertEqual(self.sorted_parcel.get_value(attribute=self.sorted_parcel.width, unit = self.centimetres, decimal_places=0), 10)
        self.assertEqual(self.sorted_parcel.get_value(attribute=self.sorted_parcel.height, unit = self.centimetres, decimal_places=0), 5)
        
    def test_parcels_units(self):
        """Tests whether unit conversion works properly."""
        length_value = self.units_parcel.get_value(attribute=self.units_parcel.length, unit = self.centimetres, decimal_places=3)
        length_direct = self.units_parcel.get_length(unit = self.centimetres, decimal_places=3)
        self.assertEqual(length_value, 82.296)
        self.assertEqual(length_direct, 82.296)
        
        width_value = self.units_parcel.get_value(attribute=self.units_parcel.width, unit = self.centimetres, decimal_places=3)
        width_direct = self.units_parcel.get_width(unit = self.centimetres, decimal_places=3)
        self.assertEqual(width_value, 73.152)
        self.assertEqual(width_direct, 73.152)
        
        height_value = self.units_parcel.get_value(attribute=self.units_parcel.height, unit = self.centimetres, decimal_places=3)
        height_direct = self.units_parcel.get_height(unit = self.centimetres, decimal_places=3)
        self.assertEqual(height_value, 27.432)
        self.assertEqual(height_direct, 27.432)
        
        weight_value = self.units_parcel.get_value(attribute=self.units_parcel.weight, unit = self.grams, decimal_places=3)
        weight_direct = self.units_parcel.get_weight(unit = self.grams, decimal_places=3)
        self.assertEqual(weight_value, 1587.573)
        self.assertEqual(weight_direct, 1587.573)
        
        with self.assertRaises(ValidationError) as error:
            self.weight_value_unit_invalid = self.units_parcel.get_value(attribute=self.units_parcel.weight, unit = self.centimetres, decimal_places=3)
        self.assertEqual(error.exception.message, 'Conversion is only possible between compatible units (distance vs. mass).')
            
        with self.assertRaises(ValidationError) as error:
            self.weight_direct_unit_invalid = self.units_parcel.get_weight(unit = self.centimetres, decimal_places=3)
        self.assertEqual(error.exception.message, 'Conversion is only possible between compatible units (distance vs. mass).')
        
        with self.assertRaises(ValidationError) as error:
            self.length_value_unit_invalid = self.units_parcel.get_value(attribute=self.units_parcel.length, unit = self.pounds, decimal_places=3)
        self.assertEqual(error.exception.message, 'Conversion is only possible between compatible units (distance vs. mass).')
            
        with self.assertRaises(ValidationError) as error:
            self.length_direct_unit_invalid = self.units_parcel.get_length(unit = self.pounds, decimal_places=3)
        self.assertEqual(error.exception.message, 'Conversion is only possible between compatible units (distance vs. mass).')

    def test_parcels_units_decimals(self):
        """Tests whether handling negative decimal places works properly."""
        length = self.units_parcel.get_length(unit = self.centimetres, decimal_places=-3)
        self.assertEqual(length, 82)
        
        width = self.units_parcel.get_width(unit = self.centimetres, decimal_places=-3)
        self.assertEqual(width, 73)
        
        height = self.units_parcel.get_height(unit = self.centimetres, decimal_places=-3)
        self.assertEqual(height, 27)
        
        weight = self.units_parcel.get_weight(unit = self.grams, decimal_places=-3)
        self.assertEqual(weight, 1588)
        
    def test_parcels_validation(self):
        """Tests whether validation works properly."""
        
        with self.assertRaises(ValidationError) as error:
            self.invalid_parcel_units_distance = Parcel.objects.create(object_owner = self.user, length=30, width=20, height=10, distance_unit = self.grams, weight=2500, mass_unit = self.grams)
        
        self.assertEqual(error.exception.message, 'Parcel measurements must be given in a unit of distance.')
        
        with self.assertRaises(ValidationError) as error:
            self.invalid_parcel_units_mass = Parcel.objects.create(object_owner = self.user, length=30, width=20, height=10, distance_unit = self.centimetres, weight=2500, mass_unit = self.yards)
        
        self.assertEqual(error.exception.message, 'Parcel weight must be given in a unit of mass.')
        
        with self.assertRaises(ValidationError) as error:
            self.invalid_parcel_figures = Parcel.objects.create(object_owner = self.user, length=-30, width=20, height=10, distance_unit = self.grams, weight=2500, mass_unit = self.grams)
        
        self.assertEqual(error.exception.message, 'Parcel dimensions must be larger than 0.')
        
        with self.assertRaises(ValidationError) as error:
            self.empty_parcel = Parcel.objects.create()
            
        self.assertEqual(error.exception.message, 'Parcel dimensions must be given.')
        
    def create_units(self):
        self.centimetres = Unit.objects.create(name="centimetre", abbreviation="cm", symbol="cm", dimension=Unit.DIMENSION_CHOICE.DISTANCE, rate=1.000000000000, plural="centimetres")
        self.yards = Unit.objects.create(name="yard", abbreviation="yd", symbol="yd", dimension=Unit.DIMENSION_CHOICE.DISTANCE, rate=0.0109361330, base=self.centimetres, plural="yard")
        self.grams = Unit.objects.create(name="gram", abbreviation="g", symbol="g", dimension=Unit.DIMENSION_CHOICE.MASS, rate=1.000000000000, plural="grams")
        self.pounds = Unit.objects.create(name="pound", abbreviation="lb", symbol="lb", dimension=Unit.DIMENSION_CHOICE.MASS, rate=0.0022046226, base=self.grams, plural="pound")
        
    def create_users(self):
        self.user = User.objects.create(username='test')