from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

# for API
from rest_framework.urlpatterns import format_suffix_patterns
from api import views
from django.conf.urls import include

UUID_REGEX_PATTERN = '([a-fA-F0-9]{32}|[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12})'

urlpatterns = patterns('',
    url(r'^addresses/$', views.AddressList.as_view()),
    url(r'^addresses/(?P<object_id>' + UUID_REGEX_PATTERN + '+)/$', views.AddressDetail.as_view()),
    
    url(r'^addresses/(?P<address_id>' + UUID_REGEX_PATTERN + '+)/locations/$', views.LocationList.as_view()),
    url(r'^locations/(?P<object_id>' + UUID_REGEX_PATTERN + '+)/$', views.LocationDetail.as_view()),
    url(r'^locations/$', views.LocationList.as_view()),
    
    url(r'^parcels/$', views.ParcelList.as_view()),
    url(r'^parcels/(?P<object_id>' + UUID_REGEX_PATTERN + '+)/$', views.ParcelDetail.as_view()),

    url(r'^shipments/$', views.ShipmentList.as_view()),
    url(r'^shipments/(?P<object_id>' + UUID_REGEX_PATTERN + '+)/$', views.ShipmentDetail.as_view()),
    
    url(r'^shipments/(?P<shipment_id>' + UUID_REGEX_PATTERN + '+)/rates(?:/(?P<currency_iso>([A-Za-z]{3})))?/$', views.RateList.as_view()),
    url(r'^rates/$', views.RateList.as_view()),
    url(r'^rates/(?P<object_id>' + UUID_REGEX_PATTERN + '+)/$', views.RateDetail.as_view()),
    
    url(r'^transactions/$', views.TransactionList.as_view()),
    url(r'^transactions/(?P<object_id>' + UUID_REGEX_PATTERN + '+)/$', views.TransactionDetail.as_view()),
    
    url(r'^transactions/(?P<transaction_id>' + UUID_REGEX_PATTERN + '+)/tracking/$', views.TrackingList.as_view()),
    url(r'^tracking/(?P<object_id>' + UUID_REGEX_PATTERN + '+)/$', views.TrackingDetail.as_view()),
    url(r'^tracking/$', views.TrackingList.as_view()),
)

urlpatterns = format_suffix_patterns(urlpatterns)

urlpatterns += patterns('',
    url(r'^auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
)