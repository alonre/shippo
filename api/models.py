import uuid
from uuidfield import UUIDField
from math import ceil
from decimal import *
from urlparse import urljoin
from datetime import timedelta
from datetime import datetime

from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import validate_ipv46_address
from django.conf import settings
from django.utils import timezone

from model_utils import Choices
from model_utils.fields import StatusField

from helpers.geography import GoogleGeography
from helpers.geoip import GeoLite

import tracking.mixpanel_track as mixp
import stripe
from adapter.models import PriceMixIn, DurationMixIn, TierMixIn, ServicelevelMixIn, EndpointMixIn

class Currency(models.Model):
    """Defines Currencies, monetary units used in one or more Countries."""

    iso = models.CharField(max_length=3)
    fedex_iso = models.CharField(max_length=3, blank=True)
    symbol = models.CharField(max_length=5, blank=True, null=True)
    name = models.CharField(max_length=100)
    rate = models.DecimalField(max_digits=15, decimal_places=6, default=1.0)
    base = models.ForeignKey('self', default=1)
    updated = models.DateTimeField(auto_now=True, editable=True)

    def __unicode__(self):
        return '%s (%s)' % (self.name, self.iso)

    def natural_key(self):
        return self.__unicode__()

class Language(models.Model):
    """Defines Languages that are spoken in one or more Countries."""

    name = models.CharField(max_length=100)
    iso2 = models.CharField(max_length=5)

    def __unicode__(self):
        return '%s (%s)' % (self.name, self.iso2)

    def natural_key(self):
        return self.__unicode__()

class Country(models.Model):
    """Defines geographical Countries along with their main language, currency and political capital city."""

    name = models.CharField(max_length=100)
    iso2 = models.CharField(max_length=2)
    iso3 = models.CharField(max_length=3, blank=True)
    tld = models.CharField(max_length=3, blank=True, null=True, default='')
    currency = models.ForeignKey(Currency)
    language = models.ForeignKey(Language)
    capital = models.CharField(max_length=100, blank=True)
    requires_zip = models.BooleanField(default=True)
    requires_state = models.BooleanField(default=False)

    def __unicode__(self):
        return '%s (%s)' % (self.name, self.iso2)

    def natural_key(self):
        return self.__unicode__()

    class Meta:
        ordering = ['name', 'iso2']

class AddressMixIn(models.Model):
    # Address Data

    company = models.CharField(max_length=100, blank=True)
    street1 = models.CharField(max_length=100, blank=True)
    street2 = models.CharField(max_length=100, blank=True)
    street_no = models.CharField(max_length=10, blank=True)
    city = models.CharField(max_length=100, blank=True)
    state = models.CharField(max_length=100, blank=True)
    zip = models.CharField(max_length=20, blank=True)
    country = models.ForeignKey(Country, blank=True, null=True)

    class Meta:
        abstract = True

class APIAddress(AddressMixIn):
    name = models.CharField(max_length=100, blank=True)
    phone = models.CharField(max_length=50, blank=True)
    email = models.EmailField(max_length=254, blank=True)

    def __unicode__(self):
        if self.country:
            country = self.country.name
        else:
            country = ''
        return '%s (%s %s, %s)' % (str(self.id), self.city, self.zip, country)

    def get_cleaned_phone(self, max_digits=15, us_national=False):
        cleaned_number = ''
        for char in self.phone:
            if char.isdigit() or char == '+':
                cleaned_number += char

        if us_national:
            if cleaned_number[:1] == '1':
                cleaned_number=cleaned_number[1:]
            elif cleaned_number[:2] == '+1' or cleaned_number[:2] == '01':
                cleaned_number=cleaned_number[2:]
            elif cleaned_number[:3] == '001':
                cleaned_number=cleaned_number[3:]

        return cleaned_number[:max_digits]

    def all_required_data(self):
        if self.email and self.street1 and self.city and self.zip and self.country:
            return True
        else:
            return False

class APIBilling(models.Model):
    PAYMENT_CHOICE = Choices('STRIPE', 'ACH', 'LASTSCHRIFT SEPA', 'ACCOUNT', 'PAYPAL') # acct = Rechnung
    SEPA_MANDATE_CHOICE = Choices('FIRMENLASTSCHRIFT', 'BASISLASTSCHRIFT')

    billing_address = models.ForeignKey(APIAddress, related_name='apibilling_billing_address', null=True, blank=True)
    payment_type = models.CharField(choices=PAYMENT_CHOICE, blank=True, max_length=50)

    # Stripe
    stripe_authorized = models.BooleanField(default=False)  # explicit field due to compliance
    stripe_token = models.CharField(max_length=50, blank=True)

    # ACH info - according to http://www.ach.com/ACH-Glossary.aspx, http://www.ach.com/How-It-Works.aspx, http://www.ippay.com/downloads/ACH_101.pdf
    ach_bank = models.ForeignKey(APIAddress, related_name='apibilling_ach_bank', null=True) # bank name derived from address->company
    ach_account_holder = models.ForeignKey(APIAddress, related_name='apibilling_ach_account_holder', null=True)
    ach_account_number = models.CharField(max_length=25, blank=True)
    ach_authorized = models.BooleanField(default=False)  # explicit field due to compliance
    ach_routing_number = models.CharField(max_length=9, blank=True)
    ach_entry_sent = models.BooleanField(default=False)
    ach_entry_class = models.CharField(max_length=10, blank=True)

    # Lastschrift info - according to https://www.firmenkunden.commerzbank.de/files/articles/besondere_handlungsempfehlung_einfuehrung_sepa-lastschrift.pdf,
    # http://www.die-deutsche-kreditwirtschaft.de/dk/zahlungsverkehr/sepa/inhalte-der-sepa/lastschrift.html
    sepa_bank = models.ForeignKey(APIAddress, related_name='apibilling_sepa_bank', null=True) # bank name derived from address->company
    sepa_account_holder = models.ForeignKey(APIAddress, related_name='apibilling_sepa_account_holder', null=True)
    sepa_account_number = models.CharField(max_length=25, blank=True)
    sepa_authorized = models.BooleanField(default=False)
    sepa_mandate = models.CharField(choices=SEPA_MANDATE_CHOICE, blank=True, max_length=50)
    sepa_mandate_reference = models.CharField(max_length=35, blank=True)
    sepa_iban = models.CharField(max_length=34, blank=True) # according to ISO 13616-1:2007 Pt. 1
    sepa_bic = models.CharField(max_length=11, blank=True)

    # Payment on Account
    account_authorized = models.BooleanField(default=False)

    # PayPal
    paypal_authorized = models.BooleanField(default=False)
    paypal_email = models.EmailField(blank=True, max_length=200)
    
    def get_stripe_token(self, *args, **kwargs):
        if self.stripe_authorized and self.stripe_token:
            stripe.api_key = settings.STRIPE_SECRET_KEY_PRODUCTION
            stripe_customer = stripe.Customer.retrieve(self.stripe_token)
            if stripe_customer.cards.count > 0:
                return self.stripe_token
        return None

class APIUser(models.Model):
    user = models.OneToOneField(User)
    address = models.ForeignKey(APIAddress, null=True, blank=True)
    billing = models.ForeignKey(APIBilling, null=True, blank=True)

    #Registration settings
    shippo_activation_code = models.CharField(max_length=64, blank=True)

    # GADB settings
    #gadb_production = models.BooleanField(default=False)

    # Deutsche Post settings
    deutsche_post_production = models.BooleanField(default=False)
    deutsche_post_dummy_credential = models.BooleanField(default=False)
    portokasse_mail = models.CharField(max_length=64, blank=True)
    portokasse_password = models.CharField(max_length=64, blank=True)

    # DHL Consumer settings
    dp_dhl_production = models.BooleanField(default=False)
    dp_dhl__dummy_credential = models.BooleanField(default=False)
    postpay_mail = models.CharField(max_length=64, blank=True)
    postpay_password = models.CharField(max_length=64, blank=True)

    # TODO: add further providers (all GADB)
    hermes_production = models.BooleanField(default=False)
    hermes_dummy_credential = models.BooleanField(default=False)
    gls_de_production = models.BooleanField(default=False)
    gls_de_dummy_credential = models.BooleanField(default=False)
    posten_no_production = models.BooleanField(default=False)
    posten_no_dummy_credential = models.BooleanField(default=False)
    posti_production = models.BooleanField(default=False)
    posti_dummy_credential = models.BooleanField(default=False)
    posten_ab_production = models.BooleanField(default=False)
    posten_ab_dummy_credential = models.BooleanField(default=False)
    laposte_production = models.BooleanField(default=False)
    laposte_dummy_credential = models.BooleanField(default=False)
    colissimo_production = models.BooleanField(default=False)
    colissimo_dummy_credential = models.BooleanField(default=False)
    chronopost_production = models.BooleanField(default=False)
    chronopost_dummy_credential = models.BooleanField(default=False)

    # FedEx settings
    fedex_account = models.CharField(max_length=9, blank=True)
    fedex_meter = models.CharField(max_length=9, blank=True)
    fedex_key = models.CharField(max_length=16, blank=True)
    fedex_password = models.CharField(max_length=50, blank=True)
    fedex_production = models.BooleanField(default=False)

    # Endicia settings
    endicia_requester_id = models.CharField(max_length=16, blank=True)
    endicia_account_id = models.CharField(max_length=16, blank=True)
    endicia_passphrase = models.CharField(max_length=64, blank=True)
    endicia_production = models.BooleanField(default=False)
    endicia_webpassword = models.CharField(max_length=17, blank=True)
    endicia_challenge_question = models.CharField(max_length=100, blank=True)
    endicia_challenge_answer = models.CharField(max_length=100, blank=True)
    endicia_temporary_account = models.BooleanField(default=False)
    endicia_refill_amount = models.DecimalField(max_digits=8, decimal_places=4, default=20)

    # Parcel2Go settings
    parcel2go_api_key = models.CharField(max_length=200, blank=True)
    parcel2go_card_reference = models.CharField(max_length=200, blank=True)
    parcel2go_production = models.BooleanField(default=False)

    # UPS Settings
    ups_access_license_number = models.CharField(max_length=24, blank=True)
    ups_account = models.CharField(max_length=24, blank=True)
    ups_user_id = models.CharField(max_length=24, blank=True)
    ups_password = models.CharField(max_length=24, blank=True)
    ups_production = models.BooleanField(default=False)

    # DHL Settings
    dhl_intraship_username = models.CharField(max_length=24, blank=True)
    dhl_intraship_password = models.CharField(max_length=64, blank=True)
    dhl_account_number = models.CharField(max_length=24, blank=True)
    dhl_production = models.BooleanField(default=False)
    
    # Royalmail Settings
    royalmail_username = models.CharField(max_length=50, blank=True)
    royalmail_password = models.CharField(max_length=50, blank=True)
    royalmail_production = models.BooleanField(default=False)

    # Mixpanel settings
    mixpanel_user_id = models.CharField(max_length=254, blank=True)

    # Revenue settings
    fee_payment_fixed = models.DecimalField(max_digits=8, decimal_places=4, default=settings.DEFAULT_FEE_PAYMENT_FIXED)
    fee_payment_fixed_currency = models.ForeignKey(Currency, null=True, blank=True, related_name='apiuser_fee_payment_fixed_currency')
    fee_payment_relative = models.DecimalField(max_digits=5, decimal_places=4, default=settings.DEFAULT_FEE_PAYMENT_RELATIVE)
    fee_shippo_fixed = models.DecimalField(max_digits=8, decimal_places=4, default=settings.DEFAULT_FEE_SHIPPO_FIXED)
    fee_shippo_fixed_currency = models.ForeignKey(Currency, null=True, blank=True, related_name='apiuser_fee_shippo_fixed_currency')
    fee_shippo_relative = models.DecimalField(max_digits=5, decimal_places=4, default=settings.DEFAULT_FEE_SHIPPO_RELATIVE)

    # Shopify settings
    shopify_shop_name = models.CharField(max_length=200, blank=True)
    shopify_access_token = models.CharField(max_length=200, blank=True)
    shopify_connected = models.BooleanField(default=False)

    # Etsy settings
    etsy_oauth_token = models.CharField(max_length=200, blank=True)
    etsy_oauth_secret = models.CharField(max_length=200, blank=True)
    etsy_connected = models.BooleanField(default=False)

    def __unicode__(self):
        return 'API User %s' % self.user.username

    def fedex_set(self):
        if self.fedex_account and self.fedex_meter: return True
        else: return False
    def fedex_set_production(self):
        if self.fedex_account and self.fedex_meter and self.fedex_production: return True
        else: return False
    def endicia_set(self):
        if self.endicia_account_id and self.endicia_passphrase: return True
        else: return False
    def endicia_set_production(self):
        if self.endicia_account_id and self.endicia_passphrase and self.endicia_production: return True
        else: return False
    def parcel2go_set(self):
        if self.parcel2go_api_key: return True
        else: return False
    def parcel2go_set_production(self):
        if self.parcel2go_api_key and self.parcel2go_production: return True
        else: return False
    def ups_set(self):
        if self.ups_account and self.ups_user_id and self.ups_password: return True
        else: return False
    def ups_set_production(self):
        if self.ups_account and self.ups_user_id and self.ups_password and self.ups_production: return True
        else: return False
    def dhl_set(self):
        if self.dhl_intraship_username and self.dhl_intraship_password and self.dhl_account_number: return True
        else: return False
    def dhl_set_production(self):
        if self.dhl_intraship_username and self.dhl_intraship_password and self.dhl_account_number and self.dhl_production: return True
        else: return False
    def royalmail_set_production(self):
        if self.royalmail_production: return True
        else: return False
    def deutsche_post_set(self):
        if self.portokasse_mail and self.portokasse_password: return True
        else: return False
    def deutsche_post_set_production(self):
        if self.portokasse_mail and self.portokasse_password and self.deutsche_post_production: return True
        else: return False
    def dp_dhl_set(self):
        if self.postpay_mail and self.postpay_password and self.dp_dhl_production: return True
        else: return False
    def dp_dhl_set_production(self):
        if self.dp_dhl_production: return True
        else: return False

    def shopify_set(self):
        if self.shopify_shop_name and self.shopify_access_token and self.shopify_connected: return True
        else: return False
    def etsy_set(self):
        if self.etsy_oauth_token and self.etsy_oauth_secret and self.etsy_connected: return True
        else: return False

class Unit(models.Model):
    """Defines Units that are used to measure distances and mass across the API."""
    DIMENSION_CHOICE = Choices('DISTANCE', 'MASS')

    name = models.CharField(max_length=15)
    plural = models.CharField(max_length=20, blank=True)
    abbreviation = models.CharField(max_length=5)
    symbol = models.CharField(max_length=5)
    dimension = models.CharField(choices=DIMENSION_CHOICE, default=DIMENSION_CHOICE.DISTANCE, max_length=10)
    rate = models.DecimalField(max_digits=20, decimal_places=10, default=1.0)
    base = models.ForeignKey('self', default=1)

    def __unicode__(self):
        return '%s (%s)' % (self.name, self.abbreviation)

    def natural_key(self):
        return self.__unicode__()

class Parcel(models.Model):
    """Defines a Parcel which is an object that is shipped from A to B."""

    OBJECT_STATE_CHOICE = Choices('INVALID', 'VALID')

    # Object-related Metadata
    cleaned = False
    object_id = UUIDField(auto=True, db_index = True, unique=True)
    object_state = models.CharField(choices=OBJECT_STATE_CHOICE, default=OBJECT_STATE_CHOICE.INVALID, max_length=20)
    object_created = models.DateTimeField(auto_now_add=True, editable=True)
    object_updated = models.DateTimeField(auto_now=True, editable=True)
    object_owner = models.ForeignKey('auth.User', related_name='parcel_owner')
    metadata = models.CharField(max_length=100, blank=True)

    # Parcel data
    length = models.DecimalField(max_digits=10, decimal_places=4)
    width = models.DecimalField(max_digits=10, decimal_places=4)
    height = models.DecimalField(max_digits=10, decimal_places=4)
    distance_unit = models.ForeignKey(Unit, related_name='parcel_distance_unit')
    weight = models.DecimalField(max_digits=10, decimal_places=4)
    mass_unit = models.ForeignKey(Unit, related_name='parcel_mass_unit')
    value_amount = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    value_currency = models.ForeignKey('api.Currency', related_name='parcel_currency_relation', null=True)

    def get_parcel_value(self, currency=None, decimal_places=2, *args, **kwargs):
        decimal_places = int(decimal_places)

        if decimal_places < 0:
            decimal_places = 0
        if currency is None:
            currency = Currency.objects.get(iso="USD")

        if not self.value_amount or not value_currency:
            result = 0.0
        elif not currency == self.value_currency:
            result = self.value_amount * (currency.rate / self.value_currency.rate)
        else:
            result = self.value_amount

        result = round(result, decimal_places)
        return result

    def get_length(self, unit, decimal_places):
        """Returns the length of a Parcel expressed in the requested unit of measurement."""
        return self.get_value(self.length, unit, decimal_places)

    def get_length_plus_height(self, unit, decimal_places):
        """Returns length plus girth of a Parcel expressed in the requested unit of measurement."""
        self.length_plus_height = self.length + self.height
        return self.get_value(self.length_plus_height, unit, decimal_places)

    def get_length_plus_girth(self, unit, decimal_places):
        """Returns length plus girth of a Parcel expressed in the requested unit of measurement."""
        self.length_plus_girth = self.length + 2 * self.height + 2 * self.width
        return self.get_value(self.length_plus_girth, unit, decimal_places)

    def get_length_plus_width_plus_height(self, unit, decimal_places):
        """Returns length plus girth of a Parcel expressed in the requested unit of measurement."""
        self.length_plus_width_plus_height = self.length + self.height + self.width
        return self.get_value(self.length_plus_width_plus_height, unit, decimal_places)

    def get_width(self, unit, decimal_places):
        """Returns the width of a Parcel expressed in the requested unit of measurement."""
        return self.get_value(self.width, unit, decimal_places)

    def get_height(self, unit, decimal_places):
        """Returns the height of a Parcel expressed in the requested unit of measurement."""
        return self.get_value(self.height, unit, decimal_places)

    def get_weight(self, unit, decimal_places):
        """Returns the weight of a Parcel expressed in the requested unit of measurement."""
        return self.get_value(self.weight, unit, decimal_places)

    def get_value(self, attribute, unit=None, decimal_places=2):      # TODO: REFACTOR to use unit object and optionally the abbreviation
        """Returns a distance property (length, width or height) of a parcel expressed in the requested unit of measurement."""

        decimal_places = int(decimal_places)

        if decimal_places < 0:
            decimal_places = 0

        if attribute is self.weight:            # identity comparison here is important
            current_unit = self.mass_unit
        else:
            current_unit = self.distance_unit   # get the unit of distance the Parcel was originally created with
            
        if unit is None:
            unit = current_unit

        if unit is current_unit:
            result = Decimal(attribute)
        elif current_unit.dimension == unit.dimension:  # are the units compatible with each other? (mass vs. dimension)
            result = Decimal(attribute) * (Decimal(unit.rate) / Decimal(current_unit.rate))    # convert between the units
        else:
            raise ValidationError('Conversion is only possible between compatible units (distance vs. mass).')
        
        result = round(result, decimal_places)
        
        # Zero weight or dimension should be avoided
        if decimal_places == 0:
            result = int(result)
            if result == 0:
                result = 1
        else:
            if result == 0.0:
                result = 0.1

        return result

    def clean(self, force_insert = False, force_update = False, using = None, *args, **kwargs):
        """Cleans the Parcel object (e.g., sort distance dimensions) and validates it (are all required dimensions given?)"""
        if not self.cleaned:
            self.validate_parcel()

            # Order the distance dimensions
            dimensions = [self.length, self.width, self.height]
            dimensions.sort()

            self.length = dimensions[2]
            self.width = dimensions[1]
            self.height = dimensions[0]

            self.cleaned = True

        super(Parcel, self).clean(*args, **kwargs)

    def validate_parcel(self):
        """Checks whether all data which is required to make a useful parcel exists and is valid."""
        if not self.length or not self.width or not self.height or not self.weight:
            self.object_state = self.OBJECT_STATE_CHOICE.INVALID
            raise ValidationError('Parcel dimensions must be given.')
        if not (self.length > 0 and self.width > 0 and self.height > 0 and self.weight > 0):
            self.object_state = self.OBJECT_STATE_CHOICE.INVALID
            raise ValidationError('Parcel dimensions must be larger than 0.')
        if not self.distance_unit.dimension == Unit.DIMENSION_CHOICE.DISTANCE:
            self.object_state = self.OBJECT_STATE_CHOICE.INVALID
            raise ValidationError('Parcel measurements must be given in a unit of distance.')
        if not self.mass_unit.dimension == Unit.DIMENSION_CHOICE.MASS:
            self.object_state = self.OBJECT_STATE_CHOICE.INVALID
            raise ValidationError('Parcel weight must be given in a unit of mass.')

        self.object_state = self.OBJECT_STATE_CHOICE.VALID

    def save(self, *args, **kwargs):
        """Overridden save() method to make sure clean() is called any time."""
        self.clean(*args, **kwargs)
        super(Parcel, self).save(*args, **kwargs)

        # Trigger Mixpanel tracking
        mixp.track_event(mixp.track_API_parcel, self.object_owner, self)

    def __unicode__(self):
        return '%s (%0.2f x %0.2f x %0.2f %s @ %0.2f %s)' % (str(self.object_id), self.length, self.width, self.height, self.distance_unit.abbreviation, self.weight, self.mass_unit.abbreviation)

    def natural_key(self):
        return self.__unicode__()

class Address(AddressMixIn):
    """Defines Addresses from which a Shipment is submitted, to which a Shipment is directed, or which is used for billing purposes."""

    # Choice Lists
    OBJECT_STATE_CHOICE = Choices('INVALID', 'COMPLETED', 'VALID') # 'VALIDATED', 'UNCHANGED'
    OBJECT_PURPOSE_CHOICE = Choices('QUOTE', 'PURCHASE')
    OBJECT_SOURCE_CHOICE = Choices('INVALID', 'IP', 'ZIP_COUNTRY', 'COUNTRY', 'CITY_COUNTRY', 'FULLY_ENTERED')

    # Default Values
    default_name = "S. Hippo"
    default_phone = "+14157419407"
    default_email = "api@goshippo.com"

    # Object-related Metadata
    cleaned = False
    object_id = UUIDField(auto=True, db_index = True, unique=True)
    object_state = models.CharField(choices=OBJECT_STATE_CHOICE, default=OBJECT_STATE_CHOICE.INVALID, max_length=20)
    object_purpose = models.CharField(choices=OBJECT_PURPOSE_CHOICE, default=OBJECT_PURPOSE_CHOICE.QUOTE, max_length=20)
    object_source = models.CharField(choices=OBJECT_SOURCE_CHOICE, default=OBJECT_SOURCE_CHOICE.INVALID, max_length=20)
    object_created = models.DateTimeField(auto_now_add=True, editable=True)
    object_updated = models.DateTimeField(auto_now=True, editable=True)
    object_geoip_record = models.TextField(max_length=5000, blank=True, editable=True, null=True)
    object_gcoder_record = models.TextField(max_length=10000, blank=True, editable=True, null=True)
    object_owner = models.ForeignKey('auth.User', related_name='address_owner')
    metadata = models.CharField(max_length=100, blank=True)
    
    name = models.CharField(max_length=100, blank=True, default=default_name)
    phone = models.CharField(max_length=50, blank=True, default=default_phone)      # TODO: Validate phone number > no spaces, no dashes, no (), only + and digits --> UPS has problems
    email = models.EmailField(max_length=254, blank=True, default=default_email)
    residential = models.NullBooleanField(default=True)

    ip = models.GenericIPAddressField(null=True, blank=True)
    lat = models.DecimalField(max_digits=15, decimal_places=10, default=0.0)
    lng = models.DecimalField(max_digits=15, decimal_places=10, default=0.0)

    # Overridden for validation - called before saving the object to the database
    def clean(self, force_insert = False, force_update = False, using = None, *args, **kwargs):
        """Cleans the Address object (e.g., strip()), validates it against its purpose
            and completes data if needed (e.g., IP --> + country & city --> + street)"""
        if not self.cleaned: # prevent clean from being called twice (e.g., first in a form then from the API)
            # treat fields first
            if not self.street1 and self.street2:
                self.street1 = self.street2
                self.street2 = ''

            # Fill empty fields (but necessary to get a quote) with dummy data
            self.fill_fields()

            # Strip all fields from white space
            self.strip_fields()

            # now, validate what the data source of the object could be
            if self.validate_fully_entered():   # it could be a clean, fully-filled address entry
                self.object_source = self.OBJECT_SOURCE_CHOICE.FULLY_ENTERED
            else:
                if self.validate_zip_country():   # it could be a dataset with ZIP & country given
                    self.object_source = self.OBJECT_SOURCE_CHOICE.ZIP_COUNTRY
                elif self.validate_city_country():  # it could be a dataset with city & country given
                    self.object_source = self.OBJECT_SOURCE_CHOICE.CITY_COUNTRY
                elif self.ip and self.validate_ip():  # it could be a dataset with only an IP
                    self.object_source = self.OBJECT_SOURCE_CHOICE.IP
                elif self.validate_country():   # it could be a dataset with only a country given
                    self.object_source = self.OBJECT_SOURCE_CHOICE.COUNTRY
                else:   # if nothing validates, it's invalid
                    self.object_source = self.OBJECT_SOURCE_CHOICE.INVALID
                    raise ValidationError('At least a valid IP address or a country have to be provided to create an Address. For an Address to be PURCHASEable, all information must be provided.')

            self.validate_object_purpose()  # check whether the data source conforms with the intended use of the object (e.g, purpose=PURCHASE only with source=FULLY_ENTERED)

            # If an Address only contains an IP to interpolate data, get relevant data (country, city) from Geolocation
            if self.object_source == self.OBJECT_SOURCE_CHOICE.IP:
                geolocation = GeoLite()
                self.object_geoip_record = geolocation.lookup_address_for_ip(self)
                self = geolocation.map_geoip_record_to_address(address = self, record = self.object_geoip_record)

                if not self.validate_country():     # it could be that the lookup was not successful. In this case, the Address is not usable and invalid.
                    self.object_state = self.OBJECT_STATE_CHOICE.INVALID
                    raise ValidationError('The IP address provided could not be used to complete this Address.')

            # If an Address does not contain all necessary data, use Google Geocoding & Google Places to interpolate.
            if (self.object_source == self.OBJECT_SOURCE_CHOICE.ZIP_COUNTRY
                or self.object_source == self.OBJECT_SOURCE_CHOICE.CITY_COUNTRY
                or self.object_source == self.OBJECT_SOURCE_CHOICE.COUNTRY
                or self.object_source == self.OBJECT_SOURCE_CHOICE.IP
                and not self.object_state == self.OBJECT_STATE_CHOICE.INVALID):

                google_geography = GoogleGeography()

                self = google_geography.complete_address(address=self)

                if self.validate_any_country_after_completion():
                    self.object_state = self.OBJECT_STATE_CHOICE.COMPLETED
                else:
                    self.object_state = self.OBJECT_STATE_CHOICE.INVALID
                    raise ValidationError('The Address could not be completed based on the data provided.')

                self.object_purpose = self.OBJECT_PURPOSE_CHOICE.QUOTE

            self.cleaned = True

        super(Address, self).clean(*args, **kwargs)

    def fill_fields(self):
        """Fills fields with dummy data, given they are empty."""

        if not self.name:
            self.name = self.default_name
            self.object_state = self.OBJECT_STATE_CHOICE.COMPLETED
        if not self.phone:
            self.phone = self.default_phone     # inserting a dummy phone address does not lead to a COMPLETED state
        if not self.email:
            self.email = self.default_email
            self.object_state = self.OBJECT_STATE_CHOICE.COMPLETED

    def strip_fields(self):
        """Removes all unnecessary white space from any user-accessible field."""

        self.name = self.name.strip()
        self.company = self.company.strip()
        self.street1 = self.street1.strip()
        self.street2 = self.street2.strip()
        self.street_no = self.street_no.strip()
        self.city = self.city.strip()
        self.state = self.state.strip()
        self.zip = self.zip.strip()
        self.phone = self.phone.strip()

    def validate_fully_entered(self):
        """Checks whether the current Address object has been entered completely and has not been filled with dummy data.
            The only way to generate a valid PURCHASE Address."""
        if (self.name and self.name != self.default_name and (self.street1 and self.city and self.country) and (self.email and self.email != self.default_email)
            and (not self.country.requires_zip or (self.country.requires_zip and self.zip))
            and (not self.country.requires_state or (self.country.requires_state and self.state))):
            return True
        else:
            return False

    def validate_ip(self):
        """Checks whether the current Address object has an IP (only an IP & sample data) to use for completion/interpolation of data.
            Only used for QUOTE Addresses. If used with a PURCHASE Address, object_state will be INVALID."""
        try:
            validate_ipv46_address(self.ip)
        except ValidationError:
            return False

        if self.ip and not (self.country and (self.zip or self.city)):
            return True
        else:
            return False

    def validate_zip_country(self):
        """Checks whether the current Address object has a ZIP & a country given to use for completion/interpolation of data.
            Only used for QUOTE Addresses. If used with a PURCHASE Address, object_state will be INVALID."""
        if self.zip and self.country and self.country.requires_zip:
            return True
        else:
            return False

    def validate_city_country(self):
        """Checks whether the current Address object has a city & a country given to use for completion/interpolation of data.
            Only used for QUOTE Addresses. If used with a PURCHASE Address, object_state will be INVALID."""
        if self.city and self.country: # todo: what about state?
            return True
        else:
            return False

    def validate_country(self):
        """Checks whether the current Address object has a country given to use for completion/interpolation of data.
            Only used for QUOTE Addresses. If used with a PURCHASE Address, object_state will be INVALID."""
        if self.country:
            return True
        else:
            return False

    def validate_any_country_after_completion(self):
        """Checks whether, after completion, useful data is within the Address object.
            Only used for QUOTE Addresses. If used with a PURCHASE Address, object_state will be INVALID."""
        if self.city and self.country and self.street1: # TODO: what about state?
            return True
        else:
            return False

    def validate_object_purpose(self):
        """Checks whether the mode of data entry corresponds to the given purpose of the Address object. For PURCHASE Addresses, only FULLY_ENTERED is allowed."""
        if self.object_purpose == self.OBJECT_PURPOSE_CHOICE.PURCHASE:
            if self.object_source == self.OBJECT_SOURCE_CHOICE.FULLY_ENTERED:
                self.object_state = self.OBJECT_STATE_CHOICE.VALID
            else:
                self.object_state = self.OBJECT_STATE_CHOICE.INVALID
                raise ValidationError('For an Address to be PURCHASE\'able, all information must be provided.')

        if self.object_purpose == self.OBJECT_PURPOSE_CHOICE.QUOTE:
            if self.object_source != self.OBJECT_SOURCE_CHOICE.INVALID:
                self.object_state = self.OBJECT_STATE_CHOICE.VALID
            else:
                self.object_state = self.OBJECT_STATE_CHOICE.INVALID
                raise ValidationError('For an Address to be used for a QUOTE, at least an IP address or a country must be provided.')

    def save(self, *args, **kwargs):
        """Overridden save() method to make sure clean() is called any time."""
        self.clean(*args, **kwargs)
        super(Address, self).save(*args, **kwargs)

        # Trigger Mixpanel tracking
        # remove Geocoder Result Object since it breaks RQ
        self.object_gcoder_record = None
        mixp.track_event(mixp.track_API_address, self.object_owner, self)

    def get_cleaned_phone(self, max_digits=15, us_national=False):
        cleaned_number = ''
        for char in self.phone:
            if char.isdigit() or char == '+':
                cleaned_number += char
                
        if us_national:
            if cleaned_number[:1] == '1':
                cleaned_number=cleaned_number[1:]
            elif cleaned_number[:2] == '+1' or cleaned_number[:2] == '01':
                cleaned_number=cleaned_number[2:]
            elif cleaned_number[:3] == '001':
                cleaned_number=cleaned_number[3:]                

        return cleaned_number[:max_digits]

    def __unicode__(self):
        if self.country:
            country = self.country.name
        else:
            country = ''
        return '%s (%s, %s %s, %s)' % (str(self.object_id), self.name, self.city, self.zip, country)

    def natural_key(self):
        return self.__unicode__()

class Shipment(models.Model):
    """Defines a Shipment which is a Parcel shipped from an Address to another Address."""

    # Choices
    OBJECT_STATE_CHOICE = Choices('INVALID', 'COMPLETED', 'VALID') #'VALIDATED', 'UNCHANGED'
    OBJECT_STATUS_CHOICE = Choices('WAITING', 'QUEUED', 'SUCCESS', 'ERROR')
    OBJECT_PURPOSE_CHOICE = Choices('QUOTE', 'PURCHASE')

    # Object-related Metadata
    cleaned = False
    object_id = UUIDField(auto=True, db_index = True, unique=True)
    object_state = models.CharField(choices=OBJECT_STATE_CHOICE, default=OBJECT_STATE_CHOICE.INVALID, max_length=20)
    object_purpose = models.CharField(choices=OBJECT_PURPOSE_CHOICE, default=OBJECT_PURPOSE_CHOICE.QUOTE, max_length=20)
    object_created = models.DateTimeField(auto_now_add=True, editable=True)
    object_updated = models.DateTimeField(auto_now=True, editable=True)
    object_owner = models.ForeignKey('auth.User', related_name='shipment_owner')
    metadata = models.CharField(max_length=100, blank=True)

    # Shipment Data
    address_from = models.ForeignKey(Address, related_name='address_from')
    address_to = models.ForeignKey(Address, related_name='address_to')
    parcel = models.ForeignKey(Parcel)

    def clean(self, force_insert = False, force_update = False, using = None, *args, **kwargs):
        """Cleans the Shipment object and validates it (are both Addresses and the Parcel valid?)"""
        if not self.cleaned:
            self.validate_shipment()
            self.validate_shipment_purpose()
            self.cleaned = True

        super(Shipment, self).clean(*args, **kwargs)

    def validate_shipment(self):
        """Checks whether a Shipment is valid. Are the Addresses and the Parcel itself valid?"""

        if self.address_from == self.address_to:
            raise ValidationError('From and To Addresses are identical. Addresses must be different to create a valid shipment.')

        # Is every object included valid?
        if (self.address_from.object_state == self.address_from.OBJECT_STATE_CHOICE.VALID
            and self.address_to.object_state == self.address_to.OBJECT_STATE_CHOICE.VALID
            and self.parcel.object_state == self.parcel.OBJECT_STATE_CHOICE.VALID):
            self.object_state = self.OBJECT_STATE_CHOICE.VALID

        # Is there at least one address that has been completed?
        elif (self.address_from.object_state == self.address_from.OBJECT_STATE_CHOICE.COMPLETED
                or self.address_to.object_state == self.address_from.OBJECT_STATE_CHOICE.COMPLETED):
            self.object_state = self.OBJECT_STATE_CHOICE.COMPLETED

        # Is there any object that is invalid?
        elif (self.address_from.object_state == self.address_from.OBJECT_STATE_CHOICE.INVALID
            or self.address_to.object_state == self.address_to.OBJECT_STATE_CHOICE.INVALID
            or self.parcel.object_state == self.parcel.OBJECT_STATE_CHOICE.INVALID):
            self.object_state = self.OBJECT_STATE_CHOICE.INVALID
            raise ValidationError('A Shipment can only be created from objects with state VALID or COMPLETED.')

        # catch any further problems
        else:
            self.object_state = self.OBJECT_STATE_CHOICE.INVALID
            raise ValidationError('Validation of Shipment failed. Incomplete or invalid data provided.')

    def validate_shipment_purpose(self):
        """Checks whether a Shipment's purpose is correct. Are the Addresses and the Parcel itself for PURCHASE or for QUOTE?"""

        # Ensure that the object can be purchased
        if (self.address_from.object_purpose == self.address_from.OBJECT_PURPOSE_CHOICE.PURCHASE
            and self.address_to.object_purpose == self.address_to.OBJECT_PURPOSE_CHOICE.PURCHASE
            and self.object_state == self.OBJECT_STATE_CHOICE.VALID
            and self.object_purpose == self.OBJECT_PURPOSE_CHOICE.PURCHASE):
            self.object_purpose = self.OBJECT_PURPOSE_CHOICE.PURCHASE

        # there might also be a situation which leads to a QUOTE state
        elif (self.address_from.object_purpose == self.address_from.OBJECT_PURPOSE_CHOICE.QUOTE
            or self.address_to.object_purpose == self.address_to.OBJECT_PURPOSE_CHOICE.QUOTE
            or self.address_to.object_state == self.address_to.OBJECT_STATE_CHOICE.COMPLETED
            or self.address_from.object_state == self.address_to.OBJECT_STATE_CHOICE.COMPLETED
            or self.object_state == self.OBJECT_STATE_CHOICE.COMPLETED
            or self.object_purpose == self.OBJECT_PURPOSE_CHOICE.QUOTE):
            if self.object_purpose == self.OBJECT_PURPOSE_CHOICE.PURCHASE:
                self.object_state = self.OBJECT_STATE_CHOICE.INVALID
                raise ValidationError('PURCHASEable Shipments can only be created from PURCHASEable and VALID Addresses.')
            else:
                self.object_purpose = self.OBJECT_PURPOSE_CHOICE.QUOTE

        else:
            self.object_state = self.OBJECT_STATE_CHOICE.INVALID
            raise ValidationError('Validation of Shipment state failed. Incomplete or invalid data provided.')

    def save(self, trigger_generation=True, *args, **kwargs):
        """Overridden save() method to make sure clean() is called any time."""
        self.clean(*args, **kwargs)
        super(Shipment, self).save(*args, **kwargs) # Call the "real" save() method.

        if trigger_generation:
            # Trigger rates generation
            from adapter.dispatch import queue_generate_rates
            queue_generate_rates(self, self.address_to.country.currency)

            # Trigger Mixpanel tracking
            mixp.track_event(mixp.track_API_shipment, self.object_owner, self)

    def get_rates_url(self, *args, **kwargs):
        api_url = settings.CURRENT_API_URL
        shipments_url = urljoin(api_url, "shipments/")
        shipment_url = urljoin(shipments_url, str(self.object_id) + "/")
        rates_url = urljoin(shipment_url, settings.RATES_URL)
        return rates_url
        
    def get_object_status(self, local_currency=None, *args, **kwargs):
        if not local_currency:
            local_currency = self.address_to.country.currency
            
        shipment_rate_query = ShipmentRateQuery.objects.filter(shipment=self, currency=local_currency)
        
        if shipment_rate_query:
            query = shipment_rate_query[0]
            if query.entered_queue and not query.left_queue:
                result = Shipment.OBJECT_STATUS_CHOICE.QUEUED
            if query.entered_queue and query.left_queue:
                result = Shipment.OBJECT_STATUS_CHOICE.SUCCESS       # TODO: What about errors?
        else:
            result = Shipment.OBJECT_STATUS_CHOICE.WAITING

        return result

    def generate_rates_attributes(self, *args, **kwargs):
        attribute_fastest = Attribute.objects.get(name=Attribute.OBJECT_NAME_CHOICES.FASTEST)
        attribute_cheapest = Attribute.objects.get(name=Attribute.OBJECT_NAME_CHOICES.CHEAPEST)
        attribute_bestvalue = Attribute.objects.get(name=Attribute.OBJECT_NAME_CHOICES.BESTVALUE)
        rates = Rate.objects.filter(shipment=self)
        
        for rate in rates:
            rate.attributes.clear()

        # Fastest rate(s)
        rates_fastest = rates.filter(arrives_by__isnull=False).order_by('days', 'arrives_by')
        if rates_fastest:
            min_days = rates_fastest.first().days
            min_time = rates_fastest.first().arrives_by

            for rate in rates_fastest:
                if rate.days and rate.days == min_days and rate.arrives_by == min_time:
                    rate.attributes.add(attribute_fastest)
        else:
            rates_fastest = rates.order_by('days')
            min_days = rates_fastest.first().days

            for rate in rates_fastest:
                if rate.days and rate.days == min_days:
                    rate.attributes.add(attribute_fastest)

        # Cheapest rate(s)
        rates_cheapest = rates.order_by('amount_local')
        min_price = rates_cheapest.first().amount_local

        for rate in rates_cheapest:
            if rate.amount_local == min_price:
                rate.attributes.add(attribute_cheapest)

        # Best value rate(s)
        min_value = 0

        # for cases of same-day delivery
        i = 0
        while min_value == 0 and rates_cheapest[i].days is not None:
            min_value = rates_cheapest[i].amount_local * rates_cheapest[i].days
            i+=1

        for rate in rates_cheapest:
            if rate.days and Decimal(rate.amount_local) * Decimal(rate.days) == Decimal(min_value):
                rate.attributes.add(attribute_bestvalue)

        return True

    def __unicode__(self):
        return str(self.object_id)

    def natural_key(self):
        return self.__unicode__()

class Attribute(models.Model):
    OBJECT_NAME_CHOICES = Choices('BESTVALUE', 'CHEAPEST', 'FASTEST')
    name = models.CharField(max_length=200, choices=OBJECT_NAME_CHOICES, unique=True)

    def __unicode__(self):
        return str(self.name)

    def natural_key(self):
        return self.__unicode__()

class Rate(PriceMixIn, DurationMixIn, TierMixIn, ServicelevelMixIn):
    """Defines a Rate, which is the calculated Price for a Shipment along with all relevant meta-data."""

    # Choice Lists
    OBJECT_STATE_CHOICE = Choices('INVALID', 'VALID') # 'VALIDATED', 'UNCHANGED' # TODO: Validation
    OBJECT_PURPOSE_CHOICE = Choices('QUOTE', 'PURCHASE')

    # Data
    adapter = models.ForeignKey('adapter.Adapter', related_name='rate_adapter_relation', )
    provider = models.ForeignKey('adapter.Provider', related_name='rate_provider_relation', )
    price = models.ForeignKey('adapter.Price', related_name='rate_price_relation', null=True, blank=True)
    currency = models.ForeignKey('api.Currency', related_name='rate_currency_relation')

    amount_local = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    currency_local = models.ForeignKey('api.Currency', related_name='rate_local_currency_relation', null=True, blank=True)

    duration = models.ForeignKey('adapter.Duration', related_name='rate_duration_relation', null=True, blank=True)
    tier = models.ForeignKey('adapter.Tier', related_name='rate_tier_relation', null=True, blank=True)

    servicelevel = models.ForeignKey('adapter.Servicelevel', related_name='rate_servicelevel_relation', null=True, blank=True)
    insurance_currency = models.ForeignKey('api.Currency', null=True, blank=True, related_name='rate_insurance_currency_relation')

    insurance_amount_local = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    insurance_currency_local = models.ForeignKey('api.Currency', null=True, blank=True, related_name='rate_insurance_currency_local_relation')

    outbound_endpoint = models.ForeignKey('adapter.Endpoint', related_name='rate_outbound_endpoint_relation', null=True, blank=True)
    inbound_endpoint = models.ForeignKey('adapter.Endpoint', related_name='rate_inbound_endpoint_relation', null=True, blank=True)

    attributes = models.ManyToManyField(Attribute, related_name='rate_attributes_relation', null=True, blank=True)

    shipment = models.ForeignKey(Shipment, related_name='rate_shipment_relation')

    # Object-related Metadata
    cleaned = False
    object_id = UUIDField(auto=True, db_index = True, unique=True)
    object_state = models.CharField(choices=OBJECT_STATE_CHOICE, default=OBJECT_STATE_CHOICE.INVALID, max_length=20)
    object_purpose = models.CharField(choices=OBJECT_PURPOSE_CHOICE, default=OBJECT_PURPOSE_CHOICE.QUOTE, max_length=20)
    object_created = models.DateTimeField(auto_now_add=True, editable=True)
    object_updated = models.DateTimeField(auto_now=True, editable=True)
    object_owner = models.ForeignKey('auth.User', related_name='rate_owner')
    object_current = models.BooleanField(default=True)
    object_api_record = models.TextField(max_length=2500, blank=True, null=True)
    object_custom_logic_applied = models.BooleanField(default=False)

    def is_purchasable(self, transaction=None, adapter=None):
        age_7days = self.object_created + timedelta(days=7)

        if self.servicelevel:
            if (self.object_state == self.OBJECT_STATE_CHOICE.VALID
                and self.object_purpose == self.OBJECT_PURPOSE_CHOICE.PURCHASE
                and self.servicelevel.available_shippo
                and datetime.today() < age_7days):
                return True
            elif settings.DEBUG:
                return True
        
        # haven't returned anything up to now means none of the cases above is true.
        if transaction and adapter:
            transaction.add_message('not_purchasable', 'The Rate for which you want to create a transaction cannot be purchased. The Rate is either not available for purchase with Shippo, its object_state is INVALID, its object_status is QUOTE, or the Rate is older than 7 days.',
                                         adapter, '', new_status = Transaction.OBJECT_STATUS_CHOICE.ERROR)

        return False

    def fill_from_price_object(self, price):
        self.provider = price.provider

        self.price = price
        self.amount = price.amount
        self.currency = price.currency
        self.insurance_currency = price.currency

        # Servicelevel
        #self.fill_from_servicelevel_object(price.servicelevel)

        # Tier
        self.tier = price.tier
        self.tier_name = price.tier.tier_name

        # Endpoints
        self.outbound_endpoint = price.outbound_endpoint
        self.inbound_endpoint = price.inbound_endpoint

    def fill_from_duration_object(self, duration):
        self.duration = duration
        self.days = duration.days
        self.duration_terms = duration.duration_terms

    def fill_from_servicelevel_object(self, servicelevel):
        self.servicelevel = servicelevel

        self.servicelevel_name = servicelevel.servicelevel_name
        self.servicelevel_terms = servicelevel.servicelevel_terms
        self.insurance = servicelevel.insurance
        self.insurance_amount = servicelevel.insurance_amount
        self.trackable = servicelevel.trackable
        self.delivery_attempts = servicelevel.delivery_attempts
        self.arrives_by = servicelevel.arrives_by
        self.available_shippo = servicelevel.available_shippo
        self.insurance_currency = servicelevel.insurance_currency

    def fill_with_local_currency(self, currency = None, *args, **kwargs):
        if not currency:
            currency = self.shipment.address_to.country.currency

        if self.price and not self.object_custom_logic_applied:
            self.amount_local = self.get_attribute(attribute = self.price.amount, currency = currency)
        else:
            self.amount_local = self.get_attribute(attribute = self.amount, currency = currency)

        if self.servicelevel:
            self.insurance_amount_local = self.get_attribute(attribute = self.servicelevel.insurance_amount, currency = currency)
        else:
            self.insurance_amount_local = self.get_attribute(attribute = self.insurance_amount, currency = currency)

        self.currency_local = currency
        self.insurance_currency_local = currency

    def get_attribute(self, attribute = None, currency = None, *args, **kwargs):
        if not attribute:
            return None

        if not currency:
            currency = Currency.objects.get(iso="USD")

        if currency and not currency == self.currency:
            result = Decimal(attribute) * (Decimal(currency.rate) / Decimal(self.currency.rate))
            return str(round(result, 2))
        else:
            return attribute

    def get_amount_in_currency(self, currency=None, *args, **kwargs):
        if not currency:
            currency = self.shipment.address_from.country.currency
        return self.get_attribute(attribute=self.amount, currency=currency)

    def validate_rate_purpose(self, *args, **kwargs):
        """Sets the Rate's purpose depending on the Shipment's purpose."""
        self.object_purpose = self.shipment.object_purpose

    def clean(self, force_insert = False, force_update = False, using = None, *args, **kwargs):
        """Cleans the Rate object and validates it."""
        self.validate_rate()
        self.validate_rate_purpose()

        super(Rate, self).clean(*args, **kwargs)

    def validate_rate(self, *args, **kwargs):
        if self.adapter and self.provider and self.amount > 0 and self.currency and self.shipment:
            self.object_state = self.OBJECT_STATE_CHOICE.VALID
        else:
            self.object_state = self.OBJECT_STATE_CHOICE.INVALID

    def save(self, *args, **kwargs):
        self.validate_rate(*args, **kwargs)
        self.validate_rate_purpose()

        super(Rate, self).save(*args, **kwargs) # Call the "real" save() method.

        # Trigger Mixpanel tracking
        mixp.track_event(mixp.track_API_rate, self.object_owner, self)

    def __unicode__(self):
        return '%s (%s %s with %s as %s from %s, %s to %s, %s)' % (str(self.object_id), self.currency.iso, self.amount,
                                                                   self.provider.name, self.servicelevel_name,
                                                                   self.shipment.address_from.city, self.shipment.address_from.country.iso2,
                                                                   self.shipment.address_to.city, self.shipment.address_to.country.iso2)

    def natural_key(self):
        return self.__unicode__()

class ShipmentRateQuery(models.Model):
    """A logging table to keep overview over the Rate generation process."""

    shipment = models.ForeignKey(Shipment, related_name='ratequeried_shipment_relation')
    currency = models.ForeignKey(Currency, related_name='ratequeried_currency_relation')
    entered_queue = models.DateTimeField(null=True)
    left_queue = models.DateTimeField(null=True)
    
    class Meta:
        unique_together = ("shipment", "currency")
        
class AddressLocationQuery(models.Model):
    """A logging table to keep overview over the Location generation process."""

    address = models.ForeignKey(Address, related_name='locationqueried_address_relation', unique=True)
    entered_queue = models.DateTimeField(null=True)
    left_queue = models.DateTimeField(null=True)
    
class Transaction(models.Model):
    # Choices
    OBJECT_STATE_CHOICE = Choices('INVALID', 'VALID')
    OBJECT_STATUS_CHOICE = Choices('WAITING', 'QUEUED', 'SUCCESS', 'ERROR')

    # Object-related Metadata
    object_id = UUIDField(auto=True, db_index = True, unique=True)
    object_state = models.CharField(choices=OBJECT_STATE_CHOICE, default=OBJECT_STATE_CHOICE.INVALID, max_length=20)
    object_status = models.CharField(choices=OBJECT_STATUS_CHOICE, default=OBJECT_STATUS_CHOICE.WAITING, max_length=20)
    object_created = models.DateTimeField(auto_now_add=True, editable=True)
    object_updated = models.DateTimeField(auto_now=True, editable=True)
    object_owner = models.ForeignKey('auth.User', related_name='transaction_owner')
    entered_queue = models.DateTimeField(null=True)
    left_queue = models.DateTimeField(null=True)
    object_api_message = models.TextField(max_length=5000, blank=True)
    object_api_response = models.TextField(max_length=10000, blank=True)
    metadata = models.CharField(max_length=100, blank=True)
    was_test = models.NullBooleanField() 

    # Transaction Data
    rate = models.ForeignKey(Rate, related_name='transaction_rate', db_index=True)
    notification_email_from = models.BooleanField()
    notification_email_to = models.BooleanField()
    notification_email_other = models.EmailField(max_length=254, blank=True)
    pickup_date = models.DateTimeField(blank=True, null=True)
    tracking_number = models.CharField(max_length=50, blank=True)
    label_url = models.URLField(max_length=2048, blank=True)
    
    def save(self, trigger_generation=True, *args, **kwargs):
        """Overridden save() method to make sure transaction is valid."""
        # TODO: validation must occur here. E.g., pickup_date necessary for certain rates?
        if not self.rate.object_state == Rate.OBJECT_STATE_CHOICE.VALID: # or not self.rate.object_purpose == Rate.OBJECT_PURPOSE_CHOICE.PURCHASE: # Remove after testing
            self.object_state = self.OBJECT_STATE_CHOICE.INVALID
            raise ValidationError('Only Rates that are PURCHASEable can be used to generate a Transaction. For a Rate to be PURCHASEable, two PURCHASEable Addresses have to be used to create a Shipment.')
        else:
            self.object_state = self.OBJECT_STATE_CHOICE.VALID

        super(Transaction, self).save(*args, **kwargs) # Call the "real" save() method.

        # Trigger label generation
        if trigger_generation and self.object_state == self.OBJECT_STATE_CHOICE.VALID:
            from adapter.dispatch import queue_generate_label
            queue_generate_label(self)

        #mixpanel tracking via dispatch to get all results
        
    def add_message(self, code, text, source, stack_trace, new_status = None, *args, **kwargs):
        message = Message(transaction=self)
        message.code = code
        message.text = text
        message.source = source
        message.stack_trace = stack_trace
        message.save()
        
        if new_status:
            self.object_status = new_status
            self.save(trigger_generation=False)
            
    def set_was_test(self, production, *args, **kwargs):
        self.was_test = not production
        
    def credentials_missing(self, *args, **kwargs):
        self.add_message(code='credentials_error', text = "You have not supplied credentials for %s. Please enter your credentials for %s and try again." % (self.rate.provider.name, self.rate.provider.name), source=self.rate.adapter, stack_trace='', new_status=Transaction.OBJECT_STATUS_CHOICE.ERROR)
        
    def refresh_track(self, *args, **kwargs):
        if self.tracking_number:
            current_tracks = Track.objects.filter(transaction=self).order_by('-object_updated')
            
            if current_tracks:
                latest_track = current_tracks[0]
                lifetime = latest_track.object_updated + timedelta(minutes=settings.TRACKING_RESULT_LIFETIME)
                
            if not current_tracks or (lifetime and lifetime < timezone.now()):
                from adapter import dispatch
                        
                config = dispatch.get_config(self.rate.adapter, self.object_owner)
                track_service = dispatch.get_track_service(config)
    
                if track_service:
                    track_service.get_status(self)

class Message(models.Model):
    """A model to save error messages, warnings, etc."""
    rate = models.ForeignKey(Rate, related_name='message_rate_relation', null=True, blank=True)
    transaction = models.ForeignKey(Transaction, related_name='message_transaction_relation', null=True, blank=True)

    code = models.CharField(max_length=25, blank=True)
    text = models.CharField(max_length=2000, blank=True)
    source = models.ForeignKey('adapter.Adapter', related_name='message_adapter_relation', null=True, blank=True)
    stack_trace = models.CharField(max_length=5000, blank=True)
    
class Location(AddressMixIn):
    """A Location is a place where Parcels can be dropped off or picked up. Usually, one Location provides service for one or more Providers."""
    provider = models.ForeignKey('adapter.Provider', related_name='location_provider_relation',)
    address = models.ForeignKey(Address, related_name='location_address_relation')
    distance_amount = models.DecimalField(max_digits=6, decimal_places=2)
    distance_unit = models.ForeignKey(Unit, related_name='location_field_relation', null=True, blank=True)
    object_api_record = models.TextField(max_length=10000, blank=True)
    
    phone = models.CharField(max_length=50, blank=True)
    email = models.EmailField(max_length=254, blank=True)
    
    lat = models.DecimalField(max_digits=15, decimal_places=10, default=0.0)
    lng = models.DecimalField(max_digits=15, decimal_places=10, default=0.0)
    
    object_id = UUIDField(auto=True, db_index = True, unique=True)
    object_created = models.DateTimeField(auto_now_add=True, editable=True)
    object_updated = models.DateTimeField(auto_now=True, editable=True)
    object_owner = models.ForeignKey('auth.User', related_name='location_owner')
    adapter = models.ForeignKey('adapter.Adapter', related_name='location_adapter')
    
    further_information = models.TextField(max_length=2000, blank=True)

class Track(models.Model):
    """"""
    DELIVERY_DATE_CHOICE = Choices('SCHEDULED', 'RESCHEDULED', 'DELIVERED', 'ESTIMATED')
    object_id = UUIDField(auto=True, db_index = True, unique=True)
    object_created = models.DateTimeField(auto_now_add=True, editable=True)
    object_updated = models.DateTimeField(auto_now=True, editable=True)
    object_owner = models.ForeignKey('auth.User', related_name='tracking_owner')
    provider = models.ForeignKey('adapter.Provider', related_name='tracking_provider')
    adapter = models.ForeignKey('adapter.Adapter', related_name='tracking_adapter')
    
    transaction = models.ForeignKey(Transaction, related_name='tracking_transaction_relation')
    pickup_date = models.DateTimeField(null=True)
    delivery_date = models.DateTimeField(null=True)
    delivery_date_status = models.CharField(choices=DELIVERY_DATE_CHOICE, max_length=100, blank=True)
    status = models.CharField(max_length=200, blank=True)
    enroute = models.CharField(max_length=200, blank=True)
    status_date = models.DateTimeField(null=True)
    status_details = models.CharField(max_length=400, blank=True)
    location_street1 = models.CharField(max_length=100, blank=True)
    location_street2 = models.CharField(max_length=100, blank=True)
    location_street_no = models.CharField(max_length=10, blank=True)
    location_city = models.CharField(max_length=100, blank=True)
    location_state = models.CharField(max_length=100, blank=True)
    location_zip = models.CharField(max_length=20, blank=True)
    location_country = models.ForeignKey(Country, blank=True, null=True, related_name='tracking_country_relation')
    location_details = models.CharField(max_length=500, blank=True)

    signed_for = models.CharField(max_length=100, blank=True)
    
