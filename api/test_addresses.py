# -*- coding: utf-8 -*-
from django.utils import unittest
from django.test import TestCase
from decimal import Decimal
from math import ceil
import sys

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from api.models import Currency, Language, Country, Unit, Parcel, Address, Shipment

class ShipmentTestCase(TestCase):
    def setUp(self):
        """Creating necessary units & parcels"""
        self.create_countries()
        self.create_users()
        self.create_units()
        
        self.create_parcels()
        #self.create_addresses()
        
    def test_address_1_fully_purchase(self):
        """Tests a PURCHASE Address that was fully entered."""
        self.address_1_fully_purchase = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.PURCHASE, name = "  Max Diez  ", company = "Diez IT GmbH & Co. KG", street1 = "Heidenkamer Str.", street_no = "2", city="Tiefenbach", zip="84184", country=self.germany, phone="+49 (80709) 9261928", email="max@diez-it.de")
        
        # values should not have been changed
        assert self.address_1_fully_purchase.name != self.address_1_fully_purchase.default_name
        assert self.address_1_fully_purchase.phone != self.address_1_fully_purchase.default_phone
        assert self.address_1_fully_purchase.email != self.address_1_fully_purchase.default_email
        
        # values should have been stripped
        self.assertEqual(self.address_1_fully_purchase.name, "   Max Diez   ".strip())
        
        # and it should have been detected that the address was fully entered
        self.assertEqual(self.address_1_fully_purchase.object_source, Address.OBJECT_SOURCE_CHOICE.FULLY_ENTERED)
        # ...and valid
        self.assertEqual(self.address_1_fully_purchase.object_state, Address.OBJECT_STATE_CHOICE.VALID)
        # ... and purchasable
        self.assertEqual(self.address_1_fully_purchase.object_purpose, Address.OBJECT_PURPOSE_CHOICE.PURCHASE)
        
        # no GeoIP lookup should have taken place...
        self.assertEqual(self.address_1_fully_purchase.object_geoip_record, None)
        # ...and no Google Geocoding...
        self.assertEqual(self.address_1_fully_purchase.object_gcoder_record, None)
        self.assertEqual(self.address_1_fully_purchase.lat, 0.0)
        self.assertEqual(self.address_1_fully_purchase.lng, 0.0)
        
    def test_address_1_fully_purchase_noZIP_country(self):
        """Tests a PURCHASE Address that was fully entered."""
        self.address_1_fully_purchase = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.PURCHASE, name = "Simon Kreuz", company = "Popout, Inc.", street1 = "Indian Summer Ct               ", street_no = "1092", city="San Jose", state="CA", country = self.usa, phone="+1 (415) 741 9393", email="simon@goshippo.com")
        
        # values should not have been changed
        assert self.address_1_fully_purchase.name != self.address_1_fully_purchase.default_name
        assert self.address_1_fully_purchase.phone != self.address_1_fully_purchase.default_phone
        assert self.address_1_fully_purchase.email != self.address_1_fully_purchase.default_email
        
        # values should have been stripped
        self.assertEqual(self.address_1_fully_purchase.name, "Simon Kreuz")
        
        # and it should have been detected that the address was fully entered
        self.assertEqual(self.address_1_fully_purchase.object_source, Address.OBJECT_SOURCE_CHOICE.FULLY_ENTERED)
        # ...and valid
        self.assertEqual(self.address_1_fully_purchase.object_state, Address.OBJECT_STATE_CHOICE.VALID)
        # ... and purchasable
        self.assertEqual(self.address_1_fully_purchase.object_purpose, Address.OBJECT_PURPOSE_CHOICE.PURCHASE)
        
        # no GeoIP lookup should have taken place...
        self.assertEqual(self.address_1_fully_purchase.object_geoip_record, None)
        # ...and no Google Geocoding...
        self.assertEqual(self.address_1_fully_purchase.object_gcoder_record, None)
        self.assertEqual(self.address_1_fully_purchase.lat, 0.0)
        self.assertEqual(self.address_1_fully_purchase.lng, 0.0)
        
    def test_address_2_fully_purchase(self):
        """Tests a PURCHASE Address that was fully entered."""
        self.address_2_fully_purchase = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.PURCHASE, name = "Simon Kreuz", company = "Popout, Inc.", street1 = "Indian Summer Ct               ", street_no = "1092", city="San Jose", zip="95122", state="CA", country = self.usa, phone="+1 (415) 741 9393", email="simon@goshippo.com")
        #self.address_2_fully_purchase.clean()
        
        # values should not have been changed
        assert self.address_2_fully_purchase.name != self.address_2_fully_purchase.default_name
        assert self.address_2_fully_purchase.phone != self.address_2_fully_purchase.default_phone
        assert self.address_2_fully_purchase.email != self.address_2_fully_purchase.default_email
        
        # values should have been stripped
        self.assertEqual(self.address_2_fully_purchase.street1, "Indian Summer Ct               ".strip())
        
        # and it should have been detected that the address was fully entered
        self.assertEqual(self.address_2_fully_purchase.object_source, Address.OBJECT_SOURCE_CHOICE.FULLY_ENTERED)
        # ...and valid
        self.assertEqual(self.address_2_fully_purchase.object_state, Address.OBJECT_STATE_CHOICE.VALID)
        # ... and purchasable
        self.assertEqual(self.address_2_fully_purchase.object_purpose, Address.OBJECT_PURPOSE_CHOICE.PURCHASE)
        
        # no GeoIP lookup should have taken place...
        self.assertEqual(self.address_2_fully_purchase.object_geoip_record, None)
        # ...and no Google Geocoding...
        self.assertEqual(self.address_2_fully_purchase.object_gcoder_record, None)
        self.assertEqual(self.address_2_fully_purchase.lat, 0.0)
        self.assertEqual(self.address_2_fully_purchase.lng, 0.0)
        
    def test_address_3_fully_quote(self):
        """Tests a QUOTE Address that was FULLY_ENTERED."""
        self.address_3_fully_quote = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.QUOTE, name = "Kathrina Baumann", street1 = "Bettinger Str.", zip="75113", city = "Grenzach-Wyhlen", country = self.germany, email="kmmbaumann@gmail.com")
        #self.address_3_fully_quote.clean()
        
        # values should not have been changed
        assert self.address_3_fully_quote.name != self.address_3_fully_quote.default_name
        # Phone number has been added, but this is no reason to INVALIDate the object
        assert self.address_3_fully_quote.phone == self.address_3_fully_quote.default_phone
        assert self.address_3_fully_quote.email != self.address_3_fully_quote.default_email
        
        # and it should have been detected that the address was fully entered
        self.assertEqual(self.address_3_fully_quote.object_source, Address.OBJECT_SOURCE_CHOICE.FULLY_ENTERED)
        # ...and valid
        self.assertEqual(self.address_3_fully_quote.object_state, Address.OBJECT_STATE_CHOICE.VALID)
        # ... and purchasable
        self.assertEqual(self.address_3_fully_quote.object_purpose, Address.OBJECT_PURPOSE_CHOICE.QUOTE)
        
        # no GeoIP lookup should have taken place...
        self.assertEqual(self.address_3_fully_quote.object_geoip_record, None)
        # ...and no Google Geocoding...
        self.assertEqual(self.address_3_fully_quote.object_gcoder_record, None)
        self.assertEqual(self.address_3_fully_quote.lat, 0.0)
        self.assertEqual(self.address_3_fully_quote.lng, 0.0)
        
    def test_address_4_ip(self):
        """Tests a QUOTE Address with only an IP address given."""
        self.address_4_ip = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.QUOTE, ip = "50.152.187.67")
        #self.address_4_ip.clean()
        
        # values should have been completed
        assert self.address_4_ip.name == self.address_4_ip.default_name
        assert self.address_4_ip.phone == self.address_4_ip.default_phone
        assert self.address_4_ip.email == self.address_4_ip.default_email
        
        # and it should have been detected that the address was fully entered
        self.assertEqual(self.address_4_ip.object_source, Address.OBJECT_SOURCE_CHOICE.IP)
        # ...and valid
        self.assertEqual(self.address_4_ip.object_state, Address.OBJECT_STATE_CHOICE.COMPLETED)
        # ... and a quote
        self.assertEqual(self.address_4_ip.object_purpose, Address.OBJECT_PURPOSE_CHOICE.QUOTE)
        
        # a GeoIP lookup should have taken place...
        assert self.address_4_ip.object_geoip_record
        # ...and Google Geocoding...
        assert self.address_4_ip.object_gcoder_record
        assert self.address_4_ip.lat != 0.0
        assert self.address_4_ip.lng != 0.0
        
        # Finally, street, city, country should be filled out:
        assert self.address_4_ip.street1
        assert self.address_4_ip.city
        assert self.address_4_ip.country != None
        
    def test_address_5_completed_city(self):
        """Tests a QUOTE Address with only a country and Umlaut city given."""
        self.address_5_completed_city = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.QUOTE, city = u'Köln', country=self.germany)
        #self.address_5_completed_city.clean()
        
        # values should have been completed
        assert self.address_5_completed_city.name == self.address_5_completed_city.default_name
        assert self.address_5_completed_city.phone == self.address_5_completed_city.default_phone
        assert self.address_5_completed_city.email == self.address_5_completed_city.default_email
        
        # and it should have been detected that the address was fully entered
        self.assertEqual(self.address_5_completed_city.object_source, Address.OBJECT_SOURCE_CHOICE.CITY_COUNTRY)
        # ...and valid
        self.assertEqual(self.address_5_completed_city.object_state, Address.OBJECT_STATE_CHOICE.COMPLETED)
        # ... and a quote
        self.assertEqual(self.address_5_completed_city.object_purpose, Address.OBJECT_PURPOSE_CHOICE.QUOTE)
        
        # no GeoIP lookup should have taken place...
        self.assertEqual(self.address_5_completed_city.object_geoip_record, None)
        # ...but Google Geocoding...
        assert self.address_5_completed_city.object_gcoder_record
        assert self.address_5_completed_city.lat != 0.0
        assert self.address_5_completed_city.lng != 0.0
        
        # Finally, street, city, country should be filled out:
        assert self.address_5_completed_city.street1
        assert self.address_5_completed_city.city
        assert self.address_5_completed_city.zip
        assert self.address_5_completed_city.country != None
        
    def test_address_6_completed_zip(self):
        """Tests a QUOTE Address with only a country and ZIP given."""
        self.address_6_completed_zip = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.QUOTE, zip="95122", country=self.usa)
        #self.address_6_completed_zip.clean()
        
        # values should have been completed
        assert self.address_6_completed_zip.name == self.address_6_completed_zip.default_name
        assert self.address_6_completed_zip.phone == self.address_6_completed_zip.default_phone
        assert self.address_6_completed_zip.email == self.address_6_completed_zip.default_email
        
        # and it should have been detected that the address was fully entered
        self.assertEqual(self.address_6_completed_zip.object_source, Address.OBJECT_SOURCE_CHOICE.ZIP_COUNTRY)
        # ...and valid
        self.assertEqual(self.address_6_completed_zip.object_state, Address.OBJECT_STATE_CHOICE.COMPLETED)
        # ... and a quote
        self.assertEqual(self.address_6_completed_zip.object_purpose, Address.OBJECT_PURPOSE_CHOICE.QUOTE)
        
        # no GeoIP lookup should have taken place...
        self.assertEqual(self.address_6_completed_zip.object_geoip_record, None)
        # ...but Google Geocoding...
        assert self.address_6_completed_zip.object_gcoder_record
        assert self.address_6_completed_zip.lat != 0.0
        assert self.address_6_completed_zip.lng != 0.0
        
        # Finally, street, city, country should be filled out:
        assert self.address_6_completed_zip.street1
        assert self.address_6_completed_zip.city
        assert self.address_6_completed_zip.zip
        assert self.address_6_completed_zip.country != None
        
    def test_address_7_completed_country(self):
        """Tests a QUOTE Address with only a country given."""
        self.address_7_completed_country = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.QUOTE, country=self.usa)
        #self.address_7_completed_country.clean()
        
        # values should have been completed
        assert self.address_7_completed_country.name == self.address_7_completed_country.default_name
        assert self.address_7_completed_country.phone == self.address_7_completed_country.default_phone
        assert self.address_7_completed_country.email == self.address_7_completed_country.default_email
        
        # and it should have been detected that the address was fully entered
        self.assertEqual(self.address_7_completed_country.object_source, Address.OBJECT_SOURCE_CHOICE.COUNTRY)
        # ...and valid
        self.assertEqual(self.address_7_completed_country.object_state, Address.OBJECT_STATE_CHOICE.COMPLETED)
        # ... and a quote
        self.assertEqual(self.address_7_completed_country.object_purpose, Address.OBJECT_PURPOSE_CHOICE.QUOTE)
        
        # no GeoIP lookup should have taken place...
        self.assertEqual(self.address_7_completed_country.object_geoip_record, None)
        # ...but Google Geocoding...
        assert self.address_7_completed_country.object_gcoder_record
        assert self.address_7_completed_country.lat != 0.0
        assert self.address_7_completed_country.lng != 0.0
        
        # Finally, street, city, country should be filled out:
        assert self.address_7_completed_country.street1
        assert self.address_7_completed_country.city
        assert self.address_7_completed_country.zip
        assert self.address_7_completed_country.country != None
        
    def test_address_8_invalid(self):
        """Tests a missing country."""
        with self.assertRaises(ValidationError) as error:
            self.address_8_invalid = Address.objects.create(object_owner = self.user, street1="Heidenkamer Str.", street_no="2", city="Tiefenbach")
        
        self.assertEqual(error.exception.message, 'At least a valid IP address or a country have to be provided to create an Address. Additionally, for an Address to be PURCHASE\'able, all information must be provided.')
        
    def test_address_9_invalid(self):
        """Tests an invalid IP address."""
        with self.assertRaises(ValidationError) as error:
            self.address_9_invalid = Address.objects.create(object_owner = self.user, ip="invalid")
        
        self.assertEqual(error.exception.message, 'At least a valid IP address or a country have to be provided to create an Address. Additionally, for an Address to be PURCHASE\'able, all information must be provided.')

    def test_address_10_invalid(self):
        """Tests a PURCHASE Address with a missing ZIP."""
        with self.assertRaises(ValidationError) as error:
            self.address_10_invalid = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.PURCHASE, name = "Kathrina Baumann", street1 = "Bettinger Str.", city = "Grenzach-Wyhlen", country = self.germany)
        
        self.assertEqual(error.exception.message, 'At least a valid IP address or a country have to be provided to create an Address. Additionally, for an Address to be PURCHASE\'able, all information must be provided.')
    
    def test_address_11_invalid(self):
        """Tests a QUOTE Address with a missing ZIP and missing country."""
        with self.assertRaises(ValidationError) as error:
            self.address_11_invalid = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.QUOTE, name = "Kathrina Baumann", street1 = "Bettinger Str.", city = "Grenzach-Wyhlen", email="kmmbaumann@gmail.com")
        
        self.assertEqual(error.exception.message, 'At least a valid IP address or a country have to be provided to create an Address. Additionally, for an Address to be PURCHASE\'able, all information must be provided.')

    def test_address_12_invalid(self):
        """Tests a QUOTE Address with a missing ZIP and missing country."""
        with self.assertRaises(ValidationError) as error:
            self.address_12_invalid = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.QUOTE, name = "Kathrina Baumann", street1 = "Bettinger Str.", country = self.germany, email="kmmbaumann@gmail.com")
        
        self.assertEqual(error.exception.message, 'At least a valid IP address or a country have to be provided to create an Address. Additionally, for an Address to be PURCHASE\'able, all information must be provided.')

    def test_address_13_invalid(self):
        """Tests a QUOTE Address with a missing ZIP and missing street."""
        with self.assertRaises(ValidationError) as error:
            self.address_13_invalid = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.QUOTE, name = "Kathrina Baumann", city = "Grenzach-Wyhlen", country = self.germany, email="kmmbaumann@gmail.com")
        
        self.assertEqual(error.exception.message, 'At least a valid IP address or a country have to be provided to create an Address. Additionally, for an Address to be PURCHASE\'able, all information must be provided.')

    def test_address_14_invalid(self):
        """Tests a QUOTE Address with a missing name and missing ZIP."""
        with self.assertRaises(ValidationError) as error:
            self.address_14_invalid = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.QUOTE, street1 = "Bettinger Str.", city = "Grenzach-Wyhlen", country = self.germany, email="kmmbaumann@gmail.com")
        
        self.assertEqual(error.exception.message, 'At least a valid IP address or a country have to be provided to create an Address. Additionally, for an Address to be PURCHASE\'able, all information must be provided.')
        
    def test_address_15_invalid(self):
        """Tests a QUOTE Address with a missing ZIP."""
        with self.assertRaises(ValidationError) as error:
            self.address_15_invalid = Address.objects.create(object_owner = self.user, object_purpose=Address.OBJECT_PURPOSE_CHOICE.QUOTE, name = "Kathrina Baumann", street1 = "Bettinger Str.", city = "Grenzach-Wyhlen", country = self.germany, email="kmmbaumann@gmail.com")
        
        self.assertEqual(error.exception.message, 'At least a valid IP address or a country have to be provided to create an Address. Additionally, for an Address to be PURCHASE\'able, all information must be provided.')
        
    def create_countries(self):
        euro_symbol= 'E' # Postgres does not support UTF8 in testing
        self.euro = Currency.objects.create(name="Euro", symbol=euro_symbol, iso="EUR", rate=1.0)
        self.dollar = Currency.objects.create(name="U.S. Dollar", symbol="$", iso="USD", rate=1.2)
        self.german = Language.objects.create(name="German", iso2="DE")
        self.english = Language.objects.create(name="English", iso2="EN")
        self.germany = Country.objects.create(name="Germany", iso2="DE", tld="DE", currency=self.euro, language=self.german, capital="Berlin")        
        self.usa = Country.objects.create(name="United States", iso2="US", tld="", currency=self.dollar, language=self.english, capital="Washington D.C.", requires_zip = False)
        
    def create_users(self):
        self.user = User.objects.create(username='test')
        
    def create_units(self):
        self.centimetres = Unit.objects.create(name="centimetre", abbreviation="cm", symbol="cm", dimension=Unit.DIMENSION_CHOICE.DISTANCE, rate=1.000000000000, plural="centimetres")
        self.yards = Unit.objects.create(name="yard", abbreviation="yd", symbol="yd", dimension=Unit.DIMENSION_CHOICE.DISTANCE, rate=0.0109361330, base=self.centimetres, plural="yard")
        self.grams = Unit.objects.create(name="gram", abbreviation="g", symbol="g", dimension=Unit.DIMENSION_CHOICE.MASS, rate=1.000000000000, plural="grams")
        self.pounds = Unit.objects.create(name="pound", abbreviation="lb", symbol="lb", dimension=Unit.DIMENSION_CHOICE.MASS, rate=0.0022046226, base=self.grams, plural="pound")
        
    def create_parcels(self):
        self.parcel_valid = Parcel.objects.create(object_owner = self.user, length=30, width=20, height=10, distance_unit = self.centimetres, weight=2500, mass_unit = self.grams)
        self.parcel_sorted = Parcel.objects.create(object_owner = self.user, length=10, width=5, height=30, distance_unit = self.centimetres, weight=2500, mass_unit = self.grams)
        self.parcel_units = Parcel.objects.create(object_owner = self.user, length=0.8, width=0.9, height=0.3, distance_unit = self.yards, weight=3.5, mass_unit = self.pounds)
        
