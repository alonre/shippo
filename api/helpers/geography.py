# -*- coding: utf-8 -*-
from django.conf import settings
from googleplaces import GooglePlaces, types, lang
from pygeocoder import Geocoder
from pygeolib import GeocoderError
from urllib2 import HTTPError
from requests.exceptions import RequestException 

class GoogleGeography:
        
    def complete_address(self, address, *args, **kwargs):
        # Get coordinates first. Usually from a partially given address (e.g., only Country or Country & ZIP)
        lat, lng = self.lookup_coordinates_for_address(address)
        address.lat = lat
        address.lng = lng
        
        # Now, we take these coordinates to get the nearest street for it.
        gcoder_record = self.lookup_address_for_coordinates(address)
        address.object_gcoder_record = gcoder_record
        
        # Take the raw record and make a (hopefully completed) address out of it.
        address = self.map_geocode_record_to_address(gcoder_record, address)
        
        return address
    
    def lookup_coordinates_for_address(self, address, *args, **kwargs):
        """First step: Lookup coordinates for an address (use Google Places)."""
        places = GooglePlaces(settings.GOOGLE_API_KEY)
        if not address.city and not address.zip:
            city = address.country.capital
        else:
            city = address.city
            
        address_string = u'%s %s %s %s' % (city, address.state, address.zip, address.country.name)
        address_string = address_string.strip().encode('utf8') # .encode is REALLY important else urllib-fuckup
        
        try:
            results_coord = places.text_search(query=address_string)
            lat, lng = results_coord.places[0].geo_location['lat'], results_coord.places[0].geo_location['lng']
            return lat, lng
        except (GooglePlacesError, urllib2.HTTPError) as ex:
            import sys
            print >>sys.stderr, "EXCEPTION: lookup_coordinates_for_address, %s" % ex
            return None
        
    def lookup_address_for_coordinates(self, address, *args, **kwargs):
        """Second step: Get the nearest address for the set of coordinates (use Google Geocode reverse geocoding)."""
        if address.lat and address.lng:
            try:
                record_address = Geocoder.reverse_geocode(lat=address.lat, lng=address.lng, region=address.country.tld, language=address.country.language.iso2)
                return record_address
            except (GeocoderError, RequestException) as ex:
                import sys
                print >>sys.stderr, "EXCEPTION: lookup_address_for_coordinates, %s" % ex
                return None
        else:
            return None
        
    def map_geocode_record_to_address(self, record, address, *args, **kwargs):
        """Third step: Take the raw record that Google Geocode returned and map it to an Address.""" 
        
        if record:
            import sys
            print >>sys.stderr, str(record)
            for item in record:
               
                # first condition rules out weird results for the wrong country
                # second result prevents addresses without streets to be generated (ex NY, Berlin)
                if item.country__short_name == address.country.iso2 and item.route:
                    address.street1 = self.get_geocode_record_attribute_value(item.route)
                    
                    address.city = self.get_formatted_city(item)
                    
                    address.street_no = self.get_geocode_record_attribute_value(item.street_number)
                    address.zip = self.get_geocode_record_attribute_value(item.postal_code)
                    address.state = self.get_geocode_record_attribute_value(item.state__short_name)
    
                    break
            
            return address
        else:
            return None
                
    def get_formatted_city(self, record, *args, **kwargs):
        administrative_lvl_3 = self.get_geocode_record_attribute_value(record.administrative_area_level_3)
        administrative_lvl_2 = self.get_geocode_record_attribute_value(record.administrative_area_level_2)
        administrative_lvl_1 = self.get_geocode_record_attribute_value(record.administrative_area_level_1)
 
        city = self.get_geocode_record_attribute_value(record.city)
        postal_town = self.get_geocode_record_attribute_value(record.postal_town)
       
        if not city:
            if postal_town:
                city = postal_town
            else:
                if administrative_lvl_3:
                    city += administrative_lvl_3
                if administrative_lvl_2:
                    city += ' '
                    city += administrative_lvl_2
        
        return city.strip()
        
    def get_geocode_record_attribute_value(self, value, *args, **kwargs):
        try:
            if value:
                return value.strip()
            else:
                return ''
        except:
            return ''