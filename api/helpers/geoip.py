import os
import json

import pygeoip

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import validate_ipv46_address, validate_ipv4_address, validate_ipv6_address

import api.models

class GeoLite:
    
    def lookup_address_for_ip(self, address):
        self.ip = address.ip
        self.is_ip = False
        self.ip_version = -1
        
        # validate whether v4 or v6 - or whether IP at all
        try:
            validate_ipv46_address(self.ip)
        except ValidationError:
            self.is_ip = False
            raise ValidationError
            
        try:
            validate_ipv4_address(self.ip)
            self.is_ip = True
            self.ip_version = 4
        except ValidationError:
            pass
            
        try:
            validate_ipv6_address(self.ip)
            self.is_ip = True
            self.ip_version = 6
        except ValidationError:
            pass

        # load appropriate DB
        if self.is_ip:
            if self.ip_version == 4:
                self.db = pygeoip.GeoIP(settings.GEOIP_CITY_IP4_DAT_FILE, flags=pygeoip.const.MEMORY_CACHE)
            elif self.ip_version == 6:
                self.db = pygeoip.GeoIP(settings.GEOIP_CITY_IP6_DAT_FILE, flags=pygeoip.const.MEMORY_CACHE)
        else:
            pass # TODO: throw exception here
    
        record = self.db.record_by_addr(self.ip)

        return record
       
    def get_geoip_record_attribute(self, record, attribute):
        try:
            if record[attribute]:
                return record[attribute]
            else:
                return ''
        except:
            return ''

    def map_geoip_record_to_address(self, record, address):
        address.city = self.get_geoip_record_attribute(record, 'city')
        iso2 = self.get_geoip_record_attribute(record, 'country_code')
        
        # Lookup country in DB for the iso2 code given by record
        country = api.models.Country()
        try:
            country = api.models.Country.objects.get(iso2=self.get_geoip_record_attribute(record, 'country_code'))
        except:
            country = None
        
        address.country = country
        address.state = self.get_geoip_record_attribute(record, 'region_code')
        address.zip = self.get_geoip_record_attribute(record, 'postal_code')
        
        return address