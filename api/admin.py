from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

# Register your models here.
from api.models import *
from rest_framework.authtoken.models import Token
from django.core import urlresolvers
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse


# Admin Classes
class AddressAdmin(admin.ModelAdmin):
    date_hierarchy = "object_created"
    fields = ("object_owner", "object_state", "object_purpose", "object_source", "object_geoip_record", "object_gcoder_record", "name", "company", "street1", "street2", "street_no", "city", "state", "zip", "phone", "email", "ip", "country")
    list_display = ["__unicode__", "object_state", "object_purpose", "object_source"]

class APIUserInline(admin.StackedInline):
    model = APIUser
    can_delete = False
    verbose_name_plural = 'api users'
    readonly_fields = ['apiaddress_link', 'apibilling_link']

    def apiaddress_link(self, obj):
        if obj.address:
            url = reverse('admin:api_apiaddress_change', args=(obj.address.id,))
            return mark_safe("<a href='%s'>%s</a>" % (url, obj.address))
        else:
            return mark_safe("<a href='#'>no address</a>")

    # the following is necessary if 'link' method is also used in list_display
    apiaddress_link.allow_tags = True

    def apibilling_link(self, obj):
        if obj.billing:
            url = reverse('admin:api_apibilling_change', args=(obj.billing.id,))
            return mark_safe("<a href='%s'>%s</a>" % (url, obj.billing))
        else:
            return mark_safe("<a href='#'>no billing</a>")

    # the following is necessary if 'link' method is also used in list_display
    apibilling_link.allow_tags = True

class TokenInline(admin.StackedInline):
    model = Token
    can_delete = False
    verbode_name_plural = 'tokens'

class UserAdmin(UserAdmin):
    inlines = (APIUserInline, TokenInline)

class MessageInline(admin.StackedInline):
    model = Message
    max_num = 0 #this does not prevent existing messages from being displayed
    can_delete = False
    verbode_name_plural = 'tokens'
    readonly_fields = ('code', 'text', 'source', 'stack_trace')
    fieldsets = (
        (None, {'fields': ('code', 'text', 'source', 'stack_trace')}),
    )

class RateAdmin(admin.ModelAdmin):
    list_display = ('object_id', 'object_state', 'object_owner', 'price')
    list_filter = ('provider', 'object_owner__is_staff', 'object_owner',)
    list_per_page = 25
    save_on_top = True
    readonly_fields = (
        'object_id', 'object_owner', 'object_state', 'object_purpose',
        'shipment_link', 'price',  'provider', 'shipment', 'servicelevel', 'outbound_endpoint', 'inbound_endpoint', 'adapter',
        'tier', 'duration', 'insurance', 'insurance_amount', 'insurance_currency', 'trackable', 'delivery_attempts', 'arrives_by', 'available_shippo',
        'object_api_record', 'servicelevel_token', 'attributes', 'object_current', 'object_custom_logic_applied',
        )
    #inlines = (MessageInline,)
    fieldsets = (
        ('Meta', {'fields': (('object_id', 'object_owner', 'object_state', 'object_purpose'),)}),
        ('Main', {'fields': ('shipment_link', 'price', ('provider', 'adapter'), 'servicelevel', ('outbound_endpoint', 'inbound_endpoint'),)}),
        ('Additional', {'fields': ('tier', 'duration', ('insurance', 'trackable', 'available_shippo'), ('insurance_amount', 'insurance_currency'), ('delivery_attempts', 'arrives_by'),)}),
        ('Tech', {'fields': ('object_api_record', 'servicelevel_token', 'attributes', ('object_current', 'object_custom_logic_applied'),)}),
    )

    def shipment_link(self, obj):
        if obj.shipment:
            url = reverse('admin:api_shipment_change', args=(obj.shipment.id,))
            return mark_safe("<a href='%s'>%s</a>" % (url, obj.shipment))
        else:
            return mark_safe("<a href='#'>no shipment</a>")

    # the following is necessary if 'link' method is also used in list_display
    shipment_link.allow_tags = True

class TransactionAdmin(admin.ModelAdmin):
    list_display = ('object_id', 'object_status', 'object_owner', 'rate', 'label_url', 'tracking_number', 'object_created', )
    list_filter = ('object_status', 'object_owner__is_staff', 'rate__adapter', 'object_owner',)
    list_per_page = 25
    save_on_top = True
    readonly_fields = (
        'object_id', 'object_owner', 'object_state', 'object_status', 'rate',
        'notification_email_from', 'notification_email_to', 'notification_email_other',
        'pickup_date', 'tracking_number', 'label_url', 'rate_link')
    inlines = (MessageInline,)
    fieldsets = (
        ('Meta', {'fields': (('object_id', 'object_owner', 'object_state', 'object_status'), )}),
        ('Rate', {'fields': ('rate_link',)}),
        ('Settings', {'fields': ('notification_email_from', 'notification_email_to', 'notification_email_other', 'pickup_date')}),
        ('Result', {'fields': ('tracking_number', 'label_url',)}),
        ('Tech', {'fields': ('object_api_message', 'object_api_response')}),
    )

    def rate_link(self, obj):
        if obj.rate:
            url = reverse('admin:api_rate_change', args=(obj.rate.id,))
            return mark_safe("<a href='%s'>%s</a>" % (url, obj.rate))
        else:
            return mark_safe("<a href='#'>no rate</a>")

    # the following is necessary if 'link' method is also used in list_display
    rate_link.allow_tags = True

class ShipmentAdmin(admin.ModelAdmin):
    list_display = ('object_id', 'object_state', 'object_purpose', 'object_owner', 'address_from', 'address_to', 'parcel', 'object_created', )
    list_filter = ('object_state', 'object_owner__is_staff', 'object_purpose')
    list_per_page = 25
    save_on_top = True
    readonly_fields = ('object_owner', 'object_state', 'object_purpose', 'address_from', 'address_to', 'parcel', 'address_from_link', 'address_to_link', 'parcel_link')
    fieldsets = (
        ('Meta', {'fields': (('object_owner', 'object_state', 'object_purpose'), )}),
        ('Objects', {'fields': ('address_from_link', 'address_to_link', 'parcel_link')}),
    )

    def address_from_link(self, obj):
        if obj.address_from:
            url = reverse('admin:api_address_change', args=(obj.address_from.id,))
            return mark_safe("<a href='%s'>%s</a>" % (url, obj.address_from))
        else:
            return mark_safe("<a href='#'>no address_from</a>")

    # the following is necessary if 'link' method is also used in list_display
    address_from_link.allow_tags = True

    def address_to_link(self, obj):
        if obj.address_to:
            url = reverse('admin:api_address_change', args=(obj.address_to.id,))
            return mark_safe("<a href='%s'>%s</a>" % (url, obj.address_to))
        else:
            return mark_safe("<a href='#'>no address_to</a>")

    # the following is necessary if 'link' method is also used in list_display
    address_to_link.allow_tags = True

    def parcel_link(self, obj):
        if obj.parcel:
            url = reverse('admin:api_parcel_change', args=(obj.parcel.id,))
            return mark_safe("<a href='%s'>%s</a>" % (url, obj.parcel))
        else:
            return mark_safe("<a href='#'>no parcel</a>")

    # the following is necessary if 'link' method is also used in list_display
    parcel_link.allow_tags = True

# registration
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

admin.site.register(Address, AddressAdmin)
admin.site.register(APIAddress)
admin.site.register(APIBilling)
admin.site.register(Country)
admin.site.register(Currency)
admin.site.register(Unit)
admin.site.register(Parcel)
admin.site.register(Language)
admin.site.register(Shipment, ShipmentAdmin)
admin.site.register(Attribute)
admin.site.register(Rate, RateAdmin)
admin.site.register(Transaction, TransactionAdmin)