from django.test import TestCase
from django.conf import settings
import sys, base64, urllib2, httplib, json, time, datetime
from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient, APITransactionTestCase, APILiveServerTestCase

class BasicAPITest(TestCase):
    def setUp(self):
        self.USERNAME = settings.SHIPPO_API_FRONTEND['test_user']
        self.PASSWORD = settings.SHIPPO_API_FRONTEND['test_password']
        self.BASE64AUTHKEY = base64.encodestring('%s:%s' % (self.USERNAME, self.PASSWORD))[:-1]
        self.HTTP_TYPE = settings.SHIPPO_API_FRONTEND['http_type']
        self.BASE_URL = settings.SHIPPO_API_FRONTEND['base_url']
        self.ADDRESSES_URL_EXT = settings.SHIPPO_API_FRONTEND['addresses_url_ext']
        self.PARCELS_URL_EXT = settings.SHIPPO_API_FRONTEND['parcels_url_ext']
        self.SHIPMENTS_URL_EXT = settings.SHIPPO_API_FRONTEND['shipments_url_ext']
        self.RATES_URL_EXT = settings.SHIPPO_API_FRONTEND['rates_url_ext']
        self.TRANSACTIONS_URL_EXT = settings.SHIPPO_API_FRONTEND['transactions_url_ext']
        self.FULL_ADDRESS = {
            "object_purpose": "PURCHASE",
            "name": "Laura Behrens Wu",
            "company": "Shippo",
            "street1": "Clayton St.",
            "street_no": "215",
            "street2": "",
            "city": "San Francisco",
            "state": "CA",
            "zip": "94117",
            "country": "US",
            "phone": "4157419393",
            "email": "test@shippo.com",
            "ip": ""
        }
        self.FULL_ADDRESS_2 = {
            "object_purpose": "PURCHASE",
            "name": "Laura Behrens Wu",
            "company": "Shippo",
            "street1": "Indian Summer Ct.",
            "street_no": "1092",
            "street2": "",
            "city": "San Jose",
            "state": "CA",
            "zip": "95122",
            "country": "US",
            "phone": "4157419654",
            "email": "test2@shippo.com",
            "ip": ""
        }
        self.COMPLETED_ADDRESS = {
            "object_purpose": "QUOTE",
            "name": "Laura Behrens Wu",
            "company": "Shippo",
            "street1": "",
            "street_no": "",
            "street2": "",
            "city": "",
            "state": "CA",
            "zip": "94116",
            "country": "US",
            "phone": "",
            "email": "yoyo@shippo.com",
            "ip": ""
        }
        self.PARCEL = {
            "length": '20',
            "width": '15',
            "height": '15',
            "distance_unit": 'cm',
            "weight": '2',
            "mass_unit": 'lb'
        }
        self.headers = {
            "Content-type": "application/json",
            "Authorization": "Basic " + self.BASE64AUTHKEY
            }
        if self.HTTP_TYPE == 'http':
            self.conn = httplib.HTTPConnection(self.BASE_URL)
        elif self.HTTP_TYPE == 'https':
            self.conn = httplib.HTTPSConnection(self.BASE_URL)

    def test_create_completed_address(self):
        url = self.ADDRESSES_URL_EXT
        data = json.dumps(self.COMPLETED_ADDRESS)
        self.conn.request("POST", url, data, self.headers)
        response = json.loads(self.conn.getresponse().read())
        import sys
        print >>sys.stderr, "RESPONSE FROM COMPLETED ADDRESS: " + str(response)
        print response
        self.assertEqual(str(response['object_state']), 'COMPLETED')
        self.assertEqual(str(response['object_source']), 'ZIP_COUNTRY')

    def test_create_rates(self):
        #create addresses
        url = self.ADDRESSES_URL_EXT
        data = json.dumps(self.FULL_ADDRESS)
        self.conn.request("POST", url, data, self.headers)
        response = json.loads(self.conn.getresponse().read())
        print response
        address_from = str(response['object_id'])
        data = json.dumps(self.FULL_ADDRESS_2)
        self.conn.request("POST", url, data, self.headers)
        response = json.loads(self.conn.getresponse().read())
        print response
        address_to = str(response['object_id'])
        #create parcel
        url = self.PARCELS_URL_EXT
        data = json.dumps(self.PARCEL)
        self.conn.request("POST", url, data, self.headers)
        response = json.loads(self.conn.getresponse().read())
        print response
        parcel = str(response['object_id'])
        #create shipment
        url = self.SHIPMENTS_URL_EXT
        data = json.dumps({
            "object_purpose": "PURCHASE",
            "address_from": address_from,
            "address_to": address_to,
            "parcel": parcel
            })
        self.conn.request("POST", url, data, self.headers)
        response = json.loads(self.conn.getresponse().read())
        print response
        shipment = str(response['object_id'])
        #get rates
        url = self.SHIPMENTS_URL_EXT + shipment + self.RATES_URL_EXT + 'USD/'
        i = 0;
        fedex = False
        usps = False
        ups = False
        #query for rates, max 10seconds, min until all providers have rates
        while i <= 15:
            self.conn.request("GET", url, '', self.headers)
            response = json.loads(self.conn.getresponse().read())
            print 'After ' + str(i) + ' seconds: ' + str(response['count']) + ' rates found'
            i = i + 1
            for rate in response['results']:
                if rate['provider'] == 'USPS': usps = True
                if rate['provider'] == 'FedEx': fedex = True
                if rate['provider'] == 'UPS': ups = True
            if usps == True and ups == True and fedex == True:
                break
            time.sleep(1)
        self.conn.request("GET", url, '', self.headers)
        response = json.loads(self.conn.getresponse().read())
        for rate in response['results']:
            print 'Rate ' + rate['provider'] + ' ' + rate['servicelevel_name'] + ' ($' + rate['amount_local'] + ')'
            if rate['provider'] == 'USPS' and rate['servicelevel_name'] == 'Standard Post':
                usps_rate = rate['object_id']
                print usps_rate
            if rate['provider'] == 'FedEx' and rate['servicelevel_name'] == 'Express Saver':
                fedex_rate = rate['object_id']
                print fedex_rate
            if rate['provider'] == 'UPS' and rate['servicelevel_name'] == 'Three-Day Select':
                ups_rate = rate['object_id']
                print ups_rate
        print str(response['count']) + ' rates in total'
        #post transactions
        url = self.TRANSACTIONS_URL_EXT
        i = datetime.datetime.now()
        data = json.dumps({
            "rate": usps_rate,
            "notification_email_from": False,
            "notification_email_to": False,
            "notification_email_other": "simon@goshippo.com",
            "pickup_date": i.isoformat()
            })
        self.conn.request("POST", url, data, self.headers)
        response = json.loads(self.conn.getresponse().read())
        usps_transaction = str(response['object_id'])
        print response
        data = json.dumps({
            "rate": fedex_rate,
            "notification_email_from": False,
            "notification_email_to": False,
            "notification_email_other": "simon@goshippo.com",
            "pickup_date": i.isoformat()
            })
        self.conn.request("POST", url, data, self.headers)
        response = json.loads(self.conn.getresponse().read())
        fedex_transaction = str(response['object_id'])
        print response
        data = json.dumps({
            "rate": ups_rate,
            "notification_email_from": False,
            "notification_email_to": False,
            "notification_email_other": "simon@goshippo.com",
            "pickup_date": i.isoformat()
            })
        self.conn.request("POST", url, data, self.headers)
        response = json.loads(self.conn.getresponse().read())
        ups_transaction = str(response['object_id'])
        print response
        i = 0;
        ups_label = False
        usps_label = False
        fedex_label = False
        while i <= 20:
            url = self.TRANSACTIONS_URL_EXT + usps_transaction + '/'
            self.conn.request("GET", url, '', self.headers)
            response = json.loads(self.conn.getresponse().read())
            print 'After ' + str(i) + ' seconds: USPS Label: ' + str(response['label_url'])
            if str(response['label_url']):
                usps_label = True
            url = self.TRANSACTIONS_URL_EXT + fedex_transaction + '/'
            self.conn.request("GET", url, '', self.headers)
            response = json.loads(self.conn.getresponse().read())
            print 'After ' + str(i) + ' seconds: FedEx Label: ' + str(response['label_url'])
            if str(response['label_url']):
                fedex_label = True
            url = self.TRANSACTIONS_URL_EXT + ups_transaction + '/'
            self.conn.request("GET", url, '', self.headers)
            response = json.loads(self.conn.getresponse().read())
            print 'After ' + str(i) + ' seconds: UPS Label: ' + str(response['label_url'])
            if str(response['label_url']):
                ups_label = True
            if ups_label and usps_label and fedex_label:
                break
            time.sleep(1)
            i = i + 1
        self.assertEqual(usps_label, True)
        self.assertEqual(fedex_label, True)
        self.assertEqual(ups_label, True)