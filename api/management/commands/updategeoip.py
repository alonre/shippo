from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from urllib2 import urlopen
import time
import os

class Command(BaseCommand):
    help = 'Updates the GeoIP databases for IPv4 and IPv6'
    
    def download_file(self, url, path):
        response = urlopen(url)
        print "Downloading..."
        
        target_gzip = os.path.join(path, os.path.basename(url))
        
        with open(target_gzip, "wb") as local_file:
            local_file.write(response.read())
            
        print "Downloaded file."
        return target_gzip
            
    def unzip_files(self, gzip_files):
        for file in gzip_files:
            print "Unzipping File"
            os.system("gzip -d %s" % file)
            
    def cleanup_link_directory(self, path):
        os.system("rm -rf \"`readlink -f %s`\"" % settings.GEOIP_CITY_BASE_DIR)
        os.system("rm -f %s" % settings.GEOIP_CITY_BASE_DIR)
        os.system("ln -s %s %s" % (path, settings.GEOIP_CITY_BASE_DIR))
        print "Cleaning up locations"
    
    def handle(self, *args, **options):
        downloaded_files = []
        
        failed = False
        
        try:
            # make a new directory for the updated version of the GeoIP database
            path = os.path.join(settings.DATA_DIR, 'geoip-' + str(time.time()))
            os.makedirs(path)
            print "Downloaded to :" + path

            downloaded_files.append(self.download_file(settings.GEOIP_CITY_IP4_DAT_URL, path))
            downloaded_files.append(self.download_file(settings.GEOIP_CITY_IP6_DAT_URL, path))
        
        except:
            failed = True
        
        if not failed:
            self.unzip_files(downloaded_file)
            self.cleanup_link_directory(path)