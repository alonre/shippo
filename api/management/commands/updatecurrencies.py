from django.core.management.base import BaseCommand, CommandError
from api.models import Currency
from django.conf import settings
from openexchangerates import OpenExchangeRatesClient

class Command(BaseCommand):
    help = 'Updates the currency rates in the database based on the rates by www.openexchangerates.com'
    
    def handle(self, *args, **options):
        currencies = Currency.objects.all()
        client = OpenExchangeRatesClient(settings.OPENEXCHANGERATES_API_KEY)
        rates = client.latest()
        
        for currency in currencies:
            try:    # if a currency is in the database but not in the data retrieved from openexchangerates.com
                currency.rate = rates['rates'][currency.iso]
            except:
                pass
            
            if currency.iso == 'SDR':
                currency.rate = rates['rates']['XDR']   # special case for special drawing rights
            
            currency.save()