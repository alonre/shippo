from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth.models import User
from api.models import APIUser
from etsy_app.helpers.import_orders import import_orders

class Command(BaseCommand):
    help = 'Imports new Etsy Orders for every APIUser with Etsy Integration.'

    def handle(self, *args, **options):
        apiusers = APIUser.objects.all()
        for apiuser in apiusers:
            if apiuser.etsy_connected and apiuser.etsy_oauth_token and apiuser.etsy_oauth_secret:
                try:
                    import_orders(apiuser)
                except Exception as e:
                    import sys
                    print >>sys.stderr, 'Etsy Import Exception: ' + str(e)