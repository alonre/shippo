from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth.models import User
from api.models import APIUser
from shopify_app.helpers.import_orders import import_orders

class Command(BaseCommand):
    help = 'Imports new Shopify Orders for every APIUser with Shopify Integration.'

    def handle(self, *args, **options):
        apiusers = APIUser.objects.all()
        for apiuser in apiusers:
            if apiuser.shopify_connected and apiuser.shopify_access_token and apiuser.shopify_shop_name:
                try:
                    import_orders(apiuser.user, apiuser)
                except Exception as e:
                	import sys
                	print >>sys.stderr, 'Shopify Import Exception: '+ str(e)