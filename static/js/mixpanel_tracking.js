var s,
MixpanelTracking = {

  settings: {
  },

  init: function() {
    s = this.settings;
    this.bindUIActions();
  },

  bindUIActions: function() {
  },

  trackShipmentSearchedMixpanel: function(mixpanel_ratescount, mixpanel_priceavg, mixpanel_durationavg){
    mixpanel.track("Shipment searched", {
      "From Country": GlobalHelpers.getURLParameter("from-country"),
      "From City": GlobalHelpers.getURLParameter("from-city"),
      "From ZIP": GlobalHelpers.getURLParameter("from-ZIP"),
      "To Country": GlobalHelpers.getURLParameter("to-country"),
      "To City": GlobalHelpers.getURLParameter("to-city"),
      "To ZIP": GlobalHelpers.getURLParameter("to-ZIP"),
      "Dimension Length": parseFloat(GlobalHelpers.getURLParameter("dimensions-length")).toFixed(2),
      "Dimension Width": parseFloat(GlobalHelpers.getURLParameter("dimensions-width")),
      "Dimension Height": parseFloat(GlobalHelpers.getURLParameter("dimensions-height")),
      "Dimension Unit": GlobalHelpers.getURLParameter("dimensions-unit"),
      "Weight": parseFloat(GlobalHelpers.getURLParameter("weight-value")),
      "Weight Unit": GlobalHelpers.getURLParameter("weight-unit"),
      "Rates returned": mixpanel_ratescount,
      "Price average": parseFloat(mixpanel_priceavg).toFixed(2),
      "Duration average": parseFloat(mixpanel_durationavg).toFixed(2)
    });
  },

  trackRateViewedMixpanel: function(rate, i){
    var trackable = rate.trackable ? 1 : 0;
    var available = rate.available_shippo ? 1 : 0;
    mixpanel.track("Rate shown", {
      "Price": parseFloat(rate.amount_local).toFixed(2),
      "Carrier": rate.provider,
      "Servicelevel": rate.servicelevel_name,
      "Duration": parseInt(rate.days),
      "Insurance": parseFloat(rate.insurance_amount_local).toFixed(2),
      "Tracking": trackable,
      "Adapter": 'Shippo API',
      "From Country": GlobalHelpers.getURLParameter("from-country"),
      "From City": GlobalHelpers.getURLParameter("from-city"),
      "From ZIP": GlobalHelpers.getURLParameter("from-ZIP"),
      "To Country": GlobalHelpers.getURLParameter("to-country"),
      "To City": GlobalHelpers.getURLParameter("to-city"),
      "To ZIP": GlobalHelpers.getURLParameter("to-ZIP"),
      "Dimension Length": parseFloat(GlobalHelpers.getURLParameter("dimensions-length")).toFixed(2),
      "Dimension Width": parseFloat(GlobalHelpers.getURLParameter("dimensions-width")),
      "Dimension Height": parseFloat(GlobalHelpers.getURLParameter("dimensions-height")),
      "Dimension Unit": GlobalHelpers.getURLParameter("dimensions-unit"),
      "Weight": parseFloat(GlobalHelpers.getURLParameter("weight-value")),
      "Weight Unit": GlobalHelpers.getURLParameter("weight-unit"),
      "Available on Shippo": available,
      "Price Rank": i
    });
  }

};