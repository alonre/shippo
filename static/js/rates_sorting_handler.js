var s,
RatesSortingHandler = {

  settings: {
  },

  init: function() {
    s = this.settings;
    this.bindUIActions();
  },

  bindUIActions: function() {
  },

  resortRates: function(sortby){
    // toggle active classes
    $(".resort-buttons").removeClass('btn-light-active');
      $(event.target).addClass('btn-light-active');
    if ( sortby == 'value' ){
      rates_list.sort(RatesSortingHandler.sortValue);
    }
    else if ( sortby == 'duration' ){
      rates_list.sort(RatesSortingHandler.sortDuration);
    }
    else{
      rates_list.sort(RatesSortingHandler.sortPrice);
    }
    // display new result
    $("#results-list-rates").empty();
    $.each(rates_list, function(idx, rate) {
      $("#results-list-rates").append(rate.rate_appender);
      //add popover to new elements
      $(".popover-toggle").popover();
    });
  },

  sortPrice: function(a, b) {
    return a.rate_object.amount_local - b.rate_object.amount_local;
  },

  sortDuration: function(a, b) {
    if ($.isNumeric(a.rate_object.days) && $.isNumeric(b.rate_object.days)){
      //if both durations are equal, sort by price
      if(a.rate_object.days == b.rate_object.days){
        return a.rate_object.amount_local - b.rate_object.amount_local;
      }
      //if durations are different, sort by duration
      else{
        return a.rate_object.days - b.rate_object.days;
      }
    }else{return b.rate_object.days;}
  },

  sortValue: function(a, b) {
    if ($.isNumeric(a.rate_bestvalue) && $.isNumeric(b.rate_bestvalue)){
      return a.rate_bestvalue - b.rate_bestvalue;
    }else{return b.rate_bestvalue;}
  }

};