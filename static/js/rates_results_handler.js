var s,
RatesResultsHandler = {

  settings: {
    http_type: '',
    base_url: '',
    get_shipment_url: '',
    shipments_url_ext: '',
    rates_url_ext: '',
    api_token: '',
    rates_auth: '',
    rates_url: '',
    query: window.location.search,
    shipment_id: '',
    getRatesIntervalDuration: 2000,
    getRatesTotalDuration: 14000,
    mixpanel_priceavg: 0,
    mixpanel_durationavg: 0,
    mixpanel_price_count: 0,
    mixpanel_duration_count: 0
  },

  init: function(http_type, base_url, get_shipment_url, shipments_url_ext, rates_url_ext, api_token) {
    s = this.settings;
    this.bindUIActions();

    s.http_type = http_type;
    s.base_url = base_url;
    s.get_shipment_url = get_shipment_url;
    s.shipments_url_ext = shipments_url_ext;
    s.rates_url_ext = rates_url_ext;
    s.api_token = api_token;
    s.rates_auth = 'Token ' + s.api_token;

    //initiate the rates generation
    this.queryShippoAPI();
  },

  bindUIActions: function() {
  },

  queryShippoAPI: function(){
    //get shipment id first; all further functions need to be initiated through ajax success function
    $.ajax({
        type: "GET",
        url: s.get_shipment_url + s.query,
        success: function(response){
          s.shipment_id = response;
          var rates_ids = new Array();
          s.rates_url = s.http_type + '://' + s.base_url + s.shipments_url_ext + s.shipment_id + s.rates_url_ext + '';
          var getrates_interval_function = setInterval(function(){
            //call getRates function
            RatesResultsHandler.getRates(rates_ids);
          }, s.getRatesIntervalDuration);
          //stop getting new rates after 10 attempts
          setTimeout(function(){
            clearInterval(getrates_interval_function);
            //we need to wait another interval before we initiate the next function
            setTimeout(function(){
              RatesResultsHandler.finishAPIQuery();
            }, s.getRatesIntervalDuration);
          }, s.getRatesTotalDuration);
        },
        error: function(xhr, textStatus, errorThrown) {
          console.log("Please report this error: " + errorThrown + ' ' + xhr.status + ' ' + xhr.responseText);
        }
    });
  },

  getRates: function(rates_ids){
    $.ajax({
      url: s.rates_url,
      contentType: "application/json",
      type: "GET",
      beforeSend: function(xhr){xhr.setRequestHeader('Authorization', s.rates_auth);},
      success: function (response) {
        $.each(response.results, function(idx, obj) {
          $("#ajax-loader").fadeOut();
          $("#rates-main").fadeIn();
          if(rates_ids.indexOf(obj.object_id) == -1){
            rates_ids.push(obj.object_id);
            RatesResultsHandler.sortanddisplayRate(obj);
          }
        });
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log( "jqXHR: " + jqXHR.statusText + ' ' + jqXHR.responseText );
        console.log( "textStatus: " + textStatus );
        console.log( "errorThrown: " + errorThrown );
      }
    });
  },

  sortanddisplayRate: function (obj){
    var appender = RatesDisplayHandler.generateRateHTML(obj);
    //save to all rates array
    var rate_item = new Object();
    rate_item.rate_object = obj;
    rate_item.rate_appender = appender;
    //we need to calculate best value because Shippo API doesn't provide this
    if ($.isNumeric(obj.days) && $.isNumeric(obj.amount)){
      rate_item.rate_bestvalue = parseFloat(obj.amount) * parseFloat(obj.days);
    }else{
      var rate_array = [];
      $.each(obj, function(index, value) {
          rate_array.push(index + ': ' + value);
      });
      var mail_message = rate_array + ';'; //need last string to convert array to string
      $.ajax({
        type: "POST",
        url: ajax_mail_url,
        data: { subject: 'Shippo: a rate had non-numeric values', message: mail_message }
      });
      rate_item.rate_bestvalue = 9999999;
    }
    rates_list.push(rate_item);
    rates_list.sort(RatesSortingHandler.sortPrice);
    // display new result
    $("#results-list-rates").empty();
    var total_rates = rates_list.length;
    var total_rates_text = 'Showing ' + total_rates + ' of ' + total_rates + ' rates';
    $('#number_of_results').html(total_rates_text);
    $.each(rates_list, function(idx, rate) {
      $("#results-list-rates").append(rate.rate_appender);
      //add popover to new elements
      $(".popover-toggle").popover();
    });
  },

  finishAPIQuery: function(){
    if (rates_list.length > 0){
      //track Mixpanel Rate Viewed
      rates_list.sort(RatesSortingHandler.sortPrice);
      i = 1;
      $.each(rates_list, function(idx, rate) {
        MixpanelTracking.trackRateViewedMixpanel(rate.rate_object, i);
        i++;
        if ($.isNumeric(rate.rate_object.amount_local) == true){
          s.mixpanel_priceavg = s.mixpanel_priceavg + parseFloat(rate.rate_object.amount_local);
          s.mixpanel_price_count++;
        }
        if ($.isNumeric(rate.rate_object.days) == true){
          s.mixpanel_durationavg = s.mixpanel_durationavg + parseInt(rate.rate_object.days);
          s.mixpanel_duration_count++;
        }
      });
      GlobalHelpers.triggerUV();
    }
    else{
      $("#ajax-loader").fadeOut();
      $("#rates-error").fadeIn();
      var mail_message = 'URL: ' + document.URL;
      $.ajax({
        type: "POST",
        url: ajax_mail_url,
        data: { subject: 'Shippo: a shipment returned no rates', message: mail_message }
      });
    }
    s.mixpanel_priceavg = s.mixpanel_priceavg / s.mixpanel_price_count;
    s.mixpanel_durationavg = s.mixpanel_durationavg / s.mixpanel_duration_count;
    s.mixpanel_ratescount = rates_list.length;
    MixpanelTracking.trackShipmentSearchedMixpanel(s.mixpanel_ratescount, s.mixpanel_priceavg, s.mixpanel_durationavg);
    $("#small-loader").fadeOut();
  }

};