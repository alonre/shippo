var s,
GoogleAutocompleteTo = {

  settings: {
  },

  init: function() {
    s = this.settings;
    var tocity = document.getElementById("google-to"); //we MUST NOT use jQuery selector here, it breaks Google Maps
    var USBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(-124.848974, 24.396308),
      new google.maps.LatLng(-66.885444, 49.384358)
    );
    var options = {
      types: ['geocode'],
      bounds: USBounds
    };
    autocomplete_to = new google.maps.places.Autocomplete(tocity, options);
    this.bindUIActions();
    this.startAutoCompleteFrom(autocomplete_to);
    this.improveAutocompleteUX();
  },

  bindUIActions: function(autocomplete_from) {
  },

  //prefill form fields based on selected place
  startAutoCompleteFrom: function(autocomplete_from){
    google.maps.event.addListener(autocomplete_to, 'place_changed', function() {
      var place = autocomplete_to.getPlace();
      var components = place.address_components;
      var tocountry = null;
      var tostate = null;
      var tocity = null;
      var tozip = null;
      var tolat = place.geometry.location.lat();
      var tolng = place.geometry.location.lng();

      for (var i = 0, component; component = components[i]; i++) {
          if (component.types[0] == 'country') {
              tocountry = component['short_name'];
          }
          if (component.types[0] == 'administrative_area_level_1') {
              tostate = component['short_name'];
          }
          if (component.types[0] == 'locality') {
              tocity = component['long_name'];
          }
          if (component.types[0] == 'postal_code') {
              tozip = component['short_name'];
          }
      }
      $("#ship-tocountry").val('');
      $("#ship-tostate").val('');
      $("#ship-tocity").val('');
      $("#ship-tozip").val('');
      $("#ship-tozip-type").val('');
      $("#ship-tocountry").val(tocountry);
      $("#flag-tocountry-group").addClass("input-group");
      $("#flag-tocountry-inner").removeClass();
      $("#flag-tocountry-inner").addClass("icon-flag-" + tocountry);
      $("#flag-tocountry-outer").show();
      $("#ship-tostate").val(tostate);
      $("#ship-tocity").val(tocity);
      if ( tozip != null ) {
        $("#flag-tocountry-group").popover("hide");
        // if Place Autocomplete finds a value, take it
        $("#ship-tozip").val(tozip);
        $('#ship-tozip-type').val('user');
      }
      else {
        //if Places Autocomplete doesnt find a value, call Google Geocoder
        var geocoder = new google.maps.Geocoder();
        var to_latlng = new google.maps.LatLng(tolat, tolng);
        geocoder.geocode({'latLng': to_latlng}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            dance:
            for (var k = 0, result; result = results[k]; k++) {
              var result_components = result.address_components;
              for (var i = 0, result_component; result_component = result_components[i]; i++) {
                  if (result_component.types[0] == 'postal_code' || result_component.types[1] == 'postal_code') {
                      tozip = result_component['short_name'];
                      //break both for loops
                      break dance;
                  }
              }
            }
          } else {
            console.log("Geocoder failed due to: " + status);
          }
          if ( tozip != null) {
            //if google geocoder finds a new zip, take it
            $("#flag-tocountry-group").popover("hide");
            //set Google API value
            $("#ship-tozip").val(tozip);
            $('#ship-tozip-type').val('geocode');
          }
          else {
            //if google geocoder doesnt finds a new zip, show popover
            $('#ship-tozip-type').val('geocode');
            $("#ship-tozip").val('');
            if(tocountry != 'HK' && tocity != 'Dubai' && tocity != 'Dublin'){
              $("#flag-tocountry-group").popover("show");
            }
          }
        });
      };
    });
  },

  improveAutocompleteUX: function(){
    //set Enter keydown in google fields to NOT submit form but choose values
    var enterto = document.getElementById('google-to');
    google.maps.event.addDomListener(enterto, 'keydown', function(e) {
      if (e.keyCode == 13) {
          e.preventDefault();
      }
      //hide validation error popout of weight because it disturbs dropdown display
      $('#ship-weight').popover("hide");
    });
  }
};