/* JSON Pretty Print for PRE Code Documentation */
function PrettyPrintJSON(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'json_number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'json_key';
            } else {
                cls = 'json_string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'json_boolean';
        } else if (/null/.test(match)) {
            cls = 'json_null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

/* Prevent some links to jump to top in click */
$('a.prevent-jump').on('click', function(e) {e.preventDefault(); return true;});

/* Enable Twitter Bootstrap Popovers on certain selectors */
$("[data-toggle='tooltip']").tooltip();
$(".popover-toggle").popover(); //popover needs extra class due to script that closes it when clicked anywhere outside

//closes popover when clicked anywhere outside
$(':not(#anything)').on('click', function (e) {
  $('.popover-toggle').each(function () {
      if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
          return;
      }
  });
});

/*minjs for viewport selectors see http://www.appelsiini.net/projects/viewport*/
(function($){$.belowthefold=function(element,settings){var fold=$(window).height()+$(window).scrollTop();return fold<=$(element).offset().top-settings.threshold;};$.abovethetop=function(element,settings){var top=$(window).scrollTop();return top>=$(element).offset().top+$(element).height()-settings.threshold;};$.rightofscreen=function(element,settings){var fold=$(window).width()+$(window).scrollLeft();return fold<=$(element).offset().left-settings.threshold;};$.leftofscreen=function(element,settings){var left=$(window).scrollLeft();return left>=$(element).offset().left+$(element).width()-settings.threshold;};$.inviewport=function(element,settings){return!$.rightofscreen(element,settings)&&!$.leftofscreen(element,settings)&&!$.belowthefold(element,settings)&&!$.abovethetop(element,settings);};$.extend($.expr[':'],{"below-the-fold":function(a,i,m){return $.belowthefold(a,{threshold:0});},"above-the-top":function(a,i,m){return $.abovethetop(a,{threshold:0});},"left-of-screen":function(a,i,m){return $.leftofscreen(a,{threshold:0});},"right-of-screen":function(a,i,m){return $.rightofscreen(a,{threshold:0});},"in-viewport":function(a,i,m){return $.inviewport(a,{threshold:0});}});})($);

$(window).scroll(function() {
if($('#rates-main').is(':visible')){
  $('#results-rollup-outer').slideDown();
  if($('footer').is(':in-viewport')){
    $('#results-rollup-outer').css('position', 'relative');
    $('#results-rollup-buffer').hide();
  }
  else{
   $('#results-rollup-outer').css('position', 'fixed');
   $('#results-rollup-buffer').show();
  }
}
});

/*trigger UserVoice on results*/
function triggerUV() {
    UserVoice.push(['show', {
      mode: 'contact'
    }]);
};

/* CSRF token in jQuery AJAX */
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}
var csrftoken = getCookie('csrftoken');
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

