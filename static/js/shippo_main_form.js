window.onload = function()
{
  	var fromcity = document.getElementById("google-from"); //we MUST NOT use jQuery selector here, it breaks Google Maps
	var tocity = document.getElementById("google-to"); //we MUST NOT use jQuery selector here, it breaks Google Maps
	var USBounds = new google.maps.LatLngBounds(
		new google.maps.LatLng(-124.848974, 24.396308),
		new google.maps.LatLng(-66.885444, 49.384358)
	);
	var options = {
		types: ['geocode'],
		bounds: USBounds
	};

	autocomplete_from = new google.maps.places.Autocomplete(fromcity, options);
	google.maps.event.addListener(autocomplete_from, 'place_changed', function() {

	  var place = autocomplete_from.getPlace();
	  var components = place.address_components;
	  var country = null;
	  var state = null;
	  var city = null;
	  var zip = null;
	  var fromlat = place.geometry.location.lat();
	  var fromlng = place.geometry.location.lng();

	  for (var i = 0, component; component = components[i]; i++) {
	      if (component.types[0] == 'country') {
	          country = component['short_name'];
	      }
	      if (component.types[0] == 'administrative_area_level_1') {
	          state = component['short_name'];
	      }
	      if (component.types[0] == 'locality') {
	          city = component['long_name'];
	      }
	      if (component.types[0] == 'postal_code') {
	          zip = component['short_name'];
	      }
	  }
	  $("#ship-fromcountry").val('');
	  $("#ship-fromstate").val('');
	  $("#ship-fromcity").val('');
	  $("#ship-fromzip").val('');
	  $("#ship-fromzip-type").val('');
	  $("#ship-fromcountry").val(country);
	  $("#flag-fromcountry-group").addClass("input-group");
	  $("#flag-fromcountry-inner").removeClass();
	  $("#flag-fromcountry-inner").addClass("icon-flag-" + country);
	  $("#flag-fromcountry-outer").show();
	  $("#ship-fromstate").val(state);
	  $("#ship-fromcity").val(city);
	  if ( zip != null ) {
	    $("#ship-fromzip").val('');
	    $("#flag-fromcountry-group").popover("hide");
	    $("#ship-fromzip").val(zip);
	    $('#ship-fromzip-type').val('user');
	  }
	  else {
	    //if Places Autocomplete doesnt find a value, call Google Geocoder
	    var geocoder = new google.maps.Geocoder();
	    var from_latlng = new google.maps.LatLng(fromlat, fromlng);
	    geocoder.geocode({'latLng': from_latlng}, function(results, status) {
	      if (status == google.maps.GeocoderStatus.OK) {
	        dance:
	        for (var k = 0, result; result = results[k]; k++) {
	          var result_components = result.address_components;
	          for (var i = 0, result_component; result_component = result_components[i]; i++) {
	              if (result_component.types[0] == 'postal_code' || result_component.types[1] == 'postal_code') {
	                  zip = result_component['short_name'];
	                  //break both for loops
	                  break dance;
	              }
	          }
	        }
	      } else {
	        console.log("Geocoder failed due to: " + status);
	      }
	      if ( zip != null) {
	        //if google geocoder finds a new zip, take it
	        $("#flag-fromcountry-group").popover("hide");
	        //set Google API value
	        $("#ship-fromzip").val(zip);
	        $('#ship-fromzip-type').val('geocode');
	      }
	      else {
	        //if google geocoder doesnt finds a new zip, show popover
	        $("#ship-fromzip").val('');
	        $('#ship-fromzip-type').val('geocode');
	        if(country != 'HK' && city != 'Dubai' && city != 'Dublin'){
	          $("#flag-fromcountry-group").popover("show");
	        }
	      }
	    });
	  };
	});


	autocomplete_to = new google.maps.places.Autocomplete(tocity, options);
	google.maps.event.addListener(autocomplete_to, 'place_changed', function() {

	  var place = autocomplete_to.getPlace();
	  var components = place.address_components;

	  var tocountry = null;
	  var tostate = null;
	  var tocity = null;
	  var tozip = null;
	  var tolat = place.geometry.location.lat();
	  var tolng = place.geometry.location.lng();

	  for (var i = 0, component; component = components[i]; i++) {
	      if (component.types[0] == 'country') {
	          tocountry = component['short_name'];
	      }
	      if (component.types[0] == 'administrative_area_level_1') {
	          tostate = component['short_name'];
	      }
	      if (component.types[0] == 'locality') {
	          tocity = component['long_name'];
	      }
	      if (component.types[0] == 'postal_code') {
	          tozip = component['short_name'];
	      }
	  }
	  $("#ship-tocountry").val('');
	  $("#ship-tostate").val('');
	  $("#ship-tocity").val('');
	  $("#ship-tozip").val('');
	  $("#ship-tozip-type").val('');
	  $("#ship-tocountry").val(tocountry);
	  $("#flag-tocountry-group").addClass("input-group");
	  $("#flag-tocountry-inner").removeClass();
	  $("#flag-tocountry-inner").addClass("icon-flag-" + tocountry);
	  $("#flag-tocountry-outer").show();
	  $("#ship-tostate").val(tostate);
	  $("#ship-tocity").val(tocity);
	  if ( tozip != null ) {
	    $("#flag-tocountry-group").popover("hide");
	    // if Place Autocomplete finds a value, take it
	    $("#ship-tozip").val(tozip);
	    $('#ship-tozip-type').val('user');
	  }
	  else {
	    //if Places Autocomplete doesnt find a value, call Google Geocoder
	    var geocoder = new google.maps.Geocoder();
	    var to_latlng = new google.maps.LatLng(tolat, tolng);
	    geocoder.geocode({'latLng': to_latlng}, function(results, status) {
	      if (status == google.maps.GeocoderStatus.OK) {
	        dance:
	        for (var k = 0, result; result = results[k]; k++) {
	          var result_components = result.address_components;
	          for (var i = 0, result_component; result_component = result_components[i]; i++) {
	              if (result_component.types[0] == 'postal_code' || result_component.types[1] == 'postal_code') {
	                  tozip = result_component['short_name'];
	                  //break both for loops
	                  break dance;
	              }
	          }
	        }
	      } else {
	        console.log("Geocoder failed due to: " + status);
	      }
	      if ( tozip != null) {
	        //if google geocoder finds a new zip, take it
	        $("#flag-tocountry-group").popover("hide");
	        //set Google API value
	        $("#ship-tozip").val(tozip);
	        $('#ship-tozip-type').val('geocode');
	      }
	      else {
	        //if google geocoder doesnt finds a new zip, show popover
	        $('#ship-tozip-type').val('geocode');
	        $("#ship-tozip").val('');
	        if(country != 'HK' && city != 'Dubai' && city != 'Dublin'){
	          $("#flag-tocountry-group").popover("show");
	        }
	      }
	    });
	  };
	});

	//set Enter keydown in google fields to NOT submit form but choose values
	var enterfrom = $('#google-from');
	google.maps.event.addDomListener(enterfrom, 'keydown', function(e) {
	  if (e.keyCode == 13) {
	      e.preventDefault();
	  }
	  //hide validation error popouts of dimensions because they disturb dropdown display
	  $('#ship-length').popover("hide");
	  $('#ship-width').popover("hide");
	  $('#ship-height').popover("hide");
	});

	var enterto = $('#google-to');
	google.maps.event.addDomListener(enterto, 'keydown', function(e) {
	  if (e.keyCode == 13) {
	      e.preventDefault();
	  }
	  //hide validation error popout of weight because it disturbs dropdown display
	  $('#ship-weight').popover("hide");
	});



	$("#home-form").validate({
      ignore: [],
      rules: {
        "from-dd": {
          required: true
        },
        "to-dd": {
          required: true
        },
        "from-country": {
          required: true
        },
        "to-country": {
          required: true
        },
        "dimensions-length": {
          number: true
        },
        "dimensions-width": {
          number: true
        },
        "dimensions-height": {
          number: true
        },
        "weight-value": {
          number: true
        }
      },
      showErrors: function(errorMap, errorList) {
        $.each(this.successList, function(index, value) {
          return $(value).popover("hide");
        });
        return $.each(errorList, function(index, value) {
          var _popover;
          _popover = $(value.element).popover({
            trigger: "manual",
            placement: "top",
            content: value.message,
            template: "<div class=\"popover\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
          });
          _popover.data("bs.popover").options.content = value.message;
          return $(value.element).popover("show");
        });
      },
      submitHandler: function(form) {
        form.submit();
      }
    });

    //display popover for from/to country hidden fields
    $('#flag-fromcountry-group').popover({
      'trigger': "manual",
      'placement': 'top',
      'content': 'Please select a valid city or postal code in the dropdown menu.'
    });
    $('#ship-fromcountry').on('show.bs.popover', function () {
      if (($("#google-from").next('div.popover:visible').length)){}
      else{
        $('#flag-fromcountry-group').popover("show");
      }
    });
    //display popover for from/to country hidden fields
    $('#flag-tocountry-group').popover({
      'trigger': "manual",
      'placement': 'top',
      'content': 'Please select a valid city or postal code in the dropdown menu.'
    });
    $('#ship-tocountry').on('show.bs.popover', function () {
      if (($("#google-to").next('div.popover:visible').length)){}
      else{

        $('#flag-tocountry-group').popover("show");
      }
    });

    //custom dropdown helpers
    $("#weightunit-label").text($("#weightunit-select").val());
    $("#dimensionunit-label").text($("#dimensionunit-select").val());

     $("#weightunit-select").change(function() {
        var weightunit = $(this).val();
        $("#weightunit-label").text(weightunit);
      });

     $("#dimensionunit-select").change(function() {
        var dimensionunit = $(this).val();
        $("#dimensionunit-label").text(dimensionunit);
      });

     $("#weightunit-select").focus(function() {
       $("#weightunit-label").parent('div').addClass("custom-select-focus");
     });

     $("#dimensionunit-select").focus(function() {
       $("#dimensionunit-label").parent('div').addClass("custom-select-focus");
     });

     $("#weightunit-select").blur(function() {
       $("#weightunit-label").parent('div').removeClass("custom-select-focus");
     });

     $("#dimensionunit-select").blur(function() {
       $("#dimensionunit-label").parent('div').removeClass("custom-select-focus");
     });
};



$(document).ready(function(){
	$('#google-from').focus();
	/*try sample shipment button*/
	$("#sample-shipment").click(function(){
	  $("#google-from").val('London, Great Britain');
	  $("#google-to").val('New York, NY, United States');
	  $("#ship-fromcountry").val('GB');
	  $("#ship-fromstate").val('');
	  $("#ship-fromcity").val('London');
	  $("#ship-fromzip").val('WC2R+0HS');
	  $("#ship-fromzip-type").val('geocode');
	  $("#ship-tocountry").val('US');
	  $("#ship-tostate").val('NY');
	  $("#ship-tocity").val('New York');
	  $("#ship-tozip").val('10007');
	  $("#ship-tozip-type").val('geocode');
	  $("#ship-length").val('30');
	  $("#ship-width").val('20');
	  $("#ship-height").val('10');
	  $("#dimensionunit-select").val('cm');
	  $("#ship-weight").val('1');
	  $("#weightunit-select").val('kg');
	  $("#home-form").submit();
	  $("html, body").animate({ scrollTop: 0 }, "fast");
	 });
	mixpanel.track('Page viewed', {'Page name' : 'Home', 'url' : window.location.pathname});
});