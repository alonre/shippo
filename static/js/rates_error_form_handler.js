var s,
RatesErrorFormHandler = {

  settings: {
  },

  init: function() {
    s = this.settings;
    this.bindUIActions();
  },

  bindUIActions: function() {
    $("#noresults-signup").submit(function() {
      submit_button = $('#noresults-form-button');
      $('#noresults-form-query').val(window.location.search);
      submit_button.prop('disabled', true);
      submit_button.html('<i class="fa fa-refresh fa-spin"></i> Sending Registration...');
      $.ajax({
        type: "POST",
        url: "internal/submitnoresultsform/",
        data: $("#noresults-signup").serialize(),
        success: function(response){
          $('#noresults-form-success').fadeIn();
          submit_button.prop('disabled', false);
          submit_button.html('<i class="fa fa-check"></i> Successfully registered');
        },
        error: function(xhr, textStatus, errorThrown) {
          $('#noresults-form-error').fadeIn();
          submit_button.prop('disabled', false);
          submit_button.html('Request Notification');
        }
      });
      return false;
    });
  },



};