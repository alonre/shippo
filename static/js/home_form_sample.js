var s,
HomeFormSample = {

  settings: {
  },

  init: function() {
    s = this.settings;
    this.bindUIActions();

  },

  bindUIActions: function() {
    $("#sample-shipment").click(function(){
      HomeFormSample.triggerSampleShipment();
    });
  },

  triggerSampleShipment: function(){
    $("#google-from").val('Berlin, Germany');
    $("#google-to").val('New York, NY, United States');
    $("#ship-fromcountry").val('DE');
    $("#ship-fromstate").val('');
    $("#ship-fromcity").val('Berlin');
    $("#ship-fromzip").val('10117');
    $("#ship-fromzip-type").val('geocode');
    $("#ship-tocountry").val('US');
    $("#ship-tostate").val('NY');
    $("#ship-tocity").val('New York');
    $("#ship-tozip").val('10007');
    $("#ship-tozip-type").val('geocode');
    $("#ship-length").val('20');
    $("#ship-width").val('16');
    $("#ship-height").val('4');
    $("#dimensionunit-select").val('cm');
    $("#ship-weight").val('1');
    $("#weightunit-select").val('lbs');
    $("#home-form").submit();
    $("html, body").animate({ scrollTop: 0 }, "fast");
  }

};