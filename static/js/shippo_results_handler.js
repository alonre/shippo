//comment for aws upload
function getShipmentIDRates(http_type, base_url, get_shipment_url, shipments_url_ext, rates_url_ext, api_token){
	var query = window.location.search;
	$.ajax({
	    type: "GET",
	    url: get_shipment_url + query,
		success: function(response){
		    var shipment_id = response;
		    var rates_ids = new Array();
		    rates_url = http_type + '://' + base_url + shipments_url_ext + shipment_id + rates_url_ext + 'USD/';
		    //get new rates every second
			var getrates = setInterval(function(){getRates(rates_url, api_token, rates_ids);}, 1000);
			//stop getting new rates after 10 attempts
			setTimeout(function(){
				clearInterval(getrates);
				//wait for 1 sec until getrates is done for sure
				setTimeout(function(){
					finishAPIQuery();
				},1100);
			},15000);
		},
		error: function(xhr, textStatus, errorThrown) {
            console.log("Please report this error: " + errorThrown + ' ' + xhr.status + ' ' + xhr.responseText);
        }
	});

	function getRates(rates_url, api_token, rates_ids){
		var rates_auth = 'Token ' + api_token;
		var days_text;
		$.ajax({
	         url: rates_url,
	         contentType: "application/json",
	         type: "GET",
	         beforeSend: function(xhr){xhr.setRequestHeader('Authorization', rates_auth);},
	         success: function (msg) {
		        $.each(msg.results, function(idx, obj) {
		        	if(obj.provider != 'Parcel2Go'){
			        	$('#ajax-loader').fadeOut();
			        	$('#rates-main').fadeIn();
			        	$('#small-loader').fadeIn();
			        	if(rates_ids.indexOf(obj.object_id) == -1){
				        	rates_ids.push(obj.object_id);
				        	sortanddisplayRate(obj);
				        }
				    }
				});
		     },
		     error: function (jqXHR, textStatus, errorThrown) {
		         console.log( "jqXHR: " + jqXHR );
		         console.log( "textStatus: " + textStatus );
		         console.log( "errorThrown: " + errorThrown );
		     }
	     });
	}
}

function sortanddisplayRate(obj){
	//generate HTML code
	appender = generateRateHTML(obj);
	//save to all rates array
	var rate_item = new Object();
	rate_item.rate_object = obj;
	rate_item.rate_appender = appender;
	//we need to calculate best value because Shippo API doesn't provide this
	if ($.isNumeric(obj.days) && $.isNumeric(obj.amount)){
		rate_item.rate_bestvalue = parseFloat(obj.amount) * parseFloat(obj.days);
	}else{
		var rate_array = []
		$.each(obj, function(index, value) {
		    rate_array.push(index + ': ' + value);
		});
		var mail_message = rate_array + ';'; //need last string to convert array to string
		console.log(obj);
		console.log(mail_message);
		$.post( ajax_mail_url, { subject: 'Shippo: a rate had non-numeric values', message: mail_message } );
		rate_item.rate_bestvalue = 9999999;
	}
	rates_list.push(rate_item);
	rates_list.sort(sortPrice);
	// display new result
	$("#results-list-rates").empty();
	var total_rates = rates_list.length;
	var total_rates_text = 'Showing ' + total_rates + ' of ' + total_rates + ' rates';
	$('#number_of_results').html(total_rates_text);
	$.each(rates_list, function(idx, rate) {
		$("#results-list-rates").append(rate.rate_appender);
		//add popover to new elements
		$(".popover-toggle").popover();
	});
}

function finishAPIQuery(){
	mixpanel_priceavg = 0;
	mixpanel_durationavg = 0;
	mixpanel_price_count = 0;
	mixpanel_duration_count = 0;
	if (rates_list.length > 0){
		//track Mixpanel Rate Viewed
		rates_list.sort(sortPrice);
		i = 1;
		$.each(rates_list, function(idx, rate) {
			trackRateViewedMixpanel(rate.rate_object, i);
			i++;
			if ($.isNumeric(rate.rate_object.amount_local) == true){
				mixpanel_priceavg = mixpanel_priceavg + parseFloat(rate.rate_object.amount_local);
				mixpanel_price_count++;
			}
			if ($.isNumeric(rate.rate_object.days) == true){
				mixpanel_durationavg = mixpanel_durationavg + parseInt(rate.rate_object.days);
				mixpanel_duration_count++;
			}
		});
		triggerUV();
	}
	else{
		$('#ajax-loader').fadeOut();
		$('#rates-error').fadeIn();
		var mail_message = 'URL: ' + document.URL;
		$.post( ajax_mail_url, { subject: 'Shippo: a shipment returned no rates', message: mail_message } );
	}
	mixpanel_priceavg = mixpanel_priceavg / mixpanel_price_count;
	mixpanel_durationavg = mixpanel_durationavg / mixpanel_duration_count;
	mixpanel_ratescount = rates_list.length;
	trackShipmentSearchedMixpanel(mixpanel_ratescount, mixpanel_priceavg, mixpanel_durationavg);
	$('#small-loader').fadeOut();
}


/* ======================= */
/* GENERATE RATE HTML CODE */
/* ======================= */

function generateRateHTML(obj){
	if (obj.currency == 'USD'){obj.currency = '$';}
	if (obj.currency_local == 'USD'){obj.currency_local = '$';}
	if (obj.insurance_currency_local == 'USD'){obj.insurance_currency_local = '$';}
	if (obj.insurance_currency == 'USD'){obj.insurance_currency = '$';}
	if (obj.currency == 'EUR'){obj.currency = '$';}
	if (obj.currency_local == 'EUR'){obj.currency_local = '$';}
	if (obj.insurance_currency_local == 'EUR'){obj.insurance_currency_local = '$';}
	if (obj.insurance_currency == 'EUR'){obj.insurance_currency = '$';}
	if (obj.currency == 'GBP'){obj.currency = '£';}
	if (obj.currency_local == 'GBP'){obj.currency_local = '£';}
	if (obj.insurance_currency_local == 'GBP'){obj.insurance_currency_local = '£';}
	if (obj.insurance_currency == 'GBP'){obj.insurance_currency = '£';}
	appender = '<div class="padding-sm border-b-grey"><div class="row">';
		appender += '<div class="col-md-2 col-sm-2 col-xs-5 text-center">';
			appender += '<p class="bold font-xxl text-blue">' + obj.currency_local + obj.amount_local;
			if (obj.currency_local != obj.currency){
				appender += '<p class="font-sm text-grey margin-b-xs margin-n-t-10">' + obj.currency + ' ' + obj.amount + '</p>';
			}
			appender += '<a class="btn btn-primary" href="' + label_manual_instructions_url + '?id=' + obj.object_id + '">Select rate</a>';
		appender += '</div>';
		appender += '<div class="col-md-1 col-sm-1 col-xs-2 padding-v-xs margin-t-xs">';
	    	appender += '<img src="' + obj.provider_image_75 + '" class="img-responsive">';
	    appender += '</div>';
	    appender += '<div class="col-md-3 col-sm-3 col-xs-5 padding-v-xs">';
	    	appender += '<p class="bold lh-40">' + obj.provider + '</p>';
	    	appender += '<p class="italic margin-n-t-10 font-sm">' + obj.servicelevel_name + '</p>';
	    appender += '</div>';
		appender += '<div class="col-md-2 col-sm-2 col-xs-7 padding-v-xs">';
			if (obj.duration_terms){
				appender += '<a class="popover-toggle prevent-jump link-black no-hover cursor-pointer" data-placement="top" title="' + obj.provider + ' ' + obj.servicelevel_name + '" data-content="' + obj.duration_terms + '">';
			}
			if ($.isNumeric(obj.days) == false){days_text = 'unknown';}
			else if (obj.days == 1){days_text = ' day';}
			else {days_text = ' days';}
			if ($.isNumeric(obj.days) == true){
				appender += '<span class="bold font-lg">' + obj.days + '</span>';
			}
			appender += '<span class="bold">' + days_text + '</span>';
			if (obj.duration_terms){
				appender += '&nbsp;&nbsp;&nbsp;<i class="fa fa-info-circle text-lightgrey font-xxxl padding-l-xs"></i>';
				appender += '</a>';
			}
			if (obj.arrives_by && obj.arrives_by != '00:00:00'){
            	var arrival_addon;
            	var arrival = obj.arrives_by.substring(0,2);
            	var arrival_minutes = obj.arrives_by.substring(2,5);
            	if (arrival >= 12){
                	arrival_addon = ' pm';
                	if (arrival >= 13){arrival = arrival - 12;}
                }else {
                	arrival_addon = ' am';
                	if (arrival < 10){arrival = arrival.substring(1,2);}
                }
                appender += '<br><span class="font-xs text-grey italic">arrives by ' + arrival + arrival_minutes + arrival_addon + '</span>';
            }
		appender += '</div>';
		appender += '<div class="col-md-4 col-sm-4 hidden-xs">';
			appender += '<ul class="fa-ul list-padding-xs">';
            if (obj.insurance){
                appender += '<li class="font-xsm"><i class="fa fa-li fa-shield"></i><strong>' + obj.insurance_currency_local + obj.insurance_amount_local + '</strong>';
                if (obj.insurance_currency_local != obj.insurance_currency){
                	appender += ' (' + obj.insurance_currency + ' ' + obj.insurance_amount + ')';
                }
                appender += ' insurance included.</li>';
            }else{
                appender += '<li class="text-lightgrey font-xsm"><i class="fa fa-li fa-shield"></i>No insurance included.</li>';
            }
            if (obj.trackable){
                appender += '<li class="font-xsm"><i class="fa fa-li fa-map-marker"></i>Free online tracking included.</li>';
            }else{
                appender += '<li class="text-lightgrey font-xsm"><i class="fa fa-li fa-map-marker"></i>No tracking available.</li>';
            }
            if (obj.outbound_endpoint == 'door'){
                appender += '<li class="font-xsm"><i class="fa fa-li fa-truck"></i>Pickup service included.</li>';
            }else if (obj.outbound_endpoint == 'door commercial'){
                appender += '<li class="font-xsm"><i class="fa fa-li fa-building"></i>Business address pickup only.</li>';
            }else if (obj.outbound_endpoint == 'door residential'){
                appender += '<li class="font-xsm"><i class="fa fa-li fa-home"></i>Residential address pickup only.</li>';
            }else {
            	appender += '<li class="text-lightgrey font-xsm"><i class="fa fa-li fa-truck"></i>No pickup service available.</li>';
            }
            if (obj.inbound_endpoint == 'carrier office'){
                appender += '<li class="font-xsm"><i class="fa fa-li fa-exclamation-sign"></i>Delivery to next ' + obj.provider + ' location only.</li>';
            }
            if (obj.servicelevel_terms){
            	appender += '<li><a class="popover-toggle prevent-jump link-black cursor-pointer no-hover" data-placement="top" title="' + obj.provider + ' ' + obj.servicelevel_name + '" data-content="' + obj.servicelevel_terms + '">';
                appender += '<span class="font-xsm"><i class="fa fa-li fa-question-circle"></i> Click here for more details</a></span></li>';
            }
        appender += '</ul>';
		appender += '</div>';
	appender += '</div></div>';
	return appender
}

/* ================= */
/* MIXPANEL TRACKING */
/* ================= */

function trackShipmentSearchedMixpanel(mixpanel_ratescount, mixpanel_priceavg, mixpanel_durationavg){
	mixpanel.track("Shipment searched", {
	    "From Country": getURLParameter("from-country"),
	    "From City": getURLParameter("from-city"),
	    "From ZIP": getURLParameter("from-ZIP"),
	    "To Country": getURLParameter("to-country"),
	    "To City": getURLParameter("to-city"),
	    "To ZIP": getURLParameter("to-ZIP"),
	    "Dimension Length": parseFloat(getURLParameter("dimensions-length")).toFixed(2),
	    "Dimension Width": parseFloat(getURLParameter("dimensions-width")),
	    "Dimension Height": parseFloat(getURLParameter("dimensions-height")),
	    "Dimension Unit": getURLParameter("dimensions-unit"),
	    "Weight": parseFloat(getURLParameter("weight-value")),
	    "Weight Unit": getURLParameter("weight-unit"),
	    "Rates returned": mixpanel_ratescount,
	    "Price average": parseFloat(mixpanel_priceavg).toFixed(2),
	    "Duration average": parseFloat(mixpanel_durationavg).toFixed(2)
	});
}

function trackRateViewedMixpanel(rate, i){
	var trackable = rate.trackable ? 1 : 0;
	var available = rate.available_shippo ? 1 : 0;
    mixpanel.track("Rate shown", {
        "Price": parseFloat(rate.amount_local).toFixed(2),
        "Carrier": rate.provider,
        "Servicelevel": rate.servicelevel_name,
        "Duration": parseInt(rate.days),
        "Insurance": parseFloat(rate.insurance_amount_local).toFixed(2),
        "Tracking": trackable,
        "Adapter": 'Shippo API',
        "From Country": getURLParameter("from-country"),
        "From City": getURLParameter("from-city"),
        "From ZIP": getURLParameter("from-ZIP"),
        "To Country": getURLParameter("to-country"),
        "To City": getURLParameter("to-city"),
        "To ZIP": getURLParameter("to-ZIP"),
        "Dimension Length": parseFloat(getURLParameter("dimensions-length")).toFixed(2),
        "Dimension Width": parseFloat(getURLParameter("dimensions-width")),
        "Dimension Height": parseFloat(getURLParameter("dimensions-height")),
        "Dimension Unit": getURLParameter("dimensions-unit"),
        "Weight": parseFloat(getURLParameter("weight-value")),
        "Weight Unit": getURLParameter("weight-unit"),
        "Available on Shippo": available,
        "Price Rank": i
    });
}

/* ================ */
/* HELPER FUNCTIONS */
/* ================ */

function resortRates(argument){
	// toggle active classes
	$('.resort-buttons').removeClass('btn-light-active');
    $(event.target).addClass('btn-light-active');
	if ( argument == 'value' ){rates_list.sort(sortValue);}
	else if ( argument == 'duration' ){rates_list.sort(sortDuration);}
	else{rates_list.sort(sortPrice);}
	// display new result
	$("#results-list-rates").empty();
	$.each(rates_list, function(idx, rate) {
		$("#results-list-rates").append(rate.rate_appender);
		//add popover to new elements
		$(".popover-toggle").popover();
	});
}

function sortPrice(a, b) { return a.rate_object.amount_local - b.rate_object.amount_local; }
function sortDuration(a, b) {
	if ($.isNumeric(a.rate_object.days) && $.isNumeric(b.rate_object.days)){
		//if both durations are equal, sort by price
		if(a.rate_object.days == b.rate_object.days){
			return a.rate_object.amount_local - b.rate_object.amount_local;
		}
		//if durations are different, sort by duration
		else{
			return a.rate_object.days - b.rate_object.days;
		}
	}else{return b.rate_object.days;}
}
function sortValue(a, b) {
	if ($.isNumeric(a.rate_bestvalue) && $.isNumeric(b.rate_bestvalue)){
		return a.rate_bestvalue - b.rate_bestvalue;
	}else{return b.rate_bestvalue;}
}

$("#noresults-signup").submit(function() {
	$('#noresults-form-query').val(window.location.search);
	submit_button = $('#noresults-form-button');
	submit_button.prop('disabled', true);
	submit_button.html('<i class="fa fa-refresh fa-spin"></i> Sending Registration...');
	$.ajax({
	    type: "POST",
	    url: "internal/submitnoresultsform/",
	    data: $("#noresults-signup").serialize(),
		success: function(response){
		    $('#noresults-form-success').fadeIn();
		    submit_button.prop('disabled', false);
			submit_button.html('<i class="fa fa-check"></i> Successfully registered');
		},
		error: function(xhr, textStatus, errorThrown) {
            $('#noresults-form-error').fadeIn();
            submit_button.prop('disabled', false);
			submit_button.html('Request Notification');
        }
	});
	return false;
});

function getURLParameter(name) {
	return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}
