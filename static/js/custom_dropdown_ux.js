var s,
CustomDropdownUX = {

  settings: {
  },

  init: function() {
    s = this.settings;
    this.bindUIActions();
  },

  bindUIActions: function() {
    //when page is loaded and dropdown does not have default value (e.g. due to back button browser), change span label value accordingly
    $(".select-styled-label").each(function() {
      selected_value = $(this).parent().prev().find('option:selected').text();
      $(this).text(selected_value);
    });

    //when dropdown changes, change the span label value
    $(".select-styled > select").change(function(){
      var selected_value = $(this).find('option:selected').text();
      $(this).parent().find(".select-styled-label").text(selected_value);
    });

    //when dropdown is focussed, add custom css class
    $(".select-styled > select").focus(function(){
      $(this).next().addClass("select-styled-default-focus");
    });
    //when dropdown loses focus, remove custom css class
    $(".select-styled > select").blur(function(){
      $(this).next().removeClass("select-styled-default-focus");
    });

  }

};