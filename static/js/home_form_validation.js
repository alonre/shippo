var s,
HomeFormValidation = {

  settings: {
  },

  init: function() {
    s = this.settings;
    this.bindUIActions();
    $("#home-form").validate({
      ignore: [],
      rules: {
        "from-dd": { required: true },
        "to-dd": { required: true },
        "from-country": { required: true },
        "to-country": { required: true },
        "dimensions-length": { required: true, number: true },
        "dimensions-width": { required: true, number: true },
        "dimensions-height": { required: true, number: true },
        "weight-value": { required: true, number: true }
      },
      showErrors: function(errorMap, errorList) {
        $.each(this.successList, function(index, value) {
          return $(value).popover("hide");
        });
        return $.each(errorList, function(index, value) {
          var _popover;
          _popover = $(value.element).popover({
            trigger: "manual",
            placement: "top",
            content: value.message,
            template: "<div class=\"popover\"><div class=\"arrow\"></div><div class=\"popover-inner\"><div class=\"popover-content\"><p></p></div></div></div>"
          });
          _popover.data("bs.popover").options.content = value.message;
          return $(value.element).popover("show");
        });
      },
      submitHandler: function(form) {
        form.submit();
      }
    });
  },

  bindUIActions: function() {
    $('#flag-fromcountry-group').popover({ 'trigger': "manual", 'placement': 'top', 'content': 'Please select a valid city or postal code in the dropdown menu.' });
    $('#flag-tocountry-group').popover({ 'trigger': "manual", 'placement': 'top', 'content': 'Please select a valid city or postal code in the dropdown menu.' });
    $('#ship-fromcountry').on('show.bs.popover', function () {
      HomeFormValidation.showValidFromPlacePopover();
    });
    $('#ship-tocountry').on('show.bs.popover', function () {
      HomeFormValidation.showValidToPlacePopover();
    });
  },

  showValidFromPlacePopover: function(){
    if (($("#google-from").next('div.popover:visible').length)){}
    else{
      $('#flag-fromcountry-group').popover("show");
    }
  },

  showValidToPlacePopover: function(){
    if (($("#google-to").next('div.popover:visible').length)){}
    else{
      $('#flag-tocountry-group').popover("show");
    }
  }

};