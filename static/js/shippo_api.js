var s,
ShippoAPI = {

  settings: {
  },

  init: function() {
    s = this.settings;
    this.bindUIActions();
  },

  bindUIActions: function() {
  },

  getShipmentID: function(http_type, base_url, get_shipment_url){
    query = window.location.search;
    $.ajax({
        type: "GET",
        url: get_shipment_url + query,
        success: function(response){
          return response;
        },
        error: function(xhr, textStatus, errorThrown) {
          console.log("Please report this error: " + errorThrown + ' ' + xhr.status + ' ' + xhr.responseText);
          return false;
        }
    });
  },

  getRates: function(rates_url, api_token, rates_ids){
    var rates_auth = 'Token ' + api_token;
    var days_text;
    $.ajax({
      url: rates_url,
      contentType: "application/json",
      type: "GET",
      beforeSend: function(xhr){xhr.setRequestHeader('Authorization', rates_auth);},
      success: function (response) {
        return response;
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log( "jqXHR: " + jqXHR );
        console.log( "textStatus: " + textStatus );
        console.log( "errorThrown: " + errorThrown );
        return false;
      }
    });
  }

};