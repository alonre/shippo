var s,
RatesDisplayHandler = {

  settings: {
  },

  init: function(http_type, base_url, get_shipment_url, shipments_url_ext, rates_url_ext, api_token) {
    s = this.settings;
    this.bindUIActions();
  },

  bindUIActions: function() {
  },

  generateRateHTML: function(obj){
    //clean rate first
    obj = RatesDisplayHandler.cleanRate(obj);
    //generate HTML
    appender = '<div class="padding-sm border-b-grey"><div class="row">';
      appender += '<div class="col-md-2 col-sm-2 col-xs-5 text-center">';
        appender += '<p class="bold font-xxl text-blue">' + obj.currency_local + obj.amount_local;
        if (obj.currency_local != obj.currency){
          appender += '<p class="font-sm text-grey margin-b-xs margin-n-t-10">' + obj.currency + ' ' + obj.amount + '</p>';
        }
        appender += '<br><a class="btn btn-primary" href="' + label_manual_instructions_url + '?id=' + obj.object_id + '">Select rate</a>';
      appender += '</div>';
      appender += '<div class="col-md-1 col-sm-1 col-xs-2 padding-v-xs margin-t-xs">';
          appender += '<img src="' + obj.provider_image_75 + '" class="img-responsive">';
        appender += '</div>';
        appender += '<div class="col-md-3 col-sm-3 col-xs-5 padding-v-xs">';
          appender += '<p class="bold lh-40">' + obj.provider + '</p>';
          appender += '<p class="italic margin-n-t-10 font-sm">' + obj.servicelevel_name + '</p>';
        appender += '</div>';
      appender += '<div class="col-md-2 col-sm-2 col-xs-7 padding-v-xs">';
        if (obj.duration_terms){
          appender += '<a class="popover-toggle prevent-jump link-black no-hover cursor-pointer" data-placement="top" title="' + obj.provider + ' ' + obj.servicelevel_name + '" data-content="' + obj.duration_terms + '">';
        }
        if ($.isNumeric(obj.days) == false){days_text = 'unknown';}
        else if (obj.days == 1){days_text = ' day';}
        else {days_text = ' days';}
        if ($.isNumeric(obj.days) == true){
          appender += '<span class="bold font-lg">' + obj.days + '</span>';
        }
        appender += '<span class="bold">' + days_text + '</span>';
        if (obj.duration_terms){
          appender += '&nbsp;&nbsp;&nbsp;<i class="fa fa-info-circle text-lightgrey font-xxxl padding-l-xs"></i>';
          appender += '</a>';
        }
        if (obj.arrives_by && obj.arrives_by != '00:00:00'){
          var arrival_addon;
          var arrival = obj.arrives_by.substring(0,2);
          var arrival_minutes = obj.arrives_by.substring(2,5);
          if (arrival >= 12){
              arrival_addon = ' pm';
              if (arrival >= 13){arrival = arrival - 12;}
            }else {
              arrival_addon = ' am';
              if (arrival < 10){arrival = arrival.substring(1,2);}
            }
            appender += '<br><span class="font-xs text-grey italic">arrives by ' + arrival + arrival_minutes + arrival_addon + '</span>';
        }
      appender += '</div>';
      appender += '<div class="col-md-4 col-sm-4 hidden-xs">';
        appender += '<ul class="fa-ul list-padding-xs">';
              if (obj.insurance){
                appender += '<li class="font-xsm"><i class="fa fa-li fa-shield"></i><strong>' + obj.insurance_currency_local + obj.insurance_amount_local + '</strong>';
                if (obj.insurance_currency_local != obj.insurance_currency){
                  appender += ' (' + obj.insurance_currency + ' ' + obj.insurance_amount + ')';
                }
                appender += ' insurance included.</li>';
              }else{
                  appender += '<li class="text-lightgrey font-xsm"><i class="fa fa-li fa-shield"></i>No insurance included.</li>';
              }
              if (obj.trackable){
                  appender += '<li class="font-xsm"><i class="fa fa-li fa-map-marker"></i>Free online tracking included.</li>';
              }else{
                  appender += '<li class="text-lightgrey font-xsm"><i class="fa fa-li fa-map-marker"></i>No tracking available.</li>';
              }
              if (obj.outbound_endpoint == 'door'){
                  appender += '<li class="font-xsm"><i class="fa fa-li fa-truck"></i>Pickup service included.</li>';
              }else if (obj.outbound_endpoint == 'door commercial'){
                  appender += '<li class="font-xsm"><i class="fa fa-li fa-building"></i>Business address pickup only.</li>';
              }else if (obj.outbound_endpoint == 'door residential'){
                  appender += '<li class="font-xsm"><i class="fa fa-li fa-home"></i>Residential address pickup only.</li>';
              }else {
                appender += '<li class="text-lightgrey font-xsm"><i class="fa fa-li fa-truck"></i>No pickup service available.</li>';
              }
              if (obj.inbound_endpoint == 'carrier office'){
                  appender += '<li class="font-xsm"><i class="fa fa-li fa-exclamation-circle"></i>Delivery to next ' + obj.provider + ' location only.</li>';
              }
              if (obj.servicelevel_terms){
                appender += '<li><a class="popover-toggle prevent-jump link-black cursor-pointer no-hover" data-placement="top" title="' + obj.provider + ' ' + obj.servicelevel_name + '" data-content="' + obj.servicelevel_terms + '">';
                  appender += '<span class="font-xsm"><i class="fa fa-li fa-question-circle"></i> Click here for more details</a></span></li>';
              }
          appender += '</ul>';
      appender += '</div>';
    appender += '</div></div>';
    return appender;
  },

  cleanRate: function(obj){
    if (obj.currency == 'USD'){obj.currency = '$';}
    if (obj.currency_local == 'USD'){obj.currency_local = '$';}
    if (obj.insurance_currency_local == 'USD'){obj.insurance_currency_local = '$';}
    if (obj.insurance_currency == 'USD'){obj.insurance_currency = '$';}
    if (obj.currency == 'EUR'){obj.currency = '€';}
    if (obj.currency_local == 'EUR'){obj.currency_local = '€';}
    if (obj.insurance_currency_local == 'EUR'){obj.insurance_currency_local = '€';}
    if (obj.insurance_currency == 'EUR'){obj.insurance_currency = '€';}
    if (obj.currency == 'GBP'){obj.currency = '£';}
    if (obj.currency_local == 'GBP'){obj.currency_local = '£';}
    if (obj.insurance_currency_local == 'GBP'){obj.insurance_currency_local = '£';}
    if (obj.insurance_currency == 'GBP'){obj.insurance_currency = '£';}
    return obj;
  }

};