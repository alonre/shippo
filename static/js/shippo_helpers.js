var s,
GlobalHelpers = {

  settings: {
  },

  init: function() {
    s = this.settings;
    this.bindUIActions();
  },

  bindUIActions: function() {
    // Prevent scroll to top on links with preventJump selector
    $('a.prevent-jump').on('click', function(e) {e.preventDefault(); return true;});
    // Toogle Tooltip and Popover on corresponding selectors
    $("[data-toggle='tooltip']").tooltip();
    $(".popover-toggle").popover();
    // Close Popover when clicking anywhere else
    $(':not(#anything)').on('click', function (e) {
      $(".popover-toggle").each(function () {
          if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
              $(this).popover('hide');
              return;
          }
      });
    });
  },

  PrettyPrintJSON: function(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'json_number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'json_key';
            } else {
                cls = 'json_string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'json_boolean';
        } else if (/null/.test(match)) {
            cls = 'json_null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
  },

  triggerUV: function() {
    UserVoice.push(['show', {
      mode: 'contact'
    }]);
  },

  getURLParameter: function(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
  }

};