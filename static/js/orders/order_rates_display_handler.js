var s,
OrderRatesDisplayHandler = {

  settings: {
  },

  init: function(http_type, base_url, get_shipment_url, shipments_url_ext, rates_url_ext, api_token) {
    s = this.settings;
    this.bindUIActions();
  },

  bindUIActions: function() {
  },

  generateRateHTML: function(obj){
    //clean rate first
    obj = OrderRatesDisplayHandler.cleanRate(obj);
    var service_info = OrderRatesDisplayHandler.createServiceInfo(obj);
    var label_button = OrderRatesDisplayHandler.createLabelButton(obj);
    //generate HTML
    appender = '<div class="padding-v-sm padding-h-sm border-b-grey hover-grey"><div class="row">';
      appender += '<div class="col-md-2 col-xs-12 hidden-sm">';
        appender += '<img src="' + obj.provider_image_75 + '">';
      appender += '</div>';
      appender += '<div class="col-md-4 col-sm-4 col-xs-12 font-sm">';
        appender += '<a class="popover-toggle prevent-jump link-black no-hover cursor-pointer" data-placement="top" title="Service Info" data-html="true" data-content="' + service_info + '">';
          appender += obj.provider + ' ' + obj.servicelevel_name;
        appender += '&nbsp;<i class="fa fa-info-circle text-lightgrey font-xl padding-l-xs"></i>';
        appender += '</a>';
      appender += '</div>';
      appender += '<div class="col-md-2 col-sm-4 col-xs-12 font-sm">';
        if (obj.duration_terms){
          appender += '<a class="popover-toggle prevent-jump link-black no-hover cursor-pointer" data-placement="top" title="Duration Info" data-content="' + obj.duration_terms + '">';
        }
        if ($.isNumeric(obj.days) == false){days_text = 'unknown';}
        else if (obj.days == 1){days_text = ' day';}
        else {days_text = ' days';}
        if ($.isNumeric(obj.days) == true){
          appender += '<span class="">' + obj.days + '</span>';
        }
        appender += '<span class="">' + days_text + '</span>';
        if (obj.duration_terms){
          appender += '&nbsp;<i class="fa fa-info-circle text-lightgrey font-xl padding-l-xs"></i>';
          appender += '</a>';
        }
      appender += '</div>';
      appender += '<div class="col-md-1 col-sm-2 col-xs-12 font-sm">';
        appender += obj.currency_local + obj.amount_local;
      appender += '</div>';
      appender += '<div class="col-md-3 col-sm-3 col-xs-12 font-sm">';
        appender += label_button;
      appender += '</div>';
    appender += '</div>';
    return appender;
  },

  cleanRate: function(obj){
    if (obj.currency == 'USD'){obj.currency = '$';}
    if (obj.currency_local == 'USD'){obj.currency_local = '$';}
    if (obj.insurance_currency_local == 'USD'){obj.insurance_currency_local = '$';}
    if (obj.insurance_currency == 'USD'){obj.insurance_currency = '$';}
    if (obj.currency == 'EUR'){obj.currency = '$';}
    if (obj.currency_local == 'EUR'){obj.currency_local = '$';}
    if (obj.insurance_currency_local == 'EUR'){obj.insurance_currency_local = '$';}
    if (obj.insurance_currency == 'EUR'){obj.insurance_currency = '$';}
    if (obj.currency == 'GBP'){obj.currency = '£';}
    if (obj.currency_local == 'GBP'){obj.currency_local = '£';}
    if (obj.insurance_currency_local == 'GBP'){obj.insurance_currency_local = '£';}
    if (obj.insurance_currency == 'GBP'){obj.insurance_currency = '£';}
    return obj;
  },

  createServiceInfo: function(obj){
    var insurance;
    var tracking;
    var endpoint_inbound;
    var endpoint_outbound;
    var terms;
    if (obj.insurance){
      insurance = 'Insured up to ' + obj.insurance_currency_local + obj.insurance_amount_local;
    }else{
      insurance = 'Not insured';
    }
    if (obj.trackable){
      tracking = 'Trackable';
    }else{
      tracking = 'Not trackable';
    }
    if (obj.outbound_endpoint == 'door'){
      endpoint_outbound = 'Includes pickup';
    }else if (obj.outbound_endpoint == 'door commercial'){
      endpoint_outbound = 'Includes pickup at commercial addresses';
    }else if (obj.outbound_endpoint == 'door residential'){
      endpoint_outbound = 'Includes pickup at residential addresses';
    }else {
      endpoint_outbound = 'No pickup included';
    }
    if (obj.inbound_endpoint == 'carrier office'){
      endpoint_inbound = 'Has to be picked up at closest ' + obj.provider + ' location by recipient';
    }
    if (obj.servicelevel_terms){
      terms = obj.servicelevel_terms;
    }

    service_info = '<ul class=\'font-standard\'><li>' + insurance + '</li><li>' + tracking + '</li><li>' + endpoint_outbound;
    if (endpoint_inbound){
      service_info += '</li><li>' + inbound_endpoint;
    }
    if (terms){
      service_info += '</li><li>' + terms;
    }
    service_info += '</li></ul>';
    return service_info;
  },

  createLabelButton: function(obj){
    //this function used to differentiate between active and inactive buttons, which is now being done by the api automatically
    /*var adapter = obj.provider;
    if (obj.servicelevel_name.indexOf("Parcel2Go") != -1){
      //parcel2go extrawurscht
      adapter = 'Parcel2Go';
    }
    if (obj.provider == 'Deutsche Post DHL' && obj.servicelevel_name.indexOf("business customer") != -1){
      //dhl business customers extrawurscht
      adapter = 'DHL';
    }*/
    var active_button = '<a class="btn btn-primary prevent-jump" href="' + label_url + obj.object_id + '/">Buy Label</a>';
    return active_button;
    /*var inactive_button = '<a class="" href="' + carrier_settings_url + '" target="_blanc"><i class="fa fa-external-link"></i> Activate ' + adapter + '</a>';
    if (carrier_status[adapter.toLowerCase()] == 'true'){
      return active_button;
    }
    else{
      return inactive_button;
    }*/
  }
};