var s,
OrderRatesResultsHandler = {

  settings: {
    http_type: '',
    base_url: '',
    rates_url_ext: '',
    api_token: '',
    rates_auth: '',
    rates_url: '',
    getRatesIntervalDuration: 2000,
    getRatesTotalDuration: 12000,
    mixpanel_priceavg: 0,
    mixpanel_durationavg: 0,
    mixpanel_price_count: 0,
    mixpanel_duration_count: 0
  },

  init: function(http_type, base_url, shipment_id, shipments_url_ext, rates_url_ext, api_token) {
    s = this.settings;
    this.bindUIActions();

    s.shipment_id = shipment_id;
    s.http_type = http_type;
    s.base_url = base_url;
    s.shipments_url_ext = shipments_url_ext;
    s.rates_url_ext = rates_url_ext;
    s.api_token = api_token;
    s.rates_auth = 'Token ' + s.api_token;

    //initiate the rates generation
    this.queryShippoAPI();
  },

  bindUIActions: function() {
  },

  queryShippoAPI: function(){
    var rates_ids = new Array();
    s.rates_url = s.http_type + '://' + s.base_url + s.shipments_url_ext + s.shipment_id + s.rates_url_ext + '';
    var getrates_interval_function = setInterval(function(){
      //call getRates function
      OrderRatesResultsHandler.getRates(rates_ids);
    }, s.getRatesIntervalDuration);
    //stop getting new rates after 10 attempts
    setTimeout(function(){
      clearInterval(getrates_interval_function);
      //we need to wait another interval before we initiate the next function
      setTimeout(function(){
        OrderRatesResultsHandler.finishAPIQuery();
      }, s.getRatesIntervalDuration);
    }, s.getRatesTotalDuration);
  },

  getRates: function(rates_ids){
    $.ajax({
      url: s.rates_url,
      contentType: "application/json",
      type: "GET",
      beforeSend: function(xhr){xhr.setRequestHeader('Authorization', s.rates_auth);},
      success: function (response) {
        $.each(response.results, function(idx, obj) {
          if(rates_ids.indexOf(obj.object_id) == -1){
            rates_ids.push(obj.object_id);
            OrderRatesResultsHandler.sortanddisplayRate(obj);
          }
        });
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log( "jqXHR: " + jqXHR.statusText + ' ' + jqXHR.responseText );
        console.log( "textStatus: " + textStatus );
        console.log( "errorThrown: " + errorThrown );
      }
    });
  },

  sortanddisplayRate: function (obj){
    var appender = OrderRatesDisplayHandler.generateRateHTML(obj);
    //save to all rates array
    var rate_item = new Object();
    rate_item.rate_object = obj;
    rate_item.rate_appender = appender;
    //we need to calculate best value because Shippo API doesn't provide this
    if ($.isNumeric(obj.days) && $.isNumeric(obj.amount)){
      rate_item.rate_bestvalue = parseFloat(obj.amount) * parseFloat(obj.days);
    }else{
      rate_item.rate_bestvalue = 9999999;
    }
    rates_list.push(rate_item);
    rates_list.sort(RatesSortingHandler.sortPrice);
    // display new result
    $("#appender").empty();
    $.each(rates_list, function(idx, rate) {
      $("#appender").append(rate.rate_appender);
      $(".popover-toggle").popover();
    });
    $('#loader-graphic').fadeOut();
    $('#appender').fadeIn();
    $('#small-loader').fadeIn();
  },

  finishAPIQuery: function(){
    if (rates_list.length > 0){
      //track Mixpanel Rate Viewed
      rates_list.sort(RatesSortingHandler.sortPrice);
      i = 1;
      $.each(rates_list, function(idx, rate) {
        MixpanelTracking.trackRateViewedMixpanel(rate.rate_object, i);
        i++;
        if ($.isNumeric(rate.rate_object.amount_local) == true){
          s.mixpanel_priceavg = s.mixpanel_priceavg + parseFloat(rate.rate_object.amount_local);
          s.mixpanel_price_count++;
        }
        if ($.isNumeric(rate.rate_object.days) == true){
          s.mixpanel_durationavg = s.mixpanel_durationavg + parseInt(rate.rate_object.days);
          s.mixpanel_duration_count++;
        }
      });
    }
    else{
      //no rates found
      $('#loader-graphic').fadeOut();
      $('#appender').fadeIn();
      $('#appender').html('<p class="padding-tb-md">Unfortunately, no rates have been found. Have you already activated some carriers in your <a href="/user/carriers">Carrier Settings</a>?<br><br>If you have problems, please let us know at info@goshippo.com – we would be happy to help you find the best rates!');
    }
    s.mixpanel_priceavg = s.mixpanel_priceavg / s.mixpanel_price_count;
    s.mixpanel_durationavg = s.mixpanel_durationavg / s.mixpanel_duration_count;
    s.mixpanel_ratescount = rates_list.length;
    MixpanelTracking.trackShipmentSearchedMixpanel(s.mixpanel_ratescount, s.mixpanel_priceavg, s.mixpanel_durationavg);
    $('#small-loader').fadeOut();
  },

};