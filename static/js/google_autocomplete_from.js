var s,
GoogleAutocompleteFrom = {

  settings: {
  },

  init: function() {
    s = this.settings;
    var fromcity = document.getElementById("google-from"); //we MUST NOT use jQuery selector here, it breaks Google Maps
    var USBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(-124.848974, 24.396308),
      new google.maps.LatLng(-66.885444, 49.384358)
    );
    var options = {
      types: ['geocode'],
      bounds: USBounds
    };
    autocomplete_from = new google.maps.places.Autocomplete(fromcity, options);
    this.bindUIActions();
    this.startAutoCompleteFrom(autocomplete_from);
    this.improveAutocompleteUX();
  },

  bindUIActions: function() {
  },

  //prefill form fields based on selected place
  startAutoCompleteFrom: function(autocomplete_from){
    google.maps.event.addListener(autocomplete_from, 'place_changed', function() {
      var place = autocomplete_from.getPlace();
      var components = place.address_components;
      var country = null;
      var state = null;
      var city = null;
      var zip = null;
      var fromlat = place.geometry.location.lat();
      var fromlng = place.geometry.location.lng();

      for (var i = 0, component; component = components[i]; i++) {
          if (component.types[0] == 'country') {
              country = component['short_name'];
          }
          if (component.types[0] == 'administrative_area_level_1') {
              state = component['short_name'];
          }
          if (component.types[0] == 'locality') {
              city = component['long_name'];
          }
          if (component.types[0] == 'postal_code') {
              zip = component['short_name'];
          }
      }
      $("#ship-fromcountry").val('');
      $("#ship-fromstate").val('');
      $("#ship-fromcity").val('');
      $("#ship-fromzip").val('');
      $("#ship-fromzip-type").val('');
      $("#ship-fromcountry").val(country);
      $("#flag-fromcountry-group").addClass("input-group");
      $("#flag-fromcountry-inner").removeClass();
      $("#flag-fromcountry-inner").addClass("icon-flag-" + country);
      $("#flag-fromcountry-outer").show();
      $("#ship-fromstate").val(state);
      $("#ship-fromcity").val(city);
      if ( zip != null ) {
        $("#ship-fromzip").val('');
        $("#flag-fromcountry-group").popover("hide");
        $("#ship-fromzip").val(zip);
        $('#ship-fromzip-type').val('user');
      }
      else {
        //if Places Autocomplete doesnt find a value, call Google Geocoder
        var geocoder = new google.maps.Geocoder();
        var from_latlng = new google.maps.LatLng(fromlat, fromlng);
        geocoder.geocode({'latLng': from_latlng}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            dance:
            for (var k = 0, result; result = results[k]; k++) {
              var result_components = result.address_components;
              for (var i = 0, result_component; result_component = result_components[i]; i++) {
                  if (result_component.types[0] == 'postal_code' || result_component.types[1] == 'postal_code') {
                      zip = result_component['short_name'];
                      //break both for loops
                      break dance;
                  }
              }
            }
          } else {
            console.log("Geocoder failed due to: " + status);
          }
          if ( zip != null) {
            //if google geocoder finds a new zip, take it
            $("#flag-fromcountry-group").popover("hide");
            //set Google API value
            $("#ship-fromzip").val(zip);
            $('#ship-fromzip-type').val('geocode');
          }
          else {
            //if google geocoder doesnt finds a new zip, show popover
            $("#ship-fromzip").val('');
            $('#ship-fromzip-type').val('geocode');
            if(country != 'HK' && city != 'Dubai' && city != 'Dublin'){
              $("#flag-fromcountry-group").popover("show");
            }
          }
        });
      };
    });
  },

  improveAutocompleteUX: function(){
    //set Enter keydown in google fields to NOT submit form but choose values
    var enterfrom = document.getElementById('google-from');
    google.maps.event.addDomListener(enterfrom, 'keydown', function(e) {
      if (e.keyCode == 13) {
          e.preventDefault();
      }
      //hide validation error popouts of dimensions because they disturb dropdown display
      $('#ship-length').popover("hide");
      $('#ship-width').popover("hide");
      $('#ship-height').popover("hide");
    });
  }
};